<?php

namespace app\components\enum;

/**
 * Class InstanceType
 * @package app\components\enum
 */
class InstanceType
{
    const ELASTICSEARCH = 'elasticsearch';
    const POSTGRESQL    = 'postgresql';
    const LOGSTASH      = 'logstash';

    /**
     * @return array
     */
    public static function values()
    {
        return [
            self::ELASTICSEARCH,
            self::POSTGRESQL,
            self::LOGSTASH
        ];
    }

    /**
     * @return array
     */
    public static function map()
    {
        return [
            self::ELASTICSEARCH => 'Elasticsearch',
            self::POSTGRESQL    => 'PostgreSQL',
            self::LOGSTASH      => 'Logstash'
        ];
    }
}