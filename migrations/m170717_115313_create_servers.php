<?php

use yii\db\Migration;

class m170717_115313_create_servers extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE servers
            (
                id SERIAL PRIMARY KEY,
                user_id INTEGER NOT NULL,
                app_id INTEGER NOT NULL,
                name TEXT NOT NULL,
                host TEXT NOT NULL,
                ssh_port SMALLINT NULL,
                ssh_username TEXT NULL,
                ssh_password TEXT NULL,
                created_at TIMESTAMP(0) DEFAULT now()
            )
        ");

        $this->execute("ALTER TABLE servers ADD CONSTRAINT servers_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
        $this->execute("ALTER TABLE servers ADD CONSTRAINT servers_app_fk FOREIGN KEY (app_id) REFERENCES applications (id)");
    }


    public function safeDown()
    {
        $this->execute('DROP TABLE servers');
    }
}
