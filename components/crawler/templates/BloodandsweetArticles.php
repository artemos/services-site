<?php

return [
    'articles' => [
        'type'  => 'collection',
        'xpath' => '//div[contains(@class, "post-container")]',
        'props' => [
            'url'   => './/h2/a/@href',
            'title' => './/h2/a/text()',
            'text'  => './/div[contains(@class, "post-content")]/h2/following-sibling::text()[1]',
            'image' => './/img/@src'
        ]
    ]
];