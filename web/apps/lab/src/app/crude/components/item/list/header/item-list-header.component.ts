import {Component, EventEmitter, Input, Output, ViewChild, AfterViewInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ItemService} from "../../../../services/item.service";

@Component({
    selector    : 'item-list-header',
    templateUrl : './item-list-header.component.html',
    styleUrls   : ['./item-list-header.component.css']
})
export class ItemListHeaderComponent {
    @Input() pagination: any;
    @Input() chart: any;
    @Input() collectionId: string;

    @Output() private onWidgetTypeChanged  = new EventEmitter();
    @Output() private onLimitChanged       = new EventEmitter();

    @ViewChild('settingsComponent')   settingsComponent;
    @ViewChild('chartFormComponent')  chartFormComponent;

    exportFormat: string;

    widgetTypes: any = [
        {type: 'table', class: 'fa fa-table'},
        {type: 'list',  class: 'fa fa-list'}
    ];

    constructor(
        private modalService: NgbModal,
        private itemService: ItemService) {
    }

    openModal(content) {
        this.modalService.open(content, { windowClass: 'modal-lg' }).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }

    openSettingsFormModal() {
        this.settingsComponent.openModal();
    }

    openChartFormModal() {
        this.chartFormComponent.openModal();
    }

    exportItems() {
        if (this.exportFormat == null) {
            return;
        }

        let data = {
            collectionId : this.collectionId,
            format       : this.exportFormat,
            limit        : 10000
        };

        location.href = this.itemService.getFetchAllUrl(data);
    }

    onWidgetTypeChangedHandler(type) {
        this.onWidgetTypeChanged.emit(type);        
    }

    onLimitChangedHandler(value) {
        this.onLimitChanged.emit(value);
    }
}
