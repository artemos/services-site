<?php

namespace app\controllers\actions\collection;

use app\components\data\ActiveDataProvider;
use app\models\Collection;
use app\components\rest\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class IndexAction extends Action
{
    public function run()
    {
        $query = Collection::find();
        $query->allowed($this->getUserId());
        $query->with('instance');

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}