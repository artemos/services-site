import {Component, OnInit} from '@angular/core';
import {ActivatedRoute}    from "@angular/router";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {CollectionService} from "../../../services/collection.service";
import {ItemService} from "../../../services/item.service";

@Component({
    selector    : 'item-view',
    templateUrl : './item-view.component.html'
})
export class ItemViewComponent implements OnInit {
    item        : any;
    collection  : any;
    crumbs : any;

    constructor(private route: ActivatedRoute,
                private httpErrorService: HttpErrorService,
                private collectionService: CollectionService,
                private itemService: ItemService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.fetchItem(params.id, params.collectionId);
            this.fetchCollection(params.collectionId, () => this.initCrumbs());
        });
    }

    private fetchItem(id, collectionId) {
        this.itemService.fetchById(id, {collectionId: collectionId}).subscribe(
            response => this.item = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    private fetchCollection(id, callback) {
        this.collectionService.fetchById(id).subscribe(
            response => {
                this.collection = response;
                callback();
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    private initCrumbs() {
        this.crumbs = {
            title: 'View item',
            links: [
                {title: 'Dashboard', href: '/'},
                {title: 'All items', href: '/collections/' + this.collection.id + '/items/'},
                {title: 'View item'}
            ]
        }
    }
}