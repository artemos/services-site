<?php

namespace app\controllers\actions\collection;

use app\components\rest\Action;
use yii\web\ServerErrorHttpException;

/**
 * Class DeleteAction
 * @package app\controllers\actions\item
 */
class DeleteAction extends Action
{
    public function run(int $id)
    {
        $collection = $this->getModel($id);

        if ($collection->delete()) {
            $this->getEsClient($collection->instanceId)
                 ->indices()
                 ->delete(['index' => $collection->name]);
        } else {
            throw new ServerErrorHttpException('Failed to delete the comment for unknown reason.');
        }

        $this->setResponseStatusCode(204);
    }
}