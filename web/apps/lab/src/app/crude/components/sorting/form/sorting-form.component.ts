import {Component, EventEmitter, ViewChild} from '@angular/core';
import {Input, OnInit, Output}              from '@angular/core';
import {NgbModal}                           from "@ng-bootstrap/ng-bootstrap";
import {ArrayService}                       from "../../../../core/services/array.service";
import {SortingService}                     from "../../../services/sorting.service";
import {FieldService} from "../../../services/field.service";

@Component({
    selector    : 'sorting-form',
    templateUrl : './sorting-form.component.html'
})
export class SortingFormComponent implements OnInit {
    @ViewChild('modal') modal: any;

    fields : any;
    form   : any = [];

    constructor(
        private modalService: NgbModal,
        private sortingService: SortingService,
        private fieldService: FieldService,
        private arrayService: ArrayService) {
    }

    ngOnInit() {
        this.sortingService.form.subscribe(
            data => this.form = data
        );

        this.fieldService.collectionFields.subscribe(
            data => this.fields = data
        );
    }

    openModal() {
        this.modalService.open(this.modal).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }    

    addSortingParam() {
        this.form.push({
            field : '',
            order : 'asc'
        });
    }

    deleteSortingParam(index) {
        this.arrayService.deleteByIndex(this.form, index);
    }

    submit() {
        this.form = this.form.filter(f => f.field != null && f.field != '' && f.order != null);
        console.log('post sorting', this.form);
        this.sortingService.data.next(this.form)
    }
}