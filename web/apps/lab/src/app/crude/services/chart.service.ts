import {Injectable} from '@angular/core';
import {Subject}    from "rxjs/Subject";

@Injectable()
export class ChartService {
    public form  = new Subject<any>();
    public data  = new Subject<any>();
    public frame = new Subject<any>();

    private types: Array<any> = [
        {
            name     : 'Line chart',
            selector : 'line-chart',
            icon     : 'mdi mdi-chart-line',
            aggTypes : ['date_histogram', 'date_histogram_term']
        },
        {
            name     : 'Pie chart',
            selector : 'pie-chart',
            icon     : 'mdi mdi-chart-pie',
            aggTypes : ['term', 'histogram']
        },
        {
            name     : 'Bar chart',
            selector : 'bar-chart',
            icon     : 'mdi mdi-chart-bar',
            aggTypes : ['term', 'histogram']
        }
    ];

    public getTypes(): Array <any> {
        return this.types;
    }
}