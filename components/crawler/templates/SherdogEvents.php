<?php

$eventProperties = [
    'name'  => './/td[2]/a',
    'title' => './/td[3]',
    'url'   => './/td[2]/a/@href',
    'date'  => './/meta[@itemprop="startDate"]/@content',
    'place' => [
        'type' => 'single',
        'props' => [
            'name' => './/td[4]',
            'flag' => './/td[4]/img/@src'
        ]
    ]
];

return [
    'events' => [
        'type'  => 'single',
        'props' => [
            'upcoming' => [
                'type'  => 'collection',
                'xpath' => '//div[@id="upcoming_tab"]/table/tr[@class="odd" or @class="even"]',
                'props' => $eventProperties
            ],
            'recent' => [
                'type'  => 'collection',
                'xpath' => '//div[@id="recentfights_tab"]/table/tr[@class="odd" or @class="even"]',
                'props' => $eventProperties
            ]
        ]
    ]
];