<?php

namespace app\controllers\actions\collection;

use app\components\rest\Action;
use app\models\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\Json;

/**
 * Class IndexFieldAction
 * @package app\controllers\actions\item
 */
class IndexFieldAction extends Action
{
    public function run(int $id)
    {
        $collection = $this->getModel($id);

        $this->initEsConnection($collection->instance);

        $fields  = $collection->fieldsArray;
        $mapping = $collection->getMapping();

        $config = Config::find()
            ->select('value')
            ->where(['key' => "component-state-item-list-{$collection->id}"])
            ->scalar();

        if ($config) {
            $config = Json::decode($config);

            if (isset($config['fields'])) {
                $config = ArrayHelper::index($config['fields'], 'name');

                foreach ($config as $key => $value) {
                    $config[$key] = [
                        'position' => $value['position'] ?? null,
                        'hidden'   => $value['hidden']   ?? false,
                        'view'     => $value['view']     ?? null
                    ];
                }
            }
        }

        $dics = array_column($fields, 'valueMap');

        if ($dics) {
            $dics = Config::find()
                ->select(['key', 'value'])
                ->where(['key' => $dics])
                ->asArray()
                ->all();

            $dics = ArrayHelper::map($dics, 'key', 'value');

            foreach ($dics as $key => $value) {
                $dics[$key] = Json::decode($value)['map'];
            };
        }


        foreach ($fields as &$field) {
            if (isset($mapping[$field['name']])) {
                $field['mapping'] = $mapping[$field['name']];
            }

            if (!isset($field['label']) || !$field['label']) {
                $field['label'] = BaseInflector::humanize($field['name']);
            }

            if ($config && isset($config[$field['name']])) {
                $field = array_merge($field, $config[$field['name']]);
            }

            if (isset($field['valueMap'])) {
                if (isset($dics[$field['valueMap']])) {
                    $field['valueMap'] = $dics[$field['valueMap']];
                } else {
                    unset($field['valueMap']);
                }
            }
        }

        usort($fields, function($a, $b) {
            $a = $a['position'] ?? null;
            $b = $b['position'] ?? null;

            return $a <=> $b;
        });

        return $fields;
    }
}