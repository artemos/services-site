<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $socialId
 * @property string $socialData
 * @property string $authKey
 * @property string $role
 * @property string $login
 * @property string $password
 * @property string $createdAt
 * @property string $updatedAt
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_ROOT = 'root';
    const ROLE_USER = 'user';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['socialId', 'socialData'], 'required'],
            [['socialId', 'socialData', 'login', 'password'], 'string'],
            [['createdAt', 'updatedAt', 'authKey'], 'safe'],
            [['socialId', 'login'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'socialId'   => 'Social ID',
            'socialData' => 'Social Data',
            'createdAt'  => 'Created At',
            'updatedAt'  => 'Updated At',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return null|static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['authKey' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}
