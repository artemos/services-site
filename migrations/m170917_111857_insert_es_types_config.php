<?php

use yii\db\Migration;

class m170917_111857_insert_es_types_config extends Migration
{
    public function safeUp()
    {
        $this->insert('configs', ['key' => 'field-types', 'value' => \yii\helpers\Json::encode(\app\components\es\Type::getAll())]);
    }

    public function safeDown()
    {
        $this->delete('configs', ['key' => 'field-types']);
    }
}
