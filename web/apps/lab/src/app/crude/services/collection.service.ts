import {Injectable}       from '@angular/core';
import {Http}             from '@angular/http';
import {IndexService}     from "./index.service";
import {ApiService}       from '../../core/services/api.service';
import {HttpErrorService} from "../../core/services/http-error.service";

@Injectable()
export class CollectionService extends ApiService {
    constructor(protected http: Http, private indexService: IndexService, private httpErrorService: HttpErrorService) {
        super(http, '/api/collections');
    }

    fetchFields(id) {
        return this.http
            .get(this.url('/' + id + '/fields'), this.getOptions())
            .map(response => response.json());
    }
}