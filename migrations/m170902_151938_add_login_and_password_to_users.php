<?php

use yii\db\Migration;

class m170902_151938_add_login_and_password_to_users extends Migration
{
    public function safeUp()
    {
        $this->addColumn('users', 'login', 'text');
        $this->addColumn('users', 'password', 'text');
    }

    public function safeDown()
    {
        $this->dropColumn('users', 'login');
        $this->dropColumn('users', 'pasword');
    }
}
