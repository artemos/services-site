import {Component, OnInit, Input} from '@angular/core';
import {InstanceService}          from "../../services/instance.service";

@Component({
    selector    : 'instance-status',
    templateUrl : './instance-status.component.html'
})
export class InstanceStatusComponent implements OnInit {
    @Input() instance: any;

    checkSsh: boolean;
    pingStatus: string;
    sshStatus: string;

    constructor(private instanceService: InstanceService) {}

    ngOnInit() {
        this.fetchPingStatus();

        if (this.instance.params.sshUsername != null && this.instance.params.sshPassword != null) {
            this.checkSsh = true;
            this.fetchSshStatus();
        }
    }

    fetchPingStatus() {
        this.instanceService.fetchPingStatus(this.instance.id).subscribe(
            response => {
                this.pingStatus = response.status;
            },
            error => {
                console.log('http error', error);
            }
        );
    }

    fetchSshStatus() {
        this.instanceService.fetchShhStatus(this.instance.id).subscribe(
            response => {
                this.sshStatus = response.status;
            },
            error => {
                console.log('http error', error);
            }
        );
    }
}