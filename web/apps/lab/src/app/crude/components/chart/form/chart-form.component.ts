import {Component, OnInit, Input} from '@angular/core';
import {EventEmitter, Output, ViewChild} from '@angular/core';
import {StateService} from "../../../../core/services/state.service";
import {ChartService} from "../../../services/chart.service";
import {AggregationService} from "../../../services/aggregation.service";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FieldService} from "../../../services/field.service";

@Component({
    selector    : 'chart-form',
    templateUrl : './chart-form.component.html'
})
export class ChartFormComponent implements OnInit {
    @Input() id: string;
    @ViewChild('modal') modal: any;

    forms      : any;
    chart      : any;
    fields     : any;
    form       : any;
    chartTypes : Array <any>;
    aggTypes   : Array <any>;
    aggParams  : any;

    constructor(
        private chartService : ChartService,
        private modalService : NgbModal,
        private fieldService : FieldService,
        private aggService   : AggregationService) {

        this.chartTypes = chartService.getTypes();
        this.aggTypes   = aggService.getTypes();
        this.aggParams  = {};

        for (let aggType of this.aggTypes) {
            this.aggParams[aggType.value] = aggType.params;
        }
    }

    ngOnInit() {
        this.fieldService.collectionFields.subscribe(
            data => this.fields = data
        );

        this.initForm();
    }

    isActiveTab(chart) {
        return this.form != null && this.form.selector == chart.selector;
    }

    initForm() {
        if (this.chart == null) {
            this.form = {
                selector    : 'line-chart',
                aggregation : {}
            };
        } else {
            this.form = {... this.chart};
        }

        if (this.forms == null) {
            this.forms = {};

            for (let type of this.chartTypes) {
                this.forms[type.selector] = {
                    aggregation: {}
                };
            }
        }
    }

    openModal() {
        this.modalService.open(this.modal).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }

    nameToLabel(name) {
        return this.fieldService.nameToLabel(name);
    }

    getFieldSelectType(aggregation, param) {
        if (aggregation.type == 'date_histogram' || (aggregation.type == 'date_histogram_term' && param == 'date_field')) {
            return 'date';
        }

        return null;
    }

    submit(close) {
        let form = this.forms[this.form.selector];
        form.selector = this.form.selector;

        this.chartService.data.next(form);

        close();
    }
}