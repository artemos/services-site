import {Component, OnInit} from '@angular/core';
import {HttpErrorService}  from "../../../../core/services/http-error.service";
import {ConfigService}     from "../../../../core/services/config.service";
import {ArrayService}      from "../../../../core/services/array.service";

@Component({
    templateUrl : './widget-list.component.html'
})
export class WidgetListComponent implements OnInit {
    widgets: any;

    crumbs: any = {
        title: 'All widgets',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All widgets'}
        ],
        buttons: [
            {
                text : 'New widget',
                route: '/widgets/new',
                class: 'btn btn-primary'
            }
        ]
    };

    constructor(private arrayService: ArrayService,
                private configService: ConfigService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        let params = {type: 'chart'};

        this.configService.fetchAll(params).subscribe(
            response => this.widgets = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    deleteWidget(widget) {
        if (!confirm("Delete widget '" + widget.value.title + "'?")) {
            return;
        }

        this.configService.delete(widget.id).subscribe(
            ()    => this.arrayService.deleteElement(this.widgets, widget),
            error => this.httpErrorService.handle(error, this)
        );
    }
}