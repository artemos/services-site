#!/bin/bash

apt-get update
apt-get install -y puppet
puppet module install puppetlabs-vcsrepo
puppet module install puppetlabs-stdlib

dpkg-reconfigure tzdata