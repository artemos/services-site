<?php

return [
    'articles' => [
        'type'  => 'collection',
        'xpath' => '//table[contains(@class, "blog")]//table',
        'props' => [
            'url'   => './/h3/a/@href',
            'title' => './/h3/a/text()',
            'text'  => './/div[contains(@class, "article-content")]',
            'image' => './/img/@src'
        ]
    ]
];