<?php

use yii\db\Migration;

class m170907_091203_rename_user_configs_table extends Migration
{
    public function safeUp()
    {
        $this->renameTable('users_configs', 'configs');
    }


    public function safeDown()
    {
        $this->renameTable('configs', 'users_configs');
    }
}
