<?php

use yii\db\Migration;

class m170911_144333_add_index_type_to_collections extends Migration
{
    public function safeUp()
    {
        $this->addColumn('collections', 'index_type', 'text');
    }


    public function safeDown()
    {
        $this->dropColumn('collections', 'index_type');
    }
}
