<?php

namespace app\controllers;

use app\components\enum\InstanceType;
use app\components\es\Mapper;
use app\models\Collection;
use app\models\EsModel;
use app\models\Instance;
use app\models\Item;
use app\models\User;
use Elasticsearch\ClientBuilder;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionTest()
    {
        $data = file_get_contents("http://85.143.219.140:9222/mma_video/mma_video/_search?size=300&_source_include=title");
        $data = Json::decode($data);

        foreach ($data['hits']['hits'] as $row) {
            echo "{$row['_source']['title']}<br/>";
       }
    }
}
