import {Component, Input} from '@angular/core';
import {ItemService}      from "../../../services/item.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {ChartComponent} from "../chart.component";

declare var Chartist: any;

@Component({
    selector    : 'bar-chart',
    templateUrl : './bar-chart.component.html'
})
export class BarChartComponent extends ChartComponent {
    @Input() axisYOffset: any;

    chartLegends : any;
    chartData    : any;

    constructor(
        protected httpErrorService: HttpErrorService,
        protected itemService: ItemService) {

        super(httpErrorService, itemService);
    }

    protected buildChartData(data) {
        let field   = Object.keys(data.aggregations)[0];
        let buckets = data.aggregations[field].buckets;

        this.chartData    = [];
        this.chartLegends = [];

        for (let data of buckets) {
            this.chartData.push(data.doc_count);
            this.chartLegends.push(data.key);
        }
    }

    protected initChart() {
        if (this.axisYOffset == null) {
            let defOffset = 70;
            let midLength = 10;
            let maxLength = this.chartLegends.reduce((a, b) =>  a.length > b.length ? a : b).length;
            let dimention = maxLength / midLength;

            if (dimention > 1) {
                this.axisYOffset = dimention * defOffset;
            } else {
                this.axisYOffset = defOffset;
            }
        }

        new Chartist.Bar('#' + this.elementId,
            {
                labels: this.chartLegends,
                series: [this.chartData]
            },
            {
                seriesBarDistance: 10,
                reverseData: true,
                horizontalBars: true,
                axisY: {
                    offset: parseInt(this.axisYOffset)
                }
            }
        );
    }
}