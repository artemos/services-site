import {Component, OnInit} from '@angular/core';
import {CollectionService} from "../../../services/collection.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {IndexService} from "../../../services/index.service";

declare var swal: any;

@Component({
    templateUrl : './collection-list.component.html'
})
export class CollectionListComponent implements OnInit {
    collections: any;

    crumbs: any = {
        title: 'All collections',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All collections'}
        ],
        buttons: [
            {
                text : 'New collection',
                route: '/collections/new',
                class: 'btn btn-primary'
            }
        ]
    };

    constructor(
        private collectionService: CollectionService,
        private httpErrorService: HttpErrorService,
        private indexService: IndexService) {
    }

    ngOnInit() {
        this.fetchCollections(() => this.fetchIndices());
    }

    fetchCollections(callback) {
        this.collectionService.fetchAll().subscribe(
            response => {
                this.collections = response.sort((a, b) => {
                    if (a.name == b.name) {
                        return 0;
                    }

                    if (a.name > b.name) {
                        return 1;
                    }

                    return -1;
                });

                callback();
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    fetchIndices() {
        let instanceIds = Array.from(new Set(this.collections.map((c) => c.instanceId)));

        for (let instanceId of instanceIds) {
            this.indexService.fetchAll({instanceId: instanceId}).subscribe(
                response => {
                    for (let index of response) {
                        for (let collection of this.collections) {
                            if (collection.instanceId == instanceId && collection.indexName == index.index) {
                                collection.index = index;
                            }
                        }
                    }
                },
                error => this.httpErrorService.handle(error, this)
            )
        }
    }

    deleteCollection(collection) {
        let name = prompt('Enter collection name to confirm deleting:');

        if (name == collection.name) {
            this.collectionService.delete(collection.id).subscribe(
                response => this.fetchCollections(() => this.fetchIndices()),
                error    => this.httpErrorService.handle(error, this)
            );
        }
    }
}