<?php
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);
define('ROOT_DIR', realpath(dirname(__FILE__) . '/../'));
define('BEARER_AUTH_KEY', '$2y$13$YIGy5v/p2gnhLIRAYc7Ceuk4B2CbVXosuCBdgfpXMKSFzGnc5dRMC');

require(__DIR__ . '/../components/debug.php');
require_once(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require __DIR__ .'/../vendor/autoload.php';

$config      = require ROOT_DIR . '/config/test.php';
$application = new yii\console\Application($config);