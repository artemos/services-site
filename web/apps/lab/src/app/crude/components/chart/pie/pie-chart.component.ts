import {Component, AfterViewInit} from '@angular/core';
import {ItemService}              from "../../../services/item.service";
import {HttpErrorService}         from "../../../../core/services/http-error.service";
import {ChartComponent}           from "../chart.component";

declare var echarts: any;

@Component({
    selector    : 'pie-chart',
    templateUrl : './pie-chart.component.html'
})
export class PieChartComponent extends ChartComponent {
    chartLegends : any;
    chartData    : any;

    constructor(
        protected httpErrorService: HttpErrorService,
        protected itemService: ItemService) {

        super(httpErrorService, itemService);
    }

    protected buildChartData(data) {
        let field   = Object.keys(data.aggregations)[0];
        let buckets = data.aggregations[field].buckets;

        this.chartData    = [];
        this.chartLegends = [];

        for (let data of buckets) {
            this.chartData.push({
                name  : data.key,
                value : data.doc_count
            });

            this.chartLegends.push(data.key);
        }
    }

    protected initChart() {
        var chart = echarts.init(document.getElementById(this.elementId));

        let options = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data: this.chartLegends
            },
            color: this.colors,
            calculable: true,
            series: [
                {
                    name: this.title,
                    type:'pie',
                    radius: ['80%', '90%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '30',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:this.chartData
                }
            ]
        };

        chart.setOption(options, true);

        $(function () {
            function resize() {
                setTimeout(function () {
                    chart.resize();
                }, 100)
            }

            $(window).on("resize", resize);
            $(".sidebartoggler").on("click", resize)
        });
    }
}