<?php

use yii\db\Migration;

/**
 * Class m171019_103414_update_instances_params
 */
class m171019_103414_update_instances_params extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $instances = Yii::$app->db->createCommand('SELECT * FROM instances')->queryAll();
        $fields    = ['sshPort', 'sshUsername', 'sshPassword', 'configPath'];

        foreach ($instances as $instance) {
            $params = [];

            foreach ($fields as $field) {
                if (isset($instance[$field])) {
                    $params[$field] = $instance[$field];
                }
            }

            if ($params) {
                $params = \yii\helpers\Json::encode($params);
                Yii::$app->db->createCommand("UPDATE instances SET params = '{$params}' WHERE id = {$instance['id']}")->execute();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171019_103414_update_instances_params cannot be reverted.\n";

        return false;
    }
    */
}
