<?php

namespace app\controllers\actions\item;

use Yii;
use app\components\es\Presenter;
use app\models\Collection;
use app\components\rest\Action;
use yii\elasticsearch\Query;
use yii\helpers\Json;
use yii\elasticsearch\ActiveDataProvider;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class IndexAction extends Action
{
    /**
     * @var
     */
    protected $mapping;

    /**
     * @param int $collectionId
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $collectionId)
    {
        $collection = Collection::getById($collectionId);

        $this->initEsConnection($collection->instance);

        $this->mapping = $collection->getMapping();

        $query = new Query();
        $query->from($collection->indexName, $collection->indexType);

        $includeFields = $this->getParam('includeFields');
        if ($includeFields) {
            $query->source(['includes' => explode(',', $includeFields)]);
        }

        $condition = $this->getParam('condition');
        if ($condition) {
            $this->applyCondition($query, $condition);
        }

        $sorting = $this->getParam('sorting');
        if ($sorting) {
            $this->applySorting($query, $sorting);
        }

        $aggregation = $this->getParam('aggregation');
        if ($aggregation) {
            $this->applyAggregation($query, $aggregation);
        }

        $page = $this->getParam('offset') ? ($this->getParam('offset') / $this->getParam('limit')) : 0;
//
//        $query->addAggregate("weightClass", [
//            'terms' => [
//                'field' => $this->addKeywordToField('weightClass')
//            ]
//        ]);
//
//        $query->addAggregate("association", [
//            'terms' => [
//                'field' => $this->addKeywordToField('association'),
//                'size'  => 10000
//            ]
//        ]);
//
//        $query->addAggregate("country", [
//            'terms' => [
//                'field' => $this->addKeywordToField('country')
//            ]
//        ]);
//

        $provider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $this->getParam('limit'),
                'page'     => $page
            ]
        ]);

        $items = $provider->getModels();
        $items = Presenter::presentItems($items);

        if ($format = $this->getParam('format')) {
            Yii::$app->response->format = $format;
        }

        $body = [
            //'condition'    => $condition,
            //'sorting'      => $sorting,
            'total'        => $provider->getTotalCount(),
            'aggregations' => $provider->getAggregations(),
            'items'        => $items
        ];

        Yii::$app->cache->set('e-tag-seed-' . Yii::$app->request->url, serialize($body));

//        $values = Json::decode($condition);
//        $values = $values[0]['value'];
//        $ids    = array_column($body['items'], 'id');
//
//        foreach ($values as $value) {
//            if (!in_array($value, $ids)) {
//                echo $value . "\n";
//            }
//        }

        return $body;
    }

    /**
     * @param Query $query
     * @param $condition
     */
    protected function applyCondition(Query $query, $condition)
    {
        $condition = Json::decode($condition);

        foreach ($condition as $data) {
            switch ($data['oper']) {
                case 'eq':
                    $query->andWhere([$this->addKeywordToField($data['field']) => $data['value']]);
                    break;

                case 'not_eq':
                    $query->andWhere(['not in', $data['field'], $data['value']]);
                    break;

                case 'gt':
                    $query->andWhere(['>', $data['field'], $data['value']]);
                    break;

                case 'gte':
                    $query->andWhere(['>=', $data['field'], $data['value']]);
                    break;

                case 'lt':
                    $query->andWhere(['<', $data['field'], $data['value']]);
                    break;

                case 'lte':
                    $query->andWhere(['<=', $data['field'], $data['value']]);
                    break;

                case 'match':
                    $query->query = ['match' => [$data['field'] => $data['value']]];
                    break;

                case 'multi_match':
                    $query->minScore($data['minScore'] ?? 10);

                    if (isset($data['stopWords'])) {
                        $mustNot = [];

                        foreach ($data['stopWords'] as $field => $words) {
                            foreach ($words as $word) {
                                $mustNot[] = [
                                    'match' => [$field => $word]
                                ];
                            }
                        }
                    }

                    $bool = [
                        'bool' => [
                            'should' => [
                                [
                                    'multi_match' => [
                                        'query'  => $data['value'],
                                        'fields' => $data['fields'],
                                        'minimum_should_match' => '100%'
                                    ]
                                ]
                            ],
                        ]
                    ];

                    if (isset($mustNot) && $mustNot) {
                        $bool['bool']['must_not'] = $mustNot;
                    }

                    $query->query = $bool;

                    break;
            }
        }
    }

    /**
     * @param Query $query
     * @param $sorting
     */
    protected function applySorting(Query $query, $sorting)
    {
        $sorting = Json::decode($sorting);

        foreach ($sorting as $i => $data) {
            $sorting[$data['field']] = $data['order'] == 'asc' ? SORT_ASC : SORT_DESC;
            unset($sorting[$i]);
        }

        $query->addOrderBy($sorting);
    }

    /**
     * @param Query $query
     * @param $aggregation
     */
    protected function applyAggregation(Query $query, $aggregation)
    {
        $aggregation  = Json::decode($aggregation);
        $keywordNames = ['field', 'term_field'];

        foreach ($keywordNames as $name) {
            if (isset($aggregation[$name])) {
                $aggregation[$name] = $this->addKeywordToField($aggregation[$name]);
            }
        }

        switch ($aggregation['type']) {
            case 'max':
                foreach ($aggregation['fields'] as $field) {
                    $query->addAggregate($field, [
                        'max' => [
                            'field' => $this->addKeywordToField($field)
                        ]
                    ]);
                }
                break;

            case 'terms':
                foreach ($aggregation['terms'] as $term) {
                    $query->addAggregate($term['field'], [
                        'terms' => [
                            'field' => $this->addKeywordToField($term['field']),
                            'size'  => $term['size']
                        ]
                    ]);
                }

                break;

            case 'term':
                $query->addAggregate("aggregation", [
                    'terms' => [
                        'field' => $aggregation['field']
                    ]
                ]);
                break;

            case 'terms':
                foreach ($aggregation['terms'] as $term) {
                    $query->addAggregate($term['field'], [
                        'terms' => [
                            'field' => $this->addKeywordToField($term['field']),
                            'size'   => $term['size'] ?? null
                        ]
                    ]);
                }
                break;

            case 'histogram':
                $query->addAggregate("aggregation", [
                    'histogram' => [
                        'field'    => $aggregation['field'],
                        'interval' => $aggregation['interval']
                    ]
                ]);
                break;

            case 'date_histogram':
                $query->addAggregate("aggregation", [
                    'date_histogram' => [
                        'field'    => $aggregation['field'],
                        'interval' => $aggregation['interval'],
                        'format'   => $aggregation['format']
                    ]
                ]);
                break;

            case 'date_histogram_term':
                $query->addAggregate("aggregation", [
                    'terms' => [
                        'field' => "{$aggregation['term_field']}"
                    ],
                    'aggs'  => [
                        "aggregation" => [
                            'date_histogram' => [
                                'field'    => "{$aggregation['date_field']}",
                                'interval' => $aggregation['interval'],
                                'format'   => $aggregation['format']
                            ]
                        ]
                    ]
                ]);
                break;
        }
    }

    /**
     * @param $field
     * @return string
     */
    protected function addKeywordToField($field)
    {
        if (!isset($this->mapping[$field])) {
            return $field;
        }

        if ($this->mapping[$field]['type'] == 'keyword') {
            return $field;
        }

        if (!isset($this->mapping[$field]['fields'])) {
            return $field;
        }

        $keyword = null;

        foreach ($this->mapping[$field]['fields'] as $key => $data) {
            if ($data['type'] == 'keyword') {
                $keyword = $key;
                break;
            }
        }

        if ($keyword && mb_strpos($field, '.' . $keyword) === false) {
            $field = $field . "." . $keyword;
        }

        return $field;
    }
}