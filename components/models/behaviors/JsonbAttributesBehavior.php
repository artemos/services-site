<?php

namespace app\components\models\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\Json;

/**
 * Class JsonbAttributesBehavior
 * @package app\components\models\behaviors
 */
class JsonbAttributesBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'serializeAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'serializeAttributes',
        ];
    }

    public function serializeAttributes()
    {
        if (!isset($this->owner->jsonbAttributes)) {
            return;
        }

        foreach ($this->owner->jsonbAttributes as $attr) {
            if ($this->owner[$attr] === null) {
                $this->owner[$attr] = [];
            }

            if (is_array($this->owner[$attr])) {
                $this->owner[$attr] = Json::encode($this->owner[$attr]);
            }
        }
    }
}