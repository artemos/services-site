<?php

return [
    'fighter' => [
        'type'  => 'single',
        'props' => [
            'name'              => '//span[@class="fn"]/text()',
            'nick'              => '//h1[@itemprop="name"]/span[@class="nickname"]',
            'association'       => [
                'xpath'   => '//h5[@class="item association"]',
                'replace' => ['Association:', '']
            ],
            'weightClass'       => '/h6[@class="item wclass"]/strong[@class="title"]',
            'birthDate'         => '//span[@itemprop="birthDate"]',
            'age'               => [
                'xpath'   => '//div[@class="birth_info"]/span[@class="item birthday"]/strong',
                'replace' => ['AGE:', ''],
                'cast'    => 'int'
            ],
            'country'           => '//strong[@itemprop="nationality"]',
            'city'              => '//span[@itemprop="addressLocality"]',
            'image'             => '//img[@class="profile_image photo"]/@src',
            'flag'              => '//img[@class="big_flag"]/@src',
            'weight'            => [
                'type' => 'single',
                'props' => [
                    'kg' => [
                        'xpath'  => '//div[@class="size_info"]/span[@class="item weight"]',
                        'regexp' => '/([0-9\.]+) kg/',
                        'cast'   => 'float'
                    ],
                    'lbs' => [
                        'xpath'  => '//div[@class="size_info"]/span[@class="item weight"]',
                        'regexp' => '/([0-9]+) lbs/',
                        'cast'   => 'int'
                    ]
                ]
            ],
            'height' => [
                'type'  => 'single',
                'props' => [
                    'cm' => [
                        'xpath'  => '//div[@class="size_info"]/span[@class="item height"]',
                        'regexp' => '/([0-9\.]+) cm/',
                        'cast'   => 'float'
                    ],
                    'ft' => [
                        'xpath'  => '//div[@class="size_info"]/span[@class="item height"]',
                        'regexp' => "/([0-9']+)/"
                    ]
                ]
            ],
            'record' => [
                'type'  => 'single',
                'props' => [
                    'wins' => [
                        'type'  => 'single',
                        'props' => [
                            'total' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph"]/span/span[@class="counter"]',
                                'cast'  => 'int'
                            ],
                            'ko' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph"]/span[@class="graph_tag"][1]',
                                'cast'  => 'int'
                            ],
                            'sub' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph"]/span[@class="graph_tag"][2]',
                                'cast'  => 'int'
                            ],
                            'dec' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph"]/span[@class="graph_tag"][3]',
                                'cast'  => 'int'
                            ],
                        ]
                    ],
                    'losses' => [
                        'type'  => 'single',
                        'props' => [
                            'total' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph loser"]/span/span[@class="counter"]',
                                'cast'  => 'int'
                            ],
                            'ko' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph loser"]/span[@class="graph_tag"][1]',
                                'cast'  => 'int'
                            ],
                            'sub' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph loser"]/span[@class="graph_tag"][2]',
                                'cast'  => 'int'
                            ],
                            'dec' => [
                                'xpath' => '//div[@class="left_side"]/div[@class="bio_graph loser"]/span[@class="graph_tag"][3]',
                                'cast'  => 'int'
                            ],
                        ]
                    ],
                ]
            ],
            'fights' => [
                'type'  => 'collection',
                'xpath' => '//tr[@class="odd" or @class="even"]',
                'props' => [
                    'round'  => [
                        'xpath' => './/td[5]/text()',
                        'cast'  => 'int'
                    ],
                    'time'   => './/td[6]/text()',
                    'result' => './/span[contains(@class, "final_result")]',
                    'method' => './/td[4]/text()',
                    'referee' => './/td[4]/span[@class="sub_line"]',
                    'date'   => './/span[@class="sub_line"]',
                    'opponent' => [
                        'type' => 'single',
                        'props' => [
                            'name' => './/td[2]/a/text()',
                            'url'  => './/td[2]/a/@href',
                        ]
                    ],
                    'event' => [
                        'type' => 'single',
                        'props' => [
                            'name' => './/td[3]/a',
                            'url'  => './/td[3]/a/@href',
                        ]
                    ]
                ]
            ]
        ]
    ]
];
