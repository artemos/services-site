<?php

use yii\db\Migration;

/**
 * Class m171019_103214_add_config_to_instances
 */
class m171019_103214_add_config_to_instances extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('instances', 'params', 'jsonb');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('instances', 'params');
    }
}
