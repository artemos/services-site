<?php

namespace app\components\es;

/**
 * Class Presenter
 * @package app\components\services\es
 */
class Presenter
{
    /**
     * @param array $response
     * @return array
     */
    public static function presentSearchResponse(array $response)
    {
        $hits = $response['hits'];

        $result['items'] = $hits['hits'];
        $result['total'] = $hits['total'];

        if (isset($response['aggregations'])) {
            $result['aggs'] = $response['aggregations'];
        }

        foreach ($result['items'] as &$item) {
            $item = self::presentItem($item);
        }

        return $result;
    }

    /**
     * @param array $items
     * @return array
     */
    public static function presentItems(array $items)
    {
        foreach ($items as &$item) {
            $item = self::presentItem($item);
        }

        return $items;
    }

    /**
     * @param array $response
     * @return array
     */
    public static function presentItem(array $response)
    {
        $response = array_merge($response['_source'], [
            'id'     => $response['_id'],
            '_score' => $response['_score']
        ]);

        unset($response['@version']);

        return $response;
    }
}