import {Component, OnInit} from '@angular/core';
import {PipelineService}   from "../../services/pipeline.service";
import {HttpErrorService}  from "../../../core/services/http-error.service";
import {LogstashConfigService} from "../../services/logstash-config.service";

declare var swal: any;

@Component({
    templateUrl : './pipeline-list.component.html'
})
export class PipelineListComponent implements OnInit {
    pipelines: any;

    crumbs: any = {
        title: 'All pipelines',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All pipelines'}
        ],
        buttons: [
            {
                text : 'New pipeline',
                route: '/pipelines/new',
                class: 'btn btn-primary'
            }
        ]
    };

    constructor(
        private pipelineService: PipelineService,
        private httpErrorService: HttpErrorService,
        private logstashConfigService: LogstashConfigService) {
    }

    ngOnInit() {
        this.pipelineService.fetchAll().subscribe(
            response => this.pipelines = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    createConfigFile(pipeline) {
        this.logstashConfigService.create({id: pipeline.id}).subscribe(
            response => {
                if (response.output.length == 0) {
                    pipeline.fileCreated = true;
                } else {
                    swal('Error', response.output, 'error');
                }
            },
            error => this.httpErrorService.handle(error, this)
        );
    }
}