<?php

namespace app\controllers\actions\item;

use app\models\Collection;
use app\components\rest\Action;

/**
 * Class DeleteAction
 * @package app\controllers\actions\item
 */
class DeleteAction extends Action
{
    public function run(string $id, int $collectionId)
    {
        $collection = Collection::getById($collectionId);

        $params = [
            'index' => $collection->indexName,
            'type'  => $collection->indexName,
            'id'    => $id
        ];

        $response = $this->getEsClient($collection->instanceId)->delete($params);

        $this->setResponseStatusCode(204);

        return $response;
    }
}