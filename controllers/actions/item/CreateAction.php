<?php

namespace app\controllers\actions\item;

use app\models\Collection;
use app\models\Item;
use app\components\rest\Action;
use yii\web\NotFoundHttpException;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class CreateAction extends Action
{
    /**
     * @return Item
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $collection = Collection::findOne($this->requireParam('collectionId'));

        if (!$collection) {
            throw new NotFoundHttpException('Collection not found');
        }

        $item = new Item(['fields' => $collection->fieldsArray]);
        $item->load($this->getRequestParams());

        if ($item->validate()) {
            $params = [
                'index' => $collection->indexName,
                'type'  => $collection->indexName,
                'body'  => $item->data,
            ];

            if (isset($item->data['id'])) {
                $params['id'] = $item->data['id'];
            }

            $this->setResponseStatusCode(201);

            return $this->getEsClient($collection->instanceId)->index($params);
        } else {
            return $item;
        }
    }
}