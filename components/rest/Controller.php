<?php

namespace app\components\rest;

use app\components\db\ActiveRecord;
use app\components\enum\InstanceType;
use app\models\Instance;
use Yii;
use yii\base\InvalidCallException;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Server;
use Elasticsearch\ClientBuilder;

/**
 * Class RestController
 * @package app\components\web
 */
class Controller extends \yii\rest\Controller
{
    public $serializer = 'app\components\rest\Serializer';

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => 'app\components\rest\OptionsAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index'  => ['GET', 'HEAD'],
            'create' => ['POST'],
            'delete' => ['DELETE'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON
                ]
            ],
            [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    'Origin'                           => [Yii::$app->request->headers->get('Origin')],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Request-Headers'   => ['content-type', 'authorization'],
                    'Access-Control-Max-Age'           => 86400,
                ]
            ],
            [
                'class'   => VerbFilter::className(),
                'actions' => $this->verbs()
            ],
            [
                'class' => RateLimiter::className()
            ],
           [
               'class'  => HttpBearerAuth::className(),
               'except' => ['options', 'login']
           ],
           [
               'class' => AccessControl::className(),
               'rules' => $this->accessRules(),
               'except' => ['options']
           ]
        ];
    }

    /**
     * @return array
     */
    protected function accessRules()
    {
        return [];
    }

    /**
     * @param $code
     */
    public function setResponseStatusCode($code)
    {
        $response = Yii::$app->getResponse();
        $response->setStatusCode($code);
    }

    /**
     * @return array
     */
    public function getRequestParams()
    {
        return Yii::$app->getRequest()->getBodyParams();
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        return Yii::$app->getRequest()->getQueryParams();
    }

    /**
     * @param $name
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function requireParam($name)
    {
        if (Yii::$app->request->isGet) {
            $params = $this->getQueryParams();
        } else {
            $params = $this->getRequestParams();
        }
        
        if (!array_key_exists($name, $params) || $params[$name] === null) {
            throw new BadRequestHttpException("'{$name}' param requried");
        }

        return $params[$name];
    }

    /**
     * @param $id
     * @param $options
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function getModel($id, $options = [])
    {
        $class = str_replace(['controllers', 'Controller'], ['models', ''], get_class($this));

        $query = $class::find()
            ->where(['id' => $id])
            ->limit(1);

        if (array_key_exists('with', $options)) {
            $query->with($options['with']);
        }

        $model = $query->one();

        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @param null $instanceId
     * @return \Elasticsearch\Client
     * @throws BadRequestHttpException
     */
    public function getEsClient($instanceId = null)
    {
        $query = Instance::find()
            ->select(new Expression("host || ':' || port"))
            ->allowed($this->getUserId())
            ->andWhere(['type' => InstanceType::ELASTICSEARCH]);

        if ($instanceId) {
            $query->andWhere(['id' => $instanceId]);
        }

        $hosts = $query->column();

        if (!$hosts) {
            throw new BadRequestHttpException('es hosts not found.');
        }

        return ClientBuilder::create()
            ->setRetries(4)
            ->setHosts($hosts)
            ->build();
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        if (Yii::$app->user->isGuest) {
            throw new InvalidCallException();
        }

        return Yii::$app->user->identity->id;
    }

    /**
     * @param null $name
     * @param null $defaultValue
     * @return array|mixed
     */
    public function getParam($name = null, $defaultValue = null)
    {
        return Yii::$app->request->get($name, $defaultValue);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function param($name)
    {
        $params = $this->getRequestParams();
        return $params[$name] ?? null;
    }

    /**
     * @param Instance $instance
     */
    public function initEsConnection(Instance $instance)
    {
        Yii::$app->elasticsearch->nodes = [
            [
                'http_address' => $instance->host . ':' . $instance->port
            ]
        ];
    }
}