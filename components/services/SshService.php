<?php

namespace app\components\services;
use yii\base\ErrorException;

/**
 * Class SSHClient
 * @package app\components\services
 */
class SshService
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var integer
     */
    protected $port;

    /**
     * @var resource
     */
    protected $connection;

    /**
     * SSHClient constructor.
     * @param $host
     * @param $username
     * @param $password
     * @param int $port
     */
    public function __construct($host, $username, $password, $port = 22)
    {
        $this->host     = $host;
        $this->port     = $port;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function authenticate()
    {
        $this->connection = ssh2_connect($this->host, $this->port);
        ssh2_auth_password($this->connection, $this->username, $this->password);
    }

    /**
     * @param $cmd
     * @return string
     */
    public function executeCommand($cmd)
    {
        $stream = ssh2_exec($this->connection, $cmd);
        stream_set_blocking($stream, true);

        $output = fread($stream, 4096);
        fclose($stream);

        return $output;
    }

    /**
     * @param $path
     * @return bool
     */
    public function isDirectoryExists($path)
    {
        return $this->isFileOrDirectoryExists($path, false);
    }

    /**
     * @param $path
     * @return bool
     */
    public function isFileExists($path)
    {
        return $this->isFileOrDirectoryExists($path, true);
    }

    /**
     * @param $path
     * @param $contents
     * @return string
     */
    public function createFile($path, $contents)
    {
        $output = $this->executeCommand("echo '" . $contents . "' > " . $path);
        return $output;
    }

    /**
     * @param $path
     * @param $isFile
     * @return bool
     */
    protected function isFileOrDirectoryExists($path, $isFile)
    {
        $flag = $isFile ? '-f' : '-d';

        $output = $this->executeCommand("if [ {$flag} {$path} ]; then echo 'exists'; else echo 'not found'; fi");
        $output = explode("\n", $output)[0];

        return $output == 'exists';
    }
}
