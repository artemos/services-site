<?php

use yii\db\Migration;

class m170718_095317_create_instances extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE instances
            (
                id SERIAL PRIMARY KEY,
                user_id INTEGER NOT NULL,
                app_id INTEGER NOT NULL,
                type TEXT NOT NULL,
                name TEXT NOT NULL,
                host TEXT,
                port INTEGER NOT NULL,
                ssh_port SMALLINT NULL,
                ssh_username TEXT NULL,
                ssh_password TEXT NULL,
                config_path TEXT NULL,
                created_at TIMESTAMP(0) DEFAULT now()
            );
        ");

        $this->execute("CREATE UNIQUE INDEX instances_user_id_name_i ON instances(user_id, name)");
        $this->execute("ALTER TABLE instances ADD CONSTRAINT instances_app_fk FOREIGN KEY (app_id) REFERENCES applications (id)");
        $this->execute("ALTER TABLE instances ADD CONSTRAINT instances_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
    }


    public function safeDown()
    {
        $this->dropTable('instances');
    }
}
