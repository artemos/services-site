import {Component, OnInit, Input} from '@angular/core';
import {Router}   from "@angular/router";
import {InstanceService}          from "../../services/instance.service";
import {HttpErrorService}         from "../../../core/services/http-error.service";

@Component({
    selector    : 'instance-card',
    templateUrl : './instance-card.component.html'
})
export class InstanceCardComponent implements OnInit {
    @Input() instance: any;
    @Input() detail: boolean;

    constructor(private router: Router,
                private instanceService: InstanceService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {

    }

    deleteInstance() {
        if (!confirm('Delete instance "' + this.instance.name + '" ?')) {
            return;
        }

        this.instanceService.delete(this.instance.id).subscribe(
            response => this.router.navigate(['instances/new']),
            error    => this.httpErrorService.handle(error, this)
        );
    }
}