import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {ApiService} from "./api.service";

@Injectable()
export class HandshakeService extends ApiService {
    constructor(protected http: Http) {
        super(http, '/api/handshake');
    }
}