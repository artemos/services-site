<?php

use yii\db\Migration;

class m170812_171726_insert_crawler_templates extends Migration
{
    public function up()
    {
        $templates = [
            'SherdogFighter'        => 'sherdog.com fighter',
            'SherdogEvents'         => 'sherdog.com events',
            'SherdogOrganization'   => 'sherdog.com organization',
            'SherdogEvent'          => 'sherdog.com event',
            'AllboxingArticles'     => 'allboxing.ru articles',
            'BloodandsweetArticles' => 'bloodandsweat.ru articles',
            'ValetudoArticles'      => 'valetudo.ru',
        ];

        $id = 1;

        foreach ($templates as $file => $name) {
            $schema = require ROOT_DIR . '/components/crawler/templates/' . $file . '.php';
            $schema = json_encode($schema);

            $data = [
                'id'      => $id,
                'name'    => $name,
                'schema'  => $schema,
                'user_id' => 1,
                'app_id'  => 2
            ];

            Yii::$app->db->createCommand()->insert('crawler_templates', $data)->execute();

            $id++;
        }
    }


    public function down()
    {
        $this->delete('crawler_templates');
    }
}
