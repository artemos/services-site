import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {ArrayService} from "../../../../core/services/array.service";
import {StateService} from "../../../../core/services/state.service";
import {ItemService} from "../../../services/item.service";
import {CollectionService} from "../../../services/collection.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {ConfigService} from "../../../../core/services/config.service";
import {FilterService} from "../../../services/filter.service";
import {SortingService} from "../../../services/sorting.service";
import {ChartService} from "../../../services/chart.service";
import {ItemListSettingsService} from "../../../services/item-list-settings.service";
import {FieldService} from "../../../services/field.service";

@Component({
    templateUrl : './item-list.component.html'
})
export class ItemListComponent implements OnInit {
    filter  : Array<any> = [];
    sorting : Array<any> = [];

    settings: any = {
        showEmptyValues  : true,
        valuesOnNextLine : false,
        showDetailButton : true,
        showEditButton   : true,
        showDeleteButton : true,
        showCreateButton : true
    };

    chart: any;

    fields: Array<any> = [];

    widgetType: string = 'list';

    pagination: any = {
        type   : 'more-button',
        limit  : 20,
        total  : 0,
        offset : 0,
        count  : 0
    };

    loading: boolean;
    items: any;
    collection: any;
    collectionId: number;
    chartVisible: any;
    crumbs: any;
    self: any;

    constructor(private route: ActivatedRoute,
                private modalService: NgbModal,
                private arrayService: ArrayService,
                private stateService: StateService,
                private itemService: ItemService,
                private configService: ConfigService,
                private collectionService: CollectionService,
                private httpErrorService: HttpErrorService,
                private filterService: FilterService,
                private fieldService: FieldService,
                private itemListSettingsService: ItemListSettingsService,
                private sortingService: SortingService,
                private chartService: ChartService) {
    }

    ngOnInit() {
        this.subscribeToRouteParams();
        this.subscribeToDataStreams();
    }

    subscribeToRouteParams() {
        this.route.params.subscribe(params => {
            this.collectionId = params.collectionId;
            this.chartVisible = null;
            this.items        = null;

            this.stateService.init("item-list-" + this.collectionId, this);

            if (this.pagination != null) {
                delete this.pagination['offset'];
                delete this.pagination['total'];
                delete this.pagination['count'];
            }

            this.fetchCollection(() => {
                this.itemListSettingsService.form.next({
                    pagination : this.pagination,
                    settings   : this.settings,
                    fields     : this.fields
                });

                this.filterService.form.next(this.filter);
                this.fieldService.collectionFields.next(this.fields);

                this.initCrumbs();
                this.loadItems({paginate: false});
            });
        });
    }

    subscribeToDataStreams() {
        this.filterService.data.subscribe(
            data => {
                this.setState({filter: data}, true);

                if (this.chart != null) {
                    this.chart.condition = data;
                    this.chartService.data.next(this.chart);
                }
            }
        );

        this.itemListSettingsService.data.subscribe(
            data => this.setState(data, true)
        );

        this.sortingService.data.subscribe(
            data => this.setState({sorting: data}, true)
        );

        this.chartService.data.subscribe(
            data => {
                this.chart = data;
                this.chart.collectionId = this.collection.id;
                this.chart.condition    = this.filter;

                this.chartService.frame.next(this.chart);
                this.chartVisible = true;
            }
        );
    }

    initCrumbs() {
        this.crumbs = {
            title: this.collection.name,
            links: [
                {title: 'Collections', href: '/collections'},
                {title: this.collection.name}
            ],
            buttons: []
        };
        
        if (this.settings.showCreateButton) {
            this.crumbs.buttons.push({
                text  : 'New item',
                route : '/collections/' + this.collectionId + '/items/new',
                class : 'btn btn-primary'
            });
        }

        if (this.configService.find('collection-' + this.collectionId + '-dashboard') != null) {
            this.crumbs.buttons.push({
                text  : 'Dashboard',
                route : '/collections/' + this.collectionId + '/dashboard',
                class : 'btn btn-info'
            });
        }
    }

    hideChart() {
        this.chartVisible = false;
        this.chart = null;
    }

    onWidgetTypeChangedHandler(type) {
        this.setState({widgetType: type}, true);
    }

    onLimitChangedHandler(limit) {
        this.pagination.limit = limit;
        this.setState({pagination: this.pagination}, true);
    }

    getState() {
        return {
            fields     : this.fields,
            filter     : this.filter,
            sorting    : this.sorting,
            settings   : this.settings,
            widgetType : this.widgetType,
            pagination : this.pagination
        }
    }

    setState(state, loadItems = false) {
        this.stateService.setState(state);

        if (loadItems) {
            this.loadItems({paginate: false});
        }
    }

    openModal(content) {
        this.modalService.open(content).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }

    getFetchParams(paginate = false) {
        let params = Object.assign({
            collectionId : this.collectionId,
            condition    : this.filter,
            limit        : this.pagination.limit
        });

        if (paginate) {
            this.pagination.offset  = (this.pagination.offset || 0) + this.pagination.limit;
            params['offset'] = this.pagination.offset;
        }

        if (this.sorting.length > 0) {
            params['sorting'] = this.sorting;
        }

        let excludeFields = this.fields.filter(f => f.hidden).map(f => f.name);
        let includeFields = this.fields.filter(f => !f.hidden).map(f => f.name);

        if (excludeFields.length > 0  && excludeFields.length < includeFields.length) {
            params['excludeFields'] = excludeFields;
        } else if (includeFields.length > 0 && includeFields.length < this.fields.length) {
            params['includeFields'] = includeFields;
        }

        return params;
    }

    loadItems(options: any = {}) {
        if (this.loading) {
            console.log('return because already loading');
            return;
        }

        let params = this.getFetchParams(options.paginate);

        console.log('fetching items:', params);

        this.loading = true;

        this.itemService.fetchAll(params).subscribe(
            response => {
                this.loading = false;

                if (options.paginate) {
                    this.items = this.items.concat(response.items);
                } else {
                    this.items = response.items;
                }

                this.pagination.total = response.total;
                this.pagination.count = this.items.length;
            },
            error => {
                this.loading = false;
                this.httpErrorService.handle(error, this);
            }
        )
    }

    paginate() {
        this.loadItems({paginate: true});
    }

    canPaginate(): boolean {
        return !this.loading && this.items.length < this.pagination.total;
    }

    getFieldSortOrder(field) {
        let param = this.sorting.find(p => p.field == field.name);

        if (param) {
            return param.order;
        }
    }

    toggleSortOrder(field) {
        let order = this.getFieldSortOrder(field);

        if (order == null) {
            this.sorting = [{
                field : field.name,
                order : 'desc'
            }];
        } else {
            let index = this.sorting.findIndex(p => p.field == field.name);
            this.sorting[index]['order'] = (order == 'asc' ? 'desc' : 'asc');
        }

        this.sortingService.data.next(this.sorting);
    }

    /**
     * TODO: refactoring
     *
     *   return new Promise(resolve => {
     *  Simulate server latency with 2 second delay
     * setTimeout(() => resolve(this.getHeroes()), 2000);
    *
     *
     * @param callback
     */
    private fetchCollection(callback) {
        this.collectionService.fetchById(this.collectionId).subscribe(
            response => {
                this.collection = response;

                this.collectionService.fetchFields(this.collection.id).subscribe(
                    response => {
                        this.fields = response;

                        console.log('fetched fields', this.fields);
                        callback();
                    },
                    error => this.httpErrorService.handle(error)
                );
            },
            error => {
                this.httpErrorService.handle(error, this)
            }
        );
    }
}