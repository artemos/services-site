import {Injectable} from '@angular/core';

@Injectable()
export class AggregationService {
    private types: Array<any> = [
        {
            text   : 'Term',
            value  : 'term',
            params : [
                {name: 'field', component: 'field-select'}
            ]
        },
        {
            text   : 'Histogram',
            value  : 'histogram',
            params : [
                {name: 'field', component: 'field-select'},
                {name: 'interval', component: 'number'}
            ]
        },
        {
            text   : 'Date histogram',
            value  : 'date_histogram',
            params : [
                {name: 'field', component: 'field-select'},
                {name: 'interval', component: 'interval-select'},
                {name: 'format', component: 'date-format-select'}
            ]
        },
        {
            text   : 'Date histogram & term',
            value  : 'date_histogram_term',
            params : [
                {name: 'term_field', component: 'field-select'},
                {name: 'date_field', component: 'field-select'},
                {name: 'interval', component: 'interval-select'},
                {name: 'format', component: 'date-format-select'}
            ]
        }
    ];

    public getTypes(): Array <any> {
        return this.types;
    }
}