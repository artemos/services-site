<?php

namespace app\models;

use Yii;
use app\components\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "collections".
 *
 * @property int $id
 * @property int $appId
 * @property int $userId
 * @property int $instanceId
 * @property array $jsonb
 * @property string $fields
 * @property string $settings
 * @property string $access
 * @property string $indexName
 * @property string $indexType
 * @property array $fieldsArray
 * @property string $name
 * @property string $createdAt
 */
class Collection extends ActiveRecord
{
    /**
     * @var array
     */
    public $jsonbAttributes = ['fields', 'settings', 'access'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instanceId', 'appId', 'name', 'userId', 'indexName', 'indexType'], 'required'],
            [['appId', 'userId', 'instanceId'], 'integer'],
            [['name', 'indexName', 'indexType'], 'string'],
            [['fields', 'settings', 'access'], 'safe'],
            ['name', 'unique', 'targetAttribute' => ['userId', 'name']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'appId'     => 'App ID',
            'userId'    => 'User ID',
            'name'      => 'Name',
            'createdAt' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstance()
    {
        return $this
            ->hasOne(Instance::className(), ['id' => 'instanceId'])
            ->select(['id', 'name', 'host', 'port']);
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $model  = parent::toArray($fields, ['instance'], $recursive);
        $decode = ['access', 'fields', 'settings'];

        foreach ($decode as $attr) {
            if (array_key_exists($attr, $model)) {
                $model[$attr] = Json::decode($model[$attr]);
            }
        }

        return $model;
    }

    /**
     * @return array
     */
    public function getFieldsArray()
    {
        if (!is_array($this->fields)) {
            return Json::decode($this->fields);
        }

        return $this->fields;
    }

    /**
     * @return array
     */
    public function getFieldLabels()
    {
        $labels = ArrayHelper::map($this->fieldsArray, 'name', 'label');

        foreach ($labels as $field => $label) {
            if (empty($label)) {
                $labels[$field] = $field;
            }
        }

        return $labels;
    }

    /**
     * @return array|mixed
     */
    public function getMapping()
    {
        /**
         * @var $command \yii\elasticsearch\Command
         */
        $command  = Yii::$app->elasticsearch->createCommand();
        $cacheKey = "collection-{$this->id}-{$this->indexName}-{$this->indexType}-mapping";
        $mapping  = Yii::$app->cache->get($cacheKey);

        if (!$mapping) {
            $mapping = $command->getMapping($this->indexName, $this->indexType);
            $mapping = $mapping[$this->indexName]['mappings'][$this->indexType]['properties'] ?? null;

            if ($mapping) {
                Yii::$app->cache->set($cacheKey, $mapping, 60 * 5);
            }
        }

        return $mapping;
    }
}
