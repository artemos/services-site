import {Component}        from '@angular/core';
import {ConfigService}    from "./core/services/config.service";
import {HttpErrorService} from "./core/services/http-error.service";
import {AuthService}      from "./user/services/auth.service";

@Component({
  selector    : 'app-root',
  templateUrl : './app.component.html',
  styleUrls   : ['./app.component.css']
})
export class AppComponent {
    layout :string = 'base';
    user   : any;
    loaded : boolean;

    constructor(
        private configService: ConfigService,
        private authService: AuthService,
        private httpErrorService: HttpErrorService) {

        if (location.pathname == '/login') {
            this.layout = 'auth';
        } else {
            let keyName = 'user-auth-key';
            let authKey = localStorage.getItem(keyName);

            localStorage.clear();

            if (authKey != null) {
                localStorage.setItem(keyName, authKey);
                authKey = null;
            }

            this.loadUserSession();
            this.loadConfigs();
        }
    }

    private loadUserSession() {
        this.authService.session().subscribe(
            response => {
                this.configService.set('user', response);
                this.user = response;
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    private loadConfigs() {
        this.configService.fetchAll().subscribe(
            response => {
                for (let config of response) {
                    this.configService.set(config.key, config.value);
                }

                this.loaded = true;
            },
            error => {
                this.httpErrorService.handle(error, this);
            }
        );
    }
}
