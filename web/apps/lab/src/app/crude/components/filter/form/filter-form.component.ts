import {Component, EventEmitter, ViewChild} from '@angular/core';
import {Input, OnInit, Output} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ArrayService} from "../../../../core/services/array.service";
import {FilterService} from "../../../services/filter.service";
import {FieldService} from "../../../services/field.service";
import {ConfigService} from "../../../../core/services/config.service";

@Component({
    selector    : 'filter-form',
    templateUrl : './filter-form.component.html'
})
export class FilterFormComponent implements OnInit{
    filter   : any;
    fields   : any;
    labels   : any = {};
    types    : any = {};
    valueMap : any = {};
    form     : any;

    @ViewChild('modal') modal: any;

    constructor(
        private modalService: NgbModal,
        private arrayService: ArrayService,
        private fieldService: FieldService,
        private filterService: FilterService) {
    }

    ngOnInit() {
        this.filterService.form.subscribe(
            data => this.form = data
        );

        this.fieldService.collectionFields.subscribe(
            data => {
                this.fields = data;

                for (let field of this.fields) {
                    this.labels[field.name] = field.label;
                    this.types[field.name]  = field.type;

                    if (field.valueMap) {
                        this.valueMap[field.name] = field.valueMap;
                    }
                }
            }
        );
    }

    openModal() {
        this.modalService.open(this.modal).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }    
    
    addFilterParam() {
        this.form.push({
            field : null,
            oper  : 'eq',
            value : null
        });
    }

    deleteFilterParam(index) {
        this.arrayService.deleteByIndex(this.form, index);
    }

    submit() {
        this.form = this.form.filter(f => f.field != null && f.oper != null && f.value != null);
        console.log('post filter', this.form);
        this.filterService.data.next(this.form);
    }
}