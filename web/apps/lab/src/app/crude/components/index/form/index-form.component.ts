import {OnInit, Input, Component} from "@angular/core";
import {ActivatedRoute}           from "@angular/router";
import {HttpErrorService}         from "../../../../core/services/http-error.service";
import {InstanceService} from "../../../../instance/services/instance.service";

@Component({
    templateUrl : './index-form.component.html'
})
export class IndexFormComponent implements OnInit {
    crumbs : any;
    instance    : any;
    errors      : Array<any>;
    form        : any;
    done        : boolean;

    constructor(
        private route: ActivatedRoute,
        private instanceService: InstanceService,
        private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.instanceService.fetchById(params.id).subscribe(
                response => {
                    this.instance = response;
                    this.initCrumbs();
                },
                error => this.httpErrorService.handle(error, this)
            );
        });

        this.initForm();
    }

    initForm() {
        this.form = {
            number_of_shards   : 1,
            number_of_replicas : 1
        };
    }

    initCrumbs() {
        this.crumbs = {
            title: 'New instance index',
            links: [
                {title: 'Instances', href: '/instances'},
                {title: this.instance.name, href: '/instances/' + this.instance.id},
                {title: 'index form'}
            ]
        };
    }
}
