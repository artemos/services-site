<?php

namespace app\controllers\actions\collection;

use Elasticsearch\Common\Exceptions\ElasticsearchException;
use app\components\rest\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class UpdateAction extends Action
{
    public function run(int $id)
    {
        /**
         * @var $collection Collection
         */
        $collection = $this->getModel($id);
        $collection->load($this->getRequestParams());

        if ($collection->validate()) {
            try {
                if ($this->param('updateIndex')) {
                    $params = [
                        'index' => $collection->indexName,
                        'type'  => $collection->indexType,
                        'body'  => [
                            $collection->indexType => [
                                'properties' => EsMapper::buildProperties($collection->fieldsArray)
                            ]
                        ]
                    ];

                    $this->getEsClient($collection->instanceId)->indices()->putMapping($params);
                }

                $collection->save(false);
            } catch (ElasticsearchException $e) {
                $collection->addError('instanceId', $e->getMessage());
            }
        }

        return $collection;
    }
}