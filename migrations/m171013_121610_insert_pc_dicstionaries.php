<?php

use yii\db\Migration;

/**
 * Class m171013_121610_insert_pc_dicstionaries
 */
class m171013_121610_insert_pc_dicstionaries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $configs = [
            'dictionary-body'          =>
                [
                    'saloon'    => 'седан',
                    'offroad'   => 'внедорожник',
                    'hatchback' => 'хэтчбек',
                    'versatile' => 'универсал',
                    'coupe'     => 'купе',
                    'minivan'   => 'минивэн',
                    'pickup'    => 'пикап',
                    'limousine' => 'лимузин',
                    'van'       => 'фургон',
                    'cabrio'    => 'кабриолет',
                    'minibus'   => 'микроавтобус',
                    'crossover' => 'кроссовер',
                ],
            'dictionary-car-state'     =>
                [
                    'good'   => 'хорошее',
                    'broken' => 'требуется ремонт',
                ],
            'dictionary-color'         =>
                [
                    'beige'     => 'Бежевый',
                    'white'     => 'Белый',
                    'blue'      => 'Голубой',
                    'yellow'    => 'Желтый',
                    'green'     => 'Зеленый',
                    'gold'      => 'Золотой',
                    'brown'     => 'Коричневый',
                    'red'       => 'Красный',
                    'orange'    => 'Оранжевый',
                    'purple'    => 'Пурпурный',
                    'pink'      => 'Розовый',
                    'silver'    => 'Серебряный',
                    'grey'      => 'Серый',
                    'dark_blue' => 'Синий',
                    'violet'    => 'Фиолетовый',
                    'black'     => 'Черный',
                ],
            'dictionary-demand-status' =>
                [
                    'new'      => 'активно',
                    'edit'     => 'требуется редактирование',
                    'canceled' => 'отменено',
                ],
            'dictionary-drive'         =>
                [
                    'front' => 'передний',
                    'rear'  => 'задний',
                    'full'  => 'полный',
                ],
            'dictionary-fuel'          =>
                [
                    'diesel' => 'дизель',
                    'hybrid' => 'гибрид',
                    'petrol' => 'бензин',
                    'gas'    => 'газ',
                ],
            'dictionary-offer-status'  =>
                [
                    'new'     => 'актуально',
                    'sold'    => 'продано',
                    'deleted' => 'удалено',
                ],
            'dictionary-person'        =>
                [
                    'individual' => 'физ. лицо',
                    'legal'      => 'юр. лицо',
                ],
            'dictionary-steering'      =>
                [
                    'right' => 'правый',
                    'left'  => 'левый',
                ],
            'dictionary-transmission'  =>
                [
                    'mechanical' => 'механика',
                    'auto'       => 'автомат',
                    'robotic'    => 'робот',
                    'cvt'        => 'вариатор',
                ],
        ];

        $names = [
            'dictionary-body' => 'кузова авто',
            'dictionary-car-state' => 'состояние авто',
            'dictionary-color' => 'цвет',
            'dictionary-demand-status' => 'статус поиска авто',
            'dictionary-drive' => 'привод авто',
            'dictionary-fuel' => 'топливо',
            'dictionary-offer-status' => 'статус обявления',
            'dictionary-steering' => 'руль авто',
            'dictionary-person' => 'лицо юр./физ',
            'dictionary-transmission' => 'КПП авто',
        ];

        foreach ($configs as $key => $value) {
            $this->insert('configs', [
                'key'   => $key,
                'value' => \yii\helpers\Json::encode([
                    'name' => $names[$key],
                    'map'  => $value
                ])
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("DELETE FROM configs WHERE key LIKE 'dictionary-%'");
    }
}
