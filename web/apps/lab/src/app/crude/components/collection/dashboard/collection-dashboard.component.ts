import {Component, OnInit}      from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {ArrayService} from "../../../../core/services/array.service";
import {ConfigService} from "../../../../core/services/config.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {CollectionService} from "../../../services/collection.service";

@Component({
    templateUrl : './collection-dashboard.component.html'
})
export class CollectionDashboardComponent implements OnInit {
    collection  : any;
    dashboard   : any;
    configs     : any;
    crumbs : any;

    constructor(
        private route: ActivatedRoute,
        private arrayService: ArrayService,
        private configService: ConfigService,
        private httpErrorService: HttpErrorService,
        private collectionService: CollectionService) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.fetchDashboard(params.collectionId);

            this.fetchCollection(params.collectionId, () => {
                this.initCrumbs();
            });
        });
    }

    removeWidget(rowIndex, colIndex) {
        if (!confirm('Remove widget from dashboard?')) {
            return;
        }

        if (confirm('Delete widget completely?')) {
            let widgetId = this.dashboard.value.rows[rowIndex].cols[colIndex].widgetId;

            this.configService.delete(widgetId).subscribe(
                ()    => {},
                error => this.httpErrorService.handle(error, this)
            );
        }

        this.arrayService.deleteByIndex(this.dashboard.value.rows[rowIndex].cols, colIndex);

        this.configService.update(this.dashboard).subscribe(
            ()    => {},
            error => this.httpErrorService.handle(error, this)
        );
    }

    private fetchCollection(id, callback) {
        this.collectionService.fetchById(id).subscribe(
            response => {
                this.collection = response;
                callback();
            },
            error => {
                this.httpErrorService.handle(error, this)
            }
        );
    }

    private fetchDashboard(collectionId) {
        let type    = 'collection-' + collectionId;
        let params  = {type: type};

        this.configService.fetchAll(params).subscribe(
            response => {
                let dashboard = response.find(c => c.key == type + '-dashboard');
                if (dashboard == null) {
                    return;
                }

                this.configs = {};

                for (let config of response) {
                    this.configs[config.id] = config;
                }

                this.dashboard = dashboard;
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    private initCrumbs() {
        this.crumbs = {
            title: 'Item analytics',
            links: [
                {title: 'Dashboard', href: '/'},
                {title: 'Collections', href: '/collections'},
                {title: this.collection.name},
                {title: 'Analytics'}
            ]
        };
    }
}