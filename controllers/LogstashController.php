<?php

namespace app\controllers;

use app\components\rest\Controller;
use app\models\Pipeline;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class LogstashController
 * @package app\controllers
 */
class LogstashController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['plugins'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionPlugins()
    {
        $plugins = file_get_contents(ROOT_DIR . '/web/assets/logstash.plugins.json');
        $plugins = Json::decode($plugins);

        return $plugins;
    }
}