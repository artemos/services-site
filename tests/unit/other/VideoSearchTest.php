<?php
namespace tests\models;

use PHPUnit\Framework\TestCase;
use app\components\models\RulesBuilder;
use yii\helpers\Json;

/**
 * Class RulesBuilder
 * @package tests\models
 */
class VideoSearchTest extends TestCase
{
    public function testOne()
    {
        $this->assertTrue(true);
        $this->search('Fedor emelianenko vs Fabricio werdum');
    }


    protected function search($query)
    {
        $condition = urlencode(Json::encode([
            [
                'fields' => 'title',
                'oper'   => 'multi_match',
                'value'  => $query
            ]
        ]));

        $response = file_get_contents("http://services-site/api/items?condition={$condition}&collectionId=20");
        $response = Json::decode($response);
        $result   = [];

        $titles   = array_column($response['items'], 'title');

        var_dump($titles);
    }
}
