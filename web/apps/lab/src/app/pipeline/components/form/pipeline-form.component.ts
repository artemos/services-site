import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router}   from "@angular/router";
import {NgbModal}                 from '@ng-bootstrap/ng-bootstrap';
import {InstanceService}          from "../../../instance/services/instance.service";
import {PipelineService}          from "../../services/pipeline.service";
import {HttpErrorService}         from "../../../core/services/http-error.service";

@Component({
    templateUrl: './pipeline-form.component.html'
})
export class PipelineFormComponent implements OnInit {
    @Input() pipeline: any;
    errors: any;
    instances: any;
    logstashPlugins;
    crumbs: any;

    constructor(private route: ActivatedRoute,
                private pipelineService: PipelineService,
                private instanceService: InstanceService,
                private httpErrorService: HttpErrorService,
                private modalService: NgbModal,
                private router: Router) {
    }

    ngOnInit() {
        this.fetchInstances();

        this.route.params.subscribe(params => {
            this.initCrumbs(params.id);
            this.initPipeline(params.id);
        });
    }

    submit() {
        this.pipelineService.save(this.pipeline).subscribe(
            response => this.router.navigate(['pipelines']),
            error    => this.httpErrorService.handle(error, this)
        )
    }

    openInputModal(content) {
        this.modalService.open(content).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }

    addInput(closeFunc) {
        closeFunc();
    }

    private fetchPipeline(id) {
        this.pipelineService.fetchById(id).subscribe(
            response => this.pipeline = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    private fetchInstances() {
        this.instanceService.fetchAll({type: 'logstash'}).subscribe(
            response => this.instances = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs = {
                title: 'New pipeline',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All pipelines', href: '/pipelines'},
                    {title: 'New pipeline'}
                ]
            };
        } else {
            this.crumbs = {
                title: 'Update pipeline',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All pipelines', href: '/pipelines'},
                    {title: 'Update pipeline'}
                ]
            }
        }
    }

    private initPipeline(id) {
        if (id == null) {
            this.pipeline = {};
        } else {
            this.fetchPipeline(id);
        }
    }
}