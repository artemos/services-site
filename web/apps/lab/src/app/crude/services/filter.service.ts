import {Injectable} from '@angular/core';
import {Subject}    from "rxjs/Subject";

@Injectable()
export class FilterService {
    public form = new Subject<any>();
    public data = new Subject<any>();

    public operLabels = {
        eq     : '=',
        not_eq : '!=',
        gt     : '>',
        gte    : '>=',
        lt     : '<',
        lte    : '<=',
    };
}