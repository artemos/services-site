<?php

use yii\db\Migration;

class m170902_164916_create_users_login_uniq_index extends Migration
{
    public function safeUp()
    {
        $this->execute("CREATE UNIQUE INDEX users_login_i on users(login)");
    }


    public function safeDown()
    {
        $this->execute("DROP INDEX users_login_i");
    }
}
