import {Injectable} from '@angular/core';

@Injectable()
export class ArrayService {
    deleteByIndex(array, index) {
        array.splice(index, 1);
    }

    deleteElement(array, element) {
        let index = array.indexOf(element);
        if (index != -1) {
            this.deleteByIndex(array, index);
        }
    }

    moveUp(array, element) {
        let elementIndex = array.indexOf(element);
        if (elementIndex <= 0) {
            return array;
        }

        let siblingIndex = elementIndex - 1;
        let tmpElement   = array[siblingIndex];

        array[siblingIndex] = element;
        array[elementIndex] = tmpElement;

        return array;
    }

    moveDown(array, element) {
        let elementIndex = array.indexOf(element);
        if (elementIndex  === -1) {
            return array;
        }

        let siblingIndex = elementIndex + 1;

        if (array[siblingIndex] == null) {
            return array;
        }

        let tmpElement = array[siblingIndex];

        array[siblingIndex] = element;
        array[elementIndex] = tmpElement;

        return array;
    }

    moveEnd(array, element) {
        let index = array.indexOf(element);
        if (index === -1) {
            return array;
        }

        this.deleteByIndex(array, index);
        array.push(element);

        return array;
    }
}