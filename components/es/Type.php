<?php

namespace app\components\es;

use app\components\Enum;

/**
 * Class Type
 * @package app\components\es
 */
class Type extends Enum
{
    const TEXT          = 'text';
    const KEYWORD       = 'keyword';
    const DATE          = 'date';
    const BOOLEAN       = 'boolean';
    const BINARY        = 'binary';
    const LONG          = 'long';
    const INTEGER       = 'integer';
    const SHORT         = 'short';
    const BYTE          = 'byte';
    const DOUBLE        = 'double';
    const OBJECT        = 'object';
    const NESTED        = 'nested';
    const GEO_POINT     = 'geo_point';
    const GEO_SHAPE     = 'geo_shape';
    const FLOAT         = 'float';
    const HALF_FLOAT    = 'half_float';
    const SCALED_FLOAT  = 'scaled_float';
    const INTEGER_RANGE = 'integer_range';
    const FLOAT_RANGE   = 'float_range';
    const LONG_RANGE    = 'long_range';
    const DOUBLE_RANGE  = 'double_range';
    const DATE_RANGE    = 'date_range';
    const IP            = 'ip';
    const COMPLETION    = 'completion';
    const TOKEN_COUNT   = 'token_count';
    const MURMUR3       = 'murmur3';
    const ATTACHMENT    = 'attachment';
}