<?php

namespace app\controllers;

use app\components\enum\InstanceType;
use app\components\rest\Controller;
use app\components\services\SshService;
use app\models\Instance;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use app\components\data\ActiveDataProvider;
use yii\base\ErrorException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class InstanceController
 * @package app\controllers
 */
class InstanceController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'health', 'ping', 'ssh', 'create', 'update', 'delete'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $query = Instance::find();
        $query->allowed($this->getUserId());

        return new ActiveDataProvider([
            'query'  => $query,
            'filter' => ['type']
        ]);
    }

    /**
     * @param $id
     * @return \app\components\models\ActiveRecord
     */
    public function actionView($id)
    {
        return $this->getModel($id);
    }

    /**
     * @return Instance
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {

        $instance = new Instance();
        $instance->load($this->getRequestParams());
        $instance->userId = $this->getUserId();

        if ($instance->save()) {
            $this->setResponseStatusCode(201);
        } elseif (!$instance->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $instance;
    }

    /**
     * @param $id
     * @return \app\components\models\ActiveRecord
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        $instance = $this->getModel($id);
        $instance->load($this->getRequestParams());

        if (!$instance->save() && !$instance->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $instance;
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        if (!$this->getModel($id)->delete()) {
            throw new ServerErrorHttpException('Failed to delete the comment for unknown reason.');
        }

        $this->setResponseStatusCode(204);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionHealth($id)
    {
        try {
            $response = $this->getEsClient($id)->cat()->health(['format' => 'json'])[0];
        } catch (ElasticsearchException $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionPing($id)
    {
        $instance = $this->getModel($id);

        $output = shell_exec('ping -c1 ' . $instance['host']);
        $status = 'fail';

        if ($output !== null) {
            preg_match('/time=[0-9\.]+ ms/', $output, $status);
            $status = $status[0];
        }

        return ['status' => $status];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionSsh($id)
    {
        $instance = $this->getModel($id);

        try {
            $client = new SshService(
                $instance->host,
                $instance->params['sshUsername'],
                $instance->params['sshPassword'],
                $instance->params['sshPort']
            );

            $client->authenticate();
            $status = 'success';
        } catch (ErrorException $e) {
            $status = 'fail';
        }

        return ['status' => $status];
    }
}