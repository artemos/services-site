import {BrowserModule}        from '@angular/platform-browser';
import {NgModule}             from '@angular/core';
import {FormsModule}          from '@angular/forms';
import {HttpModule}           from '@angular/http';
import {Routes, RouterModule} from '@angular/router';
import {NgbModule}            from '@ng-bootstrap/ng-bootstrap';
import {AppComponent}         from './app.component';

const routes: Routes = [
    {
        path: '',
        loadChildren: './modules/landing/landing.module'
    },
    {
        path: 'login',
        loadChildren: './modules/login/login.module'
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
