import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {ApiService} from "../../core/services/api.service";
import {Observable} from "rxjs/Observable";


@Injectable()
export class AuthService extends ApiService{
    constructor(protected http: Http) {
        super(http, '/api/auth');
    }

    login(login, password): Observable <any> {
        let body = {
            login    : login,
            password : password
        };

        return this.http
            .post(this.url('/login'), body, this.getOptions(false))
            .map(response => response.json());
    }

    session(): Observable <any> {
        return this.http
            .get(this.url('/session'), this.getOptions())
            .map(response => response.json());
    }

    getUserId() {
        let user = localStorage.getItem('user');

        if (user == null) {
            return null;
        }

        return JSON.parse(user).id;
    }
}