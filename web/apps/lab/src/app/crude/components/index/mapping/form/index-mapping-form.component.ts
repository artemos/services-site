import {Component, OnInit} from '@angular/core';
import {ActivatedRoute}    from "@angular/router";
import {IndexService}      from "../../../../services/index.service";
import {MappingService}    from "../../../../services/mapping.service";
import {HttpErrorService}  from "../../../../../core/services/http-error.service";
import {CollectionService} from "../../../../services/collection.service";

@Component({
    templateUrl : './index-mapping-form.component.html'
})
export class IndexMappingFormComponent implements OnInit {
    instanceId : number;
    indexName  : string;
    indexType  : string;
    mapping    : any;
    done       : boolean;

    crumbs : any = {
        title: 'Collection index mapping',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'update mapping'}
        ]
    };

    constructor(private route: ActivatedRoute,
                private httpErrorService: HttpErrorService,
                private mappingService: MappingService,
                private indexService: IndexService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.instanceId = params.id;
            this.indexName  = params.name;
            this.indexType  = params.name;

            this.indexService.fetchMapping(this.indexName, this.indexType, this.instanceId).subscribe(
                response => this.mapping = this.mappingService.mappingToArray(response),
                error    => this.httpErrorService.handle(error, this)
            );
        });
    }

    addField() {
        this.mapping.push({});
    }

    submit() {
        // console.log(
        //     'mapping',
        //     this.mapping,
        //     this.mappingService.mappingFromArray(this.mapping)
        // );

        let data = {
            instanceId : this.instanceId,
            indexName  : this.indexName,
            indexType  : this.indexType,
            mapping    : this.mappingService.mappingFromArray(this.mapping)
        };

        this.done = false;

        this.indexService.updateMapping(data).subscribe(
            response => this.done = true,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}