<?php

namespace app\models;

use app\components\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "users_configs".
 *
 * @property int $id
 * @property int $userId
 * @property string $key
 * @property string $value
 *
 * @property User $user
 */
class Config extends ActiveRecord
{
    /**
     * @var array
     */
    public $jsonbAttributes = ['value'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['userId'], 'integer'],
            [['key'], 'string'],
            ['userId', 'unique', 'targetAttribute' => ['userId', 'key'], 'when' => function($config) {
                return !empty($config->userId);
            }],
            ['key', 'unique', 'when' => function($config) {
                return empty($config->userId);
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'     => 'ID',
            'userId' => 'User ID',
            'key'    => 'Key',
            'value'  => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $config = parent::toArray($fields, $expand, $recursive);
        $config['value'] = Json::decode($config['value']);

        return $config;
    }
}
