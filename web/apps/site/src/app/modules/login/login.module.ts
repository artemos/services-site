import {NgModule}             from '@angular/core';
import {CommonModule}         from '@angular/common';
import {FormsModule}          from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent}       from "./components/list/Login.component";

const routes: Routes = [
    {
        path: '',
        component: LoginComponent
    }
];

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        LoginComponent
    ],
    providers: [
    ],
})
export default class LoginModule {
}