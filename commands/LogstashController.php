<?php

namespace app\commands;

use app\components\services\ElasticSiteHtmlParser;
use yii\console\Controller;
use DOMDocument;
use DOMXPath;
use yii\helpers\Json;

/**
 * Class LogstashController
 * @package app\commands
 */
class LogstashController extends Controller
{
    public function actionGeneratePluginsConfig()
    {
        $pages = [
            'input'  => 'https://www.elastic.co/guide/en/logstash/current/input-plugins.html',
            'output' => 'https://www.elastic.co/guide/en/logstash/current/output-plugins.html'
        ];

        $config = ['input' => [], 'output' => []];

        foreach ($pages as $type => $url) {
            $plugins = ElasticSiteHtmlParser::parsePage($url)['tables'][0];

            foreach ($plugins as $i => $data) {
                $data = [
                    'name'   => $data['Plugin']['text'],
                    'desc'   => $data['Description'],
                    'href'   => "https://www.elastic.co/guide/en/logstash/current/{$data['Plugin']['href']}",
                    'repo'   => $data['Github repository']['href'],
                    'params' => ['main' => [], 'common' => []]
                ];

                $plugin   = ElasticSiteHtmlParser::parsePage($data['href']);
                $tables   = $plugin['tables'];
                $paramMap = [0 => 'main', 1 => 'common'];

                if (count($tables) > 2) {
                    $tables = array_slice($tables, -2);
                }

                foreach ($tables as $t => $table) {
                    foreach ($table as $row) {
                        if (!isset($row['Setting'])) {
                            dd($row);
                        }

                        $data['params'][$paramMap[$t]][] = [
                            'name'    => $row['Setting']['text'],
                            'type'    => is_array($row['Input type']) ? $row['Input type']['text'] : $row['Input type'],
                            'require' => $row['Required'] === 'Yes',
                            'default' => $row['Default'] ?? null,
                            'desc'    => $row['Description'] ?? null
                        ];
                    }
                }

                $config[$type][] = $data;
            }

            file_put_contents(ROOT_DIR . '/runtime/logstash.plugins.json', Json::encode($config));
        }
    }
}
