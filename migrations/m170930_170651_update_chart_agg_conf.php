<?php

use yii\db\Migration;

/**
 * Class m170930_170651_update_chart_agg_conf
 */
class m170930_170651_update_chart_agg_conf extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $configs = Yii::$app->db->createCommand("SELECT * FROM configs")->queryAll();

        foreach ($configs as $config) {
            $value = \yii\helpers\Json::decode($config['value']);

            if (isset($value['aggregation'])) {
                foreach ($value['aggregation']['params'] as $pname => $pvalue) {
                    $value['aggregation'][$pname] = $pvalue;
                }

                unset($value['aggregation']['params']);

                if (isset($value['aggregation']['title'])) {
                    unset($value['aggregation']['title']);
                }

                Yii::$app->db->createCommand()->update('configs', ['value' => \yii\helpers\Json::encode($value)], ['id' => $config['id']])->execute();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m170930_170651_update_chart_agg_conf cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170930_170651_update_chart_agg_conf cannot be reverted.\n";

        return false;
    }
    */
}
