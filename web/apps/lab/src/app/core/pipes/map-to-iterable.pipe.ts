import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'mapToIterable'
})
export class MapToIterablePipe implements PipeTransform {
    transform(dict) {
        let a = [];
        for (let key in dict) {
            if (dict.hasOwnProperty(key)) {
                a.push({key: key, value: dict[key]});
            }
        }

        a[a.length - 1]['last'] = true;

        return a;
    }
}
