import {Component, OnInit, ViewChild} from '@angular/core';
import {ArrayService} from "../../../../core/services/array.service";
import {FilterService} from "../../../services/filter.service";
import {FieldService} from "../../../services/field.service";

@Component({
    selector    : 'filter-bar',
    templateUrl : './filter-bar.component.html',
    styleUrls   : ['./filter-bar.component.css']
})
export class FilterBarComponent implements OnInit {
    @ViewChild('filterFormComponent') filterFormComponent;

    filter     : any;
    fields     : any;
    operLabels : any;
    valueMap   : any = {};

    constructor(
        private arrayService: ArrayService,
        private fieldService: FieldService,
        private filterService: FilterService) {
    }

    ngOnInit() {
        this.operLabels = this.filterService.operLabels;

        this.filterService.form.subscribe(
            data => this.filter = data
        );

        this.fieldService.collectionFields.subscribe(
            data => {
                this.fields = data;

                for (let field of this.fields) {
                    if (field.valueMap) {
                        this.valueMap[field.name] = field.valueMap;
                    }
                }
            }
        );
    }

    deleteFilterParam(index) {
        this.arrayService.deleteByIndex(this.filter, index);
        this.filterService.data.next(this.filter);
    }

    openFilterFormModal() {
        this.filterFormComponent.openModal();
    }

    getFieldLabel(name) {
        return this.fieldService.getFieldLabel(name ,this.fields);
    }
}
