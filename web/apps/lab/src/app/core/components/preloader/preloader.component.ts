import {Component, Input} from '@angular/core';

@Component({
    selector    : 'preloader',
    templateUrl : './preloader.component.html'
})
export class PreloaderComponent {
    @Input() visible: boolean;
}
