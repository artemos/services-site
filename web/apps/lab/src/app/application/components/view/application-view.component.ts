import {Component, OnInit}  from '@angular/core';
import {ActivatedRoute}     from "@angular/router";
import {ApplicationService} from "../../services/application.service";
import {HttpErrorService}   from "../../../core/services/http-error.service";

@Component({
    templateUrl: './application-view.component.html'
})
export class ApplicationViewComponent implements OnInit {
    crumbs: any = {
        title: 'View form',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All applications', href: '/applications'},
            {title: 'View form'}
        ]
    };

    application: any;

    constructor(private route: ActivatedRoute,
                private httpErrorService: HttpErrorService,
                private applicationService: ApplicationService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.fetchApplication(params.id);
        });
    }

    fetchApplication(id) {
        this.applicationService.fetchById(id).subscribe(
            response => this.application = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    initCrumbs() {
        this.crumbs = {};
    }
}