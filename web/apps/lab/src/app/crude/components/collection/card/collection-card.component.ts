import {Component, Input}  from '@angular/core';
import {Router}            from "@angular/router";
import {CollectionService} from "../../../services/collection.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    selector    : 'collection-card',
    templateUrl : './collection-card.component.html'
})
export class CollectionCardComponent {
    @Input() collection: any;
    @Input() detail: boolean;

    constructor(
        private router: Router,
        private collectionService: CollectionService,
        private httpErrorService: HttpErrorService) {
    }

    deleteCollection() {
        if (!confirm('Delete collection "' + this.collection.name + '" ?')) {
            return;
        }

        this.collectionService.delete(this.collection.id).subscribe(
            response => this.router.navigate(['collections/new']),
            error    => this.httpErrorService.handle(error, this)
        );
    }
}