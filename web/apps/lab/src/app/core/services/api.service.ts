import {Http}                    from '@angular/http';
import {Observable}              from 'rxjs/Observable';
import {Headers, RequestOptions} from '@angular/http';

export class ApiService {
    resourceUrl: string;

    constructor(protected http: Http, resourceUrl) {
        const baseUrl = location.hostname == 'localhost' ? 'http://services-site' : '';
        this.resourceUrl = baseUrl + resourceUrl;
    }

    getFetchAllUrl(params?): string {
        let url = this.resourceUrl;

        if (params != null) {
            url = this.queryUrl(url, params);
        }

        return url;
    }

    fetchAll(params?): Observable <any> {
        for (let param in params) {
            if (params.hasOwnProperty(param)) {
                params[param] = encodeURIComponent(params[param]);
            }
        }

        return this.http
            .get(this.getFetchAllUrl(params), this.getOptions())
            .map(response => response.json());
    }

    fetchById(id, params?): Observable <any> {
        let url = this.resourceUrl + '/' + id;

        if (params != null) {
            url = this.queryUrl(url, params);
        }

        return this.http
            .get(url, this.getOptions())
            .map(response => response.json());
    }

    save(data): Observable <any> {
        if (data.id == null) {
            return this.create(data);
        } else {
            return this.update(data);
        }
    }

    create(data): Observable <any> {
        return this.http
            .post(this.resourceUrl, data, this.getOptions())
            .map(response => response.json())
    }

    update(data): Observable <any> {
        return this.http
            .put(this.resourceUrl + '/' + data.id, data, this.getOptions())
            .map(response => response.json())
    }

    delete(id, params?): Observable <any> {
        let url = this.resourceUrl +  '/' + id;

        if (params != null) {
            url = this.queryUrl(url, params);
        }

        return this.http.delete(url, this.getOptions()).map(response => response.json())
    }

    getOptions(auth = true) {
        let headers = new Headers();

        headers.append("Content-Type", "application/json; charset=UTF-8");

        if (auth) {
            headers.append("Authorization", "Bearer " + localStorage.getItem('user-auth-key'));
        }

        return new RequestOptions({ headers: headers, withCredentials: true });
    }

    url(part) {
        return this.resourceUrl + part;
    }

    queryUrl(url, params) {
        let query = [];

        for (let param in params) {
            if (params.hasOwnProperty(param)) {
                query.push(param + '=' + params[param]);
            }
        }

        return url +'?' + query.join('&');
    }
}