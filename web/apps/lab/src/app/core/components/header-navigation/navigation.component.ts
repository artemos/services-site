import {Component, Input} from '@angular/core';

@Component({
  selector: 'ma-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent {
	@Input() layout;
    @Input() user;
}
