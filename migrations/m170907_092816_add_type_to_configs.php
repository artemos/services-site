<?php

use yii\db\Migration;

class m170907_092816_add_type_to_configs extends Migration
{
    public function safeUp()
    {
        $this->addColumn('configs', 'type', 'text');
        $this->execute("UPDATE configs SET type = 'component_state'");
        $this->execute("ALTER TABLE configs ALTER COLUMN type SET NOT NULL");
    }


    public function safeDown()
    {
        $this->dropColumn('configs', 'type');
    }
}
