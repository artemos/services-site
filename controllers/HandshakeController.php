<?php

namespace app\controllers;

use app\components\enum\InstanceType;
use app\components\rest\Controller;

/**
 * Class HandshakeController
 * @package app\controllers
 */
class HandshakeController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        return [
            'enums' => [
                'instanceType' => [
                    'map'    => InstanceType::map(),
                    'values' => InstanceType::values()
                ]
            ]
        ];
    }
}