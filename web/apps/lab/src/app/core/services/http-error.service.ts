import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';

declare var swal: any;

@Injectable()
export class HttpErrorService {
    readonly STATUS_UNPROCESSABLE_ENTITY = 422;
    readonly STATUS_UN_AUTHORIZED        = 401;

    handle(error, context?, callback?) {
        switch (error.status) {
            case this.STATUS_UNPROCESSABLE_ENTITY:
                if (context) {
                    context.errors = JSON.parse(error._body).errors;
                }
                break;

            case this.STATUS_UN_AUTHORIZED:
                /**
                 * @TODO: fix double redirect
                 */
                location.href = '/login';
                break;

            case 0:
                console.log('http error', 'Server error, please try later.');
                break;

            default:
                let message = JSON.parse(error._body).message;
                
                if (callback != null) {
                    callback(message);
                } else {
                    swal('Error', message, 'error');
                }
        }
    }
}