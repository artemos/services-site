import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router}   from "@angular/router";
import {ItemService} from "../../../services/item.service";
import {CollectionService} from "../../../services/collection.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    templateUrl : './item-form.component.html'
})
export class ItemFormComponent implements OnInit {
    item         : any;
    collection   : any;
    collectionId : number;
    errors       : any;
    crumbs  : any;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private itemService: ItemService,
                private collectionService: CollectionService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.collectionId = params.collectionId;

            this.fetchCollection();
            this.initCrumbs(params.id);
            this.initItem(params.id);
        });

    }

    fetchItem(id) {
        this.itemService.fetchById(id, {collectonId: this.collectionId}).subscribe(
            response => this.item = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    saveItem() {
        let data = Object.assign({collectionId: this.collectionId}, this.item);

        this.itemService.create(data).subscribe(
            response => this.router.navigate(['items/' + this.collectionId  + '/' + response.id]),
            error    => this.httpErrorService.handle(error, this)
        );
    }

    onArrayElementValueChanged(event, field) {
        this.item[field.name] = event.value;
    }

    fetchCollection() {
        this.collectionService.fetchById(this.collectionId).subscribe(
            response => this.collection = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    fieldHasError(field) {
        if (this.errors == null) {
            return false;
        }

        for (let error of this.errors) {
            if (error.field === field.name) {
                return true;
            }
        }

        return false;
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs = {
                title: 'New item',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All items', href: '/collections/' + this.collectionId + '/items'},
                    {title: 'New item'}
                ]
            }
        } else {
            this.crumbs = {
                title: 'Update item',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All items', href: '/collections/' + this.collectionId + '/items/' + this.collectionId},
                    {title: 'Update item'}
                ]
            };
        }
    }

    private initItem(id) {
        if (id == null) {
            this.item = {};
        } else {
            this.fetchItem(id);
        }
    }
}