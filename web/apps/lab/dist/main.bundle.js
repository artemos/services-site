webpackJsonp([0],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-progress [color]=\"'white'\" [showSpinner]=\"false\"></ng-progress>\n\n<!-- ============================================================== -->\n<!-- Main wrapper - style you can find in pages.scss -->\n<!-- ============================================================== -->\n<div id=\"main-wrapper\">\n  <!-- ============================================================== -->\n    <!-- Topbar header - style you can find in pages.scss -->\n    <!-- ============================================================== -->\n    <ma-navigation [layout]=\"layout\" [user]=\"user\"></ma-navigation>\n    <!-- ============================================================== -->\n    <!-- Left Sidebar - style you can find in sidebar.scss  -->\n    <!-- ============================================================== -->\n    <sidebar *ngIf=\"layout == 'base' && user != null && loaded\" [user]=\"user\"></sidebar>\n    <!-- ============================================================== -->\n    <!-- End Left Sidebar - style you can find in sidebar.scss  -->\n    <!-- ============================================================== -->\n    <!-- ============================================================== -->\n    <!-- Page wrapper  -->\n    <!-- ============================================================== -->\n    <div *ngIf=\"layout == 'base'\" class=\"page-wrapper\">\n        <!-- ============================================================== -->\n        <!-- Container fluid  -->\n        <!-- ============================================================== -->\n        <div class=\"container-fluid\">\n            <!--<ma-mybar></ma-mybar>-->\n            <!--<breadcrumb></breadcrumb>-->\n          <!-- ============================================================== -->\n            <!-- Start Page Content -->\n            <!-- ============================================================== -->\n            <router-outlet *ngIf=\"loaded\"></router-outlet>\n            <!-- ============================================================== -->\n            <!-- End Start Page Content -->\n            <!-- ============================================================== -->\n            <!-- ============================================================== -->\n            <!-- Right sidebar -->\n            <!-- ============================================================== -->\n            <ma-rightsidebar></ma-rightsidebar>\n            <!-- ============================================================== -->\n            <!-- End Right sidebar -->\n            <!-- ============================================================== -->\n        </div>\n        <!-- ============================================================== -->\n        <!-- End Container fluid  -->\n        <!-- ============================================================== -->\n        <!-- ============================================================== -->\n        <!-- footer -->\n        <!-- ============================================================== -->\n        <footer class=\"footer\">\n            © 2017 Material Admin by wrappixel.com\n        </footer>\n        <!-- ============================================================== -->\n        <!-- End footer -->\n        <!-- ============================================================== -->\n    </div>\n    <!-- ============================================================== -->\n    <!-- End Page wrapper  -->\n    <!-- ============================================================== -->\n\n    <div *ngIf=\"layout == 'auth'\" class=\"auth-wrapper\" style=\"padding-top: 140px\">\n        <router-outlet></router-outlet>\n\n        <footer class=\"footer\" style=\"left: 0;\">\n            © 2017 Material Admin by wrappixel.com\n        </footer>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_services_auth_service__ = __webpack_require__("../../../../../src/app/user/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(configService, authService, httpErrorService) {
        this.configService = configService;
        this.authService = authService;
        this.httpErrorService = httpErrorService;
        this.layout = 'base';
        if (location.pathname == '/login') {
            this.layout = 'auth';
        }
        else {
            var keyName = 'user-auth-key';
            var authKey = localStorage.getItem(keyName);
            localStorage.clear();
            if (authKey != null) {
                localStorage.setItem(keyName, authKey);
                authKey = null;
            }
            this.loadUserSession();
            this.loadConfigs();
        }
    }
    AppComponent.prototype.loadUserSession = function () {
        var _this = this;
        this.authService.session().subscribe(function (response) {
            _this.configService.set('user', response);
            _this.user = response;
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    AppComponent.prototype.loadConfigs = function () {
        var _this = this;
        this.configService.fetchAll().subscribe(function (response) {
            for (var _i = 0, response_1 = response; _i < response_1.length; _i++) {
                var config = response_1[_i];
                _this.configService.set(config.key, config.value);
            }
            _this.loaded = true;
        }, function (error) {
            _this.httpErrorService.handle(error, _this);
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__["a" /* ConfigService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__user_services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__user_services_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], AppComponent);

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_sortablejs__ = __webpack_require__("../../../../angular-sortablejs/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_sortablejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular_sortablejs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_progressbar__ = __webpack_require__("../../../../ngx-progressbar/modules/ngx-progressbar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_select2__ = __webpack_require__("../../../../ng2-select2/ng2-select2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_select2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_select2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_moment__ = __webpack_require__("../../../../angular2-moment/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angular2_infinite_scroll__ = __webpack_require__("../../../../angular2-infinite-scroll/angular2-infinite-scroll.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angular2_infinite_scroll___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_angular2_infinite_scroll__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__core_components_header_navigation_navigation_component__ = __webpack_require__("../../../../../src/app/core/components/header-navigation/navigation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__core_components_sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/core/components/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_components_right_sidebar_rightsidebar_component__ = __webpack_require__("../../../../../src/app/core/components/right-sidebar/rightsidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__user_components_navigation_user_navigation_component__ = __webpack_require__("../../../../../src/app/user/components/navigation/user-navigation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__test_components_test_test_component__ = __webpack_require__("../../../../../src/app/test/components/test/test.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__core_components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/core/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__user_components_form_login_login_form_component__ = __webpack_require__("../../../../../src/app/user/components/form/login/login-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__instance_components_view_instance_view_component__ = __webpack_require__("../../../../../src/app/instance/components/view/instance-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__instance_components_card_instance_card_component__ = __webpack_require__("../../../../../src/app/instance/components/card/instance-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__instance_components_form_instance_form_component__ = __webpack_require__("../../../../../src/app/instance/components/form/instance-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__instance_services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__core_services_handshake_service__ = __webpack_require__("../../../../../src/app/core/services/handshake.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pipeline_services_pipeline_service__ = __webpack_require__("../../../../../src/app/pipeline/services/pipeline.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pipeline_services_logstash_plugin_service__ = __webpack_require__("../../../../../src/app/pipeline/services/logstash-plugin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pipeline_services_logstash_config_service__ = __webpack_require__("../../../../../src/app/pipeline/services/logstash-config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__application_services_application_service__ = __webpack_require__("../../../../../src/app/application/services/application.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__user_services_auth_service__ = __webpack_require__("../../../../../src/app/user/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__user_services_user_service__ = __webpack_require__("../../../../../src/app/user/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__core_services_url_service__ = __webpack_require__("../../../../../src/app/core/services/url.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__core_services_state_service__ = __webpack_require__("../../../../../src/app/core/services/state.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__crude_services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__crude_services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__crude_services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__crude_services_aggregation_service__ = __webpack_require__("../../../../../src/app/crude/services/aggregation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__crude_services_chart_service__ = __webpack_require__("../../../../../src/app/crude/services/chart.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__crude_services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__instance_components_list_instance_list_component__ = __webpack_require__("../../../../../src/app/instance/components/list/instance-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__instance_components_health_instance_health_component__ = __webpack_require__("../../../../../src/app/instance/components/health/instance-health.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__instance_components_status_instance_status_component__ = __webpack_require__("../../../../../src/app/instance/components/status/instance-status.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pipeline_components_list_pipeline_list_component__ = __webpack_require__("../../../../../src/app/pipeline/components/list/pipeline-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pipeline_components_form_pipeline_form_component__ = __webpack_require__("../../../../../src/app/pipeline/components/form/pipeline-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pipeline_components_logstash_config_status_logstash_config_status_component__ = __webpack_require__("../../../../../src/app/pipeline/components/logstash-config-status/logstash-config-status.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__application_components_list_application_list_component__ = __webpack_require__("../../../../../src/app/application/components/list/application-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__application_components_form_application_form_component__ = __webpack_require__("../../../../../src/app/application/components/form/application-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__application_components_view_application_view_component__ = __webpack_require__("../../../../../src/app/application/components/view/application-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__application_components_card_application_card_component__ = __webpack_require__("../../../../../src/app/application/components/card/application-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__application_components_form_select_application_select_component__ = __webpack_require__("../../../../../src/app/application/components/form/select/application-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__core_components_preloader_preloader_component__ = __webpack_require__("../../../../../src/app/core/components/preloader/preloader.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__core_components_status_label_status_label_component__ = __webpack_require__("../../../../../src/app/core/components/status-label/status-label.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__core_components_error_message_error_message_component__ = __webpack_require__("../../../../../src/app/core/components/error-message/error-message.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__core_components_config_form_config_form_component__ = __webpack_require__("../../../../../src/app/core/components/config/form/config-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__core_components_breadcrumb_breadcrumb_component__ = __webpack_require__("../../../../../src/app/core/components/breadcrumb/breadcrumb.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__core_components_config_list_config_list_component__ = __webpack_require__("../../../../../src/app/core/components/config/list/config-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__crude_components_collection_list_collection_list_component__ = __webpack_require__("../../../../../src/app/crude/components/collection/list/collection-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__crude_components_collection_form_collection_form_component__ = __webpack_require__("../../../../../src/app/crude/components/collection/form/collection-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__crude_components_collection_view_collection_view_component__ = __webpack_require__("../../../../../src/app/crude/components/collection/view/collection-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__crude_components_collection_card_collection_card_component__ = __webpack_require__("../../../../../src/app/crude/components/collection/card/collection-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__crude_components_item_field_list_field_list_component__ = __webpack_require__("../../../../../src/app/crude/components/item/field/list/field-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__crude_components_item_field_form_field_form_component__ = __webpack_require__("../../../../../src/app/crude/components/item/field/form/field-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__crude_components_item_field_validator_label_validator_label_component__ = __webpack_require__("../../../../../src/app/crude/components/item/field/validator-label/validator-label.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__crude_components_item_list_item_list_component__ = __webpack_require__("../../../../../src/app/crude/components/item/list/item-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__crude_components_item_form_item_form_component__ = __webpack_require__("../../../../../src/app/crude/components/item/form/item-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__crude_components_item_view_item_view_component__ = __webpack_require__("../../../../../src/app/crude/components/item/view/item-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__crude_components_item_card_item_card_component__ = __webpack_require__("../../../../../src/app/crude/components/item/card/item-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__crude_components_collection_dashboard_collection_dashboard_component__ = __webpack_require__("../../../../../src/app/crude/components/collection/dashboard/collection-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__crude_components_filter_form_filter_form_component__ = __webpack_require__("../../../../../src/app/crude/components/filter/form/filter-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__crude_components_filter_bar_filter_bar_component__ = __webpack_require__("../../../../../src/app/crude/components/filter/bar/filter-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__crude_components_sorting_form_sorting_form_component__ = __webpack_require__("../../../../../src/app/crude/components/sorting/form/sorting-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__crude_components_sorting_bar_sorting_bar_component__ = __webpack_require__("../../../../../src/app/crude/components/sorting/bar/sorting-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__core_pipes_map_to_iterable_pipe__ = __webpack_require__("../../../../../src/app/core/pipes/map-to-iterable.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__core_pipes_not_hidden_pipe__ = __webpack_require__("../../../../../src/app/core/pipes/not-hidden.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__crude_components_chart_form_chart_form_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/form/chart-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__crude_components_item_field_form_select_field_select_component__ = __webpack_require__("../../../../../src/app/crude/components/item/field/form/select/field-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__crude_components_aggregation_list_aggregation_list_component__ = __webpack_require__("../../../../../src/app/crude/components/aggregation/list/aggregation-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__crude_components_aggregation_form_aggregation_form_component__ = __webpack_require__("../../../../../src/app/crude/components/aggregation/form/aggregation-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__crude_components_aggregation_form_select_aggregation_select_component__ = __webpack_require__("../../../../../src/app/crude/components/aggregation/form/select/aggregation-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__crude_components_form_aggregation_type_select_aggregation_type_select_component__ = __webpack_require__("../../../../../src/app/crude/components/form/aggregation-type-select/aggregation-type-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83__crude_components_widget_frame_widget_frame_component__ = __webpack_require__("../../../../../src/app/crude/components/widget/frame/widget-frame.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__crude_components_widget_view_widget_view_component__ = __webpack_require__("../../../../../src/app/crude/components/widget/view/widget-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__crude_components_widget_form_widget_form_component__ = __webpack_require__("../../../../../src/app/crude/components/widget/form/widget-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__crude_components_widget_list_widget_list_component__ = __webpack_require__("../../../../../src/app/crude/components/widget/list/widget-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__user_components_profile_profile_component__ = __webpack_require__("../../../../../src/app/user/components/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_88__user_components_form_select_user_select_component__ = __webpack_require__("../../../../../src/app/user/components/form/select/user-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_89__user_components_form_access_select_access_select_component__ = __webpack_require__("../../../../../src/app/user/components/form/access-select/access-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_90__crude_components_collection_form_select_collection_select_component__ = __webpack_require__("../../../../../src/app/crude/components/collection/form/select/collection-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_91__instance_components_form_select_instance_select_component__ = __webpack_require__("../../../../../src/app/instance/components/form/select/instance-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_92__crude_components_form_interval_select_interval_select_component__ = __webpack_require__("../../../../../src/app/crude/components/form/interval-select/interval-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93__crude_components_form_date_format_select_date_format_select_component__ = __webpack_require__("../../../../../src/app/crude/components/form/date-format-select/date-format-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_94__crude_components_chart_line_line_chart_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/line/line-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_95__crude_components_chart_pie_pie_chart_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/pie/pie-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_96__crude_components_chart_bar_bar_chart_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/bar/bar-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_97__crude_components_item_list_header_item_list_header_component__ = __webpack_require__("../../../../../src/app/crude/components/item/list/header/item-list-header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_98__crude_components_item_list_settings_form_item_list_settings_form_component__ = __webpack_require__("../../../../../src/app/crude/components/item/list/settings-form/item-list-settings-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_99__crude_components_index_form_index_form_component__ = __webpack_require__("../../../../../src/app/crude/components/index/form/index-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_100__crude_components_index_mapping_view_index_mapping_view_component__ = __webpack_require__("../../../../../src/app/crude/components/index/mapping/view/index-mapping-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_101__crude_components_index_mapping_form_index_mapping_form_component__ = __webpack_require__("../../../../../src/app/crude/components/index/mapping/form/index-mapping-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_102__crude_directives_field_value_directive__ = __webpack_require__("../../../../../src/app/crude/directives/field-value.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_103__crude_components_item_field_form_type_select_field_type_select_component__ = __webpack_require__("../../../../../src/app/crude/components/item/field/form/type-select/field-type-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_104__crude_services_filter_service__ = __webpack_require__("../../../../../src/app/crude/services/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_105__crude_services_sorting_service__ = __webpack_require__("../../../../../src/app/crude/services/sorting.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_106__crude_services_mapping_service__ = __webpack_require__("../../../../../src/app/crude/services/mapping.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_107__crude_services_item_list_settings_service__ = __webpack_require__("../../../../../src/app/crude/services/item-list-settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_108_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_108_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_108_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













































































































__WEBPACK_IMPORTED_MODULE_108_moment__["locale"]('ru');
var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_18__core_components_dashboard_dashboard_component__["a" /* DashboardComponent */]
    },
    {
        path: 'test',
        component: __WEBPACK_IMPORTED_MODULE_17__test_components_test_test_component__["a" /* TestComponent */]
    },
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_19__user_components_form_login_login_form_component__["a" /* LoginFormComponent */]
    },
    {
        path: 'applications',
        component: __WEBPACK_IMPORTED_MODULE_48__application_components_list_application_list_component__["a" /* ApplicationListComponent */]
    },
    {
        path: 'applications/new',
        component: __WEBPACK_IMPORTED_MODULE_49__application_components_form_application_form_component__["a" /* ApplicationFormComponent */]
    },
    {
        path: 'applications/:id',
        component: __WEBPACK_IMPORTED_MODULE_50__application_components_view_application_view_component__["a" /* ApplicationViewComponent */]
    },
    {
        path: 'applications/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_49__application_components_form_application_form_component__["a" /* ApplicationFormComponent */]
    },
    {
        path: 'instances',
        component: __WEBPACK_IMPORTED_MODULE_42__instance_components_list_instance_list_component__["a" /* InstanceListComponent */]
    },
    {
        path: 'instances/new',
        component: __WEBPACK_IMPORTED_MODULE_22__instance_components_form_instance_form_component__["a" /* InstanceFormComponent */]
    },
    {
        path: 'instances/:id',
        component: __WEBPACK_IMPORTED_MODULE_20__instance_components_view_instance_view_component__["a" /* InstanceViewComponent */]
    },
    {
        path: 'instances/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_22__instance_components_form_instance_form_component__["a" /* InstanceFormComponent */]
    },
    {
        path: 'instances/:id/indices/:name/mapping',
        component: __WEBPACK_IMPORTED_MODULE_100__crude_components_index_mapping_view_index_mapping_view_component__["a" /* IndexMappingViewComponent */]
    },
    {
        path: 'instances/:id/indices/:name/mapping/update',
        component: __WEBPACK_IMPORTED_MODULE_101__crude_components_index_mapping_form_index_mapping_form_component__["a" /* IndexMappingFormComponent */]
    },
    {
        path: 'instances/:id/indices/new',
        component: __WEBPACK_IMPORTED_MODULE_99__crude_components_index_form_index_form_component__["a" /* IndexFormComponent */]
    },
    {
        path: 'configs',
        component: __WEBPACK_IMPORTED_MODULE_58__core_components_config_list_config_list_component__["a" /* ConfigListComponent */],
    },
    {
        path: 'configs/new',
        component: __WEBPACK_IMPORTED_MODULE_56__core_components_config_form_config_form_component__["a" /* ConfigFormComponent */],
    },
    {
        path: 'configs/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_56__core_components_config_form_config_form_component__["a" /* ConfigFormComponent */]
    },
    {
        path: 'collections',
        component: __WEBPACK_IMPORTED_MODULE_59__crude_components_collection_list_collection_list_component__["a" /* CollectionListComponent */]
    },
    {
        path: 'collections/new',
        component: __WEBPACK_IMPORTED_MODULE_60__crude_components_collection_form_collection_form_component__["a" /* CollectionFormComponent */]
    },
    {
        path: 'collections/:id',
        component: __WEBPACK_IMPORTED_MODULE_61__crude_components_collection_view_collection_view_component__["a" /* CollectionViewComponent */]
    },
    {
        path: 'collections/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_60__crude_components_collection_form_collection_form_component__["a" /* CollectionFormComponent */]
    },
    {
        path: 'collections/:collectionId/items',
        component: __WEBPACK_IMPORTED_MODULE_66__crude_components_item_list_item_list_component__["a" /* ItemListComponent */]
    },
    {
        path: 'collections/:collectionId/dashboard',
        component: __WEBPACK_IMPORTED_MODULE_70__crude_components_collection_dashboard_collection_dashboard_component__["a" /* CollectionDashboardComponent */]
    },
    {
        path: 'collections/:collectionId/items/new',
        component: __WEBPACK_IMPORTED_MODULE_67__crude_components_item_form_item_form_component__["a" /* ItemFormComponent */]
    },
    {
        path: 'collections/:collectionId/items/:id',
        component: __WEBPACK_IMPORTED_MODULE_68__crude_components_item_view_item_view_component__["a" /* ItemViewComponent */]
    },
    {
        path: 'collections/:collectionId/items/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_67__crude_components_item_form_item_form_component__["a" /* ItemFormComponent */]
    },
    {
        path: 'aggregations',
        component: __WEBPACK_IMPORTED_MODULE_79__crude_components_aggregation_list_aggregation_list_component__["a" /* AggregationListComponent */]
    },
    {
        path: 'aggregations/new',
        component: __WEBPACK_IMPORTED_MODULE_80__crude_components_aggregation_form_aggregation_form_component__["a" /* AggregationFormComponent */]
    },
    {
        path: 'aggregations/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_80__crude_components_aggregation_form_aggregation_form_component__["a" /* AggregationFormComponent */]
    },
    {
        path: 'widgets',
        component: __WEBPACK_IMPORTED_MODULE_86__crude_components_widget_list_widget_list_component__["a" /* WidgetListComponent */]
    },
    {
        path: 'widgets/new',
        component: __WEBPACK_IMPORTED_MODULE_85__crude_components_widget_form_widget_form_component__["a" /* WidgetFormComponent */]
    },
    {
        path: 'widgets/:id',
        component: __WEBPACK_IMPORTED_MODULE_84__crude_components_widget_view_widget_view_component__["a" /* WidgetViewComponent */]
    },
    {
        path: 'widgets/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_85__crude_components_widget_form_widget_form_component__["a" /* WidgetFormComponent */]
    },
    {
        path: 'pipelines',
        component: __WEBPACK_IMPORTED_MODULE_45__pipeline_components_list_pipeline_list_component__["a" /* PipelineListComponent */]
    },
    {
        path: 'pipelines/new',
        component: __WEBPACK_IMPORTED_MODULE_46__pipeline_components_form_pipeline_form_component__["a" /* PipelineFormComponent */]
    },
    {
        path: 'pipelines/update/:id',
        component: __WEBPACK_IMPORTED_MODULE_46__pipeline_components_form_pipeline_form_component__["a" /* PipelineFormComponent */]
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_102__crude_directives_field_value_directive__["a" /* FieldValueDirective */],
            __WEBPACK_IMPORTED_MODULE_89__user_components_form_access_select_access_select_component__["a" /* AccessSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_80__crude_components_aggregation_form_aggregation_form_component__["a" /* AggregationFormComponent */],
            __WEBPACK_IMPORTED_MODULE_79__crude_components_aggregation_list_aggregation_list_component__["a" /* AggregationListComponent */],
            __WEBPACK_IMPORTED_MODULE_81__crude_components_aggregation_form_select_aggregation_select_component__["a" /* AggregationSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_82__crude_components_form_aggregation_type_select_aggregation_type_select_component__["a" /* AggregationTypeSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_51__application_components_card_application_card_component__["a" /* ApplicationCardComponent */],
            __WEBPACK_IMPORTED_MODULE_49__application_components_form_application_form_component__["a" /* ApplicationFormComponent */],
            __WEBPACK_IMPORTED_MODULE_48__application_components_list_application_list_component__["a" /* ApplicationListComponent */],
            __WEBPACK_IMPORTED_MODULE_52__application_components_form_select_application_select_component__["a" /* ApplicationSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_50__application_components_view_application_view_component__["a" /* ApplicationViewComponent */],
            __WEBPACK_IMPORTED_MODULE_96__crude_components_chart_bar_bar_chart_component__["a" /* BarChartComponent */],
            __WEBPACK_IMPORTED_MODULE_57__core_components_breadcrumb_breadcrumb_component__["a" /* BreadcrumbComponent */],
            __WEBPACK_IMPORTED_MODULE_77__crude_components_chart_form_chart_form_component__["a" /* ChartFormComponent */],
            __WEBPACK_IMPORTED_MODULE_62__crude_components_collection_card_collection_card_component__["a" /* CollectionCardComponent */],
            __WEBPACK_IMPORTED_MODULE_70__crude_components_collection_dashboard_collection_dashboard_component__["a" /* CollectionDashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_64__crude_components_item_field_form_field_form_component__["a" /* CollectionFieldFormComponent */],
            __WEBPACK_IMPORTED_MODULE_63__crude_components_item_field_list_field_list_component__["a" /* CollectionFieldListComponent */],
            __WEBPACK_IMPORTED_MODULE_60__crude_components_collection_form_collection_form_component__["a" /* CollectionFormComponent */],
            __WEBPACK_IMPORTED_MODULE_59__crude_components_collection_list_collection_list_component__["a" /* CollectionListComponent */],
            __WEBPACK_IMPORTED_MODULE_90__crude_components_collection_form_select_collection_select_component__["a" /* CollectionSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_61__crude_components_collection_view_collection_view_component__["a" /* CollectionViewComponent */],
            __WEBPACK_IMPORTED_MODULE_56__core_components_config_form_config_form_component__["a" /* ConfigFormComponent */],
            __WEBPACK_IMPORTED_MODULE_58__core_components_config_list_config_list_component__["a" /* ConfigListComponent */],
            __WEBPACK_IMPORTED_MODULE_18__core_components_dashboard_dashboard_component__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_93__crude_components_form_date_format_select_date_format_select_component__["a" /* DateFormatSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_55__core_components_error_message_error_message_component__["a" /* ErrorMessageComponent */],
            __WEBPACK_IMPORTED_MODULE_78__crude_components_item_field_form_select_field_select_component__["a" /* FieldSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_103__crude_components_item_field_form_type_select_field_type_select_component__["a" /* FieldTypeSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_71__crude_components_filter_form_filter_form_component__["a" /* FilterFormComponent */],
            __WEBPACK_IMPORTED_MODULE_21__instance_components_card_instance_card_component__["a" /* InstanceCardComponent */],
            __WEBPACK_IMPORTED_MODULE_22__instance_components_form_instance_form_component__["a" /* InstanceFormComponent */],
            __WEBPACK_IMPORTED_MODULE_43__instance_components_health_instance_health_component__["a" /* InstanceHealthComponent */],
            __WEBPACK_IMPORTED_MODULE_42__instance_components_list_instance_list_component__["a" /* InstanceListComponent */],
            __WEBPACK_IMPORTED_MODULE_91__instance_components_form_select_instance_select_component__["a" /* InstanceSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_44__instance_components_status_instance_status_component__["a" /* InstanceStatusComponent */],
            __WEBPACK_IMPORTED_MODULE_20__instance_components_view_instance_view_component__["a" /* InstanceViewComponent */],
            __WEBPACK_IMPORTED_MODULE_100__crude_components_index_mapping_view_index_mapping_view_component__["a" /* IndexMappingViewComponent */],
            __WEBPACK_IMPORTED_MODULE_101__crude_components_index_mapping_form_index_mapping_form_component__["a" /* IndexMappingFormComponent */],
            __WEBPACK_IMPORTED_MODULE_92__crude_components_form_interval_select_interval_select_component__["a" /* IntervalSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_69__crude_components_item_card_item_card_component__["a" /* ItemCardComponent */],
            __WEBPACK_IMPORTED_MODULE_67__crude_components_item_form_item_form_component__["a" /* ItemFormComponent */],
            __WEBPACK_IMPORTED_MODULE_66__crude_components_item_list_item_list_component__["a" /* ItemListComponent */],
            __WEBPACK_IMPORTED_MODULE_97__crude_components_item_list_header_item_list_header_component__["a" /* ItemListHeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_98__crude_components_item_list_settings_form_item_list_settings_form_component__["a" /* ItemListSettingsFormComponent */],
            __WEBPACK_IMPORTED_MODULE_68__crude_components_item_view_item_view_component__["a" /* ItemViewComponent */],
            __WEBPACK_IMPORTED_MODULE_94__crude_components_chart_line_line_chart_component__["a" /* LineChartComponent */],
            __WEBPACK_IMPORTED_MODULE_19__user_components_form_login_login_form_component__["a" /* LoginFormComponent */],
            __WEBPACK_IMPORTED_MODULE_47__pipeline_components_logstash_config_status_logstash_config_status_component__["a" /* LogstashConfigStatusComponent */],
            __WEBPACK_IMPORTED_MODULE_75__core_pipes_map_to_iterable_pipe__["a" /* MapToIterablePipe */],
            __WEBPACK_IMPORTED_MODULE_12__core_components_header_navigation_navigation_component__["a" /* NavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_76__core_pipes_not_hidden_pipe__["a" /* NotHiddenPipe */],
            __WEBPACK_IMPORTED_MODULE_95__crude_components_chart_pie_pie_chart_component__["a" /* PieChartComponent */],
            __WEBPACK_IMPORTED_MODULE_46__pipeline_components_form_pipeline_form_component__["a" /* PipelineFormComponent */],
            __WEBPACK_IMPORTED_MODULE_45__pipeline_components_list_pipeline_list_component__["a" /* PipelineListComponent */],
            __WEBPACK_IMPORTED_MODULE_53__core_components_preloader_preloader_component__["a" /* PreloaderComponent */],
            __WEBPACK_IMPORTED_MODULE_87__user_components_profile_profile_component__["a" /* ProfileComponent */],
            __WEBPACK_IMPORTED_MODULE_14__core_components_right_sidebar_rightsidebar_component__["a" /* RightSidebarComponent */],
            __WEBPACK_IMPORTED_MODULE_13__core_components_sidebar_sidebar_component__["a" /* SidebarComponent */],
            __WEBPACK_IMPORTED_MODULE_73__crude_components_sorting_form_sorting_form_component__["a" /* SortingFormComponent */],
            __WEBPACK_IMPORTED_MODULE_74__crude_components_sorting_bar_sorting_bar_component__["a" /* SortingBarComponent */],
            __WEBPACK_IMPORTED_MODULE_54__core_components_status_label_status_label_component__["a" /* StatusLabelComponent */],
            __WEBPACK_IMPORTED_MODULE_17__test_components_test_test_component__["a" /* TestComponent */],
            __WEBPACK_IMPORTED_MODULE_16__user_components_navigation_user_navigation_component__["a" /* UserNavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_88__user_components_form_select_user_select_component__["a" /* UserSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_65__crude_components_item_field_validator_label_validator_label_component__["a" /* ValidatorLabelComponent */],
            __WEBPACK_IMPORTED_MODULE_85__crude_components_widget_form_widget_form_component__["a" /* WidgetFormComponent */],
            __WEBPACK_IMPORTED_MODULE_83__crude_components_widget_frame_widget_frame_component__["a" /* WidgetFrameComponent */],
            __WEBPACK_IMPORTED_MODULE_86__crude_components_widget_list_widget_list_component__["a" /* WidgetListComponent */],
            __WEBPACK_IMPORTED_MODULE_84__crude_components_widget_view_widget_view_component__["a" /* WidgetViewComponent */],
            __WEBPACK_IMPORTED_MODULE_72__crude_components_filter_bar_filter_bar_component__["a" /* FilterBarComponent */],
            __WEBPACK_IMPORTED_MODULE_99__crude_components_index_form_index_form_component__["a" /* IndexFormComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common__["a" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_9_ng2_select2__["Select2Module"],
            __WEBPACK_IMPORTED_MODULE_10_angular2_moment__["MomentModule"],
            __WEBPACK_IMPORTED_MODULE_6_angular_sortablejs__["SortablejsModule"],
            __WEBPACK_IMPORTED_MODULE_11_angular2_infinite_scroll__["InfiniteScrollModule"],
            __WEBPACK_IMPORTED_MODULE_7_ngx_progressbar__["a" /* NgProgressModule */],
            __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot(routes)
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_39__crude_services_aggregation_service__["a" /* AggregationService */],
            __WEBPACK_IMPORTED_MODULE_29__application_services_application_service__["a" /* ApplicationService */],
            __WEBPACK_IMPORTED_MODULE_33__core_services_array_service__["a" /* ArrayService */],
            __WEBPACK_IMPORTED_MODULE_30__user_services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_40__crude_services_chart_service__["a" /* ChartService */],
            __WEBPACK_IMPORTED_MODULE_38__crude_services_collection_service__["a" /* CollectionService */],
            __WEBPACK_IMPORTED_MODULE_32__core_services_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_41__crude_services_field_service__["a" /* FieldService */],
            __WEBPACK_IMPORTED_MODULE_24__core_services_handshake_service__["a" /* HandshakeService */],
            __WEBPACK_IMPORTED_MODULE_26__core_services_http_error_service__["a" /* HttpErrorService */],
            __WEBPACK_IMPORTED_MODULE_37__crude_services_index_service__["a" /* IndexService */],
            __WEBPACK_IMPORTED_MODULE_23__instance_services_instance_service__["a" /* InstanceService */],
            __WEBPACK_IMPORTED_MODULE_36__crude_services_item_service__["a" /* ItemService */],
            __WEBPACK_IMPORTED_MODULE_28__pipeline_services_logstash_config_service__["a" /* LogstashConfigService */],
            __WEBPACK_IMPORTED_MODULE_27__pipeline_services_logstash_plugin_service__["a" /* LogstashPluginService */],
            __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["b" /* NgbAlert */],
            __WEBPACK_IMPORTED_MODULE_25__pipeline_services_pipeline_service__["a" /* PipelineService */],
            __WEBPACK_IMPORTED_MODULE_35__core_services_state_service__["a" /* StateService */],
            __WEBPACK_IMPORTED_MODULE_34__core_services_url_service__["a" /* UrlService */],
            __WEBPACK_IMPORTED_MODULE_31__user_services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_104__crude_services_filter_service__["a" /* FilterService */],
            __WEBPACK_IMPORTED_MODULE_105__crude_services_sorting_service__["a" /* SortingService */],
            __WEBPACK_IMPORTED_MODULE_106__crude_services_mapping_service__["a" /* MappingService */],
            __WEBPACK_IMPORTED_MODULE_107__crude_services_item_list_settings_service__["a" /* ItemListSettingsService */],
            {
                provide: __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* BrowserXhr */],
                useClass: __WEBPACK_IMPORTED_MODULE_7_ngx_progressbar__["b" /* NgProgressBrowserXhr */]
            }
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/application/components/card/application-card.component.html":
/***/ (function(module, exports) {

module.exports = "<preloader [visible]=\"application == null\"></preloader>\n\n<div *ngIf=\"application != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">{{application.name}}</h3>\n                <h6 *ngIf=\"application.description != null\" class=\"card-subtitle\">{{application.description}}</h6>\n                \n                <ul class=\"list-inline font-14\">\n                    <li>Collections {{application.collections.length}}</li>\n                    <li>Instances {{application.instances.length}}</li>\n                </ul>\n                \n                <hr class=\"m-t-0 m-b-20\">\n    \n                <a [routerLink]=\"['/applications/' + application.id]\"  class=\"card-link btn btn-secondary btn-sm\">View</a>\n                <a [routerLink]=\"['/applications/update/' + application.id]\" class=\"btn btn-info card-link btn-sm\">Update</a>\n                \n                <button *ngIf=\"detail\" (click)=\"deleteApplication()\" class=\"btn btn-danger card-link btn-sm\">Delete</button>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/application/components/card/application-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_application_service__ = __webpack_require__("../../../../../src/app/application/services/application.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationCardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApplicationCardComponent = (function () {
    function ApplicationCardComponent(router, httpErrorService, applicationService) {
        this.router = router;
        this.httpErrorService = httpErrorService;
        this.applicationService = applicationService;
    }
    ApplicationCardComponent.prototype.deleteApplication = function () {
        var _this = this;
        if (!confirm('Delete form "' + this.application.name + '" ?')) {
            return;
        }
        this.applicationService.delete(this.application.id).subscribe(function (response) { return _this.router.navigateByUrl('/applications'); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return ApplicationCardComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ApplicationCardComponent.prototype, "application", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], ApplicationCardComponent.prototype, "detail", void 0);
ApplicationCardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'application-card',
        template: __webpack_require__("../../../../../src/app/application/components/card/application-card.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_application_service__["a" /* ApplicationService */]) === "function" && _c || Object])
], ApplicationCardComponent);

var _a, _b, _c;
//# sourceMappingURL=application-card.component.js.map

/***/ }),

/***/ "../../../../../src/app/application/components/form/application-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"form == null\"></preloader>\n\n<div *ngIf=\"form != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <div class=\"form-group\">\n                    <label>Name</label>\n                    <input [(ngModel)]=\"form.name\" name=\"form.name\" type=\"text\" class=\"form-control\"/>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Description</label>\n                    <textarea [(ngModel)]=\"form.description\" name=\"form.description\" class=\"form-control\"></textarea>\n                </div>\n                \n                <access-select [model]=\"application\"></access-select>\n                \n                <button (click)=\"submit()\" [disabled]=\"applicationService.loading\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">{{this.form.id != null ? 'Update' : 'Create'}}</button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/application/components/form/application-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_application_service__ = __webpack_require__("../../../../../src/app/application/services/application.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApplicationFormComponent = (function () {
    function ApplicationFormComponent(route, httpErrorService, applicationService, router) {
        this.route = route;
        this.httpErrorService = httpErrorService;
        this.applicationService = applicationService;
        this.router = router;
    }
    ApplicationFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.initCrumbs(params.id);
            if (params.id) {
                _this.fetchApplication(params.id);
            }
            else {
                _this.form = {};
            }
        });
    };
    ApplicationFormComponent.prototype.fetchApplication = function (id) {
        var _this = this;
        this.applicationService.fetchById(id).subscribe(function (response) { return _this.form = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ApplicationFormComponent.prototype.submit = function () {
        var _this = this;
        this.applicationService.save(this.form).subscribe(function (response) { return _this.router.navigate(['applications/' + response.id]); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ApplicationFormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New form',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All applications', href: '/applications' },
                    { title: 'New form' }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update form',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All applications', href: '/applications' },
                    { title: 'Update form' }
                ]
            };
        }
    };
    return ApplicationFormComponent;
}());
ApplicationFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/application/components/form/application-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_application_service__["a" /* ApplicationService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
], ApplicationFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=application-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/application/components/form/select/application-select.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"applications\" class=\"form-group\">\n    <label>Application</label>\n    <select [(ngModel)]=\"model[field]\" name=\"model.appId\" class=\"custom-select col-12\">\n        <option *ngFor=\"let application of applications\" [ngValue]=\"application.id\">{{application.name}}</option>\n    </select>\n</div>\n\n    "

/***/ }),

/***/ "../../../../../src/app/application/components/form/select/application-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_application_service__ = __webpack_require__("../../../../../src/app/application/services/application.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApplicationSelectComponent = (function () {
    function ApplicationSelectComponent(applicationService, httpErrorService) {
        this.applicationService = applicationService;
        this.httpErrorService = httpErrorService;
        this.field = 'appId';
    }
    ApplicationSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.applicationService.fetchAll().subscribe(function (response) { return _this.applications = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return ApplicationSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ApplicationSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ApplicationSelectComponent.prototype, "field", void 0);
ApplicationSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'application-select',
        template: __webpack_require__("../../../../../src/app/application/components/form/select/application-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_application_service__["a" /* ApplicationService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], ApplicationSelectComponent);

var _a, _b;
//# sourceMappingURL=application-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/application/components/list/application-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"applications == null\"></preloader>\n\n<div *ngIf=\"applications != null && applications.length > 0\" class=\"row\">\n    <div *ngFor=\"let application of applications\" class=\"col-12\">\n        <application-card [application]=\"application\" [detail]=\"false\"></application-card>\n    </div>\n</div>\n\n<div *ngIf=\"applications != null && applications.length == 0\" class=\"col-12\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <div class=\"alert alert-info\">\n                <p>You haven't applications yet.</p>\n            </div>\n    \n            <p><a [routerLink]=\"['/applications/new']\" class=\"btn btn-success\">Create application</a></p>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/application/components/list/application-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_application_service__ = __webpack_require__("../../../../../src/app/application/services/application.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApplicationListComponent = (function () {
    function ApplicationListComponent(applicationService, httpErrorService) {
        this.applicationService = applicationService;
        this.httpErrorService = httpErrorService;
        this.crumbs = {
            title: 'All applications',
            buttons: [
                {
                    text: 'New application',
                    route: '/applications/new',
                    class: 'btn btn-primary'
                }
            ],
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All applications' }
            ]
        };
    }
    ApplicationListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.applicationService.fetchAll().subscribe(function (response) { return _this.applications = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return ApplicationListComponent;
}());
ApplicationListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/application/components/list/application-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_application_service__["a" /* ApplicationService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], ApplicationListComponent);

var _a, _b;
//# sourceMappingURL=application-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/application/components/view/application-view.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"application == null\"></preloader>\n\n<div *ngIf=\"application != null\" class=\"row\">\n    <div class=\"col-12\">\n        <application-card [application]=\"application\" [detail]=\"true\"></application-card>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/application/components/view/application-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_application_service__ = __webpack_require__("../../../../../src/app/application/services/application.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ApplicationViewComponent = (function () {
    function ApplicationViewComponent(route, httpErrorService, applicationService) {
        this.route = route;
        this.httpErrorService = httpErrorService;
        this.applicationService = applicationService;
        this.crumbs = {
            title: 'View form',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All applications', href: '/applications' },
                { title: 'View form' }
            ]
        };
    }
    ApplicationViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.fetchApplication(params.id);
        });
    };
    ApplicationViewComponent.prototype.fetchApplication = function (id) {
        var _this = this;
        this.applicationService.fetchById(id).subscribe(function (response) { return _this.application = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ApplicationViewComponent.prototype.initCrumbs = function () {
        this.crumbs = {};
    };
    return ApplicationViewComponent;
}());
ApplicationViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/application/components/view/application-view.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_application_service__["a" /* ApplicationService */]) === "function" && _c || Object])
], ApplicationViewComponent);

var _a, _b, _c;
//# sourceMappingURL=application-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/application/services/application.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApplicationService = (function (_super) {
    __extends(ApplicationService, _super);
    function ApplicationService(http) {
        var _this = _super.call(this, http, '/api/applications') || this;
        _this.http = http;
        return _this;
    }
    return ApplicationService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
ApplicationService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], ApplicationService);

var _a;
//# sourceMappingURL=application.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/breadcrumb/breadcrumb.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row page-titles\">\n    <div class=\"col-6 align-self-center\">\n        <h3 class=\"text-themecolor m-b-0 m-t-0\">{{data.title}}</h3>\n        <ol class=\"breadcrumb\">\n            <ng-template ngFor let-link [ngForOf]=\"data?.links\" let-last=\"last\">\n                <li class=\"breadcrumb-item\" *ngIf=\"!last && link.href != null\" [routerLink]=\"link.href\">\n                    <a href='javascript:void(0)'>{{link.title}}</a>\n                </li>\n                \n                <li class=\"breadcrumb-item active\" *ngIf=\"last || link.href == null\">{{link.title}}</li>\n            </ng-template>\n        </ol>\n    </div>\n    <div class=\"col-6 align-self-center\">\n        <div class=\"d-flex m-t-10 justify-content-end\">\n            <div *ngIf=\"data?.buttons\">\n                <a *ngFor=\"let button of data.buttons\" [routerLink]=\"button.route\" [queryParams]=\"button.params\" class=\"{{button.class}} m-l-5 btn-sm\">\n                    {{button.text}}\n                </a>\n            </div>\n            \n            <div>\n                <button class=\"right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10\"><i class=\"ti-settings text-white\"></i></button>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/core/components/breadcrumb/breadcrumb.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BreadcrumbComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BreadcrumbComponent = (function () {
    function BreadcrumbComponent(titleService) {
        this.titleService = titleService;
    }
    BreadcrumbComponent.prototype.ngOnInit = function () {
        this.titleService.setTitle(this.data.title);
    };
    return BreadcrumbComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BreadcrumbComponent.prototype, "data", void 0);
BreadcrumbComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'breadcrumb',
        template: __webpack_require__("../../../../../src/app/core/components/breadcrumb/breadcrumb.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* Title */]) === "function" && _a || Object])
], BreadcrumbComponent);

var _a;
//# sourceMappingURL=breadcrumb.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/config/form/config-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"form == null\"></preloader>\n\n<div *ngIf=\"form != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <form class=\"form-horizontal\">\n                <user-select [model]=\"form\"></user-select>\n\n                <div class=\"form-group\">\n                    <label>Key</label>\n                    <input [(ngModel)]=\"form.key\" name=\"form.key\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div class=\"form-group\">\n                    <label>Value</label>\n                    <textarea [(ngModel)]=\"form.value\" name=\"form.value\" class=\"form-control\" rows=\"15\"></textarea>\n                </div>\n    \n                <div *ngIf=\"done === true\" class=\"alert alert-success\">Success!</div>\n                \n                <button (click)=\"submit()\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">\n                    Save\n                </button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/core/components/config/form/config-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__form_component_form_component__ = __webpack_require__("../../../../../src/app/core/components/form-component/form.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigFormComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ConfigFormComponent = (function (_super) {
    __extends(ConfigFormComponent, _super);
    function ConfigFormComponent(service, route, router, httpErrorService) {
        var _this = _super.call(this, service, route, router, httpErrorService, {
            name: 'config',
            listRoute: '/configs'
        }) || this;
        _this.service = service;
        _this.route = route;
        _this.router = router;
        _this.httpErrorService = httpErrorService;
        return _this;
    }
    ConfigFormComponent.prototype.afterFetch = function (form) {
        form.value = JSON.stringify(form.value, undefined, 4);
        return form;
    };
    return ConfigFormComponent;
}(__WEBPACK_IMPORTED_MODULE_4__form_component_form_component__["a" /* FormComponent */]));
ConfigFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/core/components/config/form/config-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_config_service__["a" /* ConfigService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_http_error_service__["a" /* HttpErrorService */]) === "function" && _d || Object])
], ConfigFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=config-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/config/list/config-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"configs == null\"></preloader>\n\n<div *ngIf=\"configs != null && configs.length > 0\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">Configs</h3>\n                <h6 class=\"card-subtitle mb-2 text-muted\">\n                    Total: {{configs.length}}\n                </h6>\n                \n                <table class=\"table\">\n                    <tr>\n                        <th>ID</th>\n                        <th>Key</th>\n                        <th class=\"text-center\">User</th>\n                        <th width=\"1\"></th>\n                        <th width=\"1\"></th>\n                    </tr>\n                    <tr *ngFor=\"let config of configs\">\n                        <td>\n                            {{config.id}}\n                        </td>\n                        <td>\n                            {{config.key}}\n                        </td>\n                        <td class=\"text-center\">\n                            {{config.userId || '-'}}\n                        </td>\n                        <td>\n                            <a [routerLink]=\"['/configs/update/' + config.id]\" class=\"btn btn-info btn-sm\">Edit</a>\n                        </td>\n                        <td>\n                            <button (click)=\"deleteConfig(config)\" class=\"btn btn-danger btn-sm\">Delete</button>\n                        </td>\n                    </tr>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"configs != null && configs.length == 0\" class=\"col-12\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <div class=\"alert alert-info\">\n                <p>You haven't configs yet.</p>\n            </div>\n    \n            <p><a [routerLink]=\"['/configs/new']\" class=\"btn btn-success\">Create config</a></p>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/core/components/config/list/config-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConfigListComponent = (function () {
    function ConfigListComponent(arrayService, configService, httpErrorService) {
        this.arrayService = arrayService;
        this.configService = configService;
        this.httpErrorService = httpErrorService;
        this.crumbs = {
            title: 'Config List',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'configs' }
            ],
            buttons: [
                {
                    text: 'New config',
                    route: '/configs/new',
                    class: 'btn btn-primary'
                }
            ]
        };
    }
    ConfigListComponent.prototype.ngOnInit = function () {
        this.fetchConfigs();
    };
    ConfigListComponent.prototype.fetchConfigs = function () {
        var _this = this;
        this.configService.fetchAll().subscribe(function (response) { return _this.configs = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ConfigListComponent.prototype.deleteConfig = function (config) {
        var _this = this;
        if (!confirm("Delete config '" + config.value.title + "'?")) {
            return;
        }
        this.configService.delete(config.id).subscribe(function () { return _this.arrayService.deleteElement(_this.configs, config); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return ConfigListComponent;
}());
ConfigListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/core/components/config/list/config-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_array_service__["a" /* ArrayService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_config_service__["a" /* ConfigService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], ConfigListComponent);

var _a, _b, _c;
//# sourceMappingURL=config-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"false\"></preloader>\n\n<div class=\"row\">\n    <div class=\"col-12\">\n        Dashboard\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/core/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
        this.crumbs = {
            title: 'Dashboard',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All applications' }
            ]
        };
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'dashboard',
        template: __webpack_require__("../../../../../src/app/core/components/dashboard/dashboard.component.html")
    }),
    __metadata("design:paramtypes", [])
], DashboardComponent);

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/error-message/error-message.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"errors != null && errors.length > 0\" class=\"alert alert-danger\">\n    <p *ngFor=\"let error of errors\">{{error.message}}</p>\n</div>"

/***/ }),

/***/ "../../../../../src/app/core/components/error-message/error-message.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorMessageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ErrorMessageComponent = (function () {
    function ErrorMessageComponent() {
    }
    return ErrorMessageComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ErrorMessageComponent.prototype, "errors", void 0);
ErrorMessageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'error-message',
        template: __webpack_require__("../../../../../src/app/core/components/error-message/error-message.component.html")
    })
], ErrorMessageComponent);

//# sourceMappingURL=error-message.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/form-component/form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormComponent; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FormComponent = (function () {
    function FormComponent(service, route, router, httpErrorService, options) {
        this.service = service;
        this.route = route;
        this.router = router;
        this.httpErrorService = httpErrorService;
        this.options = options;
    }
    FormComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.embed) {
            if (this.data == null) {
                this.initForm();
            }
            else {
                this.form = this.data;
            }
        }
        else {
            this.route.params.subscribe(function (params) {
                _this.initCrumbs(params.id);
                if (params.id) {
                    _this.fetch(params.id);
                }
                else {
                    _this.initForm();
                }
            });
        }
    };
    FormComponent.prototype.fetch = function (id) {
        var _this = this;
        this.service.fetchById(id).subscribe(function (response) { return _this.form = _this.afterFetch(response); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    FormComponent.prototype.afterFetch = function (form) {
        return form;
    };
    FormComponent.prototype.initForm = function () {
        this.form = {};
    };
    FormComponent.prototype.beforeSubmit = function (form) {
        return form;
    };
    FormComponent.prototype.submit = function () {
        var _this = this;
        this.done = false;
        var data = this.beforeSubmit(__assign({}, this.form));
        this.service.save(data).subscribe(function (response) {
            _this.done = true;
            if (_this.apply != null) {
                _this.apply(_this.form);
            }
            if (_this.options.viewRoute != null && !_this.embed) {
                _this.router.navigate([_this.options.viewRoute.replace(':id', response.id)]);
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    FormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New ' + this.options.name,
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: this.options.name + ' list', href: this.options.listRoute },
                    { title: 'New ' + this.options.name }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update ' + this.options.name,
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: this.options.name + ' list', href: this.options.listRoute },
                    { title: 'Update ' + this.options.name }
                ]
            };
        }
    };
    return FormComponent;
}());

__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FormComponent.prototype, "data", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], FormComponent.prototype, "embed", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FormComponent.prototype, "apply", void 0);
//# sourceMappingURL=form.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/header-navigation/navigation.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- Topbar header - style you can find in pages.scss -->\n<!-- ============================================================== -->\n<header class=\"topbar\">\n    <nav class=\"navbar top-navbar navbar-toggleable-sm navbar-light\">\n        <!-- ============================================================== -->\n        <!-- Logo -->\n        <!-- ============================================================== -->\n        <div class=\"navbar-header\">\n            <a class=\"navbar-brand\" href=\"/\">\n                <!-- Logo icon -->\n                <b>\n                    <!--You can put here icon as well // <i class=\"wi wi-sunset\"></i> //-->\n                    <!-- Dark Logo icon -->\n                    <img src=\"assets/images/logo-icon.png\" alt=\"homepage\" class=\"dark-logo\"/>\n                    <!-- Light Logo icon -->\n                    <img src=\"assets/images/logo-light-icon.png\" alt=\"homepage\" class=\"light-logo\"/>\n                </b>\n                <!--End Logo icon -->\n                <!-- Logo text -->\n                <span>\n                 <!-- dark Logo text -->\n                 <img src=\"assets/images/logo-text.png\" alt=\"homepage\" class=\"dark-logo\"/>\n                    <!-- Light Logo text -->\n                 <img src=\"assets/images/logo-light-text.png\" class=\"light-logo\" alt=\"homepage\"/></span>\n            </a>\n        </div>\n        <!-- ============================================================== -->\n        <!-- End Logo -->\n        <!-- ============================================================== -->\n        <div *ngIf=\"layout == 'base'\" class=\"navbar-collapse\">\n            <!-- ============================================================== -->\n            <!-- toggle and nav items -->\n            <!-- ============================================================== -->\n            <ul class=\"navbar-nav mr-auto mt-md-0\">\n                <!-- This is  -->\n                <li class=\"nav-item\"><a class=\"nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark\"\n                                        href=\"javascript:void(0)\"><i class=\"mdi mdi-menu\"></i></a></li>\n                <li class=\"nav-item\"><a\n                        class=\"nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark\"\n                        href=\"javascript:void(0)\"><i class=\"ti-menu\"></i></a></li>\n                <!-- ============================================================== -->\n                <!-- Search -->\n                <!-- ============================================================== -->\n                <li class=\"nav-item hidden-sm-down search-box\">\n                    <a class=\"nav-link hidden-sm-down text-muted waves-effect waves-dark\" href=\"javascript:void(0)\"><i\n                            class=\"ti-search\"></i></a>\n                    <form class=\"app-search\">\n                        <input type=\"text\" class=\"form-control\" placeholder=\"Search & enter\"> <a class=\"srh-btn\"><i\n                            class=\"ti-close\"></i></a></form>\n                </li>\n                <!-- ============================================================== -->\n                <!-- Messages -->\n                <!-- ============================================================== -->\n                <li class=\"nav-item dropdown mega-dropdown\"><a\n                        class=\"nav-link dropdown-toggle text-muted waves-effect waves-dark\" href=\"\"\n                        data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i\n                        class=\"mdi mdi-view-grid\"></i></a>\n                    <div class=\"dropdown-menu scale-up-left\">\n                        <ul class=\"mega-dropdown-menu row\">\n                            <li class=\"col-lg-3 col-xlg-2 m-b-30\">\n                                <h4 class=\"m-b-20\">CAROUSEL</h4>\n                                <!-- CAROUSEL -->\n                                <div id=\"carouselExampleControls\" class=\"carousel slide\" data-ride=\"carousel\">\n                                    <div class=\"carousel-inner\" role=\"listbox\">\n                                        <div class=\"carousel-item active\">\n                                            <div class=\"container\"><img class=\"d-block img-fluid\"\n                                                                        src=\"assets/images/big/img1.jpg\"\n                                                                        alt=\"First slide\"></div>\n                                        </div>\n                                        <div class=\"carousel-item\">\n                                            <div class=\"container\"><img class=\"d-block img-fluid\"\n                                                                        src=\"assets/images/big/img2.jpg\"\n                                                                        alt=\"Second slide\"></div>\n                                        </div>\n                                        <div class=\"carousel-item\">\n                                            <div class=\"container\"><img class=\"d-block img-fluid\"\n                                                                        src=\"assets/images/big/img3.jpg\"\n                                                                        alt=\"Third slide\"></div>\n                                        </div>\n                                    </div>\n                                    <a class=\"carousel-control-prev\" href=\"#carouselExampleControls\" role=\"button\"\n                                       data-slide=\"prev\"> <span class=\"carousel-control-prev-icon\"\n                                                                aria-hidden=\"true\"></span> <span class=\"sr-only\">Previous</span>\n                                    </a>\n                                    <a class=\"carousel-control-next\" href=\"#carouselExampleControls\" role=\"button\"\n                                       data-slide=\"next\"> <span class=\"carousel-control-next-icon\"\n                                                                aria-hidden=\"true\"></span> <span\n                                            class=\"sr-only\">Next</span> </a>\n                                </div>\n                                <!-- End CAROUSEL -->\n                            </li>\n                            <li class=\"col-lg-3 m-b-30\">\n                                <h4 class=\"m-b-20\">ACCORDION</h4>\n                                <!-- Accordian -->\n                                <div id=\"accordion\" class=\"nav-accordion\" role=\"tablist\" aria-multiselectable=\"true\">\n                                    <div class=\"card\">\n                                        <div class=\"card-header\" role=\"tab\" id=\"headingOne\">\n                                            <h5 class=\"mb-0\">\n                                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\"\n                                                   aria-expanded=\"true\" aria-controls=\"collapseOne\">\n                                                    Collapsible Group Item #1\n                                                </a>\n                                            </h5></div>\n                                        <div id=\"collapseOne\" class=\"collapse show\" role=\"tabpanel\"\n                                             aria-labelledby=\"headingOne\">\n                                            <div class=\"card-block\"> Anim pariatur cliche reprehenderit, enim eiusmod\n                                                high.\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"card\">\n                                        <div class=\"card-header\" role=\"tab\" id=\"headingTwo\">\n                                            <h5 class=\"mb-0\">\n                                                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\"\n                                                   href=\"#collapseTwo\" aria-expanded=\"false\"\n                                                   aria-controls=\"collapseTwo\">\n                                                    Collapsible Group Item #2\n                                                </a>\n                                            </h5></div>\n                                        <div id=\"collapseTwo\" class=\"collapse\" role=\"tabpanel\"\n                                             aria-labelledby=\"headingTwo\">\n                                            <div class=\"card-block\"> Anim pariatur cliche reprehenderit, enim eiusmod\n                                                high life accusamus terry richardson ad squid.\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <div class=\"card\">\n                                        <div class=\"card-header\" role=\"tab\" id=\"headingThree\">\n                                            <h5 class=\"mb-0\">\n                                                <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\"\n                                                   href=\"#collapseThree\" aria-expanded=\"false\"\n                                                   aria-controls=\"collapseThree\">\n                                                    Collapsible Group Item #3\n                                                </a>\n                                            </h5></div>\n                                        <div id=\"collapseThree\" class=\"collapse\" role=\"tabpanel\"\n                                             aria-labelledby=\"headingThree\">\n                                            <div class=\"card-block\"> Anim pariatur cliche reprehenderit, enim eiusmod\n                                                high life accusamus terry richardson ad squid.\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </li>\n                            <li class=\"col-lg-3  m-b-30\">\n                                <h4 class=\"m-b-20\">CONTACT US</h4>\n                                <!-- Contact -->\n                                <form>\n                                    <div class=\"form-group\">\n                                        <input type=\"text\" class=\"form-control\" id=\"exampleInputname1\"\n                                               placeholder=\"Enter Name\"></div>\n                                    <div class=\"form-group\">\n                                        <input type=\"email\" class=\"form-control\" placeholder=\"Enter email\"></div>\n                                    <div class=\"form-group\">\n                                        <textarea class=\"form-control\" id=\"exampleTextarea\" rows=\"3\"\n                                                  placeholder=\"Message\"></textarea>\n                                    </div>\n                                    <button type=\"submit\" class=\"btn btn-info\">Submit</button>\n                                </form>\n                            </li>\n                            <li class=\"col-lg-3 col-xlg-4 m-b-30\">\n                                <h4 class=\"m-b-20\">List style</h4>\n                                <!-- List style -->\n                                <ul class=\"list-style-none\">\n                                    <li><a href=\"javascript:void(0)\"><i class=\"fa fa-check text-success\"></i> You can\n                                        give link</a></li>\n                                    <li><a href=\"javascript:void(0)\"><i class=\"fa fa-check text-success\"></i> Give link</a>\n                                    </li>\n                                    <li><a href=\"javascript:void(0)\"><i class=\"fa fa-check text-success\"></i> Another\n                                        Give link</a></li>\n                                    <li><a href=\"javascript:void(0)\"><i class=\"fa fa-check text-success\"></i> Forth link</a>\n                                    </li>\n                                    <li><a href=\"javascript:void(0)\"><i class=\"fa fa-check text-success\"></i> Another\n                                        fifth link</a></li>\n                                </ul>\n                            </li>\n                        </ul>\n                    </div>\n                </li>\n                <!-- ============================================================== -->\n                <!-- End Messages -->\n                <!-- ============================================================== -->\n\n\n            </ul>\n            <!-- ============================================================== -->\n            <!-- User profile and search -->\n            <!-- ============================================================== -->\n            <ul class=\"navbar-nav my-lg-0\">\n                <!-- ============================================================== -->\n                <!-- Comment -->\n                <!-- ============================================================== -->\n                <li class=\"nav-item dropdown\">\n                    <a class=\"nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark\" href=\"\"\n                       data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i\n                            class=\"mdi mdi-message\"></i>\n                        <div class=\"notify\"><span class=\"heartbit\"></span> <span class=\"point\"></span></div>\n                    </a>\n                    <div class=\"dropdown-menu dropdown-menu-right mailbox scale-up\">\n                        <ul>\n                            <li>\n                                <div class=\"drop-title\">Notifications</div>\n                            </li>\n                            <li>\n                                <div class=\"message-center\">\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"btn btn-danger btn-circle\"><i class=\"fa fa-link\"></i></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Luanch Admin</h5> <span\n                                                class=\"mail-desc\">Just see the my new admin!</span> <span class=\"time\">9:30 AM</span>\n                                        </div>\n                                    </a>\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"btn btn-success btn-circle\"><i class=\"ti-calendar\"></i></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Event today</h5> <span class=\"mail-desc\">Just a reminder that you have event</span>\n                                            <span class=\"time\">9:10 AM</span></div>\n                                    </a>\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"btn btn-info btn-circle\"><i class=\"ti-settings\"></i></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Settings</h5> <span class=\"mail-desc\">You can customize this template as you want</span>\n                                            <span class=\"time\">9:08 AM</span></div>\n                                    </a>\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"btn btn-primary btn-circle\"><i class=\"ti-user\"></i></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Pavan kumar</h5> <span class=\"mail-desc\">Just see the my admin!</span>\n                                            <span class=\"time\">9:02 AM</span></div>\n                                    </a>\n                                </div>\n                            </li>\n                            <li>\n                                <a class=\"nav-link text-center\" href=\"javascript:void(0);\"> <strong>Check all\n                                    notifications</strong> <i class=\"fa fa-angle-right\"></i> </a>\n                            </li>\n                        </ul>\n                    </div>\n                </li>\n                <!-- ============================================================== -->\n                <!-- End Comment -->\n                <!-- ============================================================== -->\n                <!-- ============================================================== -->\n                <!-- Messages -->\n                <!-- ============================================================== -->\n                <li class=\"nav-item dropdown\">\n                    <a class=\"nav-link dropdown-toggle text-muted waves-effect waves-dark\" href=\"\" id=\"2\"\n                       data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i class=\"mdi mdi-email\"></i>\n                        <div class=\"notify\"><span class=\"heartbit\"></span> <span class=\"point\"></span></div>\n                    </a>\n                    <div class=\"dropdown-menu mailbox dropdown-menu-right scale-up\" aria-labelledby=\"2\">\n                        <ul>\n                            <li>\n                                <div class=\"drop-title\">You have 4 new messages</div>\n                            </li>\n                            <li>\n                                <div class=\"message-center\">\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"user-img\"><img src=\"assets/images/users/1.jpg\" alt=\"user\"\n                                                                   class=\"img-circle\"> <span\n                                                class=\"profile-status online pull-right\"></span></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Pavan kumar</h5> <span class=\"mail-desc\">Just see the my admin!</span>\n                                            <span class=\"time\">9:30 AM</span></div>\n                                    </a>\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"user-img\"><img src=\"assets/images/users/2.jpg\" alt=\"user\"\n                                                                   class=\"img-circle\"> <span\n                                                class=\"profile-status busy pull-right\"></span></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Sonu Nigam</h5> <span\n                                                class=\"mail-desc\">I've sung a song! See you at</span> <span\n                                                class=\"time\">9:10 AM</span></div>\n                                    </a>\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"user-img\"><img src=\"assets/images/users/3.jpg\" alt=\"user\"\n                                                                   class=\"img-circle\"> <span\n                                                class=\"profile-status away pull-right\"></span></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Arijit Sinh</h5> <span class=\"mail-desc\">I am a singer!</span> <span\n                                                class=\"time\">9:08 AM</span></div>\n                                    </a>\n                                    <!-- Message -->\n                                    <a href=\"#\">\n                                        <div class=\"user-img\"><img src=\"assets/images/users/4.jpg\" alt=\"user\"\n                                                                   class=\"img-circle\"> <span\n                                                class=\"profile-status offline pull-right\"></span></div>\n                                        <div class=\"mail-contnet\">\n                                            <h5>Pavan kumar</h5> <span class=\"mail-desc\">Just see the my admin!</span>\n                                            <span class=\"time\">9:02 AM</span></div>\n                                    </a>\n                                </div>\n                            </li>\n                            <li>\n                                <a class=\"nav-link text-center\" href=\"javascript:void(0);\"> <strong>See all\n                                    e-Mails</strong> <i class=\"fa fa-angle-right\"></i> </a>\n                            </li>\n                        </ul>\n                    </div>\n                </li>\n                <!-- ============================================================== -->\n                <!-- End Messages -->\n                <!-- ============================================================== -->\n\n                <!-- ============================================================== -->\n                <!-- Profile -->\n                <!-- ============================================================== -->\n                <li class=\"nav-item dropdown\" user-navigation [user]=\"user\"></li>\n                <!-- ============================================================== -->\n                <!-- Language -->\n                <!-- ============================================================== -->\n                <li class=\"nav-item dropdown\">\n                    <a class=\"nav-link dropdown-toggle text-muted waves-effect waves-dark\" href=\"\"\n                       data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i\n                            class=\"flag-icon flag-icon-us\"></i></a>\n                    <div class=\"dropdown-menu dropdown-menu-right scale-up\"><a class=\"dropdown-item\" href=\"#\"><i\n                            class=\"flag-icon flag-icon-in\"></i> India</a> <a class=\"dropdown-item\" href=\"#\"><i\n                            class=\"flag-icon flag-icon-fr\"></i> French</a> <a class=\"dropdown-item\" href=\"#\"><i\n                            class=\"flag-icon flag-icon-cn\"></i> China</a> <a class=\"dropdown-item\" href=\"#\"><i\n                            class=\"flag-icon flag-icon-de\"></i> Dutch</a></div>\n                </li>\n            </ul>\n        </div>\n    </nav>\n</header>\n<!-- ============================================================== -->\n<!-- End Topbar header -->\n<!-- ============================================================== -->\n"

/***/ }),

/***/ "../../../../../src/app/core/components/header-navigation/navigation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavigationComponent = (function () {
    function NavigationComponent() {
    }
    return NavigationComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], NavigationComponent.prototype, "layout", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], NavigationComponent.prototype, "user", void 0);
NavigationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ma-navigation',
        template: __webpack_require__("../../../../../src/app/core/components/header-navigation/navigation.component.html")
    })
], NavigationComponent);

//# sourceMappingURL=navigation.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/preloader/preloader.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"visible\" class=\"row\">\n    <div class=\"col-12 component-preloader\">\n        <svg class=\"circular\" viewBox=\"25 25 50 50\">\n            <circle class=\"path\" cx=\"50\" cy=\"50\" r=\"20\" fill=\"none\" stroke-width=\"2\" stroke-miterlimit=\"10\" />\n        </svg>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/core/components/preloader/preloader.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreloaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PreloaderComponent = (function () {
    function PreloaderComponent() {
    }
    return PreloaderComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], PreloaderComponent.prototype, "visible", void 0);
PreloaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'preloader',
        template: __webpack_require__("../../../../../src/app/core/components/preloader/preloader.component.html")
    })
], PreloaderComponent);

//# sourceMappingURL=preloader.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/right-sidebar/rightsidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"right-sidebar\">\n    <div class=\"slimscrollright\">\n        <div class=\"rpanel-title\"> Service Panel <span><i class=\"ti-close right-side-toggle\"></i></span></div>\n        <div class=\"r-panel-body\">\n            <ul id=\"themecolors\" class=\"m-t-20\">\n                <li><b>With Light sidebar</b></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"default\" class=\"default-theme\">1</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"green\" class=\"green-theme\">2</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"red\" class=\"red-theme\">3</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"blue\" class=\"blue-theme working\">4</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"purple\" class=\"purple-theme\">5</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"megna\" class=\"megna-theme\">6</a></li>\n                <li class=\"d-block m-t-30\"><b>With Dark sidebar</b></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"default-dark\" class=\"default-dark-theme\">7</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"green-dark\" class=\"green-dark-theme\">8</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"red-dark\" class=\"red-dark-theme\">9</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"blue-dark\" class=\"blue-dark-theme\">10</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"purple-dark\" class=\"purple-dark-theme\">11</a></li>\n                <li><a href=\"javascript:void(0)\" data-theme=\"megna-dark\" class=\"megna-dark-theme \">12</a></li>\n            </ul>\n            <ul class=\"m-t-20 chatonline\">\n                <li><b>Chat option</b></li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/1.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Varun Dhavan <small class=\"text-success\">online</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/2.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Genelia Deshmukh <small class=\"text-warning\">Away</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/3.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Ritesh Deshmukh <small class=\"text-danger\">Busy</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/4.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Arijit Sinh <small class=\"text-muted\">Offline</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/5.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Govinda Star <small class=\"text-success\">online</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/6.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>John Abraham<small class=\"text-success\">online</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/7.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Hritik Roshan<small class=\"text-success\">online</small></span></a>\n                </li>\n                <li>\n                    <a href=\"javascript:void(0)\"><img src=\"assets/images/users/8.jpg\" alt=\"user-img\" class=\"img-circle\"> <span>Pwandeep rajan <small class=\"text-success\">online</small></span></a>\n                </li>\n            </ul>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/core/components/right-sidebar/rightsidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RightSidebarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RightSidebarComponent = (function () {
    function RightSidebarComponent() {
    }
    RightSidebarComponent.prototype.ngOnInit = function () {
    };
    return RightSidebarComponent;
}());
RightSidebarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ma-rightsidebar',
        template: __webpack_require__("../../../../../src/app/core/components/right-sidebar/rightsidebar.component.html")
    }),
    __metadata("design:paramtypes", [])
], RightSidebarComponent);

//# sourceMappingURL=rightsidebar.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- Left Sidebar - style you can find in sidebar.scss  -->\n<!-- ============================================================== -->\n<aside class=\"left-sidebar\">\n    <!-- Sidebar scroll-->\n    <div class=\"scroll-sidebar\">\n        <!-- User profile -->\n        <div class=\"user-profile\" style=\"background: url(assets/images/background/user-info.jpg) no-repeat;\">\n            <!-- User profile image -->\n            <div class=\"profile-img\"><img src=\"assets/images/users/1.jpg\" alt=\"user\"/></div>\n            <!-- User profile text-->\n            <div class=\"profile-text\">\n                <a href=\"#\" class=\"dropdown-toggle link u-dropdown\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"true\">Markarn Doe <span class=\"caret\"></span></a>\n                <div class=\"dropdown-menu animated flipInY\">\n                    <a href=\"javascript:void(0)\" class=\"dropdown-item\"><i class=\"ti-user\"></i> My Profile</a>\n                    <a href=\"javascript:void(0)\" class=\"dropdown-item\"><i class=\"ti-wallet\"></i> My Balance</a>\n                    <a href=\"javascript:void(0)\" class=\"dropdown-item\"><i class=\"ti-email\"></i> Inbox</a>\n                    <div class=\"dropdown-divider\"></div>\n                    <a href=\"javascript:void(0)\" class=\"dropdown-item\"><i class=\"ti-settings\"></i> Account Setting</a>\n                    <div class=\"dropdown-divider\"></div>\n                    <a href=\"javascript:void(0)\" class=\"dropdown-item\"><i class=\"fa fa-power-off\"></i> Logout</a>\n                </div>\n            </div>\n        </div>\n        <!-- End User profile text-->\n        <!-- Sidebar navigation-->\n        <nav class=\"sidebar-nav\">\n            <ul id=\"sidebarnav\">\n                <ng-container *ngFor=\"let data of sidebar\">\n                    <li class=\"nav-small-cap\">{{data.title}}</li>\n    \n                    <li *ngFor=\"let section of data.sections\">\n                        <a [routerLinkActiveOptions]=\"{exact: true}\" [routerLink]=\"[section.link]\" [queryParams]=\"section.params\" routerLinkActive=\"active\">\n                            <i class=\"{{section.icon}}\"></i>\n                            <span class=\"hide-menu\">{{section.text}}</span>\n                        </a>\n                    </li>\n                </ng-container>\n            </ul>\n        </nav>\n        <!-- End Sidebar navigation -->\n    </div>\n    <!-- End Sidebar scroll-->\n    <!-- Bottom points-->\n    <div class=\"sidebar-footer\">\n        <!-- item-->\n        <a href=\"\" class=\"link\" data-toggle=\"tooltip\" title=\"Settings\"><i class=\"ti-settings\"></i></a>\n        <!-- item-->\n        <a href=\"\" class=\"link\" data-toggle=\"tooltip\" title=\"Email\"><i class=\"mdi mdi-gmail\"></i></a>\n        <!-- item-->\n        <a href=\"\" class=\"link\" data-toggle=\"tooltip\" title=\"Logout\"><i class=\"mdi mdi-power\"></i></a>\n    </div>\n    <!-- End Bottom points-->\n</aside>\n<!-- ============================================================== -->\n<!-- End Left Sidebar - style you can find in sidebar.scss  -->\n<!-- ============================================================== -->\n"

/***/ }),

/***/ "../../../../../src/app/core/components/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SidebarComponent = (function () {
    function SidebarComponent(configService) {
        this.configService = configService;
        this.sidebar = [];
        this.rootSections = [
            {
                text: "Applications",
                link: "applications",
                icon: "mdi mdi-application"
            },
            {
                text: "Instances",
                link: "instances",
                icon: "mdi mdi-server"
            },
            {
                text: "Collections",
                link: "collections",
                icon: "mdi mdi-database"
            },
            {
                text: "Widgets",
                link: "widgets",
                icon: "mdi mdi-widgets"
            },
            {
                text: "Pipelines",
                link: "pipelines",
                icon: "mdi mdi-chemical-weapon"
            },
            {
                text: "Configs",
                link: "configs",
                icon: "mdi mdi-settings"
            }
        ];
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var config = this.configService.find('sidebar');
        if (config != null || config.sections != null) {
            this.sidebar.push({
                title: config.title,
                sections: config.sections
            });
        }
        if (this.user.role == 'root') {
            this.sidebar.push({
                title: 'NAVIGATION',
                sections: this.rootSections
            });
        }
    };
    return SidebarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], SidebarComponent.prototype, "user", void 0);
SidebarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sidebar',
        template: __webpack_require__("../../../../../src/app/core/components/sidebar/sidebar.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], SidebarComponent);

var _a;
//# sourceMappingURL=sidebar.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/components/status-label/status-label.component.html":
/***/ (function(module, exports) {

module.exports = "<span\n    [ngClass]=\"{\n        'label-light-megna'   : status == 'green' || status == 'open',\n        'label-light-danger'  : status == 'red',\n        'label-light-warning' : status == 'yellow'\n    }\"\n    class=\"label\">\n    {{status}}\n</span>"

/***/ }),

/***/ "../../../../../src/app/core/components/status-label/status-label.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusLabelComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatusLabelComponent = (function () {
    function StatusLabelComponent() {
    }
    return StatusLabelComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], StatusLabelComponent.prototype, "status", void 0);
StatusLabelComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'status-label',
        template: __webpack_require__("../../../../../src/app/core/components/status-label/status-label.component.html")
    })
], StatusLabelComponent);

//# sourceMappingURL=status-label.component.js.map

/***/ }),

/***/ "../../../../../src/app/core/pipes/map-to-iterable.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapToIterablePipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MapToIterablePipe = (function () {
    function MapToIterablePipe() {
    }
    MapToIterablePipe.prototype.transform = function (dict) {
        var a = [];
        for (var key in dict) {
            if (dict.hasOwnProperty(key)) {
                a.push({ key: key, value: dict[key] });
            }
        }
        a[a.length - 1]['last'] = true;
        return a;
    };
    return MapToIterablePipe;
}());
MapToIterablePipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'mapToIterable'
    })
], MapToIterablePipe);

//# sourceMappingURL=map-to-iterable.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/core/pipes/not-hidden.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotHiddenPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NotHiddenPipe = (function () {
    function NotHiddenPipe() {
    }
    NotHiddenPipe.prototype.transform = function (items) {
        return items.filter(function (item) { return !item.hidden; });
    };
    return NotHiddenPipe;
}());
NotHiddenPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'notHidden'
    })
], NotHiddenPipe);

//# sourceMappingURL=not-hidden.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });

var ApiService = (function () {
    function ApiService(http, resourceUrl) {
        this.http = http;
        var baseUrl = location.hostname == 'localhost' ? 'http://services-site' : '';
        this.resourceUrl = baseUrl + resourceUrl;
    }
    ApiService.prototype.getFetchAllUrl = function (params) {
        var url = this.resourceUrl;
        if (params != null) {
            url = this.queryUrl(url, params);
        }
        return url;
    };
    ApiService.prototype.fetchAll = function (params) {
        for (var param in params) {
            if (params.hasOwnProperty(param)) {
                params[param] = encodeURIComponent(params[param]);
            }
        }
        return this.http
            .get(this.getFetchAllUrl(params), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    ApiService.prototype.fetchById = function (id, params) {
        var url = this.resourceUrl + '/' + id;
        if (params != null) {
            url = this.queryUrl(url, params);
        }
        return this.http
            .get(url, this.getOptions())
            .map(function (response) { return response.json(); });
    };
    ApiService.prototype.save = function (data) {
        if (data.id == null) {
            return this.create(data);
        }
        else {
            return this.update(data);
        }
    };
    ApiService.prototype.create = function (data) {
        return this.http
            .post(this.resourceUrl, data, this.getOptions())
            .map(function (response) { return response.json(); });
    };
    ApiService.prototype.update = function (data) {
        return this.http
            .put(this.resourceUrl + '/' + data.id, data, this.getOptions())
            .map(function (response) { return response.json(); });
    };
    ApiService.prototype.delete = function (id, params) {
        var url = this.resourceUrl + '/' + id;
        if (params != null) {
            url = this.queryUrl(url, params);
        }
        return this.http.delete(url, this.getOptions()).map(function (response) { return response.json(); });
    };
    ApiService.prototype.getOptions = function (auth) {
        if (auth === void 0) { auth = true; }
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* Headers */]();
        headers.append("Content-Type", "application/json; charset=UTF-8");
        if (auth) {
            headers.append("Authorization", "Bearer " + localStorage.getItem('user-auth-key'));
        }
        return new __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* RequestOptions */]({ headers: headers, withCredentials: true });
    };
    ApiService.prototype.url = function (part) {
        return this.resourceUrl + part;
    };
    ApiService.prototype.queryUrl = function (url, params) {
        var query = [];
        for (var param in params) {
            if (params.hasOwnProperty(param)) {
                query.push(param + '=' + params[param]);
            }
        }
        return url + '?' + query.join('&');
    };
    return ApiService;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/array.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArrayService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ArrayService = (function () {
    function ArrayService() {
    }
    ArrayService.prototype.deleteByIndex = function (array, index) {
        array.splice(index, 1);
    };
    ArrayService.prototype.deleteElement = function (array, element) {
        var index = array.indexOf(element);
        if (index != -1) {
            this.deleteByIndex(array, index);
        }
    };
    ArrayService.prototype.moveUp = function (array, element) {
        var elementIndex = array.indexOf(element);
        if (elementIndex <= 0) {
            return array;
        }
        var siblingIndex = elementIndex - 1;
        var tmpElement = array[siblingIndex];
        array[siblingIndex] = element;
        array[elementIndex] = tmpElement;
        return array;
    };
    ArrayService.prototype.moveDown = function (array, element) {
        var elementIndex = array.indexOf(element);
        if (elementIndex === -1) {
            return array;
        }
        var siblingIndex = elementIndex + 1;
        if (array[siblingIndex] == null) {
            return array;
        }
        var tmpElement = array[siblingIndex];
        array[siblingIndex] = element;
        array[elementIndex] = tmpElement;
        return array;
    };
    ArrayService.prototype.moveEnd = function (array, element) {
        var index = array.indexOf(element);
        if (index === -1) {
            return array;
        }
        this.deleteByIndex(array, index);
        array.push(element);
        return array;
    };
    return ArrayService;
}());
ArrayService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], ArrayService);

//# sourceMappingURL=array.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/config.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConfigService = ConfigService_1 = (function (_super) {
    __extends(ConfigService, _super);
    function ConfigService(http) {
        var _this = _super.call(this, http, '/api/configs') || this;
        _this.http = http;
        return _this;
    }
    ConfigService.prototype.upsert = function (data) {
        return this.http
            .post(this.url('/upsert'), data, this.getOptions())
            .map(function (response) { return response.json(); });
    };
    ConfigService.prototype.find = function (key) {
        var config = localStorage.getItem(key);
        if (config == null) {
            return null;
        }
        try {
            return JSON.parse(config);
        }
        catch (e) {
            console.log('Load config error', e);
            return null;
        }
    };
    ConfigService.prototype.set = function (key, value) {
        ConfigService_1.configs[key] = value;
        localStorage.setItem(key, JSON.stringify(value));
    };
    ConfigService.prototype.findAllByPrefix = function (prefix) {
        var configs = [];
        for (var key in ConfigService_1.configs) {
            if (ConfigService_1.configs.hasOwnProperty(key) && key.startsWith(prefix)) {
                configs.push({
                    key: key,
                    value: ConfigService_1.configs[key]
                });
            }
        }
        return configs;
    };
    ConfigService.prototype.findValueByKey = function (key) {
        return ConfigService_1.configs[key];
    };
    ConfigService.prototype.getDictionaries = function () {
        var configs = this.findAllByPrefix('dictionary-');
        for (var i = 0; i < configs.length; i++) {
            configs[i]['name'] = configs[i].value.name;
            configs[i]['map'] = configs[i].value.map;
            delete configs[i].value;
        }
        return configs;
    };
    ConfigService.prototype.generateKey = function (prefix) {
        var date = new Date();
        var parts = [
            date.getFullYear(),
            date.getMonth(),
            date.getDay(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds()
        ];
        return prefix + '-' + parts.join('');
    };
    return ConfigService;
}(__WEBPACK_IMPORTED_MODULE_2__api_service__["a" /* ApiService */]));
ConfigService.configs = {};
ConfigService = ConfigService_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], ConfigService);

var ConfigService_1, _a;
//# sourceMappingURL=config.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/handshake.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandshakeService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HandshakeService = (function (_super) {
    __extends(HandshakeService, _super);
    function HandshakeService(http) {
        var _this = _super.call(this, http, '/api/handshake') || this;
        _this.http = http;
        return _this;
    }
    return HandshakeService;
}(__WEBPACK_IMPORTED_MODULE_2__api_service__["a" /* ApiService */]));
HandshakeService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], HandshakeService);

var _a;
//# sourceMappingURL=handshake.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/http-error.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpErrorService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var HttpErrorService = (function () {
    function HttpErrorService() {
        this.STATUS_UNPROCESSABLE_ENTITY = 422;
        this.STATUS_UN_AUTHORIZED = 401;
    }
    HttpErrorService.prototype.handle = function (error, context, callback) {
        switch (error.status) {
            case this.STATUS_UNPROCESSABLE_ENTITY:
                if (context) {
                    context.errors = JSON.parse(error._body).errors;
                }
                break;
            case this.STATUS_UN_AUTHORIZED:
                /**
                 * @TODO: fix double redirect
                 */
                location.href = '/login';
                break;
            case 0:
                console.log('http error', 'Server error, please try later.');
                break;
            default:
                var message = JSON.parse(error._body).message;
                if (callback != null) {
                    callback(message);
                }
                else {
                    swal('Error', message, 'error');
                }
        }
    };
    return HttpErrorService;
}());
HttpErrorService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], HttpErrorService);

//# sourceMappingURL=http-error.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/state.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_services_auth_service__ = __webpack_require__("../../../../../src/app/user/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StateService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StateService = (function () {
    function StateService(configService, authService, httpErrorService) {
        this.configService = configService;
        this.authService = authService;
        this.httpErrorService = httpErrorService;
    }
    StateService.prototype.init = function (key, context) {
        this.key = key;
        this.context = context;
        this.loadState();
    };
    StateService.prototype.setState = function (state) {
        for (var param in state) {
            if (state.hasOwnProperty(param)) {
                this.context[param] = state[param];
            }
        }
        this.saveState();
    };
    StateService.prototype.saveState = function () {
        var _this = this;
        var state = this.context.getState();
        var key = this.getStorageKey();
        this.configService.set(key, state);
        var data = {
            key: key,
            value: state,
            userId: this.authService.getUserId()
        };
        this.configService.upsert(data).subscribe(function () { return console.log('state syncronyzed'); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    StateService.prototype.loadState = function () {
        var state = null;
        var item = this.configService.find(this.getStorageKey());
        if (item == null) {
            state = this.context.getState();
        }
        else {
            state = item;
        }
        for (var prop in state) {
            if (state.hasOwnProperty(prop)) {
                this.context[prop] = state[prop];
            }
        }
        var defaultState = this.context.getState();
        for (var prop in defaultState) {
            if (defaultState.hasOwnProperty(prop) && state[prop] == null) {
                this.context[prop] = defaultState[prop];
            }
        }
        console.log('loaded state', state);
    };
    StateService.prototype.getStorageKey = function () {
        return 'component-state-' + this.key;
    };
    return StateService;
}());
StateService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__user_services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__user_services_auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], StateService);

var _a, _b, _c;
//# sourceMappingURL=state.service.js.map

/***/ }),

/***/ "../../../../../src/app/core/services/url.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UrlService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var UrlService = (function () {
    function UrlService() {
    }
    UrlService.prototype.isValid = function (url) {
        var pattern = new RegExp('^(https?:\\/\\/)?' +
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' +
            '((\\d{1,3}\\.){3}\\d{1,3}))' +
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' +
            '(\\?[;&a-z\\d%_.~+=-]*)?' +
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(url);
    };
    return UrlService;
}());
UrlService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], UrlService);

//# sourceMappingURL=url.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/aggregation/form/aggregation-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"config == null\"></preloader>\n\n<div *ngIf=\"config != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <div class=\"form-group\">\n                    <label>Title</label>\n                    <input [(ngModel)]=\"config.value.title\" name=\"config.value.title\" type=\"text\" class=\"form-control\"/>\n                </div>\n                \n                <collection-select [model]=\"config.value\"></collection-select>\n                \n                <div class=\"form-group\">\n                    <label>Type</label>\n                    \n                    <select [(ngModel)]=\"config.value.type\" name=\"config.value.type\" class=\"custom-select col-12\">\n                        <option *ngFor=\"let type of types\" [ngValue]=\"type.value\">{{type.text}}</option>\n                    </select>\n                </div>\n    \n                <div *ngIf=\"config.value.type != 'date_histogram_term'\" class=\"form-group\">\n                    <label>Field</label>\n                    <input [(ngModel)]=\"config.value.params.field\" name=\"config.value.params.field\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"config.value.type == 'date_histogram_term'\" class=\"form-group\">\n                    <label>Term field</label>\n                    <input [(ngModel)]=\"config.value.params.term_field\" name=\"config.value.params.term_field\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"config.value.type == 'date_histogram_term'\" class=\"form-group\">\n                    <label>Date field</label>\n                    <input [(ngModel)]=\"config.value.params.date_field\" name=\"config.value.params.date_field\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"['date_histogram', 'date_histogram_term'].indexOf(config.value.type) != -1\" class=\"form-group\">\n                    <label>Interval</label>\n                    <input [(ngModel)]=\"config.value.params.interval\" name=\"config.value.params.interval\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"['date_histogram', 'date_histogram_term'].indexOf(config.value.type) != -1\" class=\"form-group\">\n                    <label>Date format</label>\n                    <input [(ngModel)]=\"config.value.params.format\" name=\"config.value.params.format\" type=\"text\" class=\"form-control\"/>\n                </div>\n                \n                <button (click)=\"submit()\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">\n                    {{config.id != null ? 'Update' : 'Create'}}\n                </button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/aggregation/form/aggregation-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AggregationFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AggregationFormComponent = (function () {
    function AggregationFormComponent(route, router, httpErrorService, configService) {
        this.route = route;
        this.router = router;
        this.httpErrorService = httpErrorService;
        this.configService = configService;
        this.types = [
            { text: 'Term', value: 'term' },
            { text: 'Date histogram', value: 'date_histogram' },
            { text: 'Date histogram & term', value: 'date_histogram_term' }
        ];
    }
    AggregationFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.initCrumbs(params.id);
            _this.initAggregation(params.id);
        });
    };
    AggregationFormComponent.prototype.fetchAggregation = function (id) {
        var _this = this;
        this.configService.fetchById(id).subscribe(function (response) { return _this.config = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    AggregationFormComponent.prototype.submit = function () {
        var _this = this;
        if (this.config.key == null) {
            this.config.key = this.configService.generateKey('aggregation');
        }
        this.configService.save(this.config).subscribe(function (response) { return _this.router.navigate(['aggregations']); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    AggregationFormComponent.prototype.initAggregation = function (id) {
        if (id == null) {
            this.config = {
                type: 'aggregation',
                value: { params: {} }
            };
        }
        else {
            this.fetchAggregation(id);
        }
    };
    AggregationFormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New aggregation',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All aggregations', href: 'aggregations' },
                    { title: 'New aggregation' }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update aggregation',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All aggregations', href: '/aggregations' },
                    { title: 'Update aggregation' }
                ]
            };
        }
    };
    return AggregationFormComponent;
}());
AggregationFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/aggregation/form/aggregation-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */]) === "function" && _d || Object])
], AggregationFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=aggregation-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/aggregation/form/select/aggregation-select.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"aggregations\" class=\"form-group\">\n    <label>Aggregation</label>\n    <select [(ngModel)]=\"model[field]\" name=\"model\" class=\"custom-select col-12\">\n        <option *ngFor=\"let aggregation of aggregations\" [ngValue]=\"aggregation.id\">{{aggregation.value.title}}</option>\n    </select>\n</div>\n\n\n\n<!--<select [(ngModel)]=\"form.value.aggregation.type\" name=\"form.value.aggregation.type\" class=\"custom-select col-12\">-->\n    <!--<option *ngFor=\"let type of aggTypes\" [ngValue]=\"type.value\">{{type.text}}</option>-->\n<!--</select>-->"

/***/ }),

/***/ "../../../../../src/app/crude/components/aggregation/form/select/aggregation-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AggregationSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AggregationSelectComponent = (function () {
    function AggregationSelectComponent(configService, httpErrorService) {
        this.configService = configService;
        this.httpErrorService = httpErrorService;
    }
    AggregationSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.configService.fetchAll({ type: 'aggregation' }).subscribe(function (response) { return _this.aggregations = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return AggregationSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AggregationSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], AggregationSelectComponent.prototype, "field", void 0);
AggregationSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'aggregation-select',
        template: __webpack_require__("../../../../../src/app/crude/components/aggregation/form/select/aggregation-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], AggregationSelectComponent);

var _a, _b;
//# sourceMappingURL=aggregation-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/aggregation/list/aggregation-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"aggregations == null\"></preloader>\n\n<div *ngIf=\"aggregations != null && aggregations.length > 0\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">Aggregations</h3>\n                <h6 class=\"card-subtitle mb-2 text-muted\">\n                    Total: {{aggregations.length}}\n                </h6>\n                \n                <table class=\"table\">\n                    <tr>\n                        <th>Title</th>\n                        <th>Params</th>\n                        <th width=\"1\"></th>\n                        <th width=\"1\"></th>\n                    </tr>\n                    <tr *ngFor=\"let aggregation of aggregations\">\n                        <td>\n                            {{aggregation.value.title}}\n                        </td>\n                        <td>\n                            {{aggregation.value.params | json}}\n                        </td>\n                        <td>\n                            <a [routerLink]=\"['/aggregations/update/' + aggregation.id]\" class=\"btn btn-info btn-sm\">\n                                <i class=\"fa fa-edit\"></i>\n                            </a>\n                        </td>\n                        <td>\n                            <button (click)=\"deleteAggregation(aggregation)\" class=\"btn btn-danger btn-sm\">\n                                <i class=\"fa fa-remove\"></i>\n                            </button>\n                        </td>\n                    </tr>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"aggregations != null && aggregations.length == 0\" class=\"col-12\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <div class=\"alert alert-info\">\n                <p>You haven't aggregations yet.</p>\n            </div>\n    \n            <p><a [routerLink]=\"['/aggregations/new']\" class=\"btn btn-success\">Create aggregation</a></p>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/aggregation/list/aggregation-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AggregationListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AggregationListComponent = (function () {
    function AggregationListComponent(arrayService, configService, httpErrorService) {
        this.arrayService = arrayService;
        this.configService = configService;
        this.httpErrorService = httpErrorService;
        this.crumbs = {
            title: 'All aggregations',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All aggregations' }
            ],
            buttons: [
                {
                    text: 'New aggregation',
                    route: '/aggregations/new',
                    class: 'btn btn-primary'
                }
            ]
        };
    }
    AggregationListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var params = { type: 'aggregation' };
        this.configService.fetchAll(params).subscribe(function (response) { return _this.aggregations = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    AggregationListComponent.prototype.deleteAggregation = function (aggregation) {
        var _this = this;
        if (!confirm("Delete aggregation '" + aggregation.value.title + "'?")) {
            return;
        }
        this.configService.delete(aggregation.id).subscribe(function () { return _this.arrayService.deleteElement(_this.aggregations, aggregation); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return AggregationListComponent;
}());
AggregationListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/aggregation/list/aggregation-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__["a" /* ArrayService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], AggregationListComponent);

var _a, _b, _c;
//# sourceMappingURL=aggregation-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/bar/bar-chart.component.html":
/***/ (function(module, exports) {

module.exports = "<h3 class=\"card-title\">{{title}}</h3>\n<div id=\"{{elementId}}\" style=\"width:100%; height:278px\"></div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/bar/bar-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chart_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/chart.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarChartComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BarChartComponent = (function (_super) {
    __extends(BarChartComponent, _super);
    function BarChartComponent(httpErrorService, itemService) {
        var _this = _super.call(this, httpErrorService, itemService) || this;
        _this.httpErrorService = httpErrorService;
        _this.itemService = itemService;
        return _this;
    }
    BarChartComponent.prototype.buildChartData = function (data) {
        var field = Object.keys(data.aggregations)[0];
        var buckets = data.aggregations[field].buckets;
        this.chartData = [];
        this.chartLegends = [];
        for (var _i = 0, buckets_1 = buckets; _i < buckets_1.length; _i++) {
            var data_1 = buckets_1[_i];
            this.chartData.push(data_1.doc_count);
            this.chartLegends.push(data_1.key);
        }
    };
    BarChartComponent.prototype.initChart = function () {
        if (this.axisYOffset == null) {
            var defOffset = 70;
            var midLength = 10;
            var maxLength = this.chartLegends.reduce(function (a, b) { return a.length > b.length ? a : b; }).length;
            var dimention = maxLength / midLength;
            if (dimention > 1) {
                this.axisYOffset = dimention * defOffset;
            }
            else {
                this.axisYOffset = defOffset;
            }
        }
        new Chartist.Bar('#' + this.elementId, {
            labels: this.chartLegends,
            series: [this.chartData]
        }, {
            seriesBarDistance: 10,
            reverseData: true,
            horizontalBars: true,
            axisY: {
                offset: parseInt(this.axisYOffset)
            }
        });
    };
    return BarChartComponent;
}(__WEBPACK_IMPORTED_MODULE_3__chart_component__["a" /* ChartComponent */]));
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BarChartComponent.prototype, "axisYOffset", void 0);
BarChartComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'bar-chart',
        template: __webpack_require__("../../../../../src/app/crude/components/chart/bar/bar-chart.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */]) === "function" && _b || Object])
], BarChartComponent);

var _a, _b;
//# sourceMappingURL=bar-chart.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ChartComponent = (function () {
    function ChartComponent(httpErrorService, itemService) {
        this.httpErrorService = httpErrorService;
        this.itemService = itemService;
        this.colors = [
            '#26c6da',
            '#1e88e5',
            '#f4c63d',
            '#745af2',
            '#f62d51',
            '#d17905',
            '#59922b',
            '#453d3f',
            '#dadada',
        ];
    }
    ChartComponent.prototype.updateState = function () {
        return this.fetchItems();
    };
    ChartComponent.prototype.initChart = function () {
        throw new Error('method should be overwrited');
    };
    ChartComponent.prototype.buildChartData = function (data) {
        throw new Error('method should be overwrited');
    };
    ChartComponent.prototype.ngOnInit = function () {
        this.fetchItems();
    };
    ChartComponent.prototype.fetchItems = function () {
        var _this = this;
        var params = {
            collectionId: this.collectionId,
            aggregation: this.aggregation,
            limit: 0
        };
        if (this.condition) {
            params['condition'] = this.condition;
        }
        this.itemService.fetchAll(params).subscribe(function (response) {
            _this.buildChartData(response);
            _this.initChart();
        });
    };
    return ChartComponent;
}());

__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ChartComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ChartComponent.prototype, "elementId", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "condition", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "aggregation", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ChartComponent.prototype, "collectionId", void 0);
//# sourceMappingURL=chart.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/form/chart-form.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Visualization</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n                <span aria-hidden=\"true\">&times;</span>\n            </button>\n    </div>\n    <div class=\"modal-body\">\n        <ul class=\"nav nav-tabs\" role=\"tablist\">\n            <li *ngFor=\"let chart of chartTypes; let i = index\" class=\"nav-item\">\n                <a \n                    class=\"nav-link\" \n                    (click)=\"form.selector = chart.selector\" \n                    [ngClass]=\"{'active': isActiveTab(chart)}\"\n                    data-toggle=\"tab\"\n                    href=\"#tab-{{chart.selector}}\" \n                    role=\"tab\">\n            \n                    <span>\n                        <i class=\"{{chart.icon}}\"></i>\n                    </span>\n                </a>\n            </li>\n        </ul>\n\n        <div class=\"tab-content tabcontent-border\">\n            <div *ngFor=\"let chart of chartTypes; let i = index\" class=\"tab-pane p-20\" [ngClass]=\"{'active': isActiveTab(chart, i)}\"\n                id=\"tab-{{chart.selector}}\" role=\"tabpanel\">\n                <form class=\"form-horizontal\">\n                    <h4 class=\"box-title\">{{chart.name}}</h4>\n                    <hr class=\"m-t-0 m-b-20\">\n\n                    <div class=\"form-group\">\n                        <label>Type</label>\n                        <aggregation-type-select\n                            [model]=\"forms[chart.selector].aggregation\"\n                            [field]=\"'type'\"\n                            [only]=\"chart.aggTypes\">\n                            \n                        </aggregation-type-select>\n                    </div>\n\n                    <div *ngIf=\"forms[chart.selector].aggregation.type != null\">\n                        <div *ngFor=\"let param of aggParams[forms[chart.selector].aggregation.type]\" class=\"form-group\">\n                            <label>{{nameToLabel(param.name)}}</label>\n\n                            <ng-container *ngIf=\"param.component == 'interval-select'\">\n                                <interval-select\n                                    [model]=\"forms[chart.selector].aggregation\"\n                                    [field]=\"'interval'\">\n                                </interval-select>\n                            </ng-container>\n\n                            <ng-container *ngIf=\"param.component == 'field-select'\">\n                                <field-select\n                                    [model]=\"forms[chart.selector].aggregation\"\n                                    [field]=\"param.name\"\n                                    [type]=\"getFieldSelectType(forms[chart.selector].aggregation, param.name)\"\n                                    [setSingleValue]=\"true\"\n                                    [fields]=\"fields\">\n                                </field-select>\n                            </ng-container>\n\n                            <ng-container *ngIf=\"param.component == 'date-format-select'\">\n                                <date-format-select\n                                    [model]=\"forms[chart.selector].aggregation\"\n                                    [field]=\"param.name\">\n                                </date-format-select>\n                            </ng-container>\n\n                            <ng-container *ngIf=\"['text', 'number'].includes(param.component)\">\n                                <input\n                                    [(ngModel)]=\"forms[chart.selector].aggregation[param.name]\"\n                                    name=\"forms_{{chart.selector}}_aggregation_{{param.name}}\"\n                                    type=\"{{param.component}}\"\n                                    class=\"form-control\" />\n                            </ng-container>\n                        </div>\n\n                        <button (click)=\"submit(c)\" class=\"btn btn-primary\">Visualize</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/form/chart-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_chart_service__ = __webpack_require__("../../../../../src/app/crude/services/chart.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_aggregation_service__ = __webpack_require__("../../../../../src/app/crude/services/aggregation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartFormComponent; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChartFormComponent = (function () {
    function ChartFormComponent(chartService, modalService, fieldService, aggService) {
        this.chartService = chartService;
        this.modalService = modalService;
        this.fieldService = fieldService;
        this.aggService = aggService;
        this.chartTypes = chartService.getTypes();
        this.aggTypes = aggService.getTypes();
        this.aggParams = {};
        for (var _i = 0, _a = this.aggTypes; _i < _a.length; _i++) {
            var aggType = _a[_i];
            this.aggParams[aggType.value] = aggType.params;
        }
    }
    ChartFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fieldService.collectionFields.subscribe(function (data) { return _this.fields = data; });
        this.initForm();
    };
    ChartFormComponent.prototype.isActiveTab = function (chart) {
        return this.form != null && this.form.selector == chart.selector;
    };
    ChartFormComponent.prototype.initForm = function () {
        if (this.chart == null) {
            this.form = {
                selector: 'line-chart',
                aggregation: {}
            };
        }
        else {
            this.form = __assign({}, this.chart);
        }
        if (this.forms == null) {
            this.forms = {};
            for (var _i = 0, _a = this.chartTypes; _i < _a.length; _i++) {
                var type = _a[_i];
                this.forms[type.selector] = {
                    aggregation: {}
                };
            }
        }
    };
    ChartFormComponent.prototype.openModal = function () {
        this.modalService.open(this.modal).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    ChartFormComponent.prototype.nameToLabel = function (name) {
        return this.fieldService.nameToLabel(name);
    };
    ChartFormComponent.prototype.getFieldSelectType = function (aggregation, param) {
        if (aggregation.type == 'date_histogram' || (aggregation.type == 'date_histogram_term' && param == 'date_field')) {
            return 'date';
        }
        return null;
    };
    ChartFormComponent.prototype.submit = function (close) {
        var form = this.forms[this.form.selector];
        form.selector = this.form.selector;
        this.chartService.data.next(form);
        close();
    };
    return ChartFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ChartFormComponent.prototype, "id", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modal'),
    __metadata("design:type", Object)
], ChartFormComponent.prototype, "modal", void 0);
ChartFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'chart-form',
        template: __webpack_require__("../../../../../src/app/crude/components/chart/form/chart-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_chart_service__["a" /* ChartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_chart_service__["a" /* ChartService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_field_service__["a" /* FieldService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_field_service__["a" /* FieldService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_aggregation_service__["a" /* AggregationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_aggregation_service__["a" /* AggregationService */]) === "function" && _d || Object])
], ChartFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=chart-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/line/line-chart.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex flex-wrap\">\n    <div>\n        <h3 class=\"card-title\">{{title}}</h3>\n        <h6 *ngIf=\"subTitle != null && subTitle.length > 0\" class=\"card-subtitle\">\n            {{subTitle}}\n        </h6>\n    </div>\n    \n    <!--<div *ngIf=\"titleLabels != null && titleLabels.length > 0\" class=\"ml-auto align-self-center\">-->\n        <!--<ul class=\"list-inline m-b-0\">-->\n            <!--<li *ngFor=\"let data of titleLabels; let i = index\">-->\n                <!--<h6 [style.color]=\"colors[i]\">-->\n                    <!--<i class=\"fa fa-circle font-10 m-r-10\"></i> {{data.text}}-->\n                <!--</h6>-->\n            <!--</li>-->\n        <!--</ul>-->\n    <!--</div>-->\n\n    <div class=\"ml-auto align-self-right\">\n        <!-- <div *ngIf=\"config != null\"> -->\n            <!--<label>Interval</label>-->\n            <!--<select [(ngModel)]=\"aggregation.params.interval\" (change)=\"updateState()\" class=\"form-control\">-->\n                <!--<option value=\"day\">Day</option>-->\n                <!--<option value=\"week\">Week</option>-->\n                <!--<option value=\"month\">Month</option>-->\n                <!--<option value=\"year\">Year</option>-->\n            <!--</select>-->\n    \n            <!--<interval-select *ngIf=\"config != null\" [model]=\"config.value.params\" (change)=\"updateState()\"></interval-select>-->\n        <!-- </div> -->\n    </div>\n</div>\n\n<div id=\"{{elementId}}\" class=\"campaign ct-charts\"></div>\n\n<div *ngIf=\"titleLabels != null && titleLabels.length > 0\" class=\"row text-center m-t-30\">\n    <table style=\"width: 100%\">\n        <tr>\n            <td *ngFor=\"let data of titleLabels; let i = index\">\n                <h1 class=\"m-b-0 font-light\" [style.color]=\"colors[i]\">{{data.count}}</h1>\n                <small [style.color]=\"colors[i]\">{{data.text}}</small>\n            </td>\n        </tr>\n    </table>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/line/line-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chart_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/chart.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LineChartComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LineChartComponent = (function (_super) {
    __extends(LineChartComponent, _super);
    function LineChartComponent(httpErrorService, itemService) {
        var _this = _super.call(this, httpErrorService, itemService) || this;
        _this.httpErrorService = httpErrorService;
        _this.itemService = itemService;
        return _this;
    }
    LineChartComponent.prototype.buildChartData = function (data) {
        switch (this.aggregation.type) {
            case 'date_histogram':
                this.buildDateHistogramData(data.aggregations);
                break;
            case 'date_histogram_term':
                this.buildDateHistogramTermData(data.aggregations);
                break;
        }
    };
    LineChartComponent.prototype.buildDateHistogramData = function (agg) {
        var field = Object.keys(agg)[0];
        var buckets = agg[field].buckets;
        var labels = [];
        var series = [];
        for (var _i = 0, buckets_1 = buckets; _i < buckets_1.length; _i++) {
            var data = buckets_1[_i];
            labels.push(data.key_as_string);
            series.push(data.doc_count);
        }
        this.chartLabels = labels;
        this.chartSeries = [series];
    };
    LineChartComponent.prototype.buildDateHistogramTermData = function (agg) {
        var field = Object.keys(agg)[0];
        var buckets = agg[field].buckets;
        var labels = [];
        var series = [];
        this.titleLabels = [];
        for (var _i = 0, buckets_2 = buckets; _i < buckets_2.length; _i++) {
            var bucket = buckets_2[_i];
            this.titleLabels.push({
                text: bucket.key,
                count: bucket.doc_count
            });
            var bucketField = Object.keys(bucket).find(function (k) { return k == 'aggregation'; });
            var childBuckets = bucket[bucketField].buckets;
            var childSeries = [];
            for (var _a = 0, childBuckets_1 = childBuckets; _a < childBuckets_1.length; _a++) {
                var data = childBuckets_1[_a];
                if (labels.indexOf(data.key_as_string) == -1) {
                    labels.push(data.key_as_string);
                }
                childSeries.push(data.doc_count);
            }
            series.push(childSeries);
        }
        this.chartLabels = labels;
        this.chartSeries = series;
    };
    LineChartComponent.prototype.initChart = function () {
        new Chartist.Line('#' + this.elementId, {
            labels: this.chartLabels,
            series: this.chartSeries,
        }, {
            scaleMinSpace: 20,
            onlyInteger: true,
        });
    };
    return LineChartComponent;
}(__WEBPACK_IMPORTED_MODULE_3__chart_component__["a" /* ChartComponent */]));
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], LineChartComponent.prototype, "subTitle", void 0);
LineChartComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'line-chart',
        template: __webpack_require__("../../../../../src/app/crude/components/chart/line/line-chart.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */]) === "function" && _b || Object])
], LineChartComponent);

var _a, _b;
//# sourceMappingURL=line-chart.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/pie/pie-chart.component.html":
/***/ (function(module, exports) {

module.exports = "<h3 class=\"card-title\">{{title}}</h3>\n\n<div id=\"{{elementId}}\" style=\"width:100%; height:278px\"></div>\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/chart/pie/pie-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chart_component__ = __webpack_require__("../../../../../src/app/crude/components/chart/chart.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PieChartComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PieChartComponent = (function (_super) {
    __extends(PieChartComponent, _super);
    function PieChartComponent(httpErrorService, itemService) {
        var _this = _super.call(this, httpErrorService, itemService) || this;
        _this.httpErrorService = httpErrorService;
        _this.itemService = itemService;
        return _this;
    }
    PieChartComponent.prototype.buildChartData = function (data) {
        var field = Object.keys(data.aggregations)[0];
        var buckets = data.aggregations[field].buckets;
        this.chartData = [];
        this.chartLegends = [];
        for (var _i = 0, buckets_1 = buckets; _i < buckets_1.length; _i++) {
            var data_1 = buckets_1[_i];
            this.chartData.push({
                name: data_1.key,
                value: data_1.doc_count
            });
            this.chartLegends.push(data_1.key);
        }
    };
    PieChartComponent.prototype.initChart = function () {
        var chart = echarts.init(document.getElementById(this.elementId));
        var options = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data: this.chartLegends
            },
            color: this.colors,
            calculable: true,
            series: [
                {
                    name: this.title,
                    type: 'pie',
                    radius: ['80%', '90%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '30',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data: this.chartData
                }
            ]
        };
        chart.setOption(options, true);
        $(function () {
            function resize() {
                setTimeout(function () {
                    chart.resize();
                }, 100);
            }
            $(window).on("resize", resize);
            $(".sidebartoggler").on("click", resize);
        });
    };
    return PieChartComponent;
}(__WEBPACK_IMPORTED_MODULE_3__chart_component__["a" /* ChartComponent */]));
PieChartComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'pie-chart',
        template: __webpack_require__("../../../../../src/app/crude/components/chart/pie/pie-chart.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */]) === "function" && _b || Object])
], PieChartComponent);

var _a, _b;
//# sourceMappingURL=pie-chart.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/card/collection-card.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n    <div class=\"card-block\">\n        <h3 class=\"card-title\">{{collection.name}}</h3>\n        <h6 class=\"card-subtitle mb-2 text-muted\">\n            <a href=\"http://{{collection.instance.host}}:{{collection.instance.port}}/{{collection.indexName}}/_search\" target=\"_blank\">\n                http://{{collection.instance.host}}:{{collection.instance.port}}/{{collection.indexName}}/_search\n            </a>\n        </h6>\n\n        <div *ngIf=\"collection.index != null && collection.index.error != null\" class=\"alert alert-danger\">\n            <p>Error: {{collection.index.error.reason}}</p>\n            <p class=\"m-t-10\">\n                <button (click)=\"createIndex(collection.indexName, collection.instanceId)\" class=\"btn btn-sm btn-success\">Create index</button>\n            </p>\n        </div>\n\n        <div *ngIf=\"collection.index != null\" class=\"m-t-20\">\n            <dl class=\"dl-horizontal\">\n                <dt>Health:</dt>\n                <dd>\n                    <status-label [status]=\"collection.index['health']\"></status-label>\n                </dd>\n                <dt>Status:</dt>\n                <dd>\n                    <status-label [status]=\"collection.index['status']\"></status-label>\n                </dd>\n                <dt>Index:</dt>\n                <dd>\n                    {{collection.index['index']}}\n                </dd>\n                <dt>UUID:</dt>\n                <dd>\n                    {{collection.index['uuid']}}\n                </dd>\n                <dt>Pri:</dt>\n                <dd>\n                    {{collection.index['pri']}}\n                </dd>\n                <dt>Rep:</dt>\n                <dd>\n                    {{collection.index['rep']}}\n                </dd>\n                <dt>Docs count:</dt>\n                <dd>\n                    {{collection.index['docs.count']}}\n                </dd>\n                <dt>Docs deleted:</dt>\n                <dd>\n                    {{collection.index['docs.deleted']}}\n                </dd>\n                <dt>Store size:</dt>\n                <dd>\n                    {{collection.index['store.size']}}\n                </dd>\n                <dt>Pri store size:</dt>\n                <dd>\n                    {{collection.index['pri.store.size']}}\n                </dd>\n            </dl>\n        </div>\n\n        <hr class=\"m-t-15 m-b-15\">\n\n        <a [routerLink]=\"['/collections/' + collection.id]\"  class=\"card-link btn btn-secondary btn-sm\">view</a>\n        <a [routerLink]=\"['/collections/update/' + collection.id]\"  class=\"card-link btn btn-info btn-sm\" href=\"#\">update</a>\n\n        <a [routerLink]=\"['/collections/' + collection.id + '/items/']\" class=\"card-link btn btn-success btn-sm\">view items</a>\n        <a [routerLink]=\"['/collections/' + collection.id + '/items/new']\" class=\"card-link btn btn-primary btn-sm\" href=\"#\">add item</a>\n\n        <button *ngIf=\"detail\" (click)=\"deleteCollection()\" class=\"btn btn-danger card-link btn-sm\">Delete</button>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/card/collection-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionCardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CollectionCardComponent = (function () {
    function CollectionCardComponent(router, collectionService, httpErrorService) {
        this.router = router;
        this.collectionService = collectionService;
        this.httpErrorService = httpErrorService;
    }
    CollectionCardComponent.prototype.deleteCollection = function () {
        var _this = this;
        if (!confirm('Delete collection "' + this.collection.name + '" ?')) {
            return;
        }
        this.collectionService.delete(this.collection.id).subscribe(function (response) { return _this.router.navigate(['collections/new']); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return CollectionCardComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CollectionCardComponent.prototype, "collection", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], CollectionCardComponent.prototype, "detail", void 0);
CollectionCardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'collection-card',
        template: __webpack_require__("../../../../../src/app/crude/components/collection/card/collection-card.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_collection_service__["a" /* CollectionService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], CollectionCardComponent);

var _a, _b, _c;
//# sourceMappingURL=collection-card.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/dashboard/collection-dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"dashboard == null\"></preloader>\n\n<div *ngIf=\"dashboard != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div *ngFor=\"let row of dashboard.value.rows; let rowIndex = index\" class=\"row\">\n            <div *ngFor=\"let col of row.cols; let colIndex = index\" class=\"{{col.class}}\">\n                <div class=\"card\">\n                    <div class=\"card-block\">\n                        <widget-frame\n                            *ngIf=\"configs[col.widgetId] != null\"\n                            [chart]=\"configs[col.widgetId].value\"\n                            [id]=\"'widget-' + col.widgetId\"\n                        >\n                        </widget-frame>\n                        \n                        <div *ngIf=\"configs[col.widgetId] == null\" class=\"alert alert-warning\">\n                            widget not exists\n                        </div>\n    \n                        <button (click)=\"removeWidget(rowIndex, colIndex)\" class=\"btn btn-danger\">Remove</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n    "

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/dashboard/collection-dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionDashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CollectionDashboardComponent = (function () {
    function CollectionDashboardComponent(route, arrayService, configService, httpErrorService, collectionService) {
        this.route = route;
        this.arrayService = arrayService;
        this.configService = configService;
        this.httpErrorService = httpErrorService;
        this.collectionService = collectionService;
    }
    CollectionDashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.fetchDashboard(params.collectionId);
            _this.fetchCollection(params.collectionId, function () {
                _this.initCrumbs();
            });
        });
    };
    CollectionDashboardComponent.prototype.removeWidget = function (rowIndex, colIndex) {
        var _this = this;
        if (!confirm('Remove widget from dashboard?')) {
            return;
        }
        if (confirm('Delete widget completely?')) {
            var widgetId = this.dashboard.value.rows[rowIndex].cols[colIndex].widgetId;
            this.configService.delete(widgetId).subscribe(function () { }, function (error) { return _this.httpErrorService.handle(error, _this); });
        }
        this.arrayService.deleteByIndex(this.dashboard.value.rows[rowIndex].cols, colIndex);
        this.configService.update(this.dashboard).subscribe(function () { }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionDashboardComponent.prototype.fetchCollection = function (id, callback) {
        var _this = this;
        this.collectionService.fetchById(id).subscribe(function (response) {
            _this.collection = response;
            callback();
        }, function (error) {
            _this.httpErrorService.handle(error, _this);
        });
    };
    CollectionDashboardComponent.prototype.fetchDashboard = function (collectionId) {
        var _this = this;
        var type = 'collection-' + collectionId;
        var params = { type: type };
        this.configService.fetchAll(params).subscribe(function (response) {
            var dashboard = response.find(function (c) { return c.key == type + '-dashboard'; });
            if (dashboard == null) {
                return;
            }
            _this.configs = {};
            for (var _i = 0, response_1 = response; _i < response_1.length; _i++) {
                var config = response_1[_i];
                _this.configs[config.id] = config;
            }
            _this.dashboard = dashboard;
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionDashboardComponent.prototype.initCrumbs = function () {
        this.crumbs = {
            title: 'Item analytics',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'Collections', href: '/collections' },
                { title: this.collection.name },
                { title: 'Analytics' }
            ]
        };
    };
    return CollectionDashboardComponent;
}());
CollectionDashboardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/collection/dashboard/collection-dashboard.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_collection_service__["a" /* CollectionService */]) === "function" && _e || Object])
], CollectionDashboardComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=collection-dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/form/collection-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"collection == null\"></preloader>\n\n<div class=\"row\">\n    <div *ngIf=\"collection != null\" class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <h4 class=\"box-title\">Collection attributes</h4>\n                <hr class=\"m-t-0 m-b-20\">\n\n                <div class=\"row\">\n                    <div class=\"col-4\">\n                        <div class=\"form-group\">\n                            <label>Name</label>\n                            <input [(ngModel)]=\"collection.name\" name=\"collection.name\" type=\"text\" class=\"form-control\"/>\n                        </div>\n                    </div>\n\n                    <div class=\"col-4\">\n                        <div class=\"form-group\">\n                            <label>Index name</label>\n                            <input [(ngModel)]=\"collection.indexName\" name=\"collection.indexName\" type=\"text\" class=\"form-control\"/>\n                        </div>\n                    </div>\n\n                    <div class=\"col-4\">\n                        <div class=\"form-group\">\n                            <label>Index type</label>\n                            <input [(ngModel)]=\"collection.indexType\" name=\"collection.indexType\" type=\"text\" class=\"form-control\"/>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-4\">\n                        <application-select [model]=\"collection\" [field]=\"'appId'\"></application-select>\n                    </div>\n                    <div class=\"col-4\">\n                        <instance-select [model]=\"collection\" [field]=\"'instanceId'\"></instance-select>\n                    </div>\n                    <div class=\"col-4\">\n                        <access-select [model]=\"collection\" [field]=\"'access'\"></access-select>\n                    </div>\n                </div>\n\n                <field-list [fields]=\"collection.fields\"></field-list>\n                \n                <button\n                    (click)=\"createFieldsFromMapping()\"\n                    class=\"btn btn-sm btn-success\">\n                    create fields by index mapping\n                </button>\n\n                <hr class=\"m-t-40\">\n\n                <div *ngIf=\"done === true\" class=\"alert alert-success\">Success!</div>\n                \n                <div class=\"row\">\n                    <div class=\"col-4\">\n                        <button\n                            (click)=\"submit()\"\n                            type=\"submit\"\n                            class=\"btn btn-success waves-effect waves-light m-r-10\">\n                            Save collection\n                        </button>\n                    </div>\n                    <div class=\"col-8 text-right\">\n                        <span *ngIf=\"collection.id == null\">\n                            <label class=\"custom-control custom-checkbox\">\n                                <input [(ngModel)]=\"collection.createIndex\" name=\"createIndex\" type=\"checkbox\" class=\"custom-control-input\"/>\n                                <span class=\"custom-control-indicator\"></span>\n                                <span class=\"custom-control-description\">Create index</span>\n                            </label>\n                        </span>\n    \n                        <span *ngIf=\"collection.id == null\">\n                            <label class=\"custom-control custom-checkbox\">\n                                <input [(ngModel)]=\"collection.createMapping\" name=\"createMapping\" type=\"checkbox\" class=\"custom-control-input\"/>\n                                <span class=\"custom-control-indicator\"></span>\n                                <span class=\"custom-control-description\">Create mapping</span>\n                            </label>\n                        </span>\n    \n                        <span *ngIf=\"collection.id != null\">\n                            <label class=\"custom-control custom-checkbox\">\n                                <input [(ngModel)]=\"collection.updateIndex\" name=\"updateIndex\" type=\"checkbox\" class=\"custom-control-input\"/>\n                                <span class=\"custom-control-indicator\"></span>\n                                <span class=\"custom-control-description\">Update index</span>\n                            </label>\n                        </span>\n                    </div>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/form/collection-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CollectionFormComponent = (function () {
    function CollectionFormComponent(route, collectionService, indexService, httpErrorService, configService) {
        this.route = route;
        this.collectionService = collectionService;
        this.indexService = indexService;
        this.httpErrorService = httpErrorService;
        this.configService = configService;
    }
    CollectionFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.types = this.configService.find('field-types');
        this.filters = this.configService.find('field-filters').map(function (v) { return { id: v, text: v }; });
        this.validators = this.configService.find('field-validators').map(function (v) { return { id: v, text: v }; });
        this.route.params.subscribe(function (params) {
            _this.initCrumbs(params.id);
            if (params.id != null) {
                _this.fetchCollection(params.id);
            }
            else {
                _this.initCollection();
            }
        });
    };
    CollectionFormComponent.prototype.fetchCollection = function (id) {
        var _this = this;
        this.collectionService.fetchById(id).subscribe(function (response) { return _this.collection = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionFormComponent.prototype.initCollection = function () {
        this.collection = {
            fields: [],
            settings: {
                detailButton: true,
                editButton: true,
                deleteButton: true
            }
        };
    };
    CollectionFormComponent.prototype.createFieldsFromMapping = function () {
        var _this = this;
        this.indexService.fetchMapping(this.collection.indexName, this.collection.indexType, this.collection.instanceId).subscribe(function (response) {
            var _loop_1 = function (name) {
                if (!response.hasOwnProperty(name)) {
                    return "continue";
                }
                var exists = _this.collection.fields.find(function (f) { return f.name == name; });
                if (!exists) {
                    _this.collection.fields.push({
                        name: name,
                        type: response[name].type
                    });
                }
            };
            for (var name in response) {
                _loop_1(name);
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionFormComponent.prototype.submit = function () {
        var _this = this;
        this.done = false;
        this.collectionService.save(this.collection).subscribe(function (response) { return _this.done = true; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionFormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New collection',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All collections', href: '/collections' },
                    { title: 'New collection' }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update collection',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All collections', href: '/collections' },
                    { title: 'Update collection' }
                ]
            };
        }
    };
    return CollectionFormComponent;
}());
CollectionFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/collection/form/collection-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_collection_service__["a" /* CollectionService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_index_service__["a" /* IndexService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_services_config_service__["a" /* ConfigService */]) === "function" && _e || Object])
], CollectionFormComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=collection-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/form/select/collection-select.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"collections\" class=\"form-group\">\n    <label>Collection</label>\n    <select [(ngModel)]=\"model\" name=\"model.collectionId\" class=\"custom-select col-12\">\n        <option *ngFor=\"let collection of collections\" [ngValue]=\"collection.id\">{{collection.name}}</option>\n    </select>\n</div>\n\n    "

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/form/select/collection-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CollectionSelectComponent = (function () {
    function CollectionSelectComponent(collectionService, httpErrorService) {
        this.collectionService = collectionService;
        this.httpErrorService = httpErrorService;
    }
    CollectionSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.collectionService.fetchAll().subscribe(function (response) { return _this.collections = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return CollectionSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CollectionSelectComponent.prototype, "model", void 0);
CollectionSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'collection-select',
        template: __webpack_require__("../../../../../src/app/crude/components/collection/form/select/collection-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_collection_service__["a" /* CollectionService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], CollectionSelectComponent);

var _a, _b;
//# sourceMappingURL=collection-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/list/collection-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"collections == null\"></preloader>\n\n<div *ngIf=\"collections != null && collections.length > 0\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">Collections</h3>\n                <h6 class=\"card-subtitle mb-2 text-muted\">\n                    Total: {{collections.length}}\n                </h6>\n\n                <table class=\"table\">\n                    <tr>\n                        <th>Name</th>\n                        <th>Health</th>\n                        <th>Status</th>\n                        <th class=\"text-center\">Count</th>\n                        <th class=\"text-center\">Deleted</th>\n                        <th class=\"text-center\">Size</th>\n                        <th width=\"1\"></th>\n                    </tr>\n                    <tr *ngFor=\"let collection of collections\">\n                        <td>\n                            <a [routerLink]=\"['/collections/' + collection.id + '/items']\">\n                                {{collection.name}}\n                            </a>\n                        </td>\n                        <td>\n                            <status-label\n                                *ngIf=\"collection.index != null\"\n                                [status]=\"collection.index['health']\">\n                            </status-label>\n                        </td>\n                        <td>\n                            <status-label\n                                *ngIf=\"collection.index != null\"\n                                [status]=\"collection.index['status']\">\n                            </status-label>\n                        </td>\n                        <td class=\"text-center\">\n                            <span *ngIf=\"collection.index != null\">{{collection.index['docs.count']}}</span>\n                        </td>\n                        <td class=\"text-center\">\n                            <span *ngIf=\"collection.index != null\">{{collection.index['docs.deleted']}}</span>\n                        </td>\n                        <td class=\"text-center\">\n                            <span *ngIf=\"collection.index != null\">\n                                {{collection.index['store.size']}}/{{collection.index['pri.store.size']}}\n                            </span>\n                        </td>\n                        <td style=\"white-space: nowrap\">\n                            <!-- <a [routerLink]=\"['/collections/' + collection.id]\"  class=\"btn btn-secondary btn-sm\">\n                                <i class=\"fa fa-eye\"></i>\n                            </a> -->\n                            <a\n                                [routerLink]=\"['/collections/update/' + collection.id]\"\n                                class=\"btn btn-info btn-sm\"\n                                href=\"#\">\n                                <i class=\"fa fa-edit\"></i>\n                            </a>\n                            \n                            <a\n                                href=\"http://{{collection.instance.host}}:{{collection.instance.port}}/{{collection.indexName}}/_search\"\n                                class=\"card-link btn btn-success btn-sm\">\n                                I\n                            </a>\n                           \n                            <a\n                                [routerLink]=\"['/instances/' + collection.instance.id + '/indices/' + collection.indexName + '/mapping']\"\n                                class=\"btn btn-success btn-sm\">\n                                M\n                            </a>\n    \n                            <a\n                                (click)=\"deleteCollection(collection)\"\n                                href=\"javascript:void(0)\"\n                                class=\"btn btn-danger btn-sm\">\n                                <i class=\"fa fa-trash\"></i>\n                           </a>\n                        </td>\n                    </tr>\n                </table>\n            </div>\n        </div>\n    </div>\n\n    <!--<div *ngFor=\"let collection of collections\" class=\"col-12\">-->\n        <!--<collection-card [collection]=\"collection\"></collection-card>-->\n    <!--</div>-->\n</div>\n\n<div *ngIf=\"collections != null && collections.length == 0\" class=\"col-12\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <div class=\"alert alert-info\">\n                You haven't collections yet.\n            </div>\n    \n            <p><a [routerLink]=\"['/collections/new']\" class=\"btn btn-success\">Create collection</a></p>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/list/collection-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CollectionListComponent = (function () {
    function CollectionListComponent(collectionService, httpErrorService, indexService) {
        this.collectionService = collectionService;
        this.httpErrorService = httpErrorService;
        this.indexService = indexService;
        this.crumbs = {
            title: 'All collections',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All collections' }
            ],
            buttons: [
                {
                    text: 'New collection',
                    route: '/collections/new',
                    class: 'btn btn-primary'
                }
            ]
        };
    }
    CollectionListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fetchCollections(function () { return _this.fetchIndices(); });
    };
    CollectionListComponent.prototype.fetchCollections = function (callback) {
        var _this = this;
        this.collectionService.fetchAll().subscribe(function (response) {
            _this.collections = response.sort(function (a, b) {
                if (a.name == b.name) {
                    return 0;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return -1;
            });
            callback();
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionListComponent.prototype.fetchIndices = function () {
        var _this = this;
        var instanceIds = Array.from(new Set(this.collections.map(function (c) { return c.instanceId; })));
        var _loop_1 = function (instanceId) {
            this_1.indexService.fetchAll({ instanceId: instanceId }).subscribe(function (response) {
                for (var _i = 0, response_1 = response; _i < response_1.length; _i++) {
                    var index = response_1[_i];
                    for (var _a = 0, _b = _this.collections; _a < _b.length; _a++) {
                        var collection = _b[_a];
                        if (collection.instanceId == instanceId && collection.indexName == index.index) {
                            collection.index = index;
                        }
                    }
                }
            }, function (error) { return _this.httpErrorService.handle(error, _this); });
        };
        var this_1 = this;
        for (var _i = 0, instanceIds_1 = instanceIds; _i < instanceIds_1.length; _i++) {
            var instanceId = instanceIds_1[_i];
            _loop_1(instanceId);
        }
    };
    CollectionListComponent.prototype.deleteCollection = function (collection) {
        var _this = this;
        var name = prompt('Enter collection name to confirm deleting:');
        if (name == collection.name) {
            this.collectionService.delete(collection.id).subscribe(function (response) { return _this.fetchCollections(function () { return _this.fetchIndices(); }); }, function (error) { return _this.httpErrorService.handle(error, _this); });
        }
    };
    return CollectionListComponent;
}());
CollectionListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/collection/list/collection-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_collection_service__["a" /* CollectionService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_index_service__["a" /* IndexService */]) === "function" && _c || Object])
], CollectionListComponent);

var _a, _b, _c;
//# sourceMappingURL=collection-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/view/collection-view.component.html":
/***/ (function(module, exports) {

module.exports = "<preloader [visible]=\"collection == null\"></preloader>\n\n<div *ngIf=\"collection != null\" class=\"row\">\n    <div class=\"col-12\">\n        <collection-card [collection]=\"collection\" [detail]=\"true\"></collection-card>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/collection/view/collection-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CollectionViewComponent = (function () {
    function CollectionViewComponent(route, indexService, httpErrorService, collectionService) {
        this.route = route;
        this.indexService = indexService;
        this.httpErrorService = httpErrorService;
        this.collectionService = collectionService;
        this.crumbs = {
            title: 'View collection',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All collections', href: '/collections' },
                { title: 'View collection' }
            ]
        };
    }
    CollectionViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.fetchCollection(params.id);
        });
    };
    CollectionViewComponent.prototype.fetchCollection = function (id) {
        var _this = this;
        this.collectionService.fetchById(id).subscribe(function (response) {
            _this.collection = response;
            _this.fetchIndex(_this.collection.indexName, _this.collection.instanceId);
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionViewComponent.prototype.fetchIndex = function (name, instanceId) {
        var _this = this;
        this.indexService.fetchByName(name, instanceId).subscribe(function (response) { return _this.collection.index = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    CollectionViewComponent.prototype.createIndex = function (name, instanceId) {
        var _this = this;
        var data = { name: name, instanceId: instanceId };
        this.indexService.create(data).subscribe(function (response) {
            if (response.error != null) {
                _this.collection.index.error = response.error;
            }
            else {
                _this.fetchIndex(name, instanceId);
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return CollectionViewComponent;
}());
CollectionViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/collection/view/collection-view.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_index_service__["a" /* IndexService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_collection_service__["a" /* CollectionService */]) === "function" && _d || Object])
], CollectionViewComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=collection-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/filter/bar/filter-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/crude/components/filter/bar/filter-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<ul *ngIf=\"filter != null\" class=\"list-inline font-14\">\n  <li style=\"padding-left: 0\">\n    <a (click)=\"openFilterFormModal()\" href=\"javascript:void(0)\">\n      <u>Filters</u><span *ngIf=\"filter.length > 0\">: </span>\n    </a>\n  </li>\n  <li *ngFor=\"let param of filter; let i = index\">\n    <span *ngIf=\"param.field != null && param.value != null\" class=\"badge badge-default\" style=\"padding: 4px !important;font-size: 12px\">\n        <span *ngIf=\"operLabels[param.oper] != null\">\n            <span *ngIf=\"param.oper == 'eq'\">\n                {{getFieldLabel(param.field)}}:\n            </span>\n\n            <span *ngIf=\"param.oper != 'eq'\">\n                {{getFieldLabel(param.field)}} {{operLabels[param.oper]}}\n            </span>\n        </span>\n\n        <span *ngIf=\"operLabels[param.oper] == null\">\n            {{getFieldLabel(param.field)}} {{param.oper}}\n        </span>\n\n        <span *ngIf=\"valueMap[param.field] == null\">\n            {{param.value}}\n        </span>\n        \n        <span *ngIf=\"valueMap[param.field] != null\">\n            {{valueMap[param.field][param.value] || param.value}}\n        </span>\n\n        <a (click)=\"deleteFilterParam(i)\" href=\"javascript:void(0)\" class=\"m-l-5\" style=\"border-left: 1px solid darkgrey;color: white;padding-left: 5px\">\n            <i class=\"fa fa-remove\"></i>\n        </a>\n    </span>\n  </li>\n</ul>\n\n<filter-form #filterFormComponent></filter-form>"

/***/ }),

/***/ "../../../../../src/app/crude/components/filter/bar/filter-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_filter_service__ = __webpack_require__("../../../../../src/app/crude/services/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterBarComponent = (function () {
    function FilterBarComponent(arrayService, fieldService, filterService) {
        this.arrayService = arrayService;
        this.fieldService = fieldService;
        this.filterService = filterService;
        this.valueMap = {};
    }
    FilterBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.operLabels = this.filterService.operLabels;
        this.filterService.form.subscribe(function (data) { return _this.filter = data; });
        this.fieldService.collectionFields.subscribe(function (data) {
            _this.fields = data;
            for (var _i = 0, _a = _this.fields; _i < _a.length; _i++) {
                var field = _a[_i];
                if (field.valueMap) {
                    _this.valueMap[field.name] = field.valueMap;
                }
            }
        });
    };
    FilterBarComponent.prototype.deleteFilterParam = function (index) {
        this.arrayService.deleteByIndex(this.filter, index);
        this.filterService.data.next(this.filter);
    };
    FilterBarComponent.prototype.openFilterFormModal = function () {
        this.filterFormComponent.openModal();
    };
    FilterBarComponent.prototype.getFieldLabel = function (name) {
        return this.fieldService.getFieldLabel(name, this.fields);
    };
    return FilterBarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filterFormComponent'),
    __metadata("design:type", Object)
], FilterBarComponent.prototype, "filterFormComponent", void 0);
FilterBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'filter-bar',
        template: __webpack_require__("../../../../../src/app/crude/components/filter/bar/filter-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/crude/components/filter/bar/filter-bar.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__["a" /* ArrayService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_field_service__["a" /* FieldService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_field_service__["a" /* FieldService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_filter_service__["a" /* FilterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_filter_service__["a" /* FilterService */]) === "function" && _c || Object])
], FilterBarComponent);

var _a, _b, _c;
//# sourceMappingURL=filter-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/filter/form/filter-form.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Filters</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <table class=\"table table-condensed table-fixed\">\n            <tr>\n                <th>Field</th>\n                <th>Operand</th>\n                <th>Value</th>\n                <th class=\"button-td\">\n                    <button (click)=\"addFilterParam()\" class=\"btn btn-sm btn-success\">\n                        <i class=\"fa fa-plus\"></i>\n                    </button>\n                </th>\n            </tr>\n            <tr *ngFor=\"let filter of form; let i = index\">\n                <td>\n                    <field-select [fields]=\"fields\" [model]=\"filter\" [field]=\"'field'\"></field-select>\n                </td>\n                <td>\n                    <select [(ngModel)]=\"filter.oper\" name=\"filter_oper_{{i}}\" class=\"form-control\">\n                        <option value=\"eq\">equals</option>\n                        <option value=\"not_eq\">not equals</option>\n                        \n                        <option *ngIf=\"['float', 'long'].includes(types[filter.field])\" value=\"gt\">greater</option>\n                        <option *ngIf=\"['float', 'long'].includes(types[filter.field])\" value=\"gte\">greater or equals</option>\n                        <option *ngIf=\"['float', 'long'].includes(types[filter.field])\" value=\"lt\">lower</option>\n                        <option *ngIf=\"['float', 'long'].includes(types[filter.field])\" value=\"lte\">lower or equals</option>\n                    </select>\n                </td>\n                <td>\n                    <span *ngIf=\"valueMap[filter.field] != null\">\n                        <select [(ngModel)]=\"filter.value\" name=\"filter_value_{{i}}\" class=\"form-control\">\n                            <option *ngFor=\"let data of valueMap[filter.field] | mapToIterable\" value=\"{{data.key}}\">\n                                {{data.value}}\n                            </option>\n                        </select>\n                    </span>\n                    \n                    <span *ngIf=\"valueMap[filter.field] == null\">\n                        <input [(ngModel)]=\"filter.value\" name=\"filter_value_{{i}}\"  type=\"text\" class=\"form-control\" />\n                    </span>\n                </td>\n                <td>\n                    <button (click)=\"deleteFilterParam(i)\" class=\"btn btn-sm btn-danger\">\n                        <i class=\"fa fa-minus\"></i>\n                    </button>\n                </td>\n            </tr>\n        </table>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success waves-effect waves-light\" (click)=\"submit(); c()\">Apply filter</button>\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\n    </div>\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/crude/components/filter/form/filter-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_filter_service__ = __webpack_require__("../../../../../src/app/crude/services/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FilterFormComponent = (function () {
    function FilterFormComponent(modalService, arrayService, fieldService, filterService) {
        this.modalService = modalService;
        this.arrayService = arrayService;
        this.fieldService = fieldService;
        this.filterService = filterService;
        this.labels = {};
        this.types = {};
        this.valueMap = {};
    }
    FilterFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.filterService.form.subscribe(function (data) { return _this.form = data; });
        this.fieldService.collectionFields.subscribe(function (data) {
            _this.fields = data;
            for (var _i = 0, _a = _this.fields; _i < _a.length; _i++) {
                var field = _a[_i];
                _this.labels[field.name] = field.label;
                _this.types[field.name] = field.type;
                if (field.valueMap) {
                    _this.valueMap[field.name] = field.valueMap;
                }
            }
        });
    };
    FilterFormComponent.prototype.openModal = function () {
        this.modalService.open(this.modal).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    FilterFormComponent.prototype.addFilterParam = function () {
        this.form.push({
            field: null,
            oper: 'eq',
            value: null
        });
    };
    FilterFormComponent.prototype.deleteFilterParam = function (index) {
        this.arrayService.deleteByIndex(this.form, index);
    };
    FilterFormComponent.prototype.submit = function () {
        this.form = this.form.filter(function (f) { return f.field != null && f.oper != null && f.value != null; });
        console.log('post filter', this.form);
        this.filterService.data.next(this.form);
    };
    return FilterFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modal'),
    __metadata("design:type", Object)
], FilterFormComponent.prototype, "modal", void 0);
FilterFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'filter-form',
        template: __webpack_require__("../../../../../src/app/crude/components/filter/form/filter-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_field_service__["a" /* FieldService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_field_service__["a" /* FieldService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_filter_service__["a" /* FilterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_filter_service__["a" /* FilterService */]) === "function" && _d || Object])
], FilterFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=filter-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/form/aggregation-type-select/aggregation-type-select.component.html":
/***/ (function(module, exports) {

module.exports = "<select [(ngModel)]=\"model[field]\" name=\"aggregation-type\" class=\"col-12 form-control\">\n    <option *ngFor=\"let type of types\" [ngValue]=\"type.value\">{{type.text}}</option>\n</select>"

/***/ }),

/***/ "../../../../../src/app/crude/components/form/aggregation-type-select/aggregation-type-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_aggregation_service__ = __webpack_require__("../../../../../src/app/crude/services/aggregation.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AggregationTypeSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AggregationTypeSelectComponent = (function () {
    function AggregationTypeSelectComponent(aggregationService) {
        this.aggregationService = aggregationService;
    }
    AggregationTypeSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.types = this.aggregationService.getTypes().filter(function (t) { return (_this.only == null || _this.only.includes(t.value)); });
    };
    return AggregationTypeSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AggregationTypeSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AggregationTypeSelectComponent.prototype, "field", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AggregationTypeSelectComponent.prototype, "only", void 0);
AggregationTypeSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'aggregation-type-select',
        template: __webpack_require__("../../../../../src/app/crude/components/form/aggregation-type-select/aggregation-type-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_aggregation_service__["a" /* AggregationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_aggregation_service__["a" /* AggregationService */]) === "function" && _a || Object])
], AggregationTypeSelectComponent);

var _a;
//# sourceMappingURL=aggregation-type-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/form/date-format-select/date-format-select.component.html":
/***/ (function(module, exports) {

module.exports = "<select [(ngModel)]=\"model[field]\" (change)=\"onChange()\" name=\"{{model}}_date_format\" class=\"col-12 form-control\">\n    <option *ngFor=\"let value of values\" value=\"{{value}}\">\n        {{value}}\n    </option>\n</select>\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/form/date-format-select/date-format-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateFormatSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DateFormatSelectComponent = (function () {
    function DateFormatSelectComponent() {
        this.values = ['dd.MM', 'dd.MM.YY'];
    }
    DateFormatSelectComponent.prototype.ngOnInit = function () {
    };
    DateFormatSelectComponent.prototype.onChange = function () {
    };
    return DateFormatSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], DateFormatSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], DateFormatSelectComponent.prototype, "field", void 0);
DateFormatSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'date-format-select',
        template: __webpack_require__("../../../../../src/app/crude/components/form/date-format-select/date-format-select.component.html")
    })
], DateFormatSelectComponent);

//# sourceMappingURL=date-format-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/form/interval-select/interval-select.component.html":
/***/ (function(module, exports) {

module.exports = "<select [(ngModel)]=\"model[field]\" (change)=\"onChange()\" name=\"{{model}}_interval\" class=\"col-12 form-control\">\n    <option *ngFor=\"let value of values\" value=\"{{value}}\">\n        {{value}}\n    </option>\n</select>\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/form/interval-select/interval-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntervalSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IntervalSelectComponent = (function () {
    function IntervalSelectComponent() {
        this.values = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second'];
    }
    IntervalSelectComponent.prototype.ngOnInit = function () {
    };
    IntervalSelectComponent.prototype.onChange = function () {
    };
    return IntervalSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], IntervalSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], IntervalSelectComponent.prototype, "field", void 0);
IntervalSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'interval-select',
        template: __webpack_require__("../../../../../src/app/crude/components/form/interval-select/interval-select.component.html")
    })
], IntervalSelectComponent);

//# sourceMappingURL=interval-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/index/form/index-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"form == null\"></preloader>\n\n<div *ngIf=\"form != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <div class=\"form-group\">\n                    <label>Number of shards</label>\n                    <input [(ngModel)]=\"form.number_of_shards\" name=\"form.number_of_shards\" type=\"number\" class=\"form-control\"/>\n                </div>\n\n                <div class=\"form-group\">\n                    <label>Number of replicas</label>\n                    <input [(ngModel)]=\"form.number_of_replicas\" name=\"form.number_of_replicas\" type=\"number\" class=\"form-control\"/>\n                </div>\n\n                <button (click)=\"submit()\" type=\"submit\" class=\"btn btn-success\">\n                    Submit\n                </button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/index/form/index-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__instance_services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IndexFormComponent = (function () {
    function IndexFormComponent(route, instanceService, httpErrorService) {
        this.route = route;
        this.instanceService = instanceService;
        this.httpErrorService = httpErrorService;
    }
    IndexFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.instanceService.fetchById(params.id).subscribe(function (response) {
                _this.instance = response;
                _this.initCrumbs();
            }, function (error) { return _this.httpErrorService.handle(error, _this); });
        });
        this.initForm();
    };
    IndexFormComponent.prototype.initForm = function () {
        this.form = {
            number_of_shards: 1,
            number_of_replicas: 1
        };
    };
    IndexFormComponent.prototype.initCrumbs = function () {
        this.crumbs = {
            title: 'New instance index',
            links: [
                { title: 'Instances', href: '/instances' },
                { title: this.instance.name, href: '/instances/' + this.instance.id },
                { title: 'index form' }
            ]
        };
    };
    return IndexFormComponent;
}());
IndexFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/index/form/index-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__instance_services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__instance_services_instance_service__["a" /* InstanceService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], IndexFormComponent);

var _a, _b, _c;
//# sourceMappingURL=index-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/index/mapping/form/index-mapping-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"mapping == null\"></preloader>\n\n<div *ngIf=\"mapping != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <form class=\"form-horizontal\">\n                <div *ngIf=\"mapping.length == 0\" class=\"alert alert-info\">\n                    no mapping fields\n                </div>\n                \n                <div *ngFor=\"let field of mapping\" class=\"row\">\n                    <div class=\"col-12\">\n                        <div class=\"row\">\n                            <div class=\"col-2\">\n                                <div class=\"form-group\">\n                                    <label>Field</label>\n                                    <input [(ngModel)]=\"field.name\" name=\"field_name_{{field.name}}\"  type=\"text\" class=\"form-control\"/>\n                                </div>\n                            </div>\n                            <div class=\"col-2\">\n                                <div class=\"form-group\">\n                                    <label>Type</label>\n                                    <field-type-select [model]=\"field\"></field-type-select>\n                                </div>\n                            </div>\n                            <div class=\"col-2\">\n                                <div class=\"form-group\">\n                                    <label>Index</label>\n    \n                                    <select [(ngModel)]=\"field.index\" name=\"field_index_{{field.name}}\" class=\"form-control\">\n                                        <option></option>\n                                        <option value=\"analyzed\">analyzed</option>\n                                        <option value=\"not_analyzed\">not_analyzed</option>\n                                    </select>\n                                </div>\n                            </div>\n                            <div class=\"col-6\">\n                                <div class=\"form-group\">\n                                    <label>Keywords</label>\n                                    <input [(ngModel)]=\"field.fields\" type=\"text\" name=\"field_fields_{{field.name}}\" class=\"form-control\">\n                                </div>\n                            </div>\n                            <div class=\"col-12\">\n                                <div class=\"form-check\">\n                                    <label class=\"custom-control custom-checkbox\">\n                                        <input [(ngModel)]=\"field.fielddata\" name=\"field_fielddata_{{field.name}}\" type=\"checkbox\" value=\"true\" class=\"custom-control-input\"/>\n                                        <span class=\"custom-control-indicator\"></span>\n                                        <span class=\"custom-control-description\">Field data</span>\n                                    </label>\n                                </div>\n                            </div>\n                        </div>\n                        \n                        <hr/>\n                    </div>\n                </div>\n    \n                <div *ngIf=\"done === true\" class=\"alert alert-success\">Success!</div>\n                \n                <button (click)=\"submit()\" class=\"btn btn-success\">Update mapping</button>\n                <button (click)=\"addField()\" class=\"btn btn-secondary m-l-10\">Add field</button>\n            </form>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/index/mapping/form/index-mapping-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_mapping_service__ = __webpack_require__("../../../../../src/app/crude/services/mapping.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexMappingFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IndexMappingFormComponent = (function () {
    function IndexMappingFormComponent(route, httpErrorService, mappingService, indexService) {
        this.route = route;
        this.httpErrorService = httpErrorService;
        this.mappingService = mappingService;
        this.indexService = indexService;
        this.crumbs = {
            title: 'Collection index mapping',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'update mapping' }
            ]
        };
    }
    IndexMappingFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.instanceId = params.id;
            _this.indexName = params.name;
            _this.indexType = params.name;
            _this.indexService.fetchMapping(_this.indexName, _this.indexType, _this.instanceId).subscribe(function (response) { return _this.mapping = _this.mappingService.mappingToArray(response); }, function (error) { return _this.httpErrorService.handle(error, _this); });
        });
    };
    IndexMappingFormComponent.prototype.addField = function () {
        this.mapping.push({});
    };
    IndexMappingFormComponent.prototype.submit = function () {
        // console.log(
        //     'mapping',
        //     this.mapping,
        //     this.mappingService.mappingFromArray(this.mapping)
        // );
        var _this = this;
        var data = {
            instanceId: this.instanceId,
            indexName: this.indexName,
            indexType: this.indexType,
            mapping: this.mappingService.mappingFromArray(this.mapping)
        };
        this.done = false;
        this.indexService.updateMapping(data).subscribe(function (response) { return _this.done = true; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return IndexMappingFormComponent;
}());
IndexMappingFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/index/mapping/form/index-mapping-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_mapping_service__["a" /* MappingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_mapping_service__["a" /* MappingService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_index_service__["a" /* IndexService */]) === "function" && _d || Object])
], IndexMappingFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=index-mapping-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/index/mapping/view/index-mapping-view.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"mapping == null\"></preloader>\n\n<div *ngIf=\"mapping != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">Mapping</h3>\n            </div>\n            \n            <table class=\"table\">\n                <tr>\n                    <th>Field</th>\n                    <th>Type</th>\n                    <th>Keyword</th>\n                </tr>\n                \n                <tr *ngFor=\"let field of mapping\">\n                    <td>{{field.name}}</td>\n                    <td>{{field.type}}</td>\n                    <td>{{field.fields}}</td>\n                </tr>\n            </table>\n            \n            <a\n                [routerLink]=\"['/instances/' + instanceId + '/indices/' + indexName + '/mapping/update']\"\n                [queryParams]=\"\"\n                class=\"btn btn-primary\">\n                Edit mapping\n            </a>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/index/mapping/view/index-mapping-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_mapping_service__ = __webpack_require__("../../../../../src/app/crude/services/mapping.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexMappingViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IndexMappingViewComponent = (function () {
    function IndexMappingViewComponent(route, httpErrorService, mappingService, indexService) {
        this.route = route;
        this.httpErrorService = httpErrorService;
        this.mappingService = mappingService;
        this.indexService = indexService;
        this.crumbs = {
            title: 'Collection index mapping',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'index mapping' }
            ]
        };
    }
    IndexMappingViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.instanceId = params.id;
            _this.indexName = params.name;
            _this.indexType = params.name;
            _this.indexService.fetchMapping(_this.indexName, _this.indexType, _this.instanceId).subscribe(function (response) { return _this.mapping = _this.mappingService.mappingToArray(response); }, function (error) { return _this.httpErrorService.handle(error, _this); });
        });
    };
    return IndexMappingViewComponent;
}());
IndexMappingViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'index-mapping-view',
        template: __webpack_require__("../../../../../src/app/crude/components/index/mapping/view/index-mapping-view.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_mapping_service__["a" /* MappingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_mapping_service__["a" /* MappingService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_index_service__["a" /* IndexService */]) === "function" && _d || Object])
], IndexMappingViewComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=index-mapping-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/card/item-card.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!item.deleted\" class=\"card\">\n    <div class=\"card-block\">\n        <h3 *ngIf=\"detail\" class=\"card-title\">Item ID: {{item.id}}</h3>\n        \n        <div *ngFor=\"let field of fields\">\n            <div *ngIf=\"!field.hidden && (settings.showEmptyValues || item[field.name] != null)\">\n                <div class=\"font-light font-14\">\n                    <span class=\"font-bold\">{{field.label}}:</span>\n                    <br *ngIf=\"settings.valuesOnNextLine\" />\n                    <span fieldValue [field]=\"field\" [item]=\"item\"></span>\n                </div>\n            </div>\n        </div>\n\n        <hr\n            *ngIf=\"settings.showDetailButton || settings.showEditButton || settings.showDeleteButton\"\n            class=\"m-t-15 m-b-15\"\n        />\n\n        <a\n            *ngIf=\"settings.showDetailButton\"\n            [routerLink]=\"['/collections/' + collection.id + '/items/' + encodeURIComponent(item.id)]\"\n            class=\"card-link btn btn-secondary btn-sm\">\n            view\n        </a>\n        \n        <a\n            *ngIf=\"settings.showEditButton\"\n            [routerLink]=\"['/collections/' + collection.id + '/items/update/' + encodeURIComponent(item.id)]\"\n            [queryParams]=\"{collectionId: collection.id}\"\n            class=\"card-link btn btn-info btn-sm\">\n            update\n        </a>\n\n        <button\n            *ngIf=\"settings.showDeleteButton\"\n            (click)=\"deleteItem(item)\"\n            class=\"btn btn-danger card-link btn-sm\">\n            Delete\n        </button>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/card/item-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemCardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ItemCardComponent = (function () {
    function ItemCardComponent(itemService, httpErrorService) {
        this.itemService = itemService;
        this.httpErrorService = httpErrorService;
    }
    ItemCardComponent.prototype.deleteItem = function (item) {
        var _this = this;
        if (!confirm('Delete item with id "' + item.id + '" ?')) {
            return;
        }
        this.itemService.delete(item.id, { collectionId: this.collection.id }).subscribe(function (response) { return item.deleted = true; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ItemCardComponent.prototype.getEsUrl = function () {
        return 'http://' + this.collection.instance.host + ':' + this.collection.instance.port + '/' +
            this.collection.indexName + '/' + this.collection.indexName + '/' + encodeURIComponent(this.item.id);
    };
    ItemCardComponent.prototype.encodeURIComponent = function (str) {
        return encodeURIComponent(str);
    };
    return ItemCardComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ItemCardComponent.prototype, "item", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ItemCardComponent.prototype, "collection", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ItemCardComponent.prototype, "fields", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], ItemCardComponent.prototype, "detail", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ItemCardComponent.prototype, "settings", void 0);
ItemCardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'item-card',
        template: __webpack_require__("../../../../../src/app/crude/components/item/card/item-card.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_item_service__["a" /* ItemService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], ItemCardComponent);

var _a, _b;
//# sourceMappingURL=item-card.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/form/field-form.component.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"form-horizontal\">\n    <div class=\"form-group\">\n        <label>Name</label>\n        <input [(ngModel)]=\"form.name\" name=\"field_name\" type=\"text\" class=\"form-control\" />\n    </div>\n    \n    <div class=\"form-group\">\n        <label>Label</label>\n        <input [(ngModel)]=\"form.label\" name=\"field_label\" type=\"text\" class=\"form-control\" />\n    </div>\n    \n    <div class=\"form-group\">\n        <label>Type</label>\n        <field-type-select [model]=\"form\"></field-type-select>\n    </div>\n\n    <div *ngIf=\"validators != null\" class=\"form-group\">\n        <label>Validators</label>\n        <select2\n                width=\"100%\"\n                (valueChanged)=\"validatorsChanged($event, i)\"\n                [data]=\"validators\"\n                [value]=\"form.validatorNames\"\n                [options]=\"{multiple:true}\"\n                ngDefaultControl>\n        </select2>\n    </div>\n    \n    <div *ngIf=\"form.validators != null && form.validators.length > 0\">\n        <div *ngFor=\"let validator of form.validators\" >\n            <div *ngIf=\"validator.params != null && validator.params.length > 0\">\n                <div *ngFor=\"let param of validator.params;let index = index;\" class=\"form-group\">\n                    <label>{{param.label}}</label>\n                    \n                    <input *ngIf=\"param.type == 'string'\" [(ngModel)]=\"param.value\" name=\"validator_param_{{param.name}}\" type=\"text\" class=\"form-control\" />\n                    <input *ngIf=\"param.type == 'number'\" [(ngModel)]=\"param.value\" name=\"validator_param_{{param.name}}\" type=\"number\" class=\"form-control\" />\n                    \n                    <select2\n                        *ngIf=\"param.type == 'array'\"\n                        width=\"100%\"\n                        (valueChanged)=\"valuesChanged($event, param)\"\n                        [data]=\"param.value\"\n                        [value]=\"param.value\"\n                        [options]=\"{multiple: true, tags: true}\"\n                        ngDefaultControl>\n                    </select2>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div *ngIf=\"filters != null\" class=\"form-group\">\n        <label>Filters</label>\n        <select2\n                width=\"100%\"\n                (valueChanged)=\"filtersChanged($event)\"\n                [data]=\"filters\"\n                [value]=\"form.filters\"\n                [options]=\"{multiple:true}\"\n                ngDefaultControl>\n        </select2>\n    </div>\n    \n    <div *ngIf=\"dics != null\" class=\"form-group\">\n        <label>Value map</label>\n        \n        <select [(ngModel)]=\"form.valueMap\" name=\"field-value-map\" class=\"form-control\">\n            <option *ngFor=\"let dic of dics\" value=\"{{dic.key}}\">\n                {{dic.name}}\n            </option>\n        </select>\n    </div>\n    \n    <div class=\"form-group\">\n        <div class=\"checkbox\">\n            <input [(ngModel)]=\"form.sortable\" name=\"field_sortable\" id=\"field_sortable\" type=\"checkbox\">\n            <label for=\"field_sortable\">Sortable</label>\n        </div>\n    </div>\n    \n    <div class=\"form-group\">\n        <div class=\"checkbox\">\n            <input [(ngModel)]=\"form.multiple\" name=\"field_multiple\" id=\"field_multiple\" type=\"checkbox\">\n            <label for=\"field_multiple\">Multiple values</label>\n        </div>\n    </div>\n    \n    <button\n        (click)=\"saveField()\"\n        class=\"btn btn-success waves-effect waves-light m-r-10\">\n        Save field\n    </button>\n</form>\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/form/field-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionFieldFormComponent; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CollectionFieldFormComponent = (function () {
    function CollectionFieldFormComponent(configService) {
        this.configService = configService;
        this.emptyForm = {
            name: "",
            type: "string",
            validators: [],
            filters: [],
        };
    }
    CollectionFieldFormComponent.prototype.ngOnInit = function () {
        this.filters = this.configService.find('field-filters').map(function (filter) {
            return { id: filter, text: filter };
        });
        this.validators = this.configService.find('field-validators').map(function (validator) {
            return {
                id: validator.name,
                text: validator.name,
                params: validator.params
            };
        });
        this.initForm();
    };
    CollectionFieldFormComponent.prototype.initForm = function () {
        this.dics = this.configService.getDictionaries();
        if (this.index == null) {
            this.form = this.emptyForm;
        }
        else {
            this.form = this.serializeFieldToForm(this.fields[this.index], this.validators);
        }
    };
    CollectionFieldFormComponent.prototype.serializeFieldToForm = function (field, allValidators) {
        var form = __assign({}, field);
        if (form.validators != null) {
            form.validatorNames = form.validators.map(function (v) { return v.name; });
        }
        return form;
    };
    CollectionFieldFormComponent.prototype.serializeFormToField = function (form) {
        var field = __assign({}, form);
        delete field.validatorNames;
        delete field.values; //@TODO: delete this later
        delete field.props; //@TODO: delete this later
        return field;
    };
    CollectionFieldFormComponent.prototype.addParamsToValidators = function (fieldValidators, allValidators) {
        var _loop_1 = function (fieldValidator) {
            var validator = allValidators.find(function (v) { return v.id == fieldValidator.name; });
            if (validator.params.length > 0) {
                fieldValidator.params = validator.params;
            }
        };
        for (var _i = 0, fieldValidators_1 = fieldValidators; _i < fieldValidators_1.length; _i++) {
            var fieldValidator = fieldValidators_1[_i];
            _loop_1(fieldValidator);
        }
        return fieldValidators;
    };
    CollectionFieldFormComponent.prototype.validatorsChanged = function (event) {
        if (this.form.validators == null) {
            this.form.validators = [];
        }
        var _loop_2 = function (validatorName) {
            var exists = this_1.form.validators.find(function (v) { return v.name == validatorName; });
            if (exists == null) {
                var validator = this_1.validators.find(function (v) { return v.id == validatorName; });
                this_1.form.validators.push({
                    name: validator.id,
                    params: validator.params
                });
            }
        };
        var this_1 = this;
        for (var _i = 0, _a = event.value; _i < _a.length; _i++) {
            var validatorName = _a[_i];
            _loop_2(validatorName);
        }
        this.form.validators = this.form.validators.filter(function (v) { return event.value.includes(v.name); });
    };
    CollectionFieldFormComponent.prototype.valuesChanged = function (event, param) {
        param.value = event.value;
    };
    CollectionFieldFormComponent.prototype.filtersChanged = function (event) {
        this.form.filters = event.value;
    };
    CollectionFieldFormComponent.prototype.saveField = function () {
        var field = this.serializeFormToField(this.form);
        if (this.index == null) {
            this.fields.push(field);
        }
        else {
            this.fields[this.index] = field;
        }
        if (this.onFinish != null) {
            this.onFinish();
        }
    };
    return CollectionFieldFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CollectionFieldFormComponent.prototype, "fields", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], CollectionFieldFormComponent.prototype, "index", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], CollectionFieldFormComponent.prototype, "onFinish", void 0);
CollectionFieldFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'collection-field-form',
        template: __webpack_require__("../../../../../src/app/crude/components/item/field/form/field-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], CollectionFieldFormComponent);

var _a;
//# sourceMappingURL=field-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/form/select/field-select.component.html":
/***/ (function(module, exports) {

module.exports = "<select\n    *ngIf=\"fields != null && fields.length > 0\"\n    [(ngModel)]=\"model[field]\"\n    name=\"field_{{field}}\"\n    class=\"form-control\">\n\n    <option *ngFor=\"let field of visibleFields\" value=\"{{field.name}}\">\n        {{field.label}}\n    </option>\n</select>\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/form/select/field-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FieldSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FieldSelectComponent = (function () {
    function FieldSelectComponent() {
        this.field = 'field';
        this.sortable = false;
        this.setSingleValue = false;
        this.visibleFields = [];
    }
    FieldSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.visibleFields = this.fields.filter(function (f) { return !f.hidden; });
        if (this.sortable) {
            this.visibleFields = this.visibleFields.filter(function (f) { return f.sortable; });
        }
        if (this.type) {
            this.visibleFields = this.visibleFields.filter(function (f) { return f.type == _this.type; });
        }
        if (this.visibleFields.length > 1) {
            this.sortFields();
        }
        else {
            if (this.visibleFields.length == 1 && this.setSingleValue) {
                this.model[this.field] = this.visibleFields[0].name;
            }
        }
    };
    FieldSelectComponent.prototype.sortFields = function () {
        this.visibleFields.sort(function (a, b) {
            if (a.label < b.label) {
                return -1;
            }
            if (a.label > b.label) {
                return 1;
            }
            return 0;
        });
    };
    return FieldSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Array)
], FieldSelectComponent.prototype, "fields", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FieldSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], FieldSelectComponent.prototype, "field", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FieldSelectComponent.prototype, "type", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], FieldSelectComponent.prototype, "sortable", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], FieldSelectComponent.prototype, "setSingleValue", void 0);
FieldSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'field-select',
        template: __webpack_require__("../../../../../src/app/crude/components/item/field/form/select/field-select.component.html")
    })
], FieldSelectComponent);

//# sourceMappingURL=field-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/form/type-select/field-type-select.component.html":
/***/ (function(module, exports) {

module.exports = "<select\n    [(ngModel)]=\"model[field]\"\n    name=\"field_type\"\n    class=\"form-control\">\n    \n    <option *ngFor=\"let type of types\" value=\"{{type}}\">{{type}}</option>\n</select>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/form/type-select/field-type-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FieldTypeSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FieldTypeSelectComponent = (function () {
    function FieldTypeSelectComponent(configService) {
        this.configService = configService;
        this.field = 'type';
    }
    FieldTypeSelectComponent.prototype.ngOnInit = function () {
        this.types = this.configService.find('field-types');
    };
    return FieldTypeSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FieldTypeSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], FieldTypeSelectComponent.prototype, "field", void 0);
FieldTypeSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'field-type-select',
        template: __webpack_require__("../../../../../src/app/crude/components/item/field/form/type-select/field-type-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], FieldTypeSelectComponent);

var _a;
//# sourceMappingURL=field-type-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/list/field-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row m-t-30\">\n    <div class=\"col-lg-6\">\n        <h4 class=\"box-title\">Collection item fields</h4>\n    </div>\n    <div class=\"col-lg-6 text-right\">\n        <button (click)=\"addField(field_form_modal)\" class=\"btn btn-sm btn-success\">\n            Add field\n        </button>\n\n        <div class=\"btn-group\">\n            <button type=\"button\" class=\"btn btn-info btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">\n                Template\n            </button>\n            <div class=\"dropdown-menu animated flipInX\">\n                <a class=\"dropdown-item\" href=\"\">empty</a>\n            </div>\n        </div>\n    </div>\n</div>\n\n<hr class=\"m-t-0 m-b-20\">\n\n<table *ngIf=\"fields != null && fields.length > 0\" class=\"table table-condensed\">\n    <tr>\n        <th>Name</th>\n        <th>Type</th>\n        <th width=\"1\">\n            <i class=\"fa fa-sort-amount-asc\"></i>\n        </th>\n        <th>Validators, Filters</th>\n        <th></th>\n    </tr>\n    <tr *ngFor=\"let field of fields; let index = index\">\n        <td>\n            {{field.name}}\n        </td>\n        <td>\n            {{field.type}}\n        </td>\n        <td>\n            <i *ngIf=\"field.sortable\" class=\"fa fa-check\"></i>          \n        </td>\n        <td>\n            <validator-label *ngFor=\"let validator of field.validators\" [validator]=\"validator\"></validator-label>\n\n            <span *ngFor=\"let filter of field.filters\" class=\"label label-inverse m-r-5\">\n                {{filter}}\n            </span>\n        </td>\n        <td class=\"text-right\">\n            <button (click)=\"editField(field_form_modal, index)\" type=\"button\" class=\"btn btn-sm btn-info\"><i class=\"fa fa-edit\"></i> </button>\n            <button (click)=\"deleteField(field)\" type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-remove\"></i> </button>\n        </td>\n    </tr>\n</table>\n\n<div *ngIf=\"fields == null || fields.length == 0\" class=\"alert alert-info\">no fields yet</div>\n\n<ng-template #field_form_modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Field form</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <collection-field-form [index]=\"activeIndex\" [fields]=\"fields\" [onFinish]=\"c\"></collection-field-form>\n    </div>\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/list/field-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionFieldListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CollectionFieldListComponent = (function () {
    function CollectionFieldListComponent(arrayService, modalService) {
        this.arrayService = arrayService;
        this.modalService = modalService;
    }
    CollectionFieldListComponent.prototype.addField = function (content) {
        this.activeIndex = null;
        this.modalService.open(content);
    };
    CollectionFieldListComponent.prototype.deleteField = function (field) {
        this.arrayService.deleteElement(this.fields, field);
    };
    CollectionFieldListComponent.prototype.editField = function (content, index) {
        this.activeIndex = index;
        this.openFieldFormModal(content);
    };
    CollectionFieldListComponent.prototype.openFieldFormModal = function (content) {
        this.modalService.open(content);
    };
    return CollectionFieldListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Array)
], CollectionFieldListComponent.prototype, "fields", void 0);
CollectionFieldListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'field-list',
        template: __webpack_require__("../../../../../src/app/crude/components/item/field/list/field-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _b || Object])
], CollectionFieldListComponent);

var _a, _b;
//# sourceMappingURL=field-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/validator-label/validator-label.component.html":
/***/ (function(module, exports) {

module.exports = "<span class=\"label label-inverse m-r-5\">\n    <!-- {{validator.name}} <span *ngIf=\"validator.params.length > 0\" class=\"text-muted\">({{paramsText(validator.params)}})</span> -->\n</span>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/field/validator-label/validator-label.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidatorLabelComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidatorLabelComponent = (function () {
    function ValidatorLabelComponent() {
    }
    ValidatorLabelComponent.prototype.paramsText = function (params) {
        return params.map(function (p) {
            var value = p.value;
            if (p.name == 'range') {
                value = value.join(',');
                if (value.length > 15) {
                    value = value.substr(0, 15) + '...';
                }
            }
            return p.name + '=' + value;
        }).join(', ');
    };
    return ValidatorLabelComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ValidatorLabelComponent.prototype, "validator", void 0);
ValidatorLabelComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'validator-label',
        template: __webpack_require__("../../../../../src/app/crude/components/item/field/validator-label/validator-label.component.html")
    })
], ValidatorLabelComponent);

//# sourceMappingURL=validator-label.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/form/item-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"collection == null || item == null\"></preloader>\n\n<div *ngIf=\"collection != null && item != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <div *ngFor=\"let field of collection.fields\" class=\"form-group\">\n                    <label>{{field.label || field.name}}</label>\n                    \n                    <input\n                        *ngIf=\"(field.values == null || field.values.length == 0) && ['integer', 'long', 'short', 'byte', 'double'].includes(field.type)\"\n                        [(ngModel)]=\"item[field.name]\"\n                        name=\"{{field.name}}\"\n                        type=\"number\"\n                        class=\"form-control\"\n                    />\n    \n                    <input\n                        *ngIf=\"(field.values == null || field.values.length == 0) && field.type == 'text'\"\n                        [(ngModel)]=\"item[field.name]\"\n                        name=\"{{field.name}}\"\n                        type=\"text\"\n                        class=\"form-control\"\n                    />\n    \n                    <div *ngIf=\"(field.values != null && field.values.length > 0) && field.type == 'array'\">\n                        <div *ngFor=\"let value of field.values\" class=\"form-check\">\n                            <label class=\"custom-control custom-checkbox\">\n                                <input [(ngModel)]=\"field.name[value]\" name=\"{{field.name}}\" value=\"{{value}}\" type=\"checkbox\" class=\"custom-control-input\"/>\n                                <span class=\"custom-control-indicator\"></span>\n                                <span class=\"custom-control-description\">{{value}}</span>\n                            </label>\n                        </div>\n                    </div>\n    \n                    <div *ngIf=\"(field.values != null && field.values.length > 0) && field.type == 'text'\">\n                        <div *ngFor=\"let value of field.values\" class=\"form-check\">\n                            <label class=\"custom-control custom-radio\">\n                                <input [(ngModel)]=\"item[field.name]\" name=\"{{field.name}}\" value=\"{{value}}\" type=\"radio\" class=\"custom-control-input\"/>\n                                <span class=\"custom-control-indicator\"></span>\n                                <span class=\"custom-control-description\">{{value}}</span>\n                            </label>\n                        </div>\n                    </div>\n                </div>\n        \n                <button (click)=\"saveItem()\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">{{item.id != null ? 'Update item' : 'Create item'}}</button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/form/item-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ItemFormComponent = (function () {
    function ItemFormComponent(route, router, itemService, collectionService, httpErrorService) {
        this.route = route;
        this.router = router;
        this.itemService = itemService;
        this.collectionService = collectionService;
        this.httpErrorService = httpErrorService;
    }
    ItemFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.collectionId = params.collectionId;
            _this.fetchCollection();
            _this.initCrumbs(params.id);
            _this.initItem(params.id);
        });
    };
    ItemFormComponent.prototype.fetchItem = function (id) {
        var _this = this;
        this.itemService.fetchById(id, { collectonId: this.collectionId }).subscribe(function (response) { return _this.item = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ItemFormComponent.prototype.saveItem = function () {
        var _this = this;
        var data = Object.assign({ collectionId: this.collectionId }, this.item);
        this.itemService.create(data).subscribe(function (response) { return _this.router.navigate(['items/' + _this.collectionId + '/' + response.id]); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ItemFormComponent.prototype.onArrayElementValueChanged = function (event, field) {
        this.item[field.name] = event.value;
    };
    ItemFormComponent.prototype.fetchCollection = function () {
        var _this = this;
        this.collectionService.fetchById(this.collectionId).subscribe(function (response) { return _this.collection = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ItemFormComponent.prototype.fieldHasError = function (field) {
        if (this.errors == null) {
            return false;
        }
        for (var _i = 0, _a = this.errors; _i < _a.length; _i++) {
            var error = _a[_i];
            if (error.field === field.name) {
                return true;
            }
        }
        return false;
    };
    ItemFormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New item',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All items', href: '/collections/' + this.collectionId + '/items' },
                    { title: 'New item' }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update item',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All items', href: '/collections/' + this.collectionId + '/items/' + this.collectionId },
                    { title: 'Update item' }
                ]
            };
        }
    };
    ItemFormComponent.prototype.initItem = function (id) {
        if (id == null) {
            this.item = {};
        }
        else {
            this.fetchItem(id);
        }
    };
    return ItemFormComponent;
}());
ItemFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/item/form/item-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_item_service__["a" /* ItemService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_collection_service__["a" /* CollectionService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _e || Object])
], ItemFormComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=item-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/header/item-list-header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/header/item-list-header.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block p-b-0\">\n                <div class=\"row\">\n                    <div class=\"col-6\">\n                        <h6 *ngIf=\"pagination != null\">\n                            Shown:\n                            <span>{{pagination.count}}</span>\n\n                            <span class=\"m-l-10\">Total: {{pagination.total | number}}</span>\n                            <span class=\"m-l-10\">\n                                Size:\n                                <div class=\"btn-group\">\n                                    <button type=\"button\" class=\"btn btn-secondary dropdown-toggle btn-sm font-14\"\n                                            data-toggle=\"dropdown\" style=\"border: 0;box-shadow: none\">\n                                        {{pagination.limit}}\n                                    </button>\n                                    <div class=\"dropdown-menu\">\n                                        <a\n                                            *ngFor=\"let value of [10, 20, 30, 50, 100]\"\n                                            (click)=\"onLimitChangedHandler(value);\"\n                                            class=\"dropdown-item font-14\"\n                                            href=\"javascript:void(0)\">\n                                            {{value}}\n                                        </a>\n                                    </div>\n                                </div>\n                            </span>\n                        </h6>\n                    </div>\n\n                    <div class=\"col-6 text-right\">\n                        <button\n                            (click)=\"openChartFormModal()\"\n                            title=\"Visualize\"\n                            class=\"btn btn-primary btn-sm\">\n                            <i class=\"ti ti-bar-chart\"></i>\n                        </button>\n\n                        <div *ngIf=\"widgetTypes != null\" class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">\n                            <button *ngFor=\"let widget of widgetTypes\" (click)=\"onWidgetTypeChangedHandler(widget.type)\"\n                                    type=\"button\" [ngClass]=\"{'active': widget.type == widgetType}\"\n                                    class=\"btn btn-info btn-sm\">\n                                <i class=\"{{widget.class}}\"></i>\n                            </button>\n                        </div>\n\n                        <button\n                            (click)=\"openModal(export_modal)\"\n                            title=\"Export data\"\n                            class=\"btn btn-primary btn-sm\">\n                            <i class=\"ti ti-export\"></i>\n                        </button>\n\n                        <button\n                            (click)=\"openSettingsFormModal()\"\n                            title=\"Settings\"\n                            class=\"btn btn-success btn-sm\">\n                            <i class=\"ti ti-settings\"></i>\n                        </button>\n                    </div>\n                </div>\n                <div class=\"row m-t-10\">\n                    <div class=\"col-9\">\n                        <filter-bar></filter-bar>\n                    </div>\n                    <div class=\"col-3 text-right\">\n                        <sorting-bar></sorting-bar>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<ng-template #export_modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Items export</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <div class=\"form-group\">\n            <label>Format</label>\n            <select [(ngModel)]=\"exportFormat\" name=\"export_type\" class=\"form-control\">\n                <option value=\"csv\">csv</option>\n            </select>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success waves-effect waves-light\" (click)=\"exportItems()\">Export</button>\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\n    </div>\n</ng-template>\n\n<item-list-settings-form #settingsComponent></item-list-settings-form>\n\n<chart-form\n    #chartFormComponent\n    [id]=\"'item-list-' + collectionId\">\n</chart-form>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/header/item-list-header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemListHeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ItemListHeaderComponent = (function () {
    function ItemListHeaderComponent(modalService, itemService) {
        this.modalService = modalService;
        this.itemService = itemService;
        this.onWidgetTypeChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onLimitChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.widgetTypes = [
            { type: 'table', class: 'fa fa-table' },
            { type: 'list', class: 'fa fa-list' }
        ];
    }
    ItemListHeaderComponent.prototype.openModal = function (content) {
        this.modalService.open(content, { windowClass: 'modal-lg' }).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    ItemListHeaderComponent.prototype.openSettingsFormModal = function () {
        this.settingsComponent.openModal();
    };
    ItemListHeaderComponent.prototype.openChartFormModal = function () {
        this.chartFormComponent.openModal();
    };
    ItemListHeaderComponent.prototype.exportItems = function () {
        if (this.exportFormat == null) {
            return;
        }
        var data = {
            collectionId: this.collectionId,
            format: this.exportFormat,
            limit: 10000
        };
        location.href = this.itemService.getFetchAllUrl(data);
    };
    ItemListHeaderComponent.prototype.onWidgetTypeChangedHandler = function (type) {
        this.onWidgetTypeChanged.emit(type);
    };
    ItemListHeaderComponent.prototype.onLimitChangedHandler = function (value) {
        this.onLimitChanged.emit(value);
    };
    return ItemListHeaderComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ItemListHeaderComponent.prototype, "pagination", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], ItemListHeaderComponent.prototype, "chart", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], ItemListHeaderComponent.prototype, "collectionId", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], ItemListHeaderComponent.prototype, "onWidgetTypeChanged", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], ItemListHeaderComponent.prototype, "onLimitChanged", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('settingsComponent'),
    __metadata("design:type", Object)
], ItemListHeaderComponent.prototype, "settingsComponent", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('chartFormComponent'),
    __metadata("design:type", Object)
], ItemListHeaderComponent.prototype, "chartFormComponent", void 0);
ItemListHeaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'item-list-header',
        template: __webpack_require__("../../../../../src/app/crude/components/item/list/header/item-list-header.component.html"),
        styles: [__webpack_require__("../../../../../src/app/crude/components/item/list/header/item-list-header.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_item_service__["a" /* ItemService */]) === "function" && _b || Object])
], ItemListHeaderComponent);

var _a, _b;
//# sourceMappingURL=item-list-header.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/item-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<item-list-header\n    *ngIf=\"collection != null\"\n    [pagination]=\"pagination\"\n    [collectionId]=\"collection.id\"\n    (onWidgetTypeChanged)=\"onWidgetTypeChangedHandler($event)\"\n    (onLimitChanged)=\"onLimitChangedHandler($event)\">\n</item-list-header>\n\n<preloader [visible]=\"collection == null || items == null\"></preloader>\n\n<div [hidden]=\"!chartVisible\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <button type=\"button\" class=\"close\" (click)=\"hideChart()\" style=\"position: absolute;right: 10px;top: 4px;\">\n                <span aria-hidden=\"true\">&times;</span>\n            </button>\n\n            <div class=\"card-block p-b-0\">\n                <widget-frame [subscribe]=\"true\"></widget-frame>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"items != null && items.length > 0 && collection != null && widgetType == 'list'\" class=\"row\">\n    <div *ngFor=\"let item of items\" class=\"col-12\">\n        <item-card\n            [item]=\"item\"\n            [fields]=\"fields\"\n            [collection]=\"collection\"\n            [settings]=\"settings\">\n        </item-card>\n    </div>\n</div>\n\n<div *ngIf=\"collection != null  && items != null && items.length > 0 && widgetType == 'table'\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <div class=\"table-responsive\">\n                    <table class=\"table\">\n                        <tr>\n                            <th *ngFor=\"let field of fields | notHidden\">\n                                <a *ngIf=\"field.sortable\"\n                                   (click)=\"toggleSortOrder(field)\"\n                                   href=\"javascript:void(0)\"\n                                   style=\"white-space: nowrap\">\n                    \n                                    {{field.label}}\n                    \n                                    <i\n                                        [ngClass]=\"{\n                                        'fa-sort'     : getFieldSortOrder(field) == null,\n                                        'fa-sort-up'  : getFieldSortOrder(field) == 'asc',\n                                        'fa-sort-down': getFieldSortOrder(field) == 'desc'\n                                    }\"\n                                        class=\"fa m-l-5\">\n                                    </i>\n                                </a>\n                \n                                <span *ngIf=\"!field.sortable\">\n                                {{field.label}}\n                            </span>\n                            </th>\n                        </tr>\n        \n                        <tr *ngFor=\"let item of items\">\n                            <td *ngFor=\"let field of fields | notHidden\">\n                                <span fieldValue [field]=\"field\" [item]=\"item\"></span>\n                            </td>\n                        </tr>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"items != null\" class=\"row\">\n    <div class=\"col-12 text-center\">\n        <div *ngIf=\"pagination.type == 'more-button' && canPaginate()\">\n            \n            <button\n                (click)=\"paginate()\"\n                type=\"button\"\n                class=\"btn btn-sm btn-rounded btn-secondary\">\n                show more\n            </button>\n        </div>\n\n        <div\n            *ngIf=\"pagination.type == 'infinite-scroll' && canPaginate()\"\n            class=\"search-results\"\n            infinite-scroll\n            [infiniteScrollDistance]=\"2\"\n            [infiniteScrollThrottle]=\"300\"\n            (scrolled)=\"paginate()\">\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"items != null && items.length == 0 && collection != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <div class=\"alert alert-info\">\n                    <p *ngIf=\"filter == null || filter.length == 0\">You haven't items yet.</p>\n                    <p *ngIf=\"filter != null || filter.length > 0\">No results found for your query</p>\n                </div>\n        \n                <p><a [routerLink]=\"['/collections/' + collection.id + '/items/new']\" class=\"btn btn-success\">Create item</a></p>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/item-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_state_service__ = __webpack_require__("../../../../../src/app/core/services/state.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_filter_service__ = __webpack_require__("../../../../../src/app/crude/services/filter.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_sorting_service__ = __webpack_require__("../../../../../src/app/crude/services/sorting.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_chart_service__ = __webpack_require__("../../../../../src/app/crude/services/chart.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_item_list_settings_service__ = __webpack_require__("../../../../../src/app/crude/services/item-list-settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var ItemListComponent = (function () {
    function ItemListComponent(route, modalService, arrayService, stateService, itemService, configService, collectionService, httpErrorService, filterService, fieldService, itemListSettingsService, sortingService, chartService) {
        this.route = route;
        this.modalService = modalService;
        this.arrayService = arrayService;
        this.stateService = stateService;
        this.itemService = itemService;
        this.configService = configService;
        this.collectionService = collectionService;
        this.httpErrorService = httpErrorService;
        this.filterService = filterService;
        this.fieldService = fieldService;
        this.itemListSettingsService = itemListSettingsService;
        this.sortingService = sortingService;
        this.chartService = chartService;
        this.filter = [];
        this.sorting = [];
        this.settings = {
            showEmptyValues: true,
            valuesOnNextLine: false,
            showDetailButton: true,
            showEditButton: true,
            showDeleteButton: true,
            showCreateButton: true
        };
        this.fields = [];
        this.widgetType = 'list';
        this.pagination = {
            type: 'more-button',
            limit: 20,
            total: 0,
            offset: 0,
            count: 0
        };
    }
    ItemListComponent.prototype.ngOnInit = function () {
        this.subscribeToRouteParams();
        this.subscribeToDataStreams();
    };
    ItemListComponent.prototype.subscribeToRouteParams = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.collectionId = params.collectionId;
            _this.chartVisible = null;
            _this.items = null;
            _this.stateService.init("item-list-" + _this.collectionId, _this);
            if (_this.pagination != null) {
                delete _this.pagination['offset'];
                delete _this.pagination['total'];
                delete _this.pagination['count'];
            }
            _this.fetchCollection(function () {
                _this.itemListSettingsService.form.next({
                    pagination: _this.pagination,
                    settings: _this.settings,
                    fields: _this.fields
                });
                _this.filterService.form.next(_this.filter);
                _this.fieldService.collectionFields.next(_this.fields);
                _this.initCrumbs();
                _this.loadItems({ paginate: false });
            });
        });
    };
    ItemListComponent.prototype.subscribeToDataStreams = function () {
        var _this = this;
        this.filterService.data.subscribe(function (data) {
            _this.setState({ filter: data }, true);
            if (_this.chart != null) {
                _this.chart.condition = data;
                _this.chartService.data.next(_this.chart);
            }
        });
        this.itemListSettingsService.data.subscribe(function (data) { return _this.setState(data, true); });
        this.sortingService.data.subscribe(function (data) { return _this.setState({ sorting: data }, true); });
        this.chartService.data.subscribe(function (data) {
            _this.chart = data;
            _this.chart.collectionId = _this.collection.id;
            _this.chart.condition = _this.filter;
            _this.chartService.frame.next(_this.chart);
            _this.chartVisible = true;
        });
    };
    ItemListComponent.prototype.initCrumbs = function () {
        this.crumbs = {
            title: this.collection.name,
            links: [
                { title: 'Collections', href: '/collections' },
                { title: this.collection.name }
            ],
            buttons: []
        };
        if (this.settings.showCreateButton) {
            this.crumbs.buttons.push({
                text: 'New item',
                route: '/collections/' + this.collectionId + '/items/new',
                class: 'btn btn-primary'
            });
        }
        if (this.configService.find('collection-' + this.collectionId + '-dashboard') != null) {
            this.crumbs.buttons.push({
                text: 'Dashboard',
                route: '/collections/' + this.collectionId + '/dashboard',
                class: 'btn btn-info'
            });
        }
    };
    ItemListComponent.prototype.hideChart = function () {
        this.chartVisible = false;
        this.chart = null;
    };
    ItemListComponent.prototype.onWidgetTypeChangedHandler = function (type) {
        this.setState({ widgetType: type }, true);
    };
    ItemListComponent.prototype.onLimitChangedHandler = function (limit) {
        this.pagination.limit = limit;
        this.setState({ pagination: this.pagination }, true);
    };
    ItemListComponent.prototype.getState = function () {
        return {
            fields: this.fields,
            filter: this.filter,
            sorting: this.sorting,
            settings: this.settings,
            widgetType: this.widgetType,
            pagination: this.pagination
        };
    };
    ItemListComponent.prototype.setState = function (state, loadItems) {
        if (loadItems === void 0) { loadItems = false; }
        this.stateService.setState(state);
        if (loadItems) {
            this.loadItems({ paginate: false });
        }
    };
    ItemListComponent.prototype.openModal = function (content) {
        this.modalService.open(content).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    ItemListComponent.prototype.getFetchParams = function (paginate) {
        if (paginate === void 0) { paginate = false; }
        var params = Object.assign({
            collectionId: this.collectionId,
            condition: this.filter,
            limit: this.pagination.limit
        });
        if (paginate) {
            this.pagination.offset = (this.pagination.offset || 0) + this.pagination.limit;
            params['offset'] = this.pagination.offset;
        }
        if (this.sorting.length > 0) {
            params['sorting'] = this.sorting;
        }
        var excludeFields = this.fields.filter(function (f) { return f.hidden; }).map(function (f) { return f.name; });
        var includeFields = this.fields.filter(function (f) { return !f.hidden; }).map(function (f) { return f.name; });
        if (excludeFields.length > 0 && excludeFields.length < includeFields.length) {
            params['excludeFields'] = excludeFields;
        }
        else if (includeFields.length > 0 && includeFields.length < this.fields.length) {
            params['includeFields'] = includeFields;
        }
        return params;
    };
    ItemListComponent.prototype.loadItems = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        if (this.loading) {
            console.log('return because already loading');
            return;
        }
        var params = this.getFetchParams(options.paginate);
        console.log('fetching items:', params);
        this.loading = true;
        this.itemService.fetchAll(params).subscribe(function (response) {
            _this.loading = false;
            if (options.paginate) {
                _this.items = _this.items.concat(response.items);
            }
            else {
                _this.items = response.items;
            }
            _this.pagination.total = response.total;
            _this.pagination.count = _this.items.length;
        }, function (error) {
            _this.loading = false;
            _this.httpErrorService.handle(error, _this);
        });
    };
    ItemListComponent.prototype.paginate = function () {
        this.loadItems({ paginate: true });
    };
    ItemListComponent.prototype.canPaginate = function () {
        return !this.loading && this.items.length < this.pagination.total;
    };
    ItemListComponent.prototype.getFieldSortOrder = function (field) {
        var param = this.sorting.find(function (p) { return p.field == field.name; });
        if (param) {
            return param.order;
        }
    };
    ItemListComponent.prototype.toggleSortOrder = function (field) {
        var order = this.getFieldSortOrder(field);
        if (order == null) {
            this.sorting = [{
                    field: field.name,
                    order: 'desc'
                }];
        }
        else {
            var index = this.sorting.findIndex(function (p) { return p.field == field.name; });
            this.sorting[index]['order'] = (order == 'asc' ? 'desc' : 'asc');
        }
        this.sortingService.data.next(this.sorting);
    };
    /**
     * TODO: refactoring
     *
     *   return new Promise(resolve => {
     *  Simulate server latency with 2 second delay
     * setTimeout(() => resolve(this.getHeroes()), 2000);
    *
     *
     * @param callback
     */
    ItemListComponent.prototype.fetchCollection = function (callback) {
        var _this = this;
        this.collectionService.fetchById(this.collectionId).subscribe(function (response) {
            _this.collection = response;
            _this.collectionService.fetchFields(_this.collection.id).subscribe(function (response) {
                _this.fields = response;
                console.log('fetched fields', _this.fields);
                callback();
            }, function (error) { return _this.httpErrorService.handle(error); });
        }, function (error) {
            _this.httpErrorService.handle(error, _this);
        });
    };
    return ItemListComponent;
}());
ItemListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/item/list/item-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_array_service__["a" /* ArrayService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_state_service__["a" /* StateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_state_service__["a" /* StateService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_item_service__["a" /* ItemService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_8__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__core_services_config_service__["a" /* ConfigService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_collection_service__["a" /* CollectionService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_7__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_9__services_filter_service__["a" /* FilterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__services_filter_service__["a" /* FilterService */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_13__services_field_service__["a" /* FieldService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_13__services_field_service__["a" /* FieldService */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_12__services_item_list_settings_service__["a" /* ItemListSettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_12__services_item_list_settings_service__["a" /* ItemListSettingsService */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_10__services_sorting_service__["a" /* SortingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__services_sorting_service__["a" /* SortingService */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_11__services_chart_service__["a" /* ChartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11__services_chart_service__["a" /* ChartService */]) === "function" && _o || Object])
], ItemListComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
//# sourceMappingURL=item-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/settings-form/item-list-settings-form.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".field-table {\n    display:table;\n    width: 100%;\n    border-collapse: collapse;\n}\n\n.field-cell {\n    padding: 0.50rem;\n    display: table-cell;\n}\n\n.field-row {\n    display: table-row;\n\n}\n\n.field-row:not(:first-child) {\n    border-top: 1px solid #eceeef;\n}\n\n.field-header-row {\n    font-weight: 500;\n    display: table-row;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/settings-form/item-list-settings-form.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">List settings</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <ul class=\"nav nav-tabs customtab\" role=\"tablist\">\n            <li class=\"nav-item\">\n                <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#tab-fields\" role=\"tab\">\n                    <span class=\"hidden-sm-up\"><i class=\"ti-home\"></i></span>\n                    <span class=\"hidden-xs-down\">Fields</span>\n                </a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-custom\" role=\"tab\">\n                    <span class=\"hidden-sm-up\"><i class=\"ti-user\"></i></span>\n                    <span class=\"hidden-xs-down\">Custom</span>\n                </a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-navigation\" role=\"tab\">\n                    <span class=\"hidden-sm-up\"><i class=\"ti-user\"></i></span>\n                    <span class=\"hidden-xs-down\">Navigation</span>\n                </a>\n            </li>\n        </ul>\n\n        <div *ngIf=\"form != null\" class=\"tab-content\">\n            <div class=\"tab-pane active p-20\" id=\"tab-fields\" role=\"tabpanel\">\n                <div class=\"field-table\" [sortablejs]=\"form.fields\" [sortablejsOptions]=\"sortableOptions\">\n                    <div class=\"field-header-row\">\n                        <div class=\"field-cell\">\n                            Name\n                        </div>\n                        <div class=\"field-cell\">\n                            View\n                        </div>\n                        <div *ngIf=\"positionArrows\" class=\"field-cell text-center\">\n                            Position\n                        </div>\n                        <div class=\"field-cell text-center\">\n                            Hidden\n                        </div>\n                    </div>\n\n                    <div *ngFor=\"let field of form.fields\" class=\"field-row\">\n                        <div [ngClass]=\"{'op-5' : field.hidden}\" class=\"field-cell\" style=\"cursor: pointer\">\n                            <i class=\"mdi mdi-drag\"></i> {{field.label || field.name}}\n                        </div>\n                        <div class=\"field-cell\">\n                            <select [(ngModel)]=\"field.view\" name=\"field-view\" class=\"form-control form-control-sm\">\n                                <optgroup label=\"Base\">\n                                    <option value=\"date_time\">date time</option>\n                                    <option value=\"date\">date</option>\n                                    <option value=\"currency\">currency</option>\n                                    <option value=\"short_link\">short link</option>\n                                    <option value=\"number\">number</option>\n                                </optgroup>\n                                <optgroup label=\"Dictionary\">\n                                    <option *ngFor=\"let dic of dics\" value=\"{{dic.key}}\">\n                                        {{dic.name}}\n                                    </option>\n                                </optgroup>\n                            </select>\n                        </div>\n                        <div *ngIf=\"positionArrows\" class=\"text-center field-cell\">\n                            <a (click)=\"changeFieldPosition(field, 'up')\" href=\"#\" class=\"m-r-10\">\n                                <i class=\"mdi  mdi-arrow-up-bold\"></i>\n                            </a>\n\n                            <a (click)=\"changeFieldPosition(field, 'down')\" href=\"#\">\n                                <i class=\"mdi  mdi-arrow-down-bold\"></i>\n                            </a>\n                        </div>\n                        <div class=\"field-cell text-center\">\n                            <label class=\"custom-control custom-checkbox\" style=\"margin-right: 0\">\n                                <input\n                                        [(ngModel)]=\"field.hidden\"\n                                        name=\"{{field.name}}_hidden\"\n                                        type=\"checkbox\"\n                                        class=\"custom-control-input\"/>\n                                <span class=\"custom-control-indicator\"></span>\n                            </label>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"tab-pane  p-20\" id=\"tab-custom\" role=\"tabpanel\">\n                <div class=\"form-check\">\n                    <label class=\"custom-control custom-checkbox\">\n                        <input [(ngModel)]=\"form.settings.showEmptyValues\" name=\"showEmptyValues\" type=\"checkbox\"\n                               class=\"custom-control-input\"/>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Show empty values</span>\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label class=\"custom-control custom-checkbox\">\n                        <input [(ngModel)]=\"form.settings.valuesOnNextLine\" name=\"valuesOnNextLine\" type=\"checkbox\"\n                               class=\"custom-control-input\"/>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Values on next line</span>\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label class=\"custom-control custom-checkbox\">\n                        <input [(ngModel)]=\"form.settings.showDetailButton\" name=\"showDetailButton\" type=\"checkbox\"\n                               value=\"true\" class=\"custom-control-input\"/>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Detail button enabled</span>\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label class=\"custom-control custom-checkbox\">\n                        <input [(ngModel)]=\"form.settings.showCreateButton\" name=\"showCreateButton\" type=\"checkbox\"\n                               value=\"true\" class=\"custom-control-input\"/>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Create button enabled</span>\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label class=\"custom-control custom-checkbox\">\n                        <input [(ngModel)]=\"form.settings.showEditButton\" name=\"showEditButton\" type=\"checkbox\"\n                               value=\"true\" class=\"custom-control-input\"/>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Edit button enabled</span>\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label class=\"custom-control custom-checkbox\">\n                        <input [(ngModel)]=\"form.settings.showDeleteButton\" name=\"showDeleteButton\" type=\"checkbox\"\n                               value=\"true\" class=\"custom-control-input\"/>\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Delete button enabled</span>\n                    </label>\n                </div>\n            </div>\n            <div class=\"tab-pane  p-20\" id=\"tab-navigation\" role=\"tabpanel\">\n                <input [(ngModel)]=\"form.pagination.type\" name=\"pagination\" type=\"radio\" id=\"more-button\"\n                       value=\"more-button\"/>\n                <label for=\"more-button\">More button</label>\n\n                <br/>\n\n                <input [(ngModel)]=\"form.pagination.type\" name=\"pagination\" type=\"radio\" id=\"infinite-scroll\"\n                       value=\"infinite-scroll\"/>\n                <label for=\"infinite-scroll\">Infinite scroll</label>\n            </div>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n        <button (click)=\"submit(); c('Apply click')\" type=\"button\" class=\"btn btn-success waves-effect waves-light\">\n            Apply\n        </button>\n    </div>\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/crude/components/item/list/settings-form/item-list-settings-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_item_list_settings_service__ = __webpack_require__("../../../../../src/app/crude/services/item-list-settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemListSettingsFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ItemListSettingsFormComponent = (function () {
    function ItemListSettingsFormComponent(arrayService, configService, itemListSettingsService, modalService) {
        this.arrayService = arrayService;
        this.configService = configService;
        this.itemListSettingsService = itemListSettingsService;
        this.modalService = modalService;
        this.positionArrows = false;
        this.sortableOptions = {
            animation: 150,
            draggable: ".field-row"
        };
    }
    ItemListSettingsFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dics = this.configService.getDictionaries();
        this.itemListSettingsService.form.subscribe(function (data) { return _this.form = data; });
    };
    ItemListSettingsFormComponent.prototype.submit = function () {
        this.itemListSettingsService.data.next(this.form);
    };
    ItemListSettingsFormComponent.prototype.changeFieldPosition = function (field, direction) {
        switch (direction) {
            case 'up':
                this.form.fields = this.arrayService.moveUp(this.form.fields, field);
                break;
            case 'down':
                this.form.fields = this.arrayService.moveDown(this.form.fields, field);
                break;
        }
        return false;
    };
    ItemListSettingsFormComponent.prototype.openModal = function () {
        this.modalService.open(this.modal).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    return ItemListSettingsFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modal'),
    __metadata("design:type", Object)
], ItemListSettingsFormComponent.prototype, "modal", void 0);
ItemListSettingsFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'item-list-settings-form',
        template: __webpack_require__("../../../../../src/app/crude/components/item/list/settings-form/item-list-settings-form.component.html"),
        styles: [__webpack_require__("../../../../../src/app/crude/components/item/list/settings-form/item-list-settings-form.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_array_service__["a" /* ArrayService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_config_service__["a" /* ConfigService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_item_list_settings_service__["a" /* ItemListSettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_item_list_settings_service__["a" /* ItemListSettingsService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _d || Object])
], ItemListSettingsFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=item-list-settings-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/item/view/item-view.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"item == null\"></preloader>\n\n<div *ngIf=\"item != null\" class=\"row\">\n    <div class=\"col-12\">\n        <item-card [item]=\"item\" [collection]=\"collection\" [detail]=\"true\"></item-card>\n    </div>\n</div>\n\n    "

/***/ }),

/***/ "../../../../../src/app/crude/components/item/view/item-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_collection_service__ = __webpack_require__("../../../../../src/app/crude/services/collection.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_item_service__ = __webpack_require__("../../../../../src/app/crude/services/item.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ItemViewComponent = (function () {
    function ItemViewComponent(route, httpErrorService, collectionService, itemService) {
        this.route = route;
        this.httpErrorService = httpErrorService;
        this.collectionService = collectionService;
        this.itemService = itemService;
    }
    ItemViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.fetchItem(params.id, params.collectionId);
            _this.fetchCollection(params.collectionId, function () { return _this.initCrumbs(); });
        });
    };
    ItemViewComponent.prototype.fetchItem = function (id, collectionId) {
        var _this = this;
        this.itemService.fetchById(id, { collectionId: collectionId }).subscribe(function (response) { return _this.item = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ItemViewComponent.prototype.fetchCollection = function (id, callback) {
        var _this = this;
        this.collectionService.fetchById(id).subscribe(function (response) {
            _this.collection = response;
            callback();
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    ItemViewComponent.prototype.initCrumbs = function () {
        this.crumbs = {
            title: 'View item',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All items', href: '/collections/' + this.collection.id + '/items/' },
                { title: 'View item' }
            ]
        };
    };
    return ItemViewComponent;
}());
ItemViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'item-view',
        template: __webpack_require__("../../../../../src/app/crude/components/item/view/item-view.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_collection_service__["a" /* CollectionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_collection_service__["a" /* CollectionService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_item_service__["a" /* ItemService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_item_service__["a" /* ItemService */]) === "function" && _d || Object])
], ItemViewComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=item-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/sorting/bar/sorting-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<ul *ngIf=\"sorting != null\" class=\"list-inline font-14\">\n    <li>\n        <a (click)=\"openSortingFormModal()\" href=\"javascript:void(0)\">\n            <u>Sort</u><span *ngIf=\"sorting.length > 0\">:</span>\n        </a>\n    </li>\n    <li *ngFor=\"let param of sorting; let i = index\">\n        <a (click)=\"toggleParamOrder(i)\" href=\"javascript:void(0)\">\n            {{getFieldLabel(param.field)}}\n            <i *ngIf=\"param.order == 'desc'\" class=\"mdi mdi-sort-descending\"></i>\n            <i *ngIf=\"param.order == 'asc'\" class=\"mdi mdi-sort-ascending\"></i>\n        </a>\n    </li>\n</ul>\n\n<sorting-form #sortingFormComponent></sorting-form>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/crude/components/sorting/bar/sorting-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_sorting_service__ = __webpack_require__("../../../../../src/app/crude/services/sorting.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SortingBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SortingBarComponent = (function () {
    function SortingBarComponent(sortingService, fieldService) {
        this.sortingService = sortingService;
        this.fieldService = fieldService;
        this.sorting = [];
    }
    SortingBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sortingService.data.subscribe(function (data) { return _this.sorting = data; });
        this.fieldService.collectionFields.subscribe(function (data) { return _this.fields = data; });
    };
    SortingBarComponent.prototype.openSortingFormModal = function () {
        this.sortingFormComponent.openModal();
    };
    SortingBarComponent.prototype.toggleParamOrder = function (index) {
        if (this.sorting[index]['order'] == 'asc') {
            this.sorting[index]['order'] = 'desc';
        }
        else {
            this.sorting[index]['order'] = 'asc';
        }
        this.sortingService.data.next(this.sorting);
    };
    SortingBarComponent.prototype.getFieldLabel = function (name) {
        return this.fieldService.getFieldLabel(name, this.fields);
    };
    return SortingBarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('sortingFormComponent'),
    __metadata("design:type", Object)
], SortingBarComponent.prototype, "sortingFormComponent", void 0);
SortingBarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sorting-bar',
        template: __webpack_require__("../../../../../src/app/crude/components/sorting/bar/sorting-bar.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_sorting_service__["a" /* SortingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_sorting_service__["a" /* SortingService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_field_service__["a" /* FieldService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_field_service__["a" /* FieldService */]) === "function" && _b || Object])
], SortingBarComponent);

var _a, _b;
//# sourceMappingURL=sorting-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/sorting/form/sorting-form.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Sorting</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <table class=\"table table-condensed table-fixed\">\n            <tr>\n                <th>Field</th>\n                <th>Order</th>\n                <th class=\"button-td\">\n                    <button (click)=\"addSortingParam()\" class=\"btn btn-sm btn-success\">\n                        <i class=\"fa fa-plus\"></i>\n                    </button>\n                </th>\n            </tr>\n            <tr *ngFor=\"let sorting of form; let i = index\">\n                <td>\n                    <field-select [fields]=\"fields\" [model]=\"sorting\" [sortable]=\"true\"></field-select>\n                </td>\n                <td>\n                    <select [(ngModel)]=\"sorting.order\" name=\"sort_order_{{i}}\" class=\"form-control\">\n                        <option value=\"asc\">asc</option>\n                        <option value=\"desc\">desc</option>\n                    </select>\n                </td>\n                <td>\n                    <button (click)=\"deleteSortingParam(i)\" class=\"btn btn-sm btn-danger\">\n                        <i class=\"fa fa-minus\"></i>\n                    </button>\n                </td>\n            </tr>\n        </table>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success waves-effect waves-light\" (click)=\"submit(); c()\">Apply sorting</button>\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\n    </div>\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/crude/components/sorting/form/sorting-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_sorting_service__ = __webpack_require__("../../../../../src/app/crude/services/sorting.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_field_service__ = __webpack_require__("../../../../../src/app/crude/services/field.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SortingFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SortingFormComponent = (function () {
    function SortingFormComponent(modalService, sortingService, fieldService, arrayService) {
        this.modalService = modalService;
        this.sortingService = sortingService;
        this.fieldService = fieldService;
        this.arrayService = arrayService;
        this.form = [];
    }
    SortingFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sortingService.form.subscribe(function (data) { return _this.form = data; });
        this.fieldService.collectionFields.subscribe(function (data) { return _this.fields = data; });
    };
    SortingFormComponent.prototype.openModal = function () {
        this.modalService.open(this.modal).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    SortingFormComponent.prototype.addSortingParam = function () {
        this.form.push({
            field: '',
            order: 'asc'
        });
    };
    SortingFormComponent.prototype.deleteSortingParam = function (index) {
        this.arrayService.deleteByIndex(this.form, index);
    };
    SortingFormComponent.prototype.submit = function () {
        this.form = this.form.filter(function (f) { return f.field != null && f.field != '' && f.order != null; });
        console.log('post sorting', this.form);
        this.sortingService.data.next(this.form);
    };
    return SortingFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modal'),
    __metadata("design:type", Object)
], SortingFormComponent.prototype, "modal", void 0);
SortingFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sorting-form',
        template: __webpack_require__("../../../../../src/app/crude/components/sorting/form/sorting-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_sorting_service__["a" /* SortingService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_sorting_service__["a" /* SortingService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_field_service__["a" /* FieldService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_field_service__["a" /* FieldService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_array_service__["a" /* ArrayService */]) === "function" && _d || Object])
], SortingFormComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=sorting-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/form/widget-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"form == null\"></preloader>\n\n<div *ngIf=\"form != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <h4 class=\"box-title\">Widget</h4>\n                <hr class=\"m-t-0 m-b-20\">\n                \n                <div class=\"form-group\">\n                    <label>Type</label>\n                    <select [(ngModel)]=\"form.value.selector\" name=\"form.value.selector\" class=\"custom-select col-12\">\n                        <option *ngFor=\"let widget of chartTypes\" [ngValue]=\"widget.selector\">{{widget.name}}</option>\n                    </select>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Title</label>\n                    <input [(ngModel)]=\"form.value.title\" name=\"form.value.title\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"form.value.selector == 'line-chart'\" class=\"form-group\">\n                    <label>Sub title</label>\n                    <input [(ngModel)]=\"form.value.subTitle\" name=\"form.value.subTitle\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"form.value.selector == 'bar-chart'\" class=\"form-group\">\n                    <label>Axis Y offset</label>\n                    <input [(ngModel)]=\"form.value.axisYOffset\" name=\"form.value.axisYOffset\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <h4 class=\"box-title\">Aggregation</h4>\n                <hr class=\"m-t-0 m-b-20\">\n    \n                <collection-select [model]=\"form.value\"></collection-select>\n                \n                <div class=\"form-group\">\n                    <label>Type</label>\n    \n                    <select [(ngModel)]=\"form.value.aggregation.type\" name=\"form.value.aggregation.type\" class=\"custom-select col-12\">\n                        <option *ngFor=\"let type of aggTypes\" [ngValue]=\"type.value\">{{type.text}}</option>\n                    </select>\n                </div>\n\n                <div *ngIf=\"form.value.aggregation.type != 'date_histogram_term'\" class=\"form-group\">\n                    <label>Field</label>\n                    <input [(ngModel)]=\"form.value.aggregation.params.field\" name=\"form.value.aggregation.params.field\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"form.value.aggregation.type == 'date_histogram_term'\" class=\"form-group\">\n                    <label>Term field</label>\n                    <input [(ngModel)]=\"form.value.aggregation.params.term_field\" name=\"form.value.aggregation.params.term_field\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"form.value.aggregation.type == 'date_histogram_term'\" class=\"form-group\">\n                    <label>Date field</label>\n                    <input [(ngModel)]=\"form.value.aggregation.params.date_field\" name=\"form.value.aggregation.params.date_field\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"['histogram', 'date_histogram', 'date_histogram_term'].indexOf(form.value.aggregation.type) != -1\" class=\"form-group\">\n                    <label>Interval</label>\n                    <input [(ngModel)]=\"form.value.aggregation.params.interval\" name=\"form.value.aggregation.params.interval\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"['date_histogram', 'date_histogram_term'].indexOf(form.value.aggregation.type) != -1\" class=\"form-group\">\n                    <label>Date format</label>\n                    <input [(ngModel)]=\"form.value.aggregation.params.format\" name=\"form.value.aggregation.params.format\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"done === true\" class=\"alert alert-success\">Success!</div>\n                \n                <button (click)=\"submit()\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">\n                    {{form.id != null ? 'Update' : 'Create'}}\n                </button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/form/widget-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_chart_service__ = __webpack_require__("../../../../../src/app/crude/services/chart.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_aggregation_service__ = __webpack_require__("../../../../../src/app/crude/services/aggregation.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_components_form_component_form_component__ = __webpack_require__("../../../../../src/app/core/components/form-component/form.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WidgetFormComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WidgetFormComponent = (function (_super) {
    __extends(WidgetFormComponent, _super);
    function WidgetFormComponent(service, route, router, httpErrorService, chartService, aggService) {
        var _this = _super.call(this, service, route, router, httpErrorService, {
            name: 'widget',
            listRoute: '/widgets'
        }) || this;
        _this.service = service;
        _this.route = route;
        _this.router = router;
        _this.httpErrorService = httpErrorService;
        _this.chartService = chartService;
        _this.aggService = aggService;
        _this.chartTypes = chartService.getTypes();
        _this.aggTypes = aggService.getTypes();
        return _this;
    }
    WidgetFormComponent.prototype.initForm = function () {
        this.form = {
            type: 'widget',
            value: {
                aggregation: {
                    params: {}
                }
            }
        };
    };
    WidgetFormComponent.prototype.beforeSubmit = function (form) {
        if (form.value.aggregation.type == 'term') {
            delete form.value.aggregation.params.interval;
            delete form.value.aggregation.params.format;
        }
        return form;
    };
    return WidgetFormComponent;
}(__WEBPACK_IMPORTED_MODULE_6__core_components_form_component_form_component__["a" /* FormComponent */]));
WidgetFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'widget-form',
        template: __webpack_require__("../../../../../src/app/crude/components/widget/form/widget-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_chart_service__["a" /* ChartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_chart_service__["a" /* ChartService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_aggregation_service__["a" /* AggregationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_aggregation_service__["a" /* AggregationService */]) === "function" && _f || Object])
], WidgetFormComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=widget-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/frame/widget-frame.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"data != null\">\n    <div *ngIf=\"data.selector == 'line-chart'\" >\n        <line-chart\n                [title]=\"data.title\"\n                [condition]=\"data.condition\"\n                [aggregation]=\"data.aggregation\"\n                [collectionId]=\"data.collectionId\"\n                [elementId]=\"'widget-' + id\">\n        </line-chart>\n    </div>\n\n    <div *ngIf=\"data.selector == 'pie-chart'\" >\n        <pie-chart\n                [title]=\"data.title\"\n                [condition]=\"data.condition\"\n                [aggregation]=\"data.aggregation\"\n                [collectionId]=\"data.collectionId\"\n                [elementId]=\"'widget-' + id\">\n        </pie-chart>\n    </div>\n\n    <div *ngIf=\"data.selector == 'bar-chart'\" >\n        <bar-chart\n                [title]=\"data.title\"\n                [condition]=\"data.condition\"\n                [aggregation]=\"data.aggregation\"\n                [collectionId]=\"data.collectionId\"\n                [elementId]=\"'widget-' + id\">\n        </bar-chart>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/frame/widget-frame.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_chart_service__ = __webpack_require__("../../../../../src/app/crude/services/chart.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WidgetFrameComponent; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WidgetFrameComponent = (function () {
    function WidgetFrameComponent(chartService) {
        this.chartService = chartService;
        this.subscribe = false;
    }
    WidgetFrameComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.chart) {
            this.data = __assign({}, this.chart);
        }
        if (this.subscribe) {
            this.chartService.frame.subscribe(function (data) {
                console.log('got chart frame data', data);
                if (_this.data != null && data.selector == _this.data.selector) {
                    console.log('refesh timeout');
                    _this.data = null;
                    setTimeout(function () {
                        _this.data = data;
                    }, 250);
                }
                else {
                    console.log('refesh native');
                    _this.data = data;
                }
            });
        }
    };
    return WidgetFrameComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], WidgetFrameComponent.prototype, "id", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], WidgetFrameComponent.prototype, "chart", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], WidgetFrameComponent.prototype, "subscribe", void 0);
WidgetFrameComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'widget-frame',
        template: __webpack_require__("../../../../../src/app/crude/components/widget/frame/widget-frame.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_chart_service__["a" /* ChartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_chart_service__["a" /* ChartService */]) === "function" && _a || Object])
], WidgetFrameComponent);

var _a;
//# sourceMappingURL=widget-frame.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/list/widget-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"widgets == null\"></preloader>\n\n<div *ngIf=\"widgets != null && widgets.length > 0\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">Widgets</h3>\n                <h6 class=\"card-subtitle mb-2 text-muted\">\n                    Total: {{widgets.length}}\n                </h6>\n                \n                <table class=\"table\">\n                    <tr>\n                        <th>ID</th>\n                        <th>Title</th>\n                        <th>Type</th>\n                        <th style=\"width: 120px\"></th>\n                    </tr>\n                    <tr *ngFor=\"let widget of widgets\">\n                        <td>\n                            {{widget.id}}\n                        </td>\n                        <td>\n                            {{widget.value.title}}\n                        </td>\n                        <td>\n                            {{widget.value.selector}}\n                        </td>\n                        <td>\n                            <a [routerLink]=\"['/widgets/' + widget.id]\" class=\"btn btn-secondary btn-sm\">\n                                <i class=\"fa fa-eye\"></i>\n                            </a>\n                            <a [routerLink]=\"['/widgets/update/' + widget.id]\" class=\"btn btn-info btn-sm\">\n                                <i class=\"fa fa-edit\"></i>\n                            </a>\n                            <button (click)=\"deleteWidget(widget)\" class=\"btn btn-danger btn-sm\">\n                                <i class=\"fa fa-remove\"></i>\n                            </button>\n                        </td>\n                    </tr>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div *ngIf=\"widgets != null && widgets.length == 0\" class=\"col-12\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <div class=\"alert alert-info\">\n                <p>You haven't widgets yet.</p>\n            </div>\n    \n            <p><a [routerLink]=\"['/widgets/new']\" class=\"btn btn-success\">Create widget</a></p>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/list/widget-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_array_service__ = __webpack_require__("../../../../../src/app/core/services/array.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WidgetListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WidgetListComponent = (function () {
    function WidgetListComponent(arrayService, configService, httpErrorService) {
        this.arrayService = arrayService;
        this.configService = configService;
        this.httpErrorService = httpErrorService;
        this.crumbs = {
            title: 'All widgets',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All widgets' }
            ],
            buttons: [
                {
                    text: 'New widget',
                    route: '/widgets/new',
                    class: 'btn btn-primary'
                }
            ]
        };
    }
    WidgetListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var params = { type: 'chart' };
        this.configService.fetchAll(params).subscribe(function (response) { return _this.widgets = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    WidgetListComponent.prototype.deleteWidget = function (widget) {
        var _this = this;
        if (!confirm("Delete widget '" + widget.value.title + "'?")) {
            return;
        }
        this.configService.delete(widget.id).subscribe(function () { return _this.arrayService.deleteElement(_this.widgets, widget); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return WidgetListComponent;
}());
WidgetListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/crude/components/widget/list/widget-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_array_service__["a" /* ArrayService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_array_service__["a" /* ArrayService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], WidgetListComponent);

var _a, _b, _c;
//# sourceMappingURL=widget-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/view/widget-view.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"config == null\"></preloader>\n\n<div *ngIf=\"config != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <widget-frame [chart]=\"config.value\"></widget-frame>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/crude/components/widget/view/widget-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WidgetViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WidgetViewComponent = (function () {
    function WidgetViewComponent(route, configService, httpErrorService) {
        this.route = route;
        this.configService = configService;
        this.httpErrorService = httpErrorService;
        this.crumbs = {
            title: 'View widget',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All widgets', href: '/widgets' },
                { title: 'View widget' }
            ]
        };
    }
    WidgetViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.configService.fetchById(params.id).subscribe(function (response) { return _this.config = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
        });
    };
    WidgetViewComponent.prototype.deleteWidget = function (widget) {
        if (!confirm("Delete widget '" + widget.value.title + "'?")) {
            return;
        }
        alert('delete it');
        // this.configService.delete(widget.id).subscribe(
        //     ()    => this.arrayService.deleteElement(this.widgets, widget),
        //     error => this.httpErrorService.handle(error, this)
        // );
    };
    return WidgetViewComponent;
}());
WidgetViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'widget-view',
        template: __webpack_require__("../../../../../src/app/crude/components/widget/view/widget-view.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* ConfigService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], WidgetViewComponent);

var _a, _b, _c;
//# sourceMappingURL=widget-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/crude/directives/field-value.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FieldValueDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FieldValueDirective = (function () {
    function FieldValueDirective(element, datePipe, decimalPipe, configService) {
        this.element = element;
        this.datePipe = datePipe;
        this.decimalPipe = decimalPipe;
        this.configService = configService;
    }
    FieldValueDirective.prototype.ngOnInit = function () {
        var value = this.item[this.field.name];
        switch (true) {
            case value == null:
                value = '---';
                break;
            case this.field.view == null:
                break;
            case this.field.view == 'date':
                value = this.datePipe.transform(value, 'MM.dd.yyyy');
                break;
            case this.field.view == 'date_time':
                value = this.datePipe.transform(value, 'MM.dd.yyyy HH.mm');
                break;
            case this.field.view == 'number':
                value = this.decimalPipe.transform(value);
                break;
            case this.field.view == 'short_link':
                value = "<a href='" + value + "' target='blank'>link</a>";
                break;
            case this.field.view.startsWith('dictionary-'):
                value = this.dictionaryValue(this.field.view, value);
                break;
        }
        this.element.nativeElement.innerHTML = value;
    };
    FieldValueDirective.prototype.dictionaryValue = function (configKey, value) {
        var configValue = this.configService.findValueByKey(configKey);
        if (configValue && configValue.map) {
            return configValue.map[value];
        }
    };
    return FieldValueDirective;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FieldValueDirective.prototype, "field", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], FieldValueDirective.prototype, "item", void 0);
FieldValueDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[fieldValue]',
        providers: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["l" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["m" /* DecimalPipe */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["l" /* DatePipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["l" /* DatePipe */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["m" /* DecimalPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["m" /* DecimalPipe */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* ConfigService */]) === "function" && _d || Object])
], FieldValueDirective);

var _a, _b, _c, _d;
//# sourceMappingURL=field-value.directive.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/aggregation.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AggregationService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AggregationService = (function () {
    function AggregationService() {
        this.types = [
            {
                text: 'Term',
                value: 'term',
                params: [
                    { name: 'field', component: 'field-select' }
                ]
            },
            {
                text: 'Histogram',
                value: 'histogram',
                params: [
                    { name: 'field', component: 'field-select' },
                    { name: 'interval', component: 'number' }
                ]
            },
            {
                text: 'Date histogram',
                value: 'date_histogram',
                params: [
                    { name: 'field', component: 'field-select' },
                    { name: 'interval', component: 'interval-select' },
                    { name: 'format', component: 'date-format-select' }
                ]
            },
            {
                text: 'Date histogram & term',
                value: 'date_histogram_term',
                params: [
                    { name: 'term_field', component: 'field-select' },
                    { name: 'date_field', component: 'field-select' },
                    { name: 'interval', component: 'interval-select' },
                    { name: 'format', component: 'date-format-select' }
                ]
            }
        ];
    }
    AggregationService.prototype.getTypes = function () {
        return this.types;
    };
    return AggregationService;
}());
AggregationService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], AggregationService);

//# sourceMappingURL=aggregation.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/chart.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ChartService = (function () {
    function ChartService() {
        this.form = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.data = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.frame = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.types = [
            {
                name: 'Line chart',
                selector: 'line-chart',
                icon: 'mdi mdi-chart-line',
                aggTypes: ['date_histogram', 'date_histogram_term']
            },
            {
                name: 'Pie chart',
                selector: 'pie-chart',
                icon: 'mdi mdi-chart-pie',
                aggTypes: ['term', 'histogram']
            },
            {
                name: 'Bar chart',
                selector: 'bar-chart',
                icon: 'mdi mdi-chart-bar',
                aggTypes: ['term', 'histogram']
            }
        ];
    }
    ChartService.prototype.getTypes = function () {
        return this.types;
    };
    return ChartService;
}());
ChartService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], ChartService);

//# sourceMappingURL=chart.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/collection.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollectionService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CollectionService = (function (_super) {
    __extends(CollectionService, _super);
    function CollectionService(http, indexService, httpErrorService) {
        var _this = _super.call(this, http, '/api/collections') || this;
        _this.http = http;
        _this.indexService = indexService;
        _this.httpErrorService = httpErrorService;
        return _this;
    }
    CollectionService.prototype.fetchFields = function (id) {
        return this.http
            .get(this.url('/' + id + '/fields'), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    return CollectionService;
}(__WEBPACK_IMPORTED_MODULE_3__core_services_api_service__["a" /* ApiService */]));
CollectionService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__index_service__["a" /* IndexService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], CollectionService);

var _a, _b, _c;
//# sourceMappingURL=collection.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/field.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FieldService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FieldService = (function () {
    function FieldService() {
        this.collectionFields = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    FieldService.prototype.nameToLabel = function (name) {
        var label = name.replace(/_/g, ' ');
        return label.charAt(0).toUpperCase() + label.slice(1);
    };
    FieldService.prototype.getFieldLabel = function (fieldName, fields) {
        var field = fields.find(function (f) { return f.name == fieldName; });
        if (field && field.label) {
            return field.label;
        }
        else {
            return this.nameToLabel(fieldName);
        }
    };
    return FieldService;
}());
FieldService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], FieldService);

//# sourceMappingURL=field.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/filter.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FilterService = (function () {
    function FilterService() {
        this.form = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.data = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.operLabels = {
            eq: '=',
            not_eq: '!=',
            gt: '>',
            gte: '>=',
            lt: '<',
            lte: '<=',
        };
    }
    return FilterService;
}());
FilterService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], FilterService);

//# sourceMappingURL=filter.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/index.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IndexService = (function (_super) {
    __extends(IndexService, _super);
    function IndexService(http) {
        var _this = _super.call(this, http, '/api/indices') || this;
        _this.http = http;
        return _this;
    }
    IndexService.prototype.fetchByName = function (name, instanceId) {
        return this.http
            .get(this.url('/' + name + '?instanceId=' + instanceId), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    IndexService.prototype.fetchMapping = function (name, type, instanceId) {
        return this.http
            .get(this.url('/' + name + '/mapping?instanceId=' + instanceId + '&indexType=' + type), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    IndexService.prototype.updateMapping = function (data) {
        return this.http
            .put(this.url('/mapping'), data, this.getOptions())
            .map(function (response) { return response.json(); });
    };
    return IndexService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
IndexService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], IndexService);

var _a;
//# sourceMappingURL=index.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/item-list-settings.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemListSettingsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ItemListSettingsService = (function () {
    function ItemListSettingsService() {
        this.form = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.data = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    return ItemListSettingsService;
}());
ItemListSettingsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], ItemListSettingsService);

//# sourceMappingURL=item-list-settings.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/item.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ItemService = (function (_super) {
    __extends(ItemService, _super);
    function ItemService(http) {
        var _this = _super.call(this, http, '/api/items') || this;
        _this.http = http;
        return _this;
    }
    ItemService.prototype.fetchAll = function (params) {
        ['aggregation', 'condition', 'sorting'].forEach(function (param) {
            if (params[param] == null) {
                return;
            }
            if (Array.isArray(params[param])) {
                if (params[param].length > 0) {
                    params[param] = JSON.stringify(params[param]);
                }
                else {
                    delete params[param];
                }
            }
            else if (typeof (params[param]) == "object") {
                params[param] = JSON.stringify(params[param]);
            }
        });
        return _super.prototype.fetchAll.call(this, params);
    };
    return ItemService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
ItemService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], ItemService);

var _a;
//# sourceMappingURL=item.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/mapping.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MappingService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MappingService = (function () {
    function MappingService() {
    }
    MappingService.prototype.mappingToArray = function (mapping) {
        var result = [];
        for (var field in mapping) {
            if (mapping.hasOwnProperty(field)) {
                var data = mapping[field];
                data.name = field;
                data.fields = JSON.stringify(data.fields);
                result.push(data);
            }
        }
        return result;
    };
    MappingService.prototype.mappingFromArray = function (mapping) {
        var result = {};
        for (var i = 0; i < mapping.length; i++) {
            var data = mapping[i];
            result[data.name] = {
                type: data.type
            };
            if (data.fields) {
                result[data.name].fields = JSON.parse(data.fields);
            }
            if (data.fielddata) {
                result[data.name].fielddata = true;
            }
            if (data.index) {
                result[data.name].index = data.index;
            }
        }
        return result;
    };
    return MappingService;
}());
MappingService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], MappingService);

//# sourceMappingURL=mapping.service.js.map

/***/ }),

/***/ "../../../../../src/app/crude/services/sorting.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SortingService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var SortingService = (function () {
    function SortingService() {
        this.data = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.form = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    return SortingService;
}());
SortingService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], SortingService);

//# sourceMappingURL=sorting.service.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/card/instance-card.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n    <div class=\"card-block\">\n        <h3 class=\"card-title\">\n            <a [routerLink]=\"['/instances/' + instance.id]\">\n                {{instance.name}}\n            </a>\n        </h3>\n        <h6 class=\"card-subtitle mb-2 text-muted\">\n            {{instance.name}}<span *ngIf=\"instance.host != null\">: {{instance.host}}</span>\n        </h6>\n        \n        <instance-health *ngIf=\"instance.type == 'elasticsearch'\" [instance]=\"instance\" [short]=\"true\"></instance-health>\n        <instance-status *ngIf=\"instance.type != 'elasticsearch'\" [instance]=\"instance\"></instance-status>\n        \n        <hr class=\"m-t-20 m-b-20\">\n        \n        <a [routerLink]=\"['/instances/' + instance.id]\"  class=\"card-link btn btn-secondary btn-sm\">view</a>\n        <a [routerLink]=\"['/instances/update/' + instance.id]\" class=\"btn btn-info card-link btn-sm\">Update</a>\n        <button *ngIf=\"detail\" (click)=\"deleteInstance()\" class=\"btn btn-danger card-link btn-sm\">Delete</button>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/instance/components/card/instance-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceCardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InstanceCardComponent = (function () {
    function InstanceCardComponent(router, instanceService, httpErrorService) {
        this.router = router;
        this.instanceService = instanceService;
        this.httpErrorService = httpErrorService;
    }
    InstanceCardComponent.prototype.ngOnInit = function () {
    };
    InstanceCardComponent.prototype.deleteInstance = function () {
        var _this = this;
        if (!confirm('Delete instance "' + this.instance.name + '" ?')) {
            return;
        }
        this.instanceService.delete(this.instance.id).subscribe(function (response) { return _this.router.navigate(['instances/new']); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return InstanceCardComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InstanceCardComponent.prototype, "instance", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], InstanceCardComponent.prototype, "detail", void 0);
InstanceCardComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'instance-card',
        template: __webpack_require__("../../../../../src/app/instance/components/card/instance-card.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_instance_service__["a" /* InstanceService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object])
], InstanceCardComponent);

var _a, _b, _c;
//# sourceMappingURL=instance-card.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/form/instance-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"form == null\"></preloader>\n\n<div *ngIf=\"form != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <application-select [model]=\"form\"></application-select>\n                \n                <div *ngIf=\"types != null\" class=\"form-group\">\n                    <label>Type</label>\n                    <select [(ngModel)]=\"form.type\" name=\"form.type\" class=\"custom-select col-12\">\n                        <option *ngFor=\"let type of types\" value=\"{{type.key}}\">{{type.val}}</option>\n                    </select>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Name</label>\n                    <input [(ngModel)]=\"form.name\" name=\"form.name\" type=\"text\" class=\"form-control\"/>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Host</label>\n                    <input [(ngModel)]=\"form.host\" name=\"form.host\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"form.type == 'postgresql'\" class=\"form-group\">\n                    <label>User name</label>\n                    <input [(ngModel)]=\"form.params.user\" name=\"form.params.user\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"form.type == 'postgresql'\" class=\"form-group\">\n                    <label>Password</label>\n                    <input [(ngModel)]=\"form.params.password\" name=\"form.params.password\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"form.type == 'postgresql'\" class=\"form-group\">\n                    <label>DB name</label>\n                    <input [(ngModel)]=\"form.params.dbname\" name=\"form.params.dbname\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"form.type != 'logstash'\" class=\"form-group\">\n                    <label>Port</label>\n                    <input [(ngModel)]=\"form.port\" name=\"form.port\" type=\"number\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"form.type == 'logstash'\" class=\"form-group\">\n                    <label>SSH port</label>\n                    <input [(ngModel)]=\"form.params.sshPort\" name=\"form.params.sshPort\" type=\"number\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"form.type == 'logstash'\" class=\"form-group\">\n                    <label>SSH username</label>\n                    <input [(ngModel)]=\"form.params.sshUsername\" name=\"form.params.sshUsername\" type=\"text\" class=\"form-control\"/>\n                </div>\n\n                <div *ngIf=\"form.type == 'logstash'\" class=\"form-group\">\n                    <label>SSH password</label>\n                    <input [(ngModel)]=\"form.params.sshPassword\" name=\"form.params.sshPassword\" type=\"password\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"form.type == 'logstash'\" class=\"form-group\">\n                    <label>Config path</label>\n                    <input [(ngModel)]=\"form.params.configPath\" name=\"form.params.configPath\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <access-select [model]=\"form\"></access-select>\n                \n                <button (click)=\"submit()\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">{{this.form.id != null ? 'Update instance' : 'Create instance'}}</button>\n            </form>\n        </div>\n    </div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/instance/components/form/instance-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_handshake_service__ = __webpack_require__("../../../../../src/app/core/services/handshake.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InstanceFormComponent = (function () {
    function InstanceFormComponent(route, httpErrorService, handshakeSevice, instanceService, router) {
        this.route = route;
        this.httpErrorService = httpErrorService;
        this.handshakeSevice = handshakeSevice;
        this.instanceService = instanceService;
        this.router = router;
    }
    InstanceFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.fetchHandshake();
            _this.initCrumbs(params.id);
            _this.initForm(params.id);
        });
    };
    InstanceFormComponent.prototype.fetchInstance = function (id) {
        var _this = this;
        this.instanceService.fetchById(id).subscribe(function (response) {
            _this.form = response;
            if (_this.form.params == null) {
                _this.form.params = {};
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    InstanceFormComponent.prototype.fetchHandshake = function () {
        var _this = this;
        this.handshakeSevice.fetchAll().subscribe(function (response) {
            var map = response.enums.instanceType.map;
            var types = [];
            for (var i in map) {
                if (map.hasOwnProperty(i)) {
                    types.push({ key: i, val: map[i] });
                }
            }
            _this.types = types;
        }, function (error) {
            _this.httpErrorService.handle(error, _this);
        });
    };
    InstanceFormComponent.prototype.submit = function () {
        var _this = this;
        this.instanceService.save(this.form).subscribe(function (response) { return _this.router.navigate(['instances/' + response.id]); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    InstanceFormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New instance',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'Instances', href: '/instances' },
                    { title: 'New instance' }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update instance',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All instances', href: '/instances' },
                    { title: 'Update instance' }
                ]
            };
        }
    };
    InstanceFormComponent.prototype.initForm = function (id) {
        if (id == null) {
            this.form = { params: {} };
        }
        else {
            this.fetchInstance(id);
        }
    };
    return InstanceFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InstanceFormComponent.prototype, "form", void 0);
InstanceFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/instance/components/form/instance-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_handshake_service__["a" /* HandshakeService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_handshake_service__["a" /* HandshakeService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_instance_service__["a" /* InstanceService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _e || Object])
], InstanceFormComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=instance-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/form/select/instance-select.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"instances\" class=\"form-group\">\n    <label>Instance</label>\n    <select [(ngModel)]=\"model[field]\" name=\"model.instanceId\" class=\"custom-select col-12\">\n        <option *ngFor=\"let instance of instances\" [ngValue]=\"instance.id\">{{instance.name}}</option>\n    </select>\n</div>\n\n    "

/***/ }),

/***/ "../../../../../src/app/instance/components/form/select/instance-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InstanceSelectComponent = (function () {
    function InstanceSelectComponent(instanceService, httpErrorService) {
        this.instanceService = instanceService;
        this.httpErrorService = httpErrorService;
    }
    InstanceSelectComponent.prototype.ngOnInit = function () {
        this.fetchInstances();
    };
    InstanceSelectComponent.prototype.fetchInstances = function () {
        var _this = this;
        this.instanceService.fetchAll({ type: 'elasticsearch' }).subscribe(function (response) { return _this.instances = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return InstanceSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InstanceSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], InstanceSelectComponent.prototype, "field", void 0);
InstanceSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'instance-select',
        template: __webpack_require__("../../../../../src/app/instance/components/form/select/instance-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], InstanceSelectComponent);

var _a, _b;
//# sourceMappingURL=instance-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/health/instance-health.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"data\">\n    <div *ngIf=\"data.error != null\" class=\"alert alert-danger\">\n        {{data.error}}\n    </div>\n    \n    <div *ngIf=\"data.error == null\" style=\"clear: both;\">\n        <dl class=\"dl-horizontal\">\n            <dt>Cluster:</dt>\n            <dd>{{data.cluster}}</dd>\n            <dt>Status:</dt>\n            <dd>\n                <status-label [status]=\"data.status\"></status-label>\n            </dd>\n        </dl>\n        \n        <dl *ngIf=\"!short || short == null\" class=\"dl-horizontal\">\n            <dt>Active shards:</dt>\n            <dd>{{data.active_shards_percent}}</dd>\n    \n            <dt>Node total:</dt>\n            <dd>{{data['node.total']}}</dd>\n    \n            <dt>Node data:</dt>\n            <dd>{{data['node.data']}}</dd>\n    \n            <dt>Shards:</dt>\n            <dd>{{data.shards}}</dd>\n        </dl>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/instance/components/health/instance-health.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceHealthComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InstanceHealthComponent = (function () {
    function InstanceHealthComponent(instanceService, httpErrorService) {
        this.instanceService = instanceService;
        this.httpErrorService = httpErrorService;
    }
    InstanceHealthComponent.prototype.ngOnInit = function () {
        this.fetchInstanceHealth();
    };
    InstanceHealthComponent.prototype.fetchInstanceHealth = function () {
        var _this = this;
        this.instanceService.fetchHealth(this.instance.id).subscribe(function (response) { return _this.data = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return InstanceHealthComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InstanceHealthComponent.prototype, "instance", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], InstanceHealthComponent.prototype, "short", void 0);
InstanceHealthComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'instance-health',
        template: __webpack_require__("../../../../../src/app/instance/components/health/instance-health.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], InstanceHealthComponent);

var _a, _b;
//# sourceMappingURL=instance-health.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/list/instance-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"instances == null\"></preloader>\n\n<div *ngIf=\"instances != null && instances.length > 0\" class=\"row\">\n    <div *ngFor=\"let instance of instances\" class=\"col-12\">\n        <instance-card [instance]=\"instance\" [detail]=\"false\"></instance-card>\n    </div>\n</div>\n\n<div *ngIf=\"instances != null && instances.length == 0\"  class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <div class=\"alert alert-info\">\n                    You haven't instances yet\n                </div>\n                \n                <p><a [routerLink]=\"['/instances/new']\" class=\"btn btn-success\">Create instance</a></p>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/instance/components/list/instance-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InstanceListComponent = (function () {
    function InstanceListComponent(instanceService, httpErrorService) {
        this.instanceService = instanceService;
        this.httpErrorService = httpErrorService;
        this.crumbs = {
            title: 'Instances',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All instances' }
            ],
            buttons: [
                { text: 'New instance', route: '/instances/new', class: 'btn btn-success' }
            ]
        };
    }
    InstanceListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.instanceService.fetchAll().subscribe(function (response) { return _this.instances = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return InstanceListComponent;
}());
InstanceListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/instance/components/list/instance-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], InstanceListComponent);

var _a, _b;
//# sourceMappingURL=instance-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/status/instance-status.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <span>\n        <span *ngIf=\"pingStatus == null\" class=\"label label-light-default\">ping: pending</span>\n        <span *ngIf=\"pingStatus == 'fail'\" class=\"label label-light-danger\">ping: fail</span>\n        <span *ngIf=\"pingStatus != null && pingStatus != 'fail'\" class=\"label label-light-megna\">ping: {{pingStatus}}</span>\n    </span>\n    \n    <span *ngIf=\"checkSsh\">\n        <span *ngIf=\"sshStatus == null\" class=\"label label-light-default\">SSH: pending</span>\n        <span *ngIf=\"sshStatus == 'fail'\" class=\"label label-light-danger\">SSH: fail</span>\n        <span *ngIf=\"sshStatus != null && sshStatus != 'fail'\" class=\"label label-light-megna\">SSH: {{sshStatus}}</span>\n    </span>\n</div>"

/***/ }),

/***/ "../../../../../src/app/instance/components/status/instance-status.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceStatusComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InstanceStatusComponent = (function () {
    function InstanceStatusComponent(instanceService) {
        this.instanceService = instanceService;
    }
    InstanceStatusComponent.prototype.ngOnInit = function () {
        this.fetchPingStatus();
        if (this.instance.params.sshUsername != null && this.instance.params.sshPassword != null) {
            this.checkSsh = true;
            this.fetchSshStatus();
        }
    };
    InstanceStatusComponent.prototype.fetchPingStatus = function () {
        var _this = this;
        this.instanceService.fetchPingStatus(this.instance.id).subscribe(function (response) {
            _this.pingStatus = response.status;
        }, function (error) {
            console.log('http error', error);
        });
    };
    InstanceStatusComponent.prototype.fetchSshStatus = function () {
        var _this = this;
        this.instanceService.fetchShhStatus(this.instance.id).subscribe(function (response) {
            _this.sshStatus = response.status;
        }, function (error) {
            console.log('http error', error);
        });
    };
    return InstanceStatusComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], InstanceStatusComponent.prototype, "instance", void 0);
InstanceStatusComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'instance-status',
        template: __webpack_require__("../../../../../src/app/instance/components/status/instance-status.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_instance_service__["a" /* InstanceService */]) === "function" && _a || Object])
], InstanceStatusComponent);

var _a;
//# sourceMappingURL=instance-status.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/components/view/instance-view.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"instance == null\"></preloader>\n\n<div class=\"row\">\n    <div *ngIf=\"instance != null\" class=\"col-12\">\n        <instance-card [instance]=\"instance\" [detail]=\"true\"></instance-card>\n    </div>\n</div>\n\n<div class=\"row\">\n    <div *ngIf=\"indices != null\" class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <div class=\"row\">\n                    <div class=\"col-lg-6\">\n                        <h3 class=\"card-title\">Indices</h3>\n                    </div>\n                    <div class=\"col-lg-6 text-right\">\n                        <select [(ngModel)]=\"indexType\" class=\"form-control m-r-10\" style=\"width:100px\">\n                            <option value=\"app\">app</option>\n                            <option value=\"all\">all</option>\n                        </select>\n                        <a [routerLink]=\"['/instances/' + instance.id + '/indices/new']\" class=\"btn btn-success\">\n                            <i class=\"fa fa-plus\"></i>\n                        </a>\n                    </div>\n                </div>\n\n                <div *ngIf=\"indices.length == 0\" class=\"alert alert-info\">\n                    This instance does not have any indexes\n                </div>\n                \n                <table *ngIf=\"indices.length > 0\" class=\"table\">\n                    <tr>\n                        <th>Name</th>\n                        <th>Health</th>\n                        <th>Status</th>\n                        <th>Count</th>\n                        <th>Size</th>\n                        <th></th>\n                    </tr>\n                    <tbody *ngFor=\"let index of indices\">\n                        <tr *ngIf=\"indexType == 'all' || !index.index.startsWith('.')\">\n                            <td>\n                                <a href=\"http://{{instance.host}}:{{instance.port}}/{{index.index}}/_search\" target=\"_blank\">\n                                    {{index.index}}\n                                </a>\n                            </td>\n                            <td>\n                                <status-label [status]=\"index.health\"></status-label>\n                            </td>\n                            <td>\n                                <status-label [status]=\"index.status\"></status-label>\n                            </td>\n                            <td>\n                                {{index['docs.count']}}\n                            </td>\n                            <td>\n                                {{index['store.size']}}\n                            </td>\n                            <td style=\"white-space: nowrap\" class=\"text-right\">\n                                <a\n                                    href=\"javascript:void(0)\"\n                                    [routerLink]=\"['/instances/' + instance.id + '/indices/' + index.index +   '/mapping']\"\n                                    class=\"btn btn-success btn-sm\">\n                                    M\n                                </a>\n                                \n                                <button\n                                    (click)=\"deleteIndex(index.index)\"\n                                    type=\"button\"\n                                    class=\"btn btn-danger btn-sm\">\n                                    <i class=\"fa fa-trash\"></i>\n                                </button>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/instance/components/view/instance-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__crude_services_index_service__ = __webpack_require__("../../../../../src/app/crude/services/index.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceViewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InstanceViewComponent = (function () {
    function InstanceViewComponent(route, indexService, httpErrorService, instanceService) {
        this.route = route;
        this.indexService = indexService;
        this.httpErrorService = httpErrorService;
        this.instanceService = instanceService;
        this.crumbs = {
            title: 'View instance',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'Instances', href: '/instances' },
                { title: 'View instance' }
            ]
        };
        this.indexType = 'app';
    }
    InstanceViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.fetchInstance(params.id);
        });
    };
    InstanceViewComponent.prototype.fetchInstance = function (id) {
        var _this = this;
        this.instanceService.fetchById(id).subscribe(function (response) {
            _this.instance = response;
            if (_this.instance.type == 'elasticsearch') {
                _this.fetchIndices(_this.instance.id);
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    InstanceViewComponent.prototype.fetchIndices = function (instanceId) {
        var _this = this;
        var data = { instanceId: instanceId };
        this.indexService.fetchAll(data).subscribe(function (response) { return _this.indices = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    InstanceViewComponent.prototype.deleteIndex = function (name) {
        var _this = this;
        if (!confirm('Delete index "' + name + '" ?')) {
            return;
        }
        this.indexService.delete(name, { instanceId: this.instance.id }).subscribe(function (response) { return _this.fetchIndices(_this.instance.id); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return InstanceViewComponent;
}());
InstanceViewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/instance/components/view/instance-view.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__crude_services_index_service__["a" /* IndexService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__crude_services_index_service__["a" /* IndexService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_instance_service__["a" /* InstanceService */]) === "function" && _d || Object])
], InstanceViewComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=instance-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/instance/services/instance.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstanceService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InstanceService = (function (_super) {
    __extends(InstanceService, _super);
    function InstanceService(http) {
        var _this = _super.call(this, http, '/api/instances') || this;
        _this.http = http;
        return _this;
    }
    InstanceService.prototype.fetchPingStatus = function (id) {
        return this.http
            .get(this.url('/' + id + '/ping'), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    InstanceService.prototype.fetchShhStatus = function (id) {
        return this.http
            .get(this.url('/' + id + '/ssh'), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    InstanceService.prototype.fetchHealth = function (id) {
        return this.http
            .get(this.url('/' + id + '/health'), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    return InstanceService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
InstanceService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], InstanceService);

var _a;
//# sourceMappingURL=instance.service.js.map

/***/ }),

/***/ "../../../../../src/app/pipeline/components/form/pipeline-form.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Select input</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n        <p>One fine body&hellip;</p>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success waves-effect waves-light\" (click)=\"addInput(c)\">Add</button>\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\n    </div>\n</ng-template>\n\n<preloader [visible]=\"pipeline == null\"></preloader>\n\n<div *ngIf=\"pipeline != null\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card card-block\">\n            <error-message [errors]=\"errors\"></error-message>\n\n            <form class=\"form-horizontal\">\n                <div class=\"form-group\">\n                    <label>Name</label>\n                    <input [(ngModel)]=\"pipeline.name\" name=\"pipeline.name\" type=\"text\" class=\"form-control\"/>\n                </div>\n    \n                <div *ngIf=\"instances\" class=\"form-group\">\n                    <label>Instance</label>\n                    <select [(ngModel)]=\"pipeline.instanceId\" name=\"pipeline.instanceId\" class=\"custom-select col-12\">\n                        <option *ngFor=\"let instance of instances\" [ngValue]=\"instance.id\">{{instance.name}}</option>\n                    </select>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Config</label>\n                    <textarea [(ngModel)]=\"pipeline.config\" name=\"pipeline.config\" rows=\"20\" class=\"form-control\"></textarea>\n                </div>\n\n                <button (click)=\"submit()\" type=\"submit\" class=\"btn btn-success waves-effect waves-light m-r-10\">{{this.pipeline.id != null ? 'Update' : 'Create'}}</button>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/pipeline/components/form/pipeline-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__instance_services_instance_service__ = __webpack_require__("../../../../../src/app/instance/services/instance.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_pipeline_service__ = __webpack_require__("../../../../../src/app/pipeline/services/pipeline.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipelineFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PipelineFormComponent = (function () {
    function PipelineFormComponent(route, pipelineService, instanceService, httpErrorService, modalService, router) {
        this.route = route;
        this.pipelineService = pipelineService;
        this.instanceService = instanceService;
        this.httpErrorService = httpErrorService;
        this.modalService = modalService;
        this.router = router;
    }
    PipelineFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fetchInstances();
        this.route.params.subscribe(function (params) {
            _this.initCrumbs(params.id);
            _this.initPipeline(params.id);
        });
    };
    PipelineFormComponent.prototype.submit = function () {
        var _this = this;
        this.pipelineService.save(this.pipeline).subscribe(function (response) { return _this.router.navigate(['pipelines']); }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    PipelineFormComponent.prototype.openInputModal = function (content) {
        this.modalService.open(content).result.then(function (result) {
            console.log("Closed with: " + result);
        });
    };
    PipelineFormComponent.prototype.addInput = function (closeFunc) {
        closeFunc();
    };
    PipelineFormComponent.prototype.fetchPipeline = function (id) {
        var _this = this;
        this.pipelineService.fetchById(id).subscribe(function (response) { return _this.pipeline = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    PipelineFormComponent.prototype.fetchInstances = function () {
        var _this = this;
        this.instanceService.fetchAll({ type: 'logstash' }).subscribe(function (response) { return _this.instances = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    PipelineFormComponent.prototype.initCrumbs = function (id) {
        if (id == null) {
            this.crumbs = {
                title: 'New pipeline',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All pipelines', href: '/pipelines' },
                    { title: 'New pipeline' }
                ]
            };
        }
        else {
            this.crumbs = {
                title: 'Update pipeline',
                links: [
                    { title: 'Dashboard', href: '/' },
                    { title: 'All pipelines', href: '/pipelines' },
                    { title: 'Update pipeline' }
                ]
            };
        }
    };
    PipelineFormComponent.prototype.initPipeline = function (id) {
        if (id == null) {
            this.pipeline = {};
        }
        else {
            this.fetchPipeline(id);
        }
    };
    return PipelineFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], PipelineFormComponent.prototype, "pipeline", void 0);
PipelineFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/pipeline/components/form/pipeline-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_pipeline_service__["a" /* PipelineService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_pipeline_service__["a" /* PipelineService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__instance_services_instance_service__["a" /* InstanceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__instance_services_instance_service__["a" /* InstanceService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _f || Object])
], PipelineFormComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=pipeline-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/pipeline/components/list/pipeline-list.component.html":
/***/ (function(module, exports) {

module.exports = "<breadcrumb *ngIf=\"crumbs != null\" [data]=\"crumbs\"></breadcrumb>\n\n<preloader [visible]=\"pipelines == null\"></preloader>\n\n<div *ngIf=\"pipelines != null && pipelines.length > 0\" class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <h3 class=\"card-title\">Pipelines</h3>\n                <h6 class=\"card-subtitle mb-2 text-muted\">\n                    Total: {{pipelines.length}}\n                </h6>\n                \n                <table class=\"table\">\n                    <tr>\n                        <th>Name</th>\n                        <th>Instance</th>\n                        <th width=\"1\"></th>\n                    </tr>\n                    <tr *ngFor=\"let pipeline of pipelines\">\n                        <td>\n                            {{pipeline.name}}\n                        </td>\n                        <td>\n                            {{pipeline.instance.name}}\n                        </td>\n                        <td style=\"white-space: nowrap\">\n                            <button\n                                (click)=\"createConfigFile(pipeline)\"\n                                class=\"btn btn-sm\"\n                                [ngClass]=\"{'btn-primary': !pipeline.fileCreated, 'btn-success': pipeline.fileCreated}\" >\n                                sync file\n                            </button>\n                            \n                            <a [routerLink]=\"['/pipelines/update/' + pipeline.id]\"  class=\"card-link btn btn-info btn-sm\">\n                                <i class=\"fa fa-edit\"></i>\n                            </a>\n                        </td>\n                    </tr>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>\n    \n<!--<div *ngFor=\"let pipeline of pipelines\" class=\"col-12\">-->\n    <!--<div class=\"card\">-->\n        <!--<div class=\"card-block\">-->\n            <!--<h3 class=\"card-title\">{{pipeline.name}}</h3>-->\n            <!--<h6 *ngIf=\"pipeline.description != null\" class=\"card-subtitle\">{{pipeline.description}}</h6>-->\n            <!---->\n\n        <!--</div>-->\n    <!--</div>-->\n<!--</div>-->\n\n<div *ngIf=\"pipelines != null && pipelines.length == 0\" class=\"col-12\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <div class=\"alert alert-info\">\n                <p>You haven't pipelines yet.</p>\n            </div>\n    \n            <p><a [routerLink]=\"['/pipelines/new']\" class=\"btn btn-success\">Create pipeline</a></p>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/pipeline/components/list/pipeline-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_pipeline_service__ = __webpack_require__("../../../../../src/app/pipeline/services/pipeline.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_logstash_config_service__ = __webpack_require__("../../../../../src/app/pipeline/services/logstash-config.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipelineListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PipelineListComponent = (function () {
    function PipelineListComponent(pipelineService, httpErrorService, logstashConfigService) {
        this.pipelineService = pipelineService;
        this.httpErrorService = httpErrorService;
        this.logstashConfigService = logstashConfigService;
        this.crumbs = {
            title: 'All pipelines',
            links: [
                { title: 'Dashboard', href: '/' },
                { title: 'All pipelines' }
            ],
            buttons: [
                {
                    text: 'New pipeline',
                    route: '/pipelines/new',
                    class: 'btn btn-primary'
                }
            ]
        };
    }
    PipelineListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pipelineService.fetchAll().subscribe(function (response) { return _this.pipelines = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    PipelineListComponent.prototype.createConfigFile = function (pipeline) {
        var _this = this;
        this.logstashConfigService.create({ id: pipeline.id }).subscribe(function (response) {
            if (response.output.length == 0) {
                pipeline.fileCreated = true;
            }
            else {
                swal('Error', response.output, 'error');
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return PipelineListComponent;
}());
PipelineListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/pipeline/components/list/pipeline-list.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_pipeline_service__["a" /* PipelineService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_pipeline_service__["a" /* PipelineService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_logstash_config_service__["a" /* LogstashConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_logstash_config_service__["a" /* LogstashConfigService */]) === "function" && _c || Object])
], PipelineListComponent);

var _a, _b, _c;
//# sourceMappingURL=pipeline-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/pipeline/components/logstash-config-status/logstash-config-status.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    Config file status: <span class=\"label label-light-{{cssClass}}\">{{status}}</span>\n</div>"

/***/ }),

/***/ "../../../../../src/app/pipeline/components/logstash-config-status/logstash-config-status.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_logstash_config_service__ = __webpack_require__("../../../../../src/app/pipeline/services/logstash-config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogstashConfigStatusComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LogstashConfigStatusComponent = (function () {
    function LogstashConfigStatusComponent(logstashConfigService, httpErrorService) {
        this.logstashConfigService = logstashConfigService;
        this.httpErrorService = httpErrorService;
        this.status = 'Checking...';
        this.cssClass = 'inverse';
    }
    LogstashConfigStatusComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.logstashConfigService.fetchById(this.pipelineId).subscribe(function (response) {
            if (response.status == 'file not exists') {
                _this.createLogstashConfig();
            }
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    LogstashConfigStatusComponent.prototype.createLogstashConfig = function () {
        var _this = this;
        this.status = 'Not exists, creating it...';
        this.cssClass = 'info';
        var config = {
            id: this.pipelineId
        };
        this.logstashConfigService.create(config).subscribe(function (response) {
            console.log(response);
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return LogstashConfigStatusComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], LogstashConfigStatusComponent.prototype, "pipelineId", void 0);
LogstashConfigStatusComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'logstash-config-status',
        template: __webpack_require__("../../../../../src/app/pipeline/components/logstash-config-status/logstash-config-status.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_logstash_config_service__["a" /* LogstashConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_logstash_config_service__["a" /* LogstashConfigService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], LogstashConfigStatusComponent);

var _a, _b;
//# sourceMappingURL=logstash-config-status.component.js.map

/***/ }),

/***/ "../../../../../src/app/pipeline/services/logstash-config.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogstashConfigService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LogstashConfigService = (function (_super) {
    __extends(LogstashConfigService, _super);
    function LogstashConfigService(http) {
        var _this = _super.call(this, http, '/api/logstash-configs') || this;
        _this.http = http;
        return _this;
    }
    return LogstashConfigService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
LogstashConfigService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], LogstashConfigService);

var _a;
//# sourceMappingURL=logstash-config.service.js.map

/***/ }),

/***/ "../../../../../src/app/pipeline/services/logstash-plugin.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogstashPluginService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LogstashPluginService = (function (_super) {
    __extends(LogstashPluginService, _super);
    function LogstashPluginService(http) {
        var _this = _super.call(this, http, '/api/logstash-plugin') || this;
        _this.http = http;
        return _this;
    }
    return LogstashPluginService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
LogstashPluginService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], LogstashPluginService);

var _a;
//# sourceMappingURL=logstash-plugin.service.js.map

/***/ }),

/***/ "../../../../../src/app/pipeline/services/pipeline.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipelineService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PipelineService = (function (_super) {
    __extends(PipelineService, _super);
    function PipelineService(http) {
        var _this = _super.call(this, http, '/api/pipelines') || this;
        _this.http = http;
        return _this;
    }
    return PipelineService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
PipelineService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], PipelineService);

var _a;
//# sourceMappingURL=pipeline.service.js.map

/***/ }),

/***/ "../../../../../src/app/test/components/test/test.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n\n                <!-- <div *ngFor=\"let item of items\" style=\"border: 1px solid green\">\n                    {{item.name}}\n                </div> -->\n\n                <table border=\"1\" [sortablejs]=\"items\">\n                    <tr *ngFor=\"let item of items\">\n                        <td>{{item.name}}</td>\n                    </tr>\n                </table>\n\n                <hr/>\n                <div [sortablejs]=\"items\">\n                    <div *ngFor=\"let item of items\">{{ item.name }}</div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!--<swal #dialog title=\"Error\" text=\"Error text here\" type=\"error\"></swal>-->\n<!--<button (click)=\"dialog.show()\">Go to my profile</button>-->\n\n<!--<div class=\"row\">-->\n<!--<div class=\"col-12\">-->\n<!--<div class=\"card\">-->\n<!--<div class=\"card-block\">-->\n<!--<div fuck>Fuck me</div>-->\n<!--<p myHighlight>Highlight me!!!</p>-->\n<!--</div>-->\n<!--</div>-->\n<!--</div>-->\n<!--</div>-->\n\n<!--<div class=\"row\">-->\n<!--<div class=\"col-12\">-->\n<!--<div class=\"card\">-->\n<!--<div class=\"card-block\">-->\n<!--<h4 class=\"card-title\">Internet Speed</h4>-->\n<!--<div id=\"gauge-chart\" style=\"width:100%; height:400px;\"></div>-->\n<!--</div>-->\n<!--</div>-->\n<!--</div>-->\n<!--</div>-->"

/***/ }),

/***/ "../../../../../src/app/test/components/test/test.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// let Chartist: any;
// let Gauge: any;
// let echarts: any;
// //var SweetAlert: any;
var TestComponent = (function () {
    function TestComponent() {
    }
    // constructor(private sw: SwalComponent) {
    // }
    TestComponent.prototype.releaseDrop = function ($event) {
        console.log('release drop', $event);
    };
    TestComponent.prototype.startDrag = function (item) {
        console.log('startDrag', item);
    };
    TestComponent.prototype.addDropItem = function ($event) {
        console.log('release drop', $event);
    };
    TestComponent.prototype.dropEventMouse = function ($event) {
        console.log('dropEventMouse', $event);
    };
    TestComponent.prototype.dragEnter = function ($event) {
        console.log('dragEnter', $event);
    };
    TestComponent.prototype.dragLeave = function ($event) {
        console.log('dragLeave', $event);
    };
    TestComponent.prototype.ngOnInit = function () {
        var items = [];
        for (var i = 0; i < 10; i++) {
            items.push({
                name: 'item ' + i
            });
        }
        this.items = items;
        // var gaugeChart = echarts.init(document.getElementById('gauge-chart'));
        // let option = {
        //     tooltip : {
        //         formatter: "{a} <br/>{b} : {c}%"
        //     },
        //     series : [
        //         {
        //             name:'Speed',
        //             type:'gauge',
        //             detail : {formatter:'{value}%'},
        //             data:[{value: 10, name: 'Speed'}],
        //             axisLine: {            // 坐标轴线
        //                 lineStyle: {       // 属性lineStyle控制线条样式
        //                     color: [[0.2, '#55ce63'],[0.8, '#009efb'],[1, '#f62d51']],
        //                 }
        //             },
        //
        //         }
        //     ]
        // };
        //
        // gaugeChart.setOption(option, true);
    };
    return TestComponent;
}());
TestComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'test',
        template: __webpack_require__("../../../../../src/app/test/components/test/test.component.html"),
    })
], TestComponent);

//# sourceMappingURL=test.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/components/form/access-select/access-select.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\n    <label>Access</label>\n\n    <select2\n        *ngIf=\"users != null\"\n        width=\"100%\"\n        (valueChanged)=\"onValueChanged($event)\"\n        [data]=\"users\"\n        [value]=\"model[field]\"\n        [options]=\"{multiple:true}\"\n        ngDefaultControl>\n    </select2>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/user/components/form/access-select/access-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/user/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccessSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AccessSelectComponent = (function () {
    function AccessSelectComponent(userService, httpErrorService) {
        this.userService = userService;
        this.httpErrorService = httpErrorService;
    }
    AccessSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.fetchAll().subscribe(function (response) {
            _this.users = response.map(function (user) {
                return {
                    id: user.id,
                    text: user.login
                };
            });
        }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    AccessSelectComponent.prototype.onValueChanged = function (event) {
        this.model = event.value.map(function (id) { return parseInt(id); });
        console.log('access value', this.model);
    };
    return AccessSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AccessSelectComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], AccessSelectComponent.prototype, "field", void 0);
AccessSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'access-select',
        template: __webpack_require__("../../../../../src/app/user/components/form/access-select/access-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], AccessSelectComponent);

var _a, _b;
//# sourceMappingURL=access-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/components/form/login/login-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login-box card\">\n    <div class=\"card-block\">\n        <error-message [errors]=\"errors\"></error-message>\n\n        <form class=\"form-horizontal form-material\" id=\"loginform\" action=\"index.html\">\n            <h3 class=\"box-title m-b-20\">Sign In</h3>\n            <div class=\"form-group \">\n                <div class=\"col-xs-12\">\n                    <input [(ngModel)]=\"login\" name=\"login\" class=\"form-control\" type=\"text\" placeholder=\"Login\" autocomplete=\"off\">\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <div class=\"col-xs-12\">\n                    <input [(ngModel)]=\"password\" name=\"password\" class=\"form-control\" type=\"password\" placeholder=\"Password\" autocomplete=\"off\">\n                </div>\n            </div>\n            <!--<div class=\"form-group\">-->\n                <!--<div class=\"col-md-12\">-->\n                    <!--<div class=\"checkbox checkbox-primary pull-left p-t-0\">-->\n                        <!--<input id=\"checkbox-signup\" type=\"checkbox\">-->\n                        <!--<label for=\"checkbox-signup\"> Remember me </label>-->\n                    <!--</div>-->\n                    <!--<a href=\"javascript:void(0)\" id=\"to-recover\" class=\"text-dark pull-right\">-->\n                        <!--<i class=\"fa fa-lock m-r-5\"></i> Forgot pwd?-->\n                    <!--</a>-->\n                <!--</div>-->\n            <!--</div>-->\n            <div class=\"form-group text-center m-t-20\">\n                <div class=\"col-xs-12\">\n                    <button (click)=\"submit()\" class=\"btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light\" type=\"button\">\n                        Log In\n                    </button>\n                </div>\n            </div>\n            <!--<div class=\"row\">-->\n                <!--<div class=\"col-xs-12 col-sm-12 col-md-12 m-t-10 text-center\">-->\n                    <!--<div class=\"social\">-->\n                        <!--<div id=\"uLogin\" data-ulogin=\"display=panel;theme=classic;fields=first_name,last_name;providers=yandex,google,vkontakte,facebook,mailru,twitter;hidden=other;redirect_uri=http%3A%2F%2Fservices-site%2Fapi%2Fauth%2Fsocial;mobilebuttons=0;\"></div>-->\n                    <!--</div>-->\n                <!--</div>-->\n            <!--</div>-->\n            <!--<div class=\"form-group m-b-0\">-->\n                <!--<div class=\"col-sm-12 text-center\">-->\n                    <!--<p>Don't have an account? <a href=\"register.html\" class=\"text-info m-l-5\"><b>Sign Up</b></a></p>-->\n                <!--</div>-->\n            <!--</div>-->\n        </form>\n        <form class=\"form-horizontal\" id=\"recoverform\" action=\"index.html\">\n            <div class=\"form-group \">\n                <div class=\"col-xs-12\">\n                    <h3>Recover Password</h3>\n                    <p class=\"text-muted\">Enter your Email and instructions will be sent to you! </p>\n                </div>\n            </div>\n            <div class=\"form-group \">\n                <div class=\"col-xs-12\">\n                    <input class=\"form-control\" type=\"text\" required=\"\" placeholder=\"Email\"></div>\n            </div>\n            <div class=\"form-group text-center m-t-20\">\n                <div class=\"col-xs-12\">\n                    <button class=\"btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light\" type=\"submit\">Reset\n                    </button>\n                </div>\n            </div>\n        </form>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/user/components/form/login/login-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("../../../../../src/app/user/services/auth.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginFormComponent = (function () {
    function LoginFormComponent(authService, httpErrorService) {
        this.authService = authService;
        this.httpErrorService = httpErrorService;
    }
    LoginFormComponent.prototype.ngOnInit = function () {
    };
    LoginFormComponent.prototype.submit = function () {
        var _this = this;
        this.authService.login(this.login, this.password).subscribe(function (response) {
            if (response.authKey != null) {
                localStorage.setItem('user-auth-key', response.authKey);
                location.href = '/';
            }
        }, function (error) {
            _this.httpErrorService.handle(error, _this);
        });
    };
    return LoginFormComponent;
}());
LoginFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/user/components/form/login/login-form.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], LoginFormComponent);

var _a, _b;
//# sourceMappingURL=login-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/components/form/select/user-select.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\n    <label>User</label>\n    <select *ngIf=\"users\" [(ngModel)]=\"model\" name=\"model.userId\" class=\"custom-select col-12\">\n        <option></option>\n        <option *ngFor=\"let user of users\" [ngValue]=\"user.id\">{{user.login}}</option>\n    </select>\n</div>\n\n    "

/***/ }),

/***/ "../../../../../src/app/user/components/form/select/user-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__ = __webpack_require__("../../../../../src/app/core/services/http-error.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/user/services/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserSelectComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserSelectComponent = (function () {
    function UserSelectComponent(userService, httpErrorService) {
        this.userService = userService;
        this.httpErrorService = httpErrorService;
    }
    UserSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.fetchAll().subscribe(function (response) { return _this.users = response; }, function (error) { return _this.httpErrorService.handle(error, _this); });
    };
    return UserSelectComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], UserSelectComponent.prototype, "model", void 0);
UserSelectComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'user-select',
        template: __webpack_require__("../../../../../src/app/user/components/form/select/user-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_http_error_service__["a" /* HttpErrorService */]) === "function" && _b || Object])
], UserSelectComponent);

var _a, _b;
//# sourceMappingURL=user-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/components/navigation/user-navigation.component.html":
/***/ (function(module, exports) {

module.exports = "<a\n    class=\"nav-link dropdown-toggle text-muted waves-effect waves-dark\"\n    href=\"/\"\n    data-toggle=\"dropdown\"\n    aria-haspopup=\"true\"\n    aria-expanded=\"false\">\n    <img src=\"assets/images/users/1.jpg\" alt=\"user\" class=\"profile-pic\"/>\n</a>\n<div class=\"dropdown-menu dropdown-menu-right scale-up\">\n    <ul *ngIf=\"user != null\" class=\"dropdown-user\">\n        <li>\n            <div class=\"dw-user-box\">\n                <div class=\"u-img\"><img src=\"assets/images/users/1.jpg\" alt=\"user\"></div>\n                <div class=\"u-text\">\n                    <h4>{{user.login}}</h4>\n                    <p class=\"text-muted\">{{user.email || 'no email'}}</p>\n                    <a [routerLink]=\"['/user/profile']\" class=\"btn btn-rounded btn-danger btn-sm\">ViewProfile</a>\n                </div>\n            </div>\n        </li>\n        <li role=\"separator\" class=\"divider\"></li>\n        <li><a href=\"#\"><i class=\"ti-user\"></i> My Profile</a></li>\n        <li><a href=\"#\"><i class=\"ti-wallet\"></i> My Balance</a></li>\n        <li><a href=\"#\"><i class=\"ti-email\"></i> Inbox</a></li>\n        <li role=\"separator\" class=\"divider\"></li>\n        <li><a href=\"#\"><i class=\"ti-settings\"></i> Account Setting</a></li>\n        <li role=\"separator\" class=\"divider\"></li>\n        <li><a (click)=\"logout()\" href=\"#\"><i class=\"fa fa-power-off\"></i> Logout</a></li>\n    </ul>\n</div>"

/***/ }),

/***/ "../../../../../src/app/user/components/navigation/user-navigation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserNavigationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserNavigationComponent = (function () {
    function UserNavigationComponent() {
    }
    UserNavigationComponent.prototype.logout = function () {
        localStorage.clear();
        location.href = '/login';
        return false;
    };
    return UserNavigationComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], UserNavigationComponent.prototype, "user", void 0);
UserNavigationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: '[user-navigation]',
        template: __webpack_require__("../../../../../src/app/user/components/navigation/user-navigation.component.html")
    })
], UserNavigationComponent);

//# sourceMappingURL=user-navigation.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/components/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <!-- Column -->\n    <div class=\"col-lg-4 col-xlg-3 col-md-5\">\n        <div class=\"card\">\n            <div class=\"card-block\">\n                <div style=\"text-align: center\" class=\"m-t-30\"> <img src=\"../assets/images/users/5.jpg\" class=\"img-circle\" width=\"150\" />\n                    <h4 class=\"card-title m-t-10\">Hanna Gover</h4>\n                    <h6 class=\"card-subtitle\">Accoubts Manager Amix corp</h6>\n                    <div class=\"row text-center justify-content-md-center\">\n                        <div class=\"col-4\"><a href=\"javascript:void(0)\" class=\"link\"><i class=\"icon-people\"></i> <font class=\"font-medium\">254</font></a></div>\n                        <div class=\"col-4\"><a href=\"javascript:void(0)\" class=\"link\"><i class=\"icon-picture\"></i> <font class=\"font-medium\">54</font></a></div>\n                    </div>\n                </div>\n            </div>\n            <div>\n                <hr> </div>\n            <div class=\"card-block\"> <small class=\"text-muted\">Email address </small>\n                <h6>hannagover@gmail.com</h6> <small class=\"text-muted p-t-30 db\">Phone</small>\n                <h6>+91 654 784 547</h6> <small class=\"text-muted p-t-30 db\">Address</small>\n                <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6>\n                <div class=\"map-box\">\n                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508\" width=\"100%\" height=\"150\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\n                </div> <small class=\"text-muted p-t-30 db\">Social Profile</small>\n                <br/>\n                <button class=\"btn btn-circle btn-secondary\"><i class=\"fa fa-facebook\"></i></button>\n                <button class=\"btn btn-circle btn-secondary\"><i class=\"fa fa-twitter\"></i></button>\n                <button class=\"btn btn-circle btn-secondary\"><i class=\"fa fa-youtube\"></i></button>\n            </div>\n        </div>\n    </div>\n    <!-- Column -->\n    <!-- Column -->\n    <div class=\"col-lg-8 col-xlg-9 col-md-7\">\n        <div class=\"card\">\n            <!-- Nav tabs -->\n            <ul class=\"nav nav-tabs profile-tab\" role=\"tablist\">\n                <li class=\"nav-item\"> <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#home\" role=\"tab\">Timeline</a> </li>\n                <li class=\"nav-item\"> <a class=\"nav-link\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\">Profile</a> </li>\n                <li class=\"nav-item\"> <a class=\"nav-link\" data-toggle=\"tab\" href=\"#settings\" role=\"tab\">Settings</a> </li>\n            </ul>\n            <!-- Tab panes -->\n            <div class=\"tab-content\">\n                <div class=\"tab-pane active\" id=\"home\" role=\"tabpanel\">\n                    <div class=\"card-block\">\n                        <div class=\"profiletimeline\">\n                            <div class=\"sl-item\">\n                                <div class=\"sl-left\"> <img src=\"../assets/images/users/1.jpg\" alt=\"user\" class=\"img-circle\" /> </div>\n                                <div class=\"sl-right\">\n                                    <div><a href=\"#\" class=\"link\">John Doe</a> <span class=\"sl-date\">5 minutes ago</span>\n                                        <p>assign a new task <a href=\"#\"> Design weblayout</a></p>\n                                        <div class=\"row\">\n                                            <div class=\"col-lg-3 col-md-6 m-b-20\"><img src=\"../assets/images/big/img1.jpg\" class=\"img-responsive radius\" /></div>\n                                            <div class=\"col-lg-3 col-md-6 m-b-20\"><img src=\"../assets/images/big/img2.jpg\" class=\"img-responsive radius\" /></div>\n                                            <div class=\"col-lg-3 col-md-6 m-b-20\"><img src=\"../assets/images/big/img3.jpg\" class=\"img-responsive radius\" /></div>\n                                            <div class=\"col-lg-3 col-md-6 m-b-20\"><img src=\"../assets/images/big/img4.jpg\" class=\"img-responsive radius\" /></div>\n                                        </div>\n                                        <div class=\"like-comm\"> <a href=\"javascript:void(0)\" class=\"link m-r-10\">2 comment</a> <a href=\"javascript:void(0)\" class=\"link m-r-10\"><i class=\"fa fa-heart text-danger\"></i> 5 Love</a> </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <hr>\n                            <div class=\"sl-item\">\n                                <div class=\"sl-left\"> <img src=\"../assets/images/users/2.jpg\" alt=\"user\" class=\"img-circle\" /> </div>\n                                <div class=\"sl-right\">\n                                    <div> <a href=\"#\" class=\"link\">John Doe</a> <span class=\"sl-date\">5 minutes ago</span>\n                                        <div class=\"m-t-20 row\">\n                                            <div class=\"col-md-3 col-xs-12\"><img src=\"../assets/images/big/img1.jpg\" alt=\"user\" class=\"img-responsive radius\" /></div>\n                                            <div class=\"col-md-9 col-xs-12\">\n                                                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. </p> <a href=\"#\" class=\"btn btn-success\"> Design weblayout</a></div>\n                                        </div>\n                                        <div class=\"like-comm m-t-20\"> <a href=\"javascript:void(0)\" class=\"link m-r-10\">2 comment</a> <a href=\"javascript:void(0)\" class=\"link m-r-10\"><i class=\"fa fa-heart text-danger\"></i> 5 Love</a> </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <hr>\n                            <div class=\"sl-item\">\n                                <div class=\"sl-left\"> <img src=\"../assets/images/users/3.jpg\" alt=\"user\" class=\"img-circle\" /> </div>\n                                <div class=\"sl-right\">\n                                    <div><a href=\"#\" class=\"link\">John Doe</a> <span class=\"sl-date\">5 minutes ago</span>\n                                        <p class=\"m-t-10\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>\n                                    </div>\n                                    <div class=\"like-comm m-t-20\"> <a href=\"javascript:void(0)\" class=\"link m-r-10\">2 comment</a> <a href=\"javascript:void(0)\" class=\"link m-r-10\"><i class=\"fa fa-heart text-danger\"></i> 5 Love</a> </div>\n                                </div>\n                            </div>\n                            <hr>\n                            <div class=\"sl-item\">\n                                <div class=\"sl-left\"> <img src=\"../assets/images/users/4.jpg\" alt=\"user\" class=\"img-circle\" /> </div>\n                                <div class=\"sl-right\">\n                                    <div><a href=\"#\" class=\"link\">John Doe</a> <span class=\"sl-date\">5 minutes ago</span>\n                                        <blockquote class=\"m-t-10\">\n                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt\n                                        </blockquote>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <!--second tab-->\n                <div class=\"tab-pane\" id=\"profile\" role=\"tabpanel\">\n                    <div class=\"card-block\">\n                        <div class=\"row\">\n                            <div class=\"col-md-3 col-xs-6 b-r\"> <strong>Full Name</strong>\n                                <br>\n                                <p class=\"text-muted\">Johnathan Deo</p>\n                            </div>\n                            <div class=\"col-md-3 col-xs-6 b-r\"> <strong>Mobile</strong>\n                                <br>\n                                <p class=\"text-muted\">(123) 456 7890</p>\n                            </div>\n                            <div class=\"col-md-3 col-xs-6 b-r\"> <strong>Email</strong>\n                                <br>\n                                <p class=\"text-muted\">johnathan@admin.com</p>\n                            </div>\n                            <div class=\"col-md-3 col-xs-6\"> <strong>Location</strong>\n                                <br>\n                                <p class=\"text-muted\">London</p>\n                            </div>\n                        </div>\n                        <hr>\n                        <p class=\"m-t-30\">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>\n                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries </p>\n                        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                        <h4 class=\"font-medium m-t-30\">Skill Set</h4>\n                        <hr>\n                        <h5 class=\"m-t-30\">Wordpress <span class=\"pull-right\">80%</span></h5>\n                        <div class=\"progress\">\n                            <div class=\"progress-bar bg-success\" role=\"progressbar\" aria-valuenow=\"80\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:80%; height:6px;\"> <span class=\"sr-only\">50% Complete</span> </div>\n                        </div>\n                        <h5 class=\"m-t-30\">HTML 5 <span class=\"pull-right\">90%</span></h5>\n                        <div class=\"progress\">\n                            <div class=\"progress-bar bg-info\" role=\"progressbar\" aria-valuenow=\"90\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:90%; height:6px;\"> <span class=\"sr-only\">50% Complete</span> </div>\n                        </div>\n                        <h5 class=\"m-t-30\">jQuery <span class=\"pull-right\">50%</span></h5>\n                        <div class=\"progress\">\n                            <div class=\"progress-bar bg-danger\" role=\"progressbar\" aria-valuenow=\"50\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:50%; height:6px;\"> <span class=\"sr-only\">50% Complete</span> </div>\n                        </div>\n                        <h5 class=\"m-t-30\">Photoshop <span class=\"pull-right\">70%</span></h5>\n                        <div class=\"progress\">\n                            <div class=\"progress-bar bg-warning\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:70%; height:6px;\"> <span class=\"sr-only\">50% Complete</span> </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"tab-pane\" id=\"settings\" role=\"tabpanel\">\n                    <div class=\"card-block\">\n                        <form class=\"form-horizontal form-material\">\n                            <div class=\"form-group\">\n                                <label class=\"col-md-12\">Full Name</label>\n                                <div class=\"col-md-12\">\n                                    <input type=\"text\" placeholder=\"Johnathan Doe\" class=\"form-control form-control-line\">\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"example-email\" class=\"col-md-12\">Email</label>\n                                <div class=\"col-md-12\">\n                                    <input type=\"email\" placeholder=\"johnathan@admin.com\" class=\"form-control form-control-line\" name=\"example-email\" id=\"example-email\">\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label class=\"col-md-12\">Password</label>\n                                <div class=\"col-md-12\">\n                                    <input type=\"password\" value=\"password\" class=\"form-control form-control-line\">\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label class=\"col-md-12\">Phone No</label>\n                                <div class=\"col-md-12\">\n                                    <input type=\"text\" placeholder=\"123 456 7890\" class=\"form-control form-control-line\">\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label class=\"col-md-12\">Message</label>\n                                <div class=\"col-md-12\">\n                                    <textarea rows=\"5\" class=\"form-control form-control-line\"></textarea>\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-12\">Select Country</label>\n                                <div class=\"col-sm-12\">\n                                    <select class=\"form-control form-control-line\">\n                                        <option>London</option>\n                                        <option>India</option>\n                                        <option>Usa</option>\n                                        <option>Canada</option>\n                                        <option>Thailand</option>\n                                    </select>\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <div class=\"col-sm-12\">\n                                    <button class=\"btn btn-success\">Update Profile</button>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <!-- Column -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/user/components/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/user/services/user.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = (function () {
    function ProfileComponent(route, router, userService) {
        this.route = route;
        this.router = router;
        this.userService = userService;
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent.prototype.dsds = function () {
        alert(1234);
    };
    return ProfileComponent;
}()); //sss
ProfileComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: __webpack_require__("../../../../../src/app/user/components/profile/profile.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */]) === "function" && _c || Object])
], ProfileComponent);

var _a, _b, _c;
//# sourceMappingURL=profile.component.js.map

/***/ }),

/***/ "../../../../../src/app/user/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = (function (_super) {
    __extends(AuthService, _super);
    function AuthService(http) {
        var _this = _super.call(this, http, '/api/auth') || this;
        _this.http = http;
        return _this;
    }
    AuthService.prototype.login = function (login, password) {
        var body = {
            login: login,
            password: password
        };
        return this.http
            .post(this.url('/login'), body, this.getOptions(false))
            .map(function (response) { return response.json(); });
    };
    AuthService.prototype.session = function () {
        return this.http
            .get(this.url('/session'), this.getOptions())
            .map(function (response) { return response.json(); });
    };
    AuthService.prototype.getUserId = function () {
        var user = localStorage.getItem('user');
        if (user == null) {
            return null;
        }
        return JSON.parse(user).id;
    };
    return AuthService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
AuthService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], AuthService);

var _a;
//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ "../../../../../src/app/user/services/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_api_service__ = __webpack_require__("../../../../../src/app/core/services/api.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = (function (_super) {
    __extends(UserService, _super);
    function UserService(http) {
        var _this = _super.call(this, http, '/api/users') || this;
        _this.http = http;
        return _this;
    }
    return UserService;
}(__WEBPACK_IMPORTED_MODULE_2__core_services_api_service__["a" /* ApiService */]));
UserService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], UserService);

var _a;
//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map