<?php

return [
    'articles' => [
        'type'  => 'collection',
        'xpath' => '//li[contains(@class, "block-article")]',
        'props' => [
            'url'   => './/h2/a/@href',
            'title' => './/h2/a/text()',
            'text'  => './/div[contains(@class, "field-name-body")]',
            'image' => './/img/@src'
        ]
    ]
];