<?php

namespace app\components\services;

use Yii;
use GuzzleHttp\Client as HttpClient;

/**
 * Class CrawlerFetcherService
 * @package app\components
 */
class CrawlerFetcherService
{
    /**
     * @param $url
     * @param int $cacheDuration
     * @return mixed|\Psr\Http\Message\StreamInterface
     */
    public static function fetch($url, $cacheDuration = 120)
    {
        $key  = md5(self::class . $url);
        $html = Yii::$app->cache->get($key);

        if ($html) {
            return $html;
        }

        $client = new HttpClient();

        $response = $client->request('GET', $url, [
            'http_errors' => false,
            'headers' => [
                'Accept'     => 'text/html,form/xhtml+xml,form/xml;q=0.9,image/webp,*/*;q=0.8',
                'User-Agent' => 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'
            ]
        ]);

        $html = $response->getBody()->getContents();

        Yii::$app->cache->set($key, $html, $cacheDuration);

        return $html;
    }
}