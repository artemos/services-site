<?php

namespace app\controllers;

use app\components\rest\Controller;
use app\components\services\SshService;
use app\models\Instance;
use app\models\Pipeline;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class LogstashConfigController
 * @package app\controllers
 */
class LogstashConfigController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete', 'sync-files'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $pipeline = Pipeline::find()
            ->where(['id' => $id])
            ->one();

        if (!$pipeline) {
            throw new NotFoundHttpException('pipeline not found');
        }

        $instance   = $pipeline->instance;
        $configPath = $instance->params['configPath'] . '/' . $pipeline->logstashConfigFileName;

        $client = new SshService(
            $instance->host,
            $instance->params['sshUsername'],
            $instance->params['sshPassword'],
            $instance->params['sshPort']
        );

        $client->authenticate();

        if ($client->isFileExists($configPath)) {
            $status = 'success';
        } else {
            $status = 'file not exists';
        }

        return ['status' => $status];
    }

    /**
     * @return array
     * @throws
     */
    public function actionCreate()
    {
        $pipeline = Pipeline::find()
            ->where(['id' => $this->requireParam('id')])
            ->one();

        if (!$pipeline) {
            throw new NotFoundHttpException('pipeline not found');
        }

        $instance = Instance::getById($pipeline->instanceId);
        $params   = Json::decode($instance->params);

        $sshParams = ['sshUsername', 'sshPassword', 'sshPort'];
        foreach ($sshParams as $param) {
            if (!isset($params[$param])) {
                throw new BadRequest('required ssh param ' . $param);
            }        
        }

        $client = new SshService($instance->host, $params['sshUsername'], $params['sshPassword'], $params['sshPort']);
        $client->authenticate();

        $path   = $params['configPath'] . "/pipeline-{$pipeline->id}.conf";
        $output = $client->createFile($path, $pipeline->config);

        return [
            'output' => $output
        ];
    }
}