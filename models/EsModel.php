<?php

namespace app\models;

use Yii;
use yii\base\InvalidCallException;
use yii\elasticsearch\ActiveRecord;
use app\components\es\Mapper;

/**
 * Class EsModel
 * @package app\models
 */
class EsModel extends ActiveRecord
{
    /**
     * @var array
     */
    protected static $fields = [];

    /**
     * @var string index Name
     */
    protected static $index ;

    /**
     * @var string index type
     */
    protected static $type;


    /**
     * @param $instance
     * @param null $indexName
     * @param null $indexType
     * @param array $fields
     */
    public static function initialize($instance, $indexName = null, $indexType = null, $fields = [])
    {
        Yii::$app->elasticsearch->nodes = [
            ['http_address' => $instance->host . ':' . $instance->port]
        ];

        self::$index  = $indexName;
        self::$type   = $indexType;
        self::$fields = $fields;
    }

    /**
     * @return string the name of the index this record is stored in.
     */
    public static function index()
    {
        return self::$index;
    }

    /**
     * @return string the name of the type of this record.
     */
    public static function type()
    {
        return self::$type;
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        if (empty(self::$fields)) {
            throw new InvalidCallException('fields must not be empty');
        }

        return [
            static::type() => [
                'properties' => Mapper::buildProperties(self::$fields)
            ]
        ];
    }

    /**
     * @param null $properties
     * @return mixed
     */
    public static function updateMapping($properties = null)
    {
        if ($properties) {
            $mapping = [
                static::type() => [
                    'properties' => $properties
                ]
            ];
        } else {
            $mapping = static::mapping();
        }

        return static::getDb()
            ->createCommand()
            ->setMapping(static::index(), static::type(), $mapping);
    }

    /**
     * Create this model's index
     */
    public static function createIndex()
    {
        return static::getDb()
            ->createCommand()
            ->createIndex(static::index(), [
                'settings' => [],
                'mappings' => static::mapping(),
                //'warmers' => [],
                //'aliases' => [],
                //'creation_date' => ''
            ]
        );
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        return static::getDb()
            ->createCommand()
            ->deleteIndex(static::index(), static::type());
    }
}