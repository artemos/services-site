<?php

use yii\db\Migration;

class m170907_112214_drop_aggregations extends Migration
{
    public function safeUp()
    {
        $this->dropTable('aggregations');
    }

    public function safeDown()
    {
        echo "m170907_112214_drop_aggregations cannot be reverted.\n";
    }
}
