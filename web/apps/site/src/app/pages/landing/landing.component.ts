import {Component, AfterViewInit} from '@angular/core';

@Component({
    templateUrl: './landing.component.html'
})
export class LandingComponent implements AfterViewInit {
    title: string;
    subtitle: string;

    constructor() {
        this.title = "Landing Page";
        this.subtitle = "This is some text within a card block."
    }

    ngAfterViewInit() {
    }
}