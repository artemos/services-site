<?php

namespace app\components\web\response;

use yii\web\ResponseFormatterInterface;

/**
 * Class CsvResponseFormatter
 * @package app\components\web\response
 */
class CsvResponseFormatter implements ResponseFormatterInterface
{
    public function format($response)
    {
        $fileName = 'items_' . date('Y-m-d H:i:s') .  '.csv';

        $response->getHeaders()
            ->set('Content-Type', 'text/csv; charset=UTF-8')
            ->set('Content-Disposition', 'attachment; filename=' . $fileName)
            ->set('Pragma', 'no-cache')
            ->set('Expires', '0');

        $response->content = $this->buildCsv($response->data['items'], $response->data['labels']);
    }

    /**
     * @param array $items
     * @param array $labels
     * @param string $delimiter
     * @param string $enclosure
     * @return string
     */
    protected function buildCsv(array $items, array $labels, $delimiter = ';', $enclosure = '"')
    {
        $cvs = "";

        array_unshift($items, $labels);

        foreach ($items as $item) {
            $fp = fopen('php://temp', 'r+');

            fputcsv($fp, $this->formatAttrubutes($item, $labels), $delimiter, $enclosure);
            rewind($fp);

            $cvs.= fread($fp, 1048576);

            fclose($fp);
        }

        return $cvs;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function formatAttrubutes(array $data, $labels)
    {
        $result = [];

        foreach (array_keys($labels) as $attr) {
            $value = $data[$attr] ?? null;

            if (is_array($value)) {
                $value = join(',', $value);
            }

            $result[$attr] = $value;
        }

        return $result;
    }
}