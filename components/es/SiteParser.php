<?php

namespace app\components\es;

use DOMDocument;
use DOMXPath;

/**
 * Class SiteParser
 * @package app\components\services\es
 */
class SiteParser
{
    public static function parsePage($url)
    {
        libxml_use_internal_errors(true);

        $doc = new DOMDocument();
        $doc->loadHTMLFile($url);

        $xpath = new DOMXPath($doc);

        $tables = self::parseTables($xpath);
        $tables = self::appendReferences($tables, $xpath);

        return [
            'tables'   => $tables
        ];
    }


    protected static function parseTables(DOMXPath $xpath)
    {
        $result = [];
        $tables = $xpath->query('//div[@class="informaltable"]/table');

        foreach ($tables as $t => $table) {
            $rows = $xpath->query(".//tr", $table);
            $map  = [];

            foreach ($rows as $r => $row) {
                if ($r == 0) {
                    $tds  = $row->getElementsByTagName('td');
                    if ($tds->length == 0) {
                        $tds  = $row->getElementsByTagName('th');
                    }

                    foreach ($tds as $i => $td) {
                        $map[$i] = trim($td->textContent);
                    }
                } else {
                    $item = [];
                    $tds  = $row->getElementsByTagName('td');

                    foreach ($tds as $i => $td) {
                        $link = $td->getElementsByTagName('a')->item(0);

                        if ($link) {
                            $item[$map[$i]] = [
                                'text' => $link->textContent,
                                'href' => $link->getAttribute('href')
                            ];
                        } else {
                            $item[$map[$i]] = $td->textContent;
                        }
                    }

                    $result[$t][] = $item;
                }
            }
        }

        return $result;
    }


    protected static function appendReferences($tables, DOMXPath $xpath)
    {
        foreach ($tables as $t => $table) {
            foreach ($table as $r => $row) {
                foreach ($row as $key => $value) {
                    if (is_array($value) && isset($value['href'])) {
                        $id = explode('#', $value['href']);

                        if (!isset($id[1])) {
                            continue;
                        }

                        $id = $id[1];

                        if (in_array($id, ['boolean', 'string', 'number', 'array', 'hash', 'codec', 'password'])) {
                            continue;
                        }

                        $link    = $xpath->query("//a[@id='{$id}']/ancestor::div[@class='section']");
                        $section = $link->item($link->length - 1);
                        $list    = $xpath->query(".//ul[@class='itemizedlist']", $section)->item(0);
                        $desc    = $xpath->query("../following-sibling::p", $list)->item(0);
                        $list    = $xpath->query('.//li', $list);

                        foreach ($list as $li) {
                            $li = trim($li->textContent);


                            $prefixes = [
                                'Default value is' => 'Default'
                            ];

                            foreach ($prefixes as $prefix => $key) {
                                if (mb_substr($li, 0, mb_strlen($prefix)) == $prefix) {
                                    $row[$key] = trim(str_replace([$prefix, ''], '', $li));
                                }
                            }
                        }

                        if ($desc) {
                            $row['Description'] = $desc->textContent;
                        }
                    }
                }

                $table[$r] = $row;
            }

            $tables[$t] = $table;
        }

//        $link    = $xpath->query("//a[@id='plugins-inputs-beats-cipher_suites']/ancestor::div[@class='section']");
//        $section = $link->item($link->length - 1);
//        $list    = $xpath->query(".//ul[@class='itemizedlist']", $section)->item(0);
//        $desc    = $xpath->query("../following-sibling::p", $list)->item(0);

       // dd($desc->textContent);


        return $tables;
    }
}