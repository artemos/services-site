<?php

namespace app\components\es;

/**
 * Class SearchQuery
 * @package app\components\es
 */
class SearchQuery
{
    /**
     * @var array
     */
    protected $equal = [];

    /**
     * @var array
     */
    protected $notEqual = [];

    /**
     * @var array
     */
    protected $in = [];

    /**
     * @var array
     */
    protected $inArray = [];

    /**
     * @var array
     */
    protected $greater = [];

    /**
     * @var array
     */
    protected $greaterOrEqual = [];

    /**
     * @var array
     */
    protected $less = [];

    /**
     * @var array
     */
    protected $lessOrEqual = [];

    /**
     * @var array
     */
    protected $between = [];

    /**
     * @var array
     */
    protected $match = [];

    /**
     * @var array
     */
    protected $exists = [];

    /**
     * @var float
     */
    protected $minScore;

    /**
     * @param $attr
     * @param $value
     * @return $this
     */
    public function attrEqual($attr, $value)
    {
        $this->equal[$attr] = $value;
        return $this;
    }

    /**
     * @param $attr
     * @param $value
     * @return $this
     */
    public function attrNotEqual($attr, $value)
    {
        $this->notEqual[$attr] = $value;
        return $this;
    }

    /**
     * @param $attr
     * @param $value
     */
    public function attrMatch($attr, $value)
    {
        $this->match[$attr] = $value;
    }


    public function attrBetween($attr, $startValue, $endValue)
    {
        $this->between[$attr] = [$startValue, $endValue];
    }

    /**
     * @param $attr
     * @param $value
     * @return $this
     */
    public function attrGreater($attr, $value)
    {
        $this->greater[$attr] = $value;
        return $this;
    }

    /**
     * @param $attr
     * @param $value
     * @return $this
     */
    public function attrLess($attr, $value)
    {
        $this->less[$attr] = $value;
        return $this;
    }

    /**
     * @param $attr
     * @param $value
     * @return $this
     */
    public function attrGreaterOrEqual($attr, $value)
    {
        $this->greaterOrEqual[$attr] = $value;
        return $this;
    }

    /**
     * @param $attr
     * @param $value
     * @return $this
     */
    public function attrLessOrEqual($attr, $value)
    {
        $this->lessOrEqual[$attr] = $value;
        return $this;
    }

    /**
     * @param $attr
     * @param $value
     */
    public function inArray($attr, $value)
    {
        $this->inArray[$attr] = $value;
    }

    /**
     * @param $attr
     */
    public function attrExists($attr)
    {
        $this->exists[] = $attr;
    }

    /**
     * @param $score
     * @return $this
     */
    public function minScore($score)
    {
        $this->minScore = $score;
        return $this;
    }

    /**
     * @return array
     */
    public function build()
    {
        $must    = [];
        $mustNot = [];

        foreach ($this->equal as $attr => $value) {
            if (is_array($value)) {
                $should = [];
                foreach ($value as $val) {
                    $should[] = ['term' => [$attr => $val]];
                }

                $must[] = ['bool' => ['should' => $should]];
            } else {
                $must[] = ['match' => [$attr => $value]];
            }
        }

        foreach ($this->notEqual as $attr => $value) {
            $mustNot[] = ['match' => [$attr => $value]];
        }

        foreach ($this->greater as $attr => $value) {
            $must[] = [
                'range' => [$attr => ['gt' => $value]]
            ];
        }

        foreach ($this->less as $attr => $value) {
            $must[] = [
                'range' => [$attr => ['lt' => $value]]
            ];
        }

        foreach ($this->greaterOrEqual as $attr => $value) {
            $must[] = [
                'range' => [$attr => ['gte' => $value]]
            ];
        }

        foreach ($this->lessOrEqual as $attr => $value) {
            $must[] = [
                'range' => [$attr => ['lte' => $value]]
            ];
        }

        foreach ($this->between as $attr => $values) {
            $must[] = [
                'range' => [$attr => ['gte' => $values[0], 'lte' => $values[1]]]
            ];
        }

        $query = [
            'bool' => [
                'must'     => $must,
                'must_not' => $mustNot
            ]
        ];

        if ($this->exists) {
            $exists = [];

            foreach ($this->exists as $attr) {
                $exists[] = ['exists' => ['field' => $attr]];
            }

            $query['bool']['should'] = $exists;
        }

        if (!$query['bool']['must']) {
            unset($query['bool']['must']);
        }

        if (!$query['bool']['must_not']) {
            unset($query['bool']['must_not']);
        }

        if (!$query['bool']) {
            unset($query['bool']);
        }

        return $query;
    }

    /**
     * @param array $params
     * @return array
     */
    public static function buildFromParams(array $params)
    {
        $query = new self();

        foreach ($params as $name => $value) {
            if (mb_strpos($name, '__') === false) {
                continue;
            }

            list($name, $oper) = explode('__', $name, 2);

            switch ($oper) {
                case 'eq':
                    $query->attrEqual($name, $value);
                    break;

                case 'not_eq':
                    $query->attrNotEqual($name, $value);
                    break;

                case 'gt':
                    $query->attrGreater($name, $value);
                    break;

                case 'gte':
                    $query->attrGreaterOrEqual($name, $value);
                    break;

                case 'lt':
                    $query->attrLess($name, $value);
                    break;

                case 'lte':
                    $query->attrLessOrEqual($name, $value);
                    break;

                case 'exists':
                    $query->attrExists($name);
                    break;
            }
        }

        return $query->build();
    }
}