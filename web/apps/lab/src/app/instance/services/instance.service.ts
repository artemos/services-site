import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ApiService} from '../../core/services/api.service';
import 'rxjs/add/operator/map';

@Injectable()
export class InstanceService extends ApiService {
    constructor(protected http: Http) {
        super(http, '/api/instances');
    }

    fetchPingStatus(id): Observable <any> {
        return this.http
            .get(this.url('/' +id + '/ping'), this.getOptions())
            .map(response => response.json());
    }

    fetchShhStatus(id): Observable <any> {
        return this.http
            .get(this.url('/' +id + '/ssh'), this.getOptions())
            .map(response => response.json());
    }

    fetchHealth(id): Observable <any> {
        return this.http
            .get(this.url('/' +id + '/health'), this.getOptions())
            .map(response => response.json());
    }
}