<?php

use yii\db\Migration;

/**
 * Class m171019_103910_drop_instances_columns
 */
class m171019_103910_drop_instances_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $fields = ['sshPort', 'sshUsername', 'sshPassword', 'configPath'];
        foreach ($fields as $field) {
            $this->dropColumn('instances', $field);
        }
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
