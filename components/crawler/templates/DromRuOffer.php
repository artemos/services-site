<?php

return [
    'offer' => [
        'type'  => 'single',
        'props' => [
            'city'   => '//span[@class="b-text-gray"][contains(text(), "Город")]/following-sibling::text()',
            'phones' => [
                'type'  => 'collection',
                'xpath' => '//span[contains(@class, "b-media-cont__label")]',
                'props' => [
                    'number' => [
                        'xpath'   => './/text()',
                        'regexp'  => '/([0-9]{1} \([0-9]{3}\) [0-9]{3}-[0-9]{4})/',
                        'replace' => [[' ', '(', ')', '-'], '']
                    ],
                ]
            ],
            'horse_power' => [
                'xpath'   => '//span[@data-section="auto-description"]',
                'replace' => ['л. с.', 'л.с.'],
                'regexp'  => '/([0-9]+) л\.с\./',
                'cast'    => 'int'
            ]
        ]
    ]
];
//
//$text = str_replace('л. с.', 'л.с.', $text);
//
//preg_match('', $text, $horsePower);