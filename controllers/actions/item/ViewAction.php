<?php

namespace app\controllers\actions\item;

use app\models\Collection;
use app\components\rest\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class ViewAction extends Action
{
    public function run(int $id, int $collectionId)
    {
        $collection = Collection::getById($collectionId);

        $params = [
            'index' => $collection->indexName,
            'type'  => $collection->indexName,
            'id'    => urldecode($id)
        ];

        throw new \Exception('implement it');

        //return EsPresenter::presentGetResponse($response);
    }
}