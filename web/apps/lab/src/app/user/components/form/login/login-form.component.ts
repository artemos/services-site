import {Component, OnInit} from '@angular/core';
import {HttpErrorService}  from "../../../../core/services/http-error.service";
import {AuthService}       from "../../../services/auth.service";

@Component({
    templateUrl : './login-form.component.html'
})
export class LoginFormComponent implements OnInit {
    login: string;
    password: string;
    errors: any;

    constructor(
        private authService: AuthService,
        private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {

    }

    submit() {
        this.authService.login(this.login, this.password).subscribe(
            response => {
                if (response.authKey != null) {
                    localStorage.setItem('user-auth-key', response.authKey);
                    location.href = '/';
                }
            },
            error => {
                this.httpErrorService.handle(error, this);
            }
        );
    }
}