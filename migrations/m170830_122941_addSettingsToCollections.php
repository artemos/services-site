<?php

use yii\db\Migration;
use app\models\Collection;
use yii\helpers\Json;

class m170830_122941_addSettingsToCollections extends Migration
{
    public function safeUp()
    {
        $this->addColumn('collections', 'settings', "jsonb null");

        Collection::updateAll(['settings' => Json::encode([
            'detailButton' => true,
            'editButton'   => true,
            'deleteButton' => true
        ])]);

        $this->execute("ALTER TABLE collections ALTER COLUMN settings SET NOT NULL");
    }


    public function safeDown()
    {
        $this->dropColumn('collections', 'settings');
    }
}
