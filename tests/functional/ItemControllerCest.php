<?php

use app\components\UserIdentity;

/**
 * Class InstanceControllerCest
 */
class ItemControllerCest
{
    protected $jsonType = [
        'id'        => 'integer',
        'item_id'   => 'string',
        'user_id'   => 'null',
        'parent_id' => 'null',
        'content'   => 'string',
    ];


    public function actionIndexBadRequest(\FunctionalTester $i)
    {
        $i->amBearerAuthenticated(BEARER_AUTH_KEY);
        $i->sendGET('/items');
        $i->seeResponseCodeIs(400);
        $i->seeResponseIsJson();
    }


    public function actionIndexSortingByOneField(\FunctionalTester $i)
    {
        $collectionId = 10;
        $limit = 100;

        $i->amBearerAuthenticated(BEARER_AUTH_KEY);
        $i->sendGET('/items', ['collectionId' => $collectionId, 'limit' => $limit, 'sort' => 'sort_field_1:asc']);
        $i->seeResponseCodeIs(200);

        $items = $i->grabResponse();
        $items = json_decode($items, true)['items'];

        $i->assertCount($limit, $items);

        $values = array_column($items, 'sort_field_1');
        $sorted = $values;

        sort($sorted);

        $i->assertTrue($values === $sorted);
    }


    public function actionIndexSortingByTwoFields(\FunctionalTester $i)
    {
        $collectionId = 10;
        $limit = 100;

        $i->amBearerAuthenticated(BEARER_AUTH_KEY);
        $i->sendGET('/items', [
            'collectionId' => $collectionId,
            'limit' => $limit,
            'sort[0]' => 'sort_field_1:asc',
            'sort[1]' => 'sort_field_2:asc',
        ]);

        $i->seeResponseCodeIs(200);

        $items = $i->grabResponse();

        $items = json_decode($items, true)['items'];

        $i->assertCount($limit, $items);

        $prevField1 = null;
        $prevField2 = null;

        foreach ($items as $item) {
            $i->assertTrue($prevField1 <= $item['sort_field_1']);

            if ($prevField1 < $item['sort_field_1']) {
                $prevField2 = null;
            }

            $i->assertTrue($prevField2 <= $item['sort_field_2']);

            $prevField1 = $item['sort_field_1'];
            $prevField2 = $item['sort_field_2'];
        }
    }
}