<?php

namespace app\components\validators;

use yii\validators\Validator;

/**
 * Class ArrayValidator
 * @package app\components\validators
 */
class ArrayValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!is_array($model[$attribute])) {
            $this->addError($model, $attribute, "{$attribute} should be type of array.");
        }
    }
}