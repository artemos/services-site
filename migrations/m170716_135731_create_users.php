<?php

use yii\db\Migration;

class m170716_135731_create_users extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE users
            (
              id          SERIAL PRIMARY KEY,
              social_id   TEXT NOT NULL,
              social_data JSONB NOT NULL,
              auth_key    TEXT NOT NULL,
              created_at TIMESTAMP(0) DEFAULT now(),
              updated_at TIMESTAMP(0) DEFAULT NULL :: TIMESTAMP WITHOUT TIME ZONE
            )
        ");

        $this->execute("CREATE UNIQUE INDEX users_auth_key_i on users(auth_key)");
        $this->execute("CREATE UNIQUE INDEX users_social_id_i on users(social_id)");
    }


    public function safeDown()
    {
        $this->execute('DROP TABLE users');
    }
}
