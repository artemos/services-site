<?php

use yii\db\Migration;

/**
 * Class m170929_140044_update_config_widgets_config_collection_id
 */
class m170929_140044_update_config_widgets_config_collection_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $configs = Yii::$app->db->createCommand("SELECT * FROM configs")->queryAll();

        foreach ($configs as $config) {
            $value = \yii\helpers\Json::decode($config['value']);
            if (isset($value['aggregation'])) {
                $value['collectionId'] = $value['aggregation']['collectionId'];
                unset($value['aggregation']['collectionId']);

                Yii::$app->db->createCommand()->update('configs', ['value' => \yii\helpers\Json::encode($value)], ['id' => $config['id']])->execute();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m170929_140044_update_config_widgets_config_collection_id cannot be reverted.\n";
    }
}
