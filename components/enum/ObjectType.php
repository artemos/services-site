<?php

namespace app\components\enum;
/**
 * Class ObjectType
 */
class ObjectType
{
    const TEXT  = 'text';
    const EMAIL = 'email';
    const PHONE = 'phone';
}