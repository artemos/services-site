<?php

use yii\db\Migration;

class m170717_115310_insert_applications extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO applications (id, app_id, api_key, user_id, name, description, created_at) VALUES
            (1, 'feff1f2c-0c87-4124-995c-9d0fcad9ce54', 'MixDb21tZW50cyxkYXRhLDQwMDQsMTUwMDM2OTE2OA==', 1, 'PerekupClub', 'Сервис поиска и проверки выгодных предложений о продаже авто с пробегом', '2017-07-18 12:12:49'),
            (2, '36287ae8-e995-4402-bdbc-5a0b47c2e168', 'MixNTUEgUGxheSwzMTYxLDE1MDEwNjIwMTM=', 1, 'MMA Play', 'Сервис  предоставления контента по тематике MMA', '2017-07-26 12:40:14');
        ");
    }


    public function safeDown()
    {
        $this->delete('applications');
    }
}
