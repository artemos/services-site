<?php

namespace app\components\web;

use Yii;
use yii\web\BadRequestHttpException;

/**
 * Class Controller
 * @package app\components\web
 */
class Controller extends \yii\web\Controller
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $request = Yii::$app->request;

        if ($request->isGet) {
            $this->params = $request->get();
        } else {
            $this->params = $request->bodyParams;
        }

        return parent::beforeAction($action);
    }
    /**
     * @param array $names
     * @return array
     * @throws BadRequestHttpException
     */
    protected function requiredParams(array $names)
    {
        $values = [];
        foreach ($names as $name) {
            $values[] = $this->requireParam($name);
        }

        return $values;
    }

    /**
     * @param $name
     * @return mixed
     * @throws BadRequestHttpException
     */
    protected function requireParam($name)
    {
        if (!array_key_exists($name, $this->params) || $this->params[$name] === null) {
            throw new BadRequestHttpException("'{$name}' param requried");
        }

        return $this->params[$name];
    }
}