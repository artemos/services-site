<?php

use yii\db\Migration;
use app\components\models\Validators;
use yii\helpers\Json;

class m170918_074852_update_validators_config extends Migration
{
    public function safeUp()
    {
        $validators = Validators::getAllWithParams();

        foreach ($validators as $validator => $params) {
            if ($params) {
                foreach ($params as $name => $data) {
                    $data['name']  = $name;
                    $params[$name] = $data;
                }

                $params = array_values($params);
            }

            $validators[$validator] = [
                'name'   => $validator,
                'params' => $params
            ];
        }

        $validators = array_values($validators);

        $this->update('configs',
            [
                'value' => Json::encode($validators)
            ],
            [
                'key' => 'field-validators',
            ]
        );
    }


    public function safeDown()
    {
        echo "m170918_074852_update_validators_config cannot be reverted.\n";
    }
}
