import {Component, OnInit, Input} from '@angular/core';
import {LogstashConfigService}    from "../../services/logstash-config.service";
import {HttpErrorService}         from "../../../core/services/http-error.service";

@Component({
    selector: 'logstash-config-status',
    templateUrl: './logstash-config-status.component.html'
})
export class LogstashConfigStatusComponent implements OnInit {
    @Input() pipelineId: number;
    status: string;
    cssClass: string;

    constructor(private logstashConfigService: LogstashConfigService, private httpErrorService: HttpErrorService) {
        this.status   = 'Checking...';
        this.cssClass = 'inverse';
    }

    ngOnInit() {
        this.logstashConfigService.fetchById(this.pipelineId).subscribe(
            response => {
                if (response.status == 'file not exists') {
                    this.createLogstashConfig();
                }
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    private createLogstashConfig() {
        this.status   = 'Not exists, creating it...';
        this.cssClass = 'info';

        const config = {
            id: this.pipelineId
        };

        this.logstashConfigService.create(config).subscribe(
            response => {
                console.log(response);
            },
            error => this.httpErrorService.handle(error, this)
        );
    }
}