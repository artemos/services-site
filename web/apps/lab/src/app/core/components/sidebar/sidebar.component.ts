import {Component, Input, OnInit} from '@angular/core';
import {ConfigService}            from "../../services/config.service";

@Component({
    selector    : 'sidebar',
    templateUrl : './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    @Input() user: any;

    sidebar: any[] = [];

    rootSections: any[] = [
        {
            text: "Applications",
            link: "applications",
            icon: "mdi mdi-application"
        },
        {
            text: "Instances",
            link: "instances",
            icon: "mdi mdi-server"
        },
        {
            text: "Collections",
            link: "collections",
            icon: "mdi mdi-database"
        },
        {
            text: "Widgets",
            link: "widgets",
            icon: "mdi mdi-widgets"
        },
        {
            text: "Pipelines",
            link: "pipelines",
            icon: "mdi mdi-chemical-weapon"
        },
        {
            text: "Configs",
            link: "configs",
            icon: "mdi mdi-settings"
        }
    ];

    constructor(private configService: ConfigService) {}

    ngOnInit() {
        let config = this.configService.find('sidebar');

        if (config != null || config.sections != null) {
            this.sidebar.push({
                title    : config.title,
                sections : config.sections
            });
        }

        if (this.user.role == 'root') {
            this.sidebar.push({
                title    : 'NAVIGATION',
                sections : this.rootSections
            });
        }
    }
}
