import {Component, OnInit} from '@angular/core';
import {ActivatedRoute}    from "@angular/router";
import {InstanceService}   from "../../services/instance.service";
import {HttpErrorService}  from "../../../core/services/http-error.service";
import {IndexService} from "../../../crude/services/index.service";

@Component({
    templateUrl : './instance-view.component.html'
})
export class InstanceViewComponent implements OnInit {
    instance  : any;
    indices   : any;
    indexType : string;

    crumbs : any = {
        title: 'View instance',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'Instances', href: '/instances'},
            {title: 'View instance'}
        ]
    };

    constructor(private route: ActivatedRoute,
                private indexService: IndexService,
                private httpErrorService: HttpErrorService,
                private instanceService: InstanceService) {

        this.indexType = 'app';
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.fetchInstance(params.id);
        });
    }

    fetchInstance(id) {
        this.instanceService.fetchById(id).subscribe(
            response => {
                this.instance = response;

                if (this.instance.type == 'elasticsearch') {
                    this.fetchIndices(this.instance.id);
                }
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    fetchIndices(instanceId) {
        let data = {instanceId: instanceId};

        this.indexService.fetchAll(data).subscribe(
            response => this.indices = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    deleteIndex(name) {
        if (!confirm('Delete index "' + name + '" ?')) {
            return;
        }

        this.indexService.delete(name, {instanceId: this.instance.id}).subscribe(
            response => this.fetchIndices(this.instance.id),
            error    => this.httpErrorService.handle(error, this)
        );
    }
}