<?php

namespace app\models;

use app\components\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "crawler_sites".
 *
 * @property int $id
 * @property int $appId
 * @property int $userId
 * @property int $collectionId
 * @property string $name
 * @property string $startUrl
 * @property string $targets
 * @property string $createdAt
 * @property string $updatedAt
 * @property Application $app
 * @property Collection $urlsCollection
 * @property Collection $pagesCollection
 * @property User $user
 */
class CrawlerSite extends ActiveRecord
{
    /**
     * @var array
     */
    public $jsonbAttributes = ['targets'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crawler_sites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appId', 'userId', 'name', 'collectionId', 'startUrl', 'targets'], 'required'],
            [['appId', 'userId', 'collectionId'], 'default', 'value' => null],
            [['appId', 'userId', 'collectionId'], 'integer'],
            [['name', 'startUrl'], 'string'],
            [['startUrl'], 'url'],
            [['createdAt', 'updatedAt'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'appId'        => 'App ID',
            'userId'       => 'User ID',
            'collectionId' => 'Pages Collection ID',
            'name'         => 'Name',
            'startUrl'     => 'Start Url',
            'targets'      => 'Targets',
            'createdAt'    => 'Created At',
            'updatedAt'    => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApp()
    {
        return $this->hasOne(Application::className(), ['id' => 'appId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(Collection::className(), ['id' => 'collectionId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $model = parent::toArray($fields, $expand, $recursive);

        if (isset($model['targets'])) {
            $model['targets'] = Json::decode($model['targets']);
        }

        return $model;
    }
}
