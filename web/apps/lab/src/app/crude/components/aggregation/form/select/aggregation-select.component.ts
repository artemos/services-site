import {Component, OnInit, Input} from '@angular/core';
import {HttpErrorService}         from '../../../../../core/services/http-error.service';
import {ConfigService}            from "../../../../../core/services/config.service";

@Component({
    selector    : 'aggregation-select',
    templateUrl : './aggregation-select.component.html'
})
export class AggregationSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string;

    aggregations: any;

    constructor(private configService: ConfigService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.configService.fetchAll({type: 'aggregation'}).subscribe(
            response => this.aggregations = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}