import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector    : 'date-format-select',
    templateUrl : './date-format-select.component.html'
})
export class DateFormatSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string;

    values: [string] = ['dd.MM', 'dd.MM.YY'];

    ngOnInit() {

    }

    onChange() {

    }
}