<?php

use yii\db\Migration;

class m170815_112926_create_users_configs extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE users_configs
            (
                id SERIAL PRIMARY KEY,
                user_id INTEGER NOT NULL,
                key TEXT NOT NULL,
                value JSONB NOT NULL
            );
        ");

        $this->execute("ALTER TABLE users_configs ADD CONSTRAINT users_configs_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
        $this->execute("CREATE UNIQUE INDEX users_configs_user_key_i ON users_configs(user_id, key)");
    }


    public function safeDown()
    {
        echo "m170815_112926_create_users_configs cannot be reverted.\n";
        return false;
    }
}
