import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector    : 'interval-select',
    templateUrl : './interval-select.component.html'
})
export class IntervalSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string;

    values: [string] = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second'];

    ngOnInit() {

    }

    onChange() {

    }
}