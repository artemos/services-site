<?php

use yii\db\Migration;

class m170905_090540_add_access_to_collection extends Migration
{
    public function safeUp()
    {
        $this->addColumn('collections', 'access', 'jsonb');
    }


    public function safeDown()
    {
        $this->dropColumn('collections', 'access');
    }
}
