import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector    : 'field-select',
    templateUrl : './field-select.component.html'
})
export class FieldSelectComponent implements OnInit {
    @Input() fields         : any[];
    @Input() model          : any;
    @Input() field          : string = 'field';
    @Input() type           : any;
    @Input() sortable       : boolean = false;
    @Input() setSingleValue : boolean = false;

    visibleFields: Array<any> = [];

    ngOnInit() {
        this.visibleFields = this.fields.filter(f => !f.hidden);

        if (this.sortable) {
            this.visibleFields = this.visibleFields.filter(f => f.sortable);
        }

        if (this.type) {
            this.visibleFields = this.visibleFields.filter(f => f.type == this.type);
        }

        if (this.visibleFields.length > 1) {
            this.sortFields();
        } else {
            if (this.visibleFields.length == 1 && this.setSingleValue) {
                this.model[this.field] = this.visibleFields[0].name;
            }
        }
    }

    private sortFields() {
        this.visibleFields.sort((a, b) => {
            if (a.label < b.label) {
                return -1;
            }

            if (a.label > b.label) {
                return 1;
            }

            return 0;
        });
    }
}