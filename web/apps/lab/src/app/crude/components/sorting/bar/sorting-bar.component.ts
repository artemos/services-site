import {Component, ViewChild, OnInit} from '@angular/core';
import {SortingService}       from "../../../services/sorting.service";
import {FieldService} from "../../../services/field.service";

@Component({
    selector    : 'sorting-bar',
    templateUrl : './sorting-bar.component.html'
})
export class SortingBarComponent implements OnInit {
    @ViewChild('sortingFormComponent') sortingFormComponent;

    sorting : any = [];
    fields  : any;

    constructor(
        private sortingService: SortingService,
        private fieldService: FieldService) {
    }

    ngOnInit() {
        this.sortingService.data.subscribe(
            data => this.sorting = data
        );

        this.fieldService.collectionFields.subscribe(
            data => this.fields = data
        );
    }

    openSortingFormModal() {
        this.sortingFormComponent.openModal();
    }

    toggleParamOrder(index) {
        if (this.sorting[index]['order'] == 'asc') {
            this.sorting[index]['order'] = 'desc';
        } else {
            this.sorting[index]['order'] = 'asc';
        }

        this.sortingService.data.next(this.sorting);
    }

    getFieldLabel(name) {
        return this.fieldService.getFieldLabel(name ,this.fields);
    }
}