<?php

namespace app\commands;

use yii\console\Controller;
use app\components\services\DataminerService;

/**
 * Class TestController
 * @package app\commands
 */
class TestController extends Controller
{
    public function actionInsertSortingItems()
    {
        $collectionId = 10;
        $instanceId   = 1;
        $indexName    = 'test_items_sorting';

        DataminerService::deleteIndex($indexName, $instanceId);

        for ($i = 1; $i <= 100; $i++) {
            $data = [
                'id'           => $i,
                'title'        => "Title {$i}",
                'sort_field_1' => rand(0, 10),
                'sort_field_2' => rand(1, 100),
                'collectionId' => $collectionId
            ];

            DataminerService::createItem($data);
        }
    }
}