import {Component, OnInit} from '@angular/core';
import {InstanceService}   from "../../services/instance.service";
import {HttpErrorService}  from "../../../core/services/http-error.service";

@Component({
    templateUrl : './instance-list.component.html'
})
export class InstanceListComponent implements OnInit {
    instances   : any;
    crumbs : any = {
        title: 'Instances',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All instances'}
        ],
        buttons: [
            {text : 'New instance', route: '/instances/new', class: 'btn btn-success'}
        ]
    };

    constructor(private instanceService: InstanceService, private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.instanceService.fetchAll().subscribe(
            response => this.instances = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}