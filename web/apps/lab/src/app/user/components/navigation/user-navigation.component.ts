import {Component, Input} from '@angular/core';

@Component({
  selector: '[user-navigation]',
  templateUrl: './user-navigation.component.html'
})
export class UserNavigationComponent {
	@Input() user;

    logout() {
        localStorage.clear();
        location.href = '/login';
        return false;
    }
}
