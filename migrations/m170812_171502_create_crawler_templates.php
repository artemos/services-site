<?php

use yii\db\Migration;

class m170812_171502_create_crawler_templates extends Migration
{
    public function up()
    {
        $this->execute("BEGIN;");

        $this->execute("
            CREATE TABLE crawler_templates
            (
                id SERIAL PRIMARY KEY NOT NULL,
                app_id INTEGER NOT NULL,
                user_id INTEGER NOT NULL,
                name TEXT,
                schema JSONB,
                created_at TIMESTAMP DEFAULT now(),
                updated_at TIMESTAMP
            );
        ");

        $this->execute("CREATE UNIQUE INDEX templates_name_uindex ON crawler_templates (user_id, name);");

        $this->execute("ALTER TABLE crawler_templates ADD CONSTRAINT crawler_templates_app_fk FOREIGN KEY (app_id) REFERENCES applications (id)");
        $this->execute("ALTER TABLE crawler_templates ADD CONSTRAINT crawler_templates_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");

        $this->execute("COMMIT;");
    }


    public function down()
    {
        $this->execute("DROP TABLE crawler_templates");
    }
}
