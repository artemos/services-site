<?php

use app\components\UserIdentity;

/**
 * Class InstanceControllerCest
 */
class InstanceControllerCest
{
    protected $jsonType = [
        'id'        => 'integer',
        'item_id'   => 'string',
        'user_id'   => 'null',
        'parent_id' => 'null',
        'content'   => 'string',
    ];


    public function testIndexWithoutCredentials(\FunctionalTester $i)
    {
        $i->sendGET('/instances');
        $i->seeResponseCodeIs(401);
        $i->seeResponseIsJson();
    }


    public function testIndexWithInvalidCredentials(\FunctionalTester $i)
    {
        $i->amBearerAuthenticated('userkey');
        $i->sendGET('/instances');
        $i->seeResponseCodeIs(401);
        $i->seeResponseIsJson();
    }


    public function actionIndexWithInvalidParams(\FunctionalTester $i)
    {
        $i->amBearerAuthenticated('invalid key');
        $i->sendGET('/instances');
        $i->seeResponseCodeIs(401);
        $i->seeResponseIsJson();
    }


    public function actionIndex(\FunctionalTester $i)
    {
        $i->amBearerAuthenticated(BEARER_AUTH_KEY);
        $i->sendGET('/instances');
        $i->seeResponseCodeIs(200);
        $i->seeResponseIsJson();
    }
}