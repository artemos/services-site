import {Component, OnInit, Input} from '@angular/core';
import {ApplicationService} from "../../../services/application.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    selector    : 'application-select',
    templateUrl : './application-select.component.html'
})
export class ApplicationSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string = 'appId';

    applications: any;

    constructor(private applicationService: ApplicationService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.applicationService.fetchAll().subscribe(
            response => this.applications = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}