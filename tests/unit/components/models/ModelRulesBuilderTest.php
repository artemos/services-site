<?php
namespace tests\models;

use PHPUnit\Framework\TestCase;
use app\components\models\RulesBuilder;

/**
 * Class RulesBuilder
 * @package tests\models
 */
class ModelRulesBuilderTest extends TestCase
{
    public function testRequired()
    {
        $fields = [
            [
                'name'  => 'name',
                'type'  => 'string',
                'props' => ['required'],
            ],
            [
                'name'  => 'age',
                'type'  => 'integer'
            ],
            [
                'name'  => 'email',
                'type'  => 'email',
                'props' => ['required', 'unique'],
            ],
            [
                'name'  => 'site',
                'type'  => 'url',
                'props' => ['required'],
            ],
            [
                'name'  => 'is_valid',
                'type'  => 'boolean'
            ],
        ];

        $expectRules = [
            [['name', 'email', 'site'], 'required'],
            [['name'], 'string'],
            [['age'], 'number'],
            [['email'], 'email'],
            [['site'], 'url'],
            [['is_valid'], 'boolean'],
            [['email'], 'unique']
        ];

        $resultRules = RulesBuilder::build($fields);

        $this->assertEquals($expectRules, $resultRules);
    }
}
