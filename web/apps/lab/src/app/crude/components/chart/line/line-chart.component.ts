import {Component, Input} from '@angular/core';
import {AfterViewInit}    from '@angular/core';
import {ItemService}      from "../../../services/item.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {ChartComponent} from "../chart.component";

declare var Chartist: any;

@Component({
    selector    : 'line-chart',
    templateUrl : './line-chart.component.html'
})
export class LineChartComponent extends ChartComponent {
    @Input() subTitle: string;

    chartLabels: any;
    chartSeries: any;
    titleLabels: any;

    constructor(
        protected httpErrorService: HttpErrorService,
        protected itemService: ItemService) {

        super(httpErrorService, itemService);
    }

    protected buildChartData(data) {
        switch (this.aggregation.type) {
            case 'date_histogram':
                this.buildDateHistogramData(data.aggregations);
                break;

            case 'date_histogram_term':
                this.buildDateHistogramTermData(data.aggregations);
                break;
        }
    }

    protected buildDateHistogramData(agg) {
        let field   = Object.keys(agg)[0];
        let buckets = agg[field].buckets;
        let labels  = [];
        let series  = [];

        for (let data of buckets) {
            labels.push(data.key_as_string);
            series.push(data.doc_count);
        }

        this.chartLabels = labels;
        this.chartSeries = [series];
    }

    private buildDateHistogramTermData(agg) {
        let field   = Object.keys(agg)[0];
        let buckets = agg[field].buckets;

        let labels = [];
        let series = [];

        this.titleLabels = [];

        for (let bucket of buckets) {
            this.titleLabels.push({
                text  : bucket.key,
                count : bucket.doc_count
            });

            let bucketField  = Object.keys(bucket).find(k => k == 'aggregation');
            let childBuckets = bucket[bucketField].buckets;
            let childSeries  = [];

            for (let data of childBuckets) {
                if (labels.indexOf(data.key_as_string) == -1) {
                    labels.push(data.key_as_string);
                }

                childSeries.push(data.doc_count);
            }

            series.push(childSeries);
        }

        this.chartLabels = labels;
        this.chartSeries = series;
    }

    protected initChart() {
        new Chartist.Line('#' + this.elementId,
            {
                labels: this.chartLabels,
                series: this.chartSeries,
            },
            {
                scaleMinSpace: 20,
                onlyInteger: true,
                // showArea: true,
                // fullWidth: true,
                // plugins: [],
                // axisY: {
                //     scaleMinSpace: 40,
                //     offset: 40
                // },
                // axisX: {
                //     scaleMinSpace: 40,
                //     offset: 40
                // }
            }
        );
    }
}