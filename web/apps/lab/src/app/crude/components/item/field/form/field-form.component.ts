import {Component, OnInit, Input} from '@angular/core';
import {ConfigService} from "../../../../../core/services/config.service";

@Component({
    selector    : 'collection-field-form',
    templateUrl : './field-form.component.html'
})
export class CollectionFieldFormComponent implements OnInit {
    @Input() fields: any;
    @Input() index: number;
    @Input() onFinish: any;

    form   : any;
    dics   : Array <any>;
    update : boolean;

    readonly emptyForm: any = {
        name       : "",
        type       : "string",
        validators : [],
        filters    : [],
    };

    validators  : Array<any>;
    filters     : Array<String>;

    constructor(private configService: ConfigService) {
    }

    ngOnInit() {
        this.filters = this.configService.find('field-filters').map(filter => {
            return {id: filter, text: filter}
        });

        this.validators = this.configService.find('field-validators').map(validator => {
            return {
                id     : validator.name,
                text   : validator.name,
                params : validator.params
            }
        });

        this.initForm();
    }

    initForm() {
        this.dics = this.configService.getDictionaries();

        if (this.index == null) {
            this.form = this.emptyForm;
        } else {
            this.form = this.serializeFieldToForm(this.fields[this.index], this.validators);
        }
    }

    serializeFieldToForm(field, allValidators) {
        let form = { ...field };

        if (form.validators != null) {
            form.validatorNames = form.validators.map((v) => v.name);
        }

        return form;
    }

    serializeFormToField(form) {
        let field = { ...form };
        delete field.validatorNames;

        delete field.values;  //@TODO: delete this later
        delete field.props;   //@TODO: delete this later

        return field;
    }

    addParamsToValidators(fieldValidators, allValidators) {
        for (let fieldValidator of fieldValidators) {
            let validator = allValidators.find(v => v.id == fieldValidator.name);

            if (validator.params.length > 0) {
                fieldValidator.params = validator.params;
            }
        }

        return fieldValidators;
    }

    validatorsChanged(event) {
        if (this.form.validators == null) {
            this.form.validators = [];
        }

        for (let validatorName of event.value) {
            let exists = this.form.validators.find(v => v.name == validatorName);

            if (exists == null) {
                let validator = this.validators.find(v => v.id == validatorName);

                this.form.validators.push({
                    name   : validator.id,
                    params : validator.params
                });
            }
        }

        this.form.validators = this.form.validators.filter(v => event.value.includes(v.name));
    }

    valuesChanged(event, param) {
        param.value = event.value;
    }

    filtersChanged(event) {
        this.form.filters = event.value;
    }

    saveField() {
        let field = this.serializeFormToField(this.form);

        if (this.index == null) {
            this.fields.push(field);
        } else {
            this.fields[this.index] = field;
        }

        if (this.onFinish != null) {
            this.onFinish();
        }
    }
}