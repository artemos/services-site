import {Component, OnInit} from '@angular/core';
import {HttpErrorService}  from "../../../services/http-error.service";
import {ConfigService}     from "../../../services/config.service";
import {ArrayService}      from "../../../services/array.service";

@Component({
    templateUrl : './config-list.component.html'
})
export class ConfigListComponent implements OnInit {
    configs: any;

    crumbs: any = {
        title: 'Config List',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'configs'}
        ],
        buttons: [
            {
                text : 'New config',
                route: '/configs/new',
                class: 'btn btn-primary'
            }
        ]
    };

    constructor(private arrayService: ArrayService,
                private configService: ConfigService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.fetchConfigs();
    }

    private fetchConfigs() {
        this.configService.fetchAll().subscribe(
            response => this.configs = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    deleteConfig(config) {
        if (!confirm("Delete config '" + config.value.title + "'?")) {
            return;
        }

        this.configService.delete(config.id).subscribe(
            ()    => this.arrayService.deleteElement(this.configs, config),
            error => this.httpErrorService.handle(error, this)
        );
    }
}