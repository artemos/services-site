import {Component, OnInit}      from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {ConfigService} from "../../../../core/services/config.service";

@Component({
    templateUrl : './aggregation-form.component.html'
})
export class AggregationFormComponent implements OnInit {
    errors      : any;
    config      : any;
    crumbs : any;

    types: any = [
        {text  : 'Term', value : 'term'},
        {text  : 'Date histogram', value : 'date_histogram'},
        {text  : 'Date histogram & term', value : 'date_histogram_term'}
    ];

    constructor(private route: ActivatedRoute,
                private router: Router,
                private httpErrorService: HttpErrorService,
                private configService: ConfigService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.initCrumbs(params.id);
            this.initAggregation(params.id);
        });
    }

    fetchAggregation(id) {
        this.configService.fetchById(id).subscribe(
            response => this.config = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    submit() {
        if (this.config.key == null) {
            this.config.key = this.configService.generateKey('aggregation');
        }

        this.configService.save(this.config).subscribe(
            response => this.router.navigate(['aggregations']),
            error    => this.httpErrorService.handle(error, this)
        )
    }

    private initAggregation(id) {
        if (id == null) {
            this.config = {
                type  : 'aggregation',
                value : {params: {}}
            }
        } else {
            this.fetchAggregation(id);
        }
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs =  {
                title: 'New aggregation',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All aggregations', href: 'aggregations'},
                    {title: 'New aggregation'}
                ]
            };
        } else {
            this.crumbs = {
                title: 'Update aggregation',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All aggregations', href: '/aggregations'},
                    {title: 'Update aggregation'}
                ]
            };
        }
    }
}