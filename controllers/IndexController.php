<?php

namespace app\controllers;

use app\components\rest\Controller;
use app\models\Collection;
use app\models\EsModel;
use app\models\Instance;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class IndexController
 * @package app\controllers
 */
class IndexController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'delete', 'view-mapping', 'update-mapping'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @param null $instanceId
     * @return array
     */
    public function actionIndex($instanceId)
    {
        try {
            $response = $this->getEsClient($instanceId)->cat()->indices(['format' => 'json']);
        } catch (ElasticsearchException $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return $response;
    }

    /**
     * @param $id
     * @param $instanceId
     * @return array|mixed
     */
    public function actionView($id, $instanceId)
    {
        try {
            $response = $this->getEsClient($instanceId)->cat()->indices(['index' => $id]);
            $response = $response[0];
        } catch (ElasticsearchException $e) {
            $response = Json::decode($e->getMessage());
        }

        return $response;
    }

    /**
     * @return array|mixed
     */
    public function actionCreate()
    {
        $instanceId = $this->requireParam('instanceId');
        $name       = $this->requireParam('name');

        try {
            $response = $this->getEsClient($instanceId)->indices()->create(['index' => $name]);
        } catch (ElasticsearchException $e) {
            $response = Json::decode($e->getMessage());
        }

        return $response;
    }

    /**
     * @param $id
     * @param $instanceId
     * @return array
     */
    public function actionDelete($id, $instanceId)
    {
        try {
            $response = $this->getEsClient($instanceId)->indices()->delete(['index' => $id]);
        } catch (ElasticsearchException $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return $response;
    }

    /**
     * @param $id
     * @param $instanceId
     * @param $indexType
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionViewMapping($id, $instanceId, $indexType)
    {
        try {
            $response = $this->getEsClient($instanceId)->indices()->getMapping(['index' => $id]);

            if (!isset($response[$id])) {
                throw new NotFoundHttpException('index not found');
            }

            if (isset($response[$id]['mappings'][$indexType])) {
                $response = $response[$id]['mappings'][$indexType]['properties'];
            } else {
                $response = [];
            }
        } catch (ElasticsearchException $e) {     
            try {
                $response = Json::decode($e->getMessage());    
            } catch(\Exception $x) {
                $response = ['error' => $e->getMessage()];
            }
        }

        return $response;
    }

    /**
     * @return array|mixed
     */
    public function actionUpdateMapping()
    {
        EsModel::initialize(
            Instance::getById($this->requireParam('instanceId')),
            $this->requireParam('indexName'),
            $this->requireParam('indexType')
        );

        return EsModel::updateMapping($this->requireParam('mapping'));
    }
}