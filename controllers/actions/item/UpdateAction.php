<?php

namespace app\controllers\actions\item;

use app\models\Collection;
use app\models\Item;
use app\components\rest\Action;
use yii\web\NotFoundHttpException;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class UpdateAction extends Action
{
    public function run($collectionId, $id)
    {
        $collection = Collection::findOne($collectionId);

        if (!$collection) {
            throw new NotFoundHttpException('Collection not found');
        }

        $params = $this->getRequestParams();

        $item = new Item(['fields' => $collection->fieldsArray, 'scenario' => 'update']);
        $item->load($params);

        if ($item->validate()) {
            $params = [
                'index'  => $collection->indexName,
                'type'   => $collection->indexName,
                'id'     => urldecode($id),
                'body'   => [
                    'doc' => $params
                ],
            ];

            $this->setResponseStatusCode(201);

            return $this->getEsClient($collection->instanceId)->update($params);
        } else {
            return $item;
        }
    }
}