import {OnInit, Input} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorService} from "../../services/http-error.service";
import {ApiService} from "../../services/api.service";

export class FormComponent implements OnInit {
    @Input() data  : any;
    @Input() embed : boolean;
    @Input() apply : any;

    crumbs : any;
    errors      : Array<any>;
    form        : any;
    done        : boolean;

    constructor(
        protected service: ApiService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected httpErrorService: HttpErrorService,
        protected options: any) {
    }

    ngOnInit() {
        if (this.embed) {
            if (this.data == null) {
                this.initForm();
            } else {
                this.form = this.data;
            }
        } else {
            this.route.params.subscribe(params => {
                this.initCrumbs(params.id);

                if (params.id) {
                    this.fetch(params.id);
                } else {
                    this.initForm();
                }
            });
        }
    }

    fetch(id) {
        this.service.fetchById(id).subscribe(
            response => this.form = this.afterFetch(response),
            error    => this.httpErrorService.handle(error, this)
        );
    }

    afterFetch(form: any) {
        return form;
    }

    initForm() {
        this.form = {};
    }

    beforeSubmit(form: any) {
        return form;
    }

    submit() {
        this.done = false;
        let data  = this.beforeSubmit({...this.form});

        this.service.save(data).subscribe(
            response => {
                this.done = true;

                if (this.apply != null) {
                    this.apply(this.form);
                }

                if (this.options.viewRoute != null && !this.embed) {
                    this.router.navigate([this.options.viewRoute.replace(':id', response.id)])
                }
            },
            error => this.httpErrorService.handle(error, this)
        )
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs = {
                title: 'New ' + this.options.name,
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: this.options.name + ' list', href: this.options.listRoute},
                    {title: 'New ' + this.options.name}
                ]
            };
        } else {
            this.crumbs = {
                title: 'Update ' + this.options.name,
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: this.options.name + ' list', href: this.options.listRoute},
                    {title: 'Update ' + this.options.name}
                ]
            };
        }
    }
}
