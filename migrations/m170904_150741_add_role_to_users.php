<?php

use yii\db\Migration;

class m170904_150741_add_role_to_users extends Migration
{
    public function safeUp()
    {
        $this->addColumn('users', 'role', 'text');
    }


    public function safeDown()
    {
        $this->dropColumn('users', 'role');
    }
}
