import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {ApiService} from '../../core/services/api.service';

@Injectable()
export class PipelineService extends ApiService {
    constructor(protected http: Http) {
        super(http, '/api/pipelines');
    }
}