<?php

namespace app\components\rest;

use Yii;

/**
 * Class OptionsAction
 * @package app\components\rest\actions
 */
class OptionsAction extends \yii\rest\OptionsAction
{
    public $collectionOptions =  ['GET', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'];

    /**
     * @param null $id
     */
    public function run($id = null)
    {
        parent::run($id);

        $allow = Yii::$app->getResponse()->getHeaders()->get('Allow');

        Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Methods', $allow);
    }
}