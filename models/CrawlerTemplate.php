<?php

namespace app\models;

use app\components\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "crawler_templates".
 *
 * @property int $id
 * @property int $appId
 * @property int $userId
 * @property string $name
 * @property string $schema
 * @property string $createdAt
 * @property string $updatedAt
 *
 * @property Application $app
 * @property User $user
 */
class CrawlerTemplate extends ActiveRecord
{
    /**
     * @var array
     */
    public $jsonbAttributes = ['schema'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crawler_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appId', 'userId'], 'required'],
            [['appId', 'userId'], 'default', 'value' => null],
            [['appId', 'userId'], 'integer'],
            [['name', 'schema'], 'string'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['userId', 'name'], 'unique', 'targetAttribute' => ['userId', 'name']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'appId'     => 'App ID',
            'userId'    => 'User ID',
            'name'      => 'Name',
            'schema'    => 'Schema',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApp()
    {
        return $this->hasOne(Application::className(), ['id' => 'appId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
