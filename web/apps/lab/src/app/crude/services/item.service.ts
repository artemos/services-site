import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {ApiService} from '../../core/services/api.service';

@Injectable()
export class ItemService extends ApiService {
    constructor(protected http: Http) {
        super(http, '/api/items');
    }

    fetchAll(params?) {
        ['aggregation', 'condition', 'sorting'].forEach((param) => {
            if (params[param] == null) {
                return;
            }

            if (Array.isArray(params[param])) {
                if (params[param].length > 0) {
                    params[param] = JSON.stringify(params[param]);
                } else {
                    delete params[param];
                }
            } else if (typeof(params[param]) == "object") {
                params[param] = JSON.stringify(params[param]);
            }
        });

        return super.fetchAll(params);
    }
}