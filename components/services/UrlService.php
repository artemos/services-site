<?php

namespace app\components\services;

/**
 * Class UrlService
 * @package app\components\services
 */
class UrlService
{
    /**
     * @param $url
     * @param $originUrl
     * @return string
     */
    public static function makeAbsolute($url, $originUrl)
    {
        $origParts = parse_url($originUrl);
        $urlParts  = parse_url($url);

        if (!isset($urlParts['scheme'])) {
            $urlParts['scheme'] = $origParts['scheme'];
        }
        if (!isset($urlParts['host'])) {
            $urlParts['host'] = $origParts['host'];
        }

        $url = "{$urlParts['scheme']}://{$urlParts['host']}{$urlParts['path']}";

        if (isset($urlParts['query'])) {
            $url.= "?{$urlParts['query']}";
        }

        return $url;
    }
}