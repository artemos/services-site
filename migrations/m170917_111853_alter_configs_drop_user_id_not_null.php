<?php

use yii\db\Migration;

class m170917_111853_alter_configs_drop_user_id_not_null extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE configs ALTER COLUMN "userId" DROP NOT NULL');
    }

    public function safeDown()
    {

    }
}
