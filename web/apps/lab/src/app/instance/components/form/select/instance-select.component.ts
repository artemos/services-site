import {Component, OnInit, Input} from '@angular/core';
import {InstanceService} from "../../../services/instance.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    selector    : 'instance-select',
    templateUrl : './instance-select.component.html'
})
export class InstanceSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string;

    instances: any;

    constructor(private instanceService: InstanceService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.fetchInstances();
    }

    fetchInstances() {
        this.instanceService.fetchAll({type: 'elasticsearch'}).subscribe(
            response => this.instances = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}