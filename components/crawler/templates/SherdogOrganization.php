<?php

$eventProperties = [
    'name'  => './/td[2]/a',
    'url'   => './/td[2]/a/@href',
    'date'  => './/meta[@itemprop="startDate"]/@content',
    'place' => [
        'type' => 'single',
        'props' => [
            'name' => './/td[3]',
            'flag' => './/td[3]/img/@src'
        ]
    ]
];

return [
    'organization' => [
        'type' => 'single',
        'props' => [
            'name'   => '//h2',
            'upcomingEvents' => [
                'type'  => 'collection',
                'xpath' => '//div[@id="upcoming_tab"]/table/tr[@class="odd" or @class="even"]',
                'props' => $eventProperties
            ],
            'recentEvents' => [
                'type'  => 'collection',
                'xpath' => '//div[@id="recent_tab"]/table/tr[@class="odd" or @class="even"]',
                'props' => $eventProperties
            ]
        ]
    ]
];