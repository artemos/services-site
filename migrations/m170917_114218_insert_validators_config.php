<?php

use yii\db\Migration;
use app\components\models\Validators;
use yii\helpers\Json;

class m170917_114218_insert_validators_config extends Migration
{
    public function safeUp()
    {
        $this->insert('configs', [
            'key'   => 'field-validators',
            'value' => Json::encode(Validators::getAll())]
        );
    }

    public function safeDown()
    {
        $this->delete('configs', ['key' => 'field-validators']);
    }
}
