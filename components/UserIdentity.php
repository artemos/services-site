<?php

namespace app\components;

use Yii;
use yii\base\Exception;
use yii\base\Object;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 * Class UserIdentity
 * @package app\components
 */
class UserIdentity extends Object implements IdentityInterface
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var array
     */
    public $db;

    /**
     * @var string
     */
    protected $authKey;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $identity = Yii::$app->redis->get($token);
        if ($identity) {
            $identity = Json::decode($identity);
            $identity = new self($identity);
            $identity->authKey = $token;
        }

        return $identity;
    }

    /**
     * @param int|string $id
     * @throws Exception
     */
    public static function findIdentity($id)
    {
        throw new Exception('called UserIdentity::findIdentity');
    }
}