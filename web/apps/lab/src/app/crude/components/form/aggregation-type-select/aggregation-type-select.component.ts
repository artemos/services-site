import {Component, OnInit, Input} from '@angular/core';
import {AggregationService} from "../../../services/aggregation.service";

@Component({
    selector    : 'aggregation-type-select',
    templateUrl : './aggregation-type-select.component.html'
})
export class AggregationTypeSelectComponent implements OnInit {
    @Input() model : any;
    @Input() field : any;
    @Input() only  : Array<string>;

    types: any;

    constructor(private aggregationService: AggregationService) {}

    ngOnInit() {
        this.types = this.aggregationService.getTypes().filter(t => (this.only == null || this.only.includes(t.value)))
    }
}