import { Component, OnInit, Input } from '@angular/core';
import { ConfigService } from '../../../../../../core/services/config.service';

@Component({
    selector: 'field-type-select',
    templateUrl: './field-type-select.component.html'
})
export class FieldTypeSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string;

    types: Array<string>;

    constructor(private configService: ConfigService) {
        this.field = 'type';
    }

    ngOnInit() {
        this.types = this.configService.find('field-types');
    }
}