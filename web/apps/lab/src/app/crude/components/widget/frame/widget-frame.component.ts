import {Component, Input, OnInit} from '@angular/core';
import {ChartService} from "../../../services/chart.service";

@Component({
    selector    : 'widget-frame',
    templateUrl : './widget-frame.component.html'
})
export class WidgetFrameComponent implements OnInit{
    @Input() id: any;
    @Input() chart: any;
    @Input() subscribe: boolean = false;

    data: any;

    constructor(private chartService: ChartService) {
    }

    ngOnInit() {
        if (this.chart) {
            this.data = {...this.chart};
        }

        if (this.subscribe) {
            this.chartService.frame.subscribe(
                data => {
                    console.log('got chart frame data', data);

                    if (this.data != null && data.selector == this.data.selector) {
                        console.log('refesh timeout');

                        this.data = null;

                        setTimeout(() => {
                            this.data = data;
                        }, 250);
                    } else {
                        console.log('refesh native');
                        this.data = data;
                    }
                }
            );
        }
    }
}