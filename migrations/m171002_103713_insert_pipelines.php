<?php

use yii\db\Migration;

/**
 * Class m171002_103713_insert_pipelines
 */
class m171002_103713_insert_pipelines extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete('pipelines');

        $pipelines = [
            ['mma_articles.conf', 'Import mma articles from rss'],
            ['pc_car_sales.conf', 'Import car sales from db'],
            ['pc_offer_views.conf', 'Import co-worker offer views from db']
        ];

        foreach ($pipelines as $pipeline) {
            $config = ROOT_DIR . '/migrations/logstash/' . $pipeline[0];
            $config = file_get_contents($config);

            $this->insert('pipelines', [
                'instanceId' => 4,
                'userId'     => 1,
                'name'       => $pipeline[1],
                'config'     => $config
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('pipelines');
    }
}
