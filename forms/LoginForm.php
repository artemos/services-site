<?php

namespace app\forms;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Class LoginForm
 * @package app\forms
 */
class LoginForm extends Model
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    protected $errorMessage = 'Login or password is incorrect';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['login', 'validateLogin', 'skipOnError' => true],
            ['login', 'validatePassword', 'skipOnError' => true]
        ];
    }


    public function validateLogin()
    {
        $user = User::find()
            ->where(['login' => $this->login])
            ->limit(1)
            ->one();

        if ($user) {
            $this->user = $user;
        } else {
            $this->addError('login', $this->errorMessage);
        }
    }


    public function validatePassword()
    {
        if (!Yii::$app->getSecurity()->validatePassword($this->password, $this->user->password)) {
            $this->addError('password', $this->errorMessage);
        }
    }
}


