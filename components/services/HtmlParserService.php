<?php

namespace app\components\services;

use DOMDocument;
use DOMXPath;
use DOMElement;
use yii\base\InvalidParamException;

/**
 * Class HtmlParserService
 * @package app\components
 */
class HtmlParserService
{
    /**
     * @param $html
     * @param array $schema
     * @return array
     */
    public static function parseHtml($html, array $schema)
    {
        libxml_use_internal_errors(true);

        $doc = new DOMDocument();
        $doc->loadHTML($html);

        $xpath = new DOMXPath($doc);
        $xpath->registerNamespace("php", "http://php.net/xpath");
        $xpath->registerPHPFunctions("preg_match");

        $result = [];

        foreach ($schema as $name => $object) {
            $result[$name] = self::parseObject($object, $xpath);
        }

        return $result;
    }

    /**
     * @param array $object
     * @param DOMXPath $xpath
     * @param null $contextNode
     * @return array
     */
    protected static function parseObject(array $object, DOMXPath $xpath, $contextNode = null)
    {
        if ($object['type'] == 'collection') {
            $items = $xpath->query($object['xpath'], $contextNode);
            $data  = [];

            foreach ($items as $item) {
                $item = self::parseObjectProperties($object['props'], $xpath, $item);
                $item = array_filter($item);

                if ($item) {
                    $data[] = $item;
                }
            }

            return $data;
        } elseif ($object['type'] == 'single') {
            return self::parseObjectProperties($object['props'], $xpath, $contextNode);
        }
    }


    /**
     * @param array $properties
     * @param DOMXPath $xpath
     * @param DOMElement|null $contextNode
     * @return array
     */
    protected static function parseObjectProperties(array $properties, DOMXPath $xpath, DOMElement $contextNode = null)
    {
        $result = [];

        foreach ($properties as $name => $property) {
            if (is_array($property) && isset($property['type'])) {
                $result[$name] = self::parseObject($property, $xpath, $contextNode);
            } else {
                $modifiers = null;

                if (is_array($property)) {
                    $xpathQuery = $property['xpath'];
                    unset($property['xpath']);

                    $modifiers = $property;
                } else {
                    $xpathQuery = $property;
                }

                $item = $xpath->query($xpathQuery, $contextNode)->item(0);
                if (!$item) {
                    continue;
                }

                $value = trim($item->nodeValue);

                if ($modifiers) {
                    $value = self::applyModifiers($value, $modifiers);
                }

                $result[$name] = $value;
            }
        }

        return $result;
    }

    /**
     * @param $value
     * @param array $modifiers
     * @return mixed
     */
    protected static function applyModifiers($value, array $modifiers = [])
    {
        foreach ($modifiers as $modifier => $args) {
            switch ($modifier) {
                case 'replace':
                    $value = str_replace($args[0], $args[1], $value);
                    break;

                case 'regexp':
                    preg_match($args, $value, $value);
                    if (isset($value[1])) {
                        $value = $value[1];
                    } else {
                        $value = null;
                    }
                    break;

                case 'match':
                    if (!preg_match($args, $value)) {
                        $value = null;
                    }
                    break;

                case 'not_match':
                    if (preg_match($args, $value)) {
                        $value = null;
                    }
                    break;

                case 'cast':
                    if ($args == 'int') {
                        $value = (int) $value;
                    } elseif ($args == 'float') {
                        $value = (float) $value;
                    }
                    break;
            }
        }

        return $value;
    }
}