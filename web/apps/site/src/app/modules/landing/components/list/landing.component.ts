import {Component, AfterViewInit} from '@angular/core';

declare var jQuery: any;

@Component({
    selector    : 'landing',
    templateUrl : './landing.component.html'
})
export class LandingComponent implements AfterViewInit {
    landings: any;

    constructor() {
    }

    ngAfterViewInit() {
        jQuery(".preloader").fadeOut();


        jQuery('#owl-demo2').owlCarousel({
            margin:50,
            nav:false,
            autoplay:true,
            loop: true,
            slideSpeed : 10,
            rewindNav : true,
            scrollPerPage : false,
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                700:{
                    items:1
                },
                1000:{
                    items:1
                },
                1100:{
                    items:1
                }
            }
        });

        jQuery('a').on('click',function(event){
            var $anchor = jQuery(this);
            jQuery('html, body').stop().animate({scrollTop: jQuery($anchor.attr('href')).offset().top - 90 }, 1000);
            event.preventDefault();
        });
    }
}