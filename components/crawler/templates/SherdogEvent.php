<?php

return [
    'event' => [
        'type' => 'single',
        'props' => [
            'name'      => '//title/text()',
            'date'      => '//div[@class="authors_info"]/span[@class="date"]/meta/@content',
            'place'     => [
                'type'       => 'single',
                'props' => [
                    'name' => '//span[@class="author"]/span',
                    'flag' => '//span[@class="author"]/img/@src'
                ]
            ],
            'organization' => [
                'type'       => 'single',
                'props' => [
                    'name' => '//h2/div/a',
                    'url'  => '//h2/div/a/@href'
                ]
            ],
            'mainFight' => [
                'type'       => 'single',
                'props' => [
                    'number' => [
                        'xpath' => '//table[contains(@class, "resume")]/tr[1]/td[1]/em/following-sibling::text()[1]',
                        'cast'  => 'int'
                    ],
                    'method' => [
                        'xpath' => '//table[contains(@class, "resume")]/tr[1]/td[2]/em/following-sibling::text()[1]'
                    ],
                    'referee' => [
                        'xpath' => '//table[contains(@class, "resume")]/tr[1]/td[3]/em/following-sibling::text()[1]'
                    ],
                    'round' => [
                        'xpath' => '//table[contains(@class, "resume")]/tr[1]/td[4]/em/following-sibling::text()[1]',
                        'cast'  => 'int'
                    ],
                    'time' => [
                        'xpath' => '//table[contains(@class, "resume")]/tr[1]/td[5]/em/following-sibling::text()[1]'
                    ],
                    'fighters' => [
                        'type'       => 'collection',
                        'xpath'      => '//div[@class="fight"]/div[contains(@class, "fighter")]',
                        'props' => [
                            'url'    => './/a[@itemprop="url"]/@href',
                            'image'  => './/a[@itemprop="url"]/img/@src',
                            'name'   => './/h3/a/span[@itemprop="name"]/text()',
                            'record' => './/span[@class="record"]/text()',
                            'result' => './/span[contains(@class, "final_result")]/text()'
                        ]
                    ]
                ]
            ],
            'fights'    => [
                'type'       => 'collection',
                'xpath'      => '//tr[@itemprop="subEvent"]',
                'props' => [
                    'number' => [
                        'xpath' => './/td[1]',
                        'cast'  => 'int'
                    ],
                    'method'  => './/td[5]/text()',
                    'referee' => './/td[5]/span/text()',
                    'round'   => [
                        'xpath' => './/td[6]',
                        'cast'  => 'int'
                    ],
                    'time'    => './/td[7]',
                    'fighters' => [
                        'type'       => 'collection',
                        'xpath'      => './/td[contains(@class, "text_left") or contains(@class, "text_right")]',
                        'props' => [
                            'url'     => './/a/@href',
                            'text'    => './/a',
                            'result'  => './/span[contains(@class, "final_result")]',
                            'image'   => './/img[contains(@class, "lazy")]/@src'
                        ]
                    ]
                ]
            ]
        ]
    ]
];