<?php

namespace app\controllers;

use app\models\User;
use app\components\rest\Controller;
use app\components\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view'],
                'allow' => true,
                'roles' => ['@']
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $query = User::find()->select(['id', 'login']);

        return new ActiveDataProvider(['query' => $query]);
    }

    /**
     * @param $id
     * @return \app\components\db\ActiveRecord
     */
    public function actionView($id)
    {
        return $this->getModel($id);
    }
}