<?php

use yii\db\Migration;

class m170905_090550_add_access_to_instances extends Migration
{
    public function safeUp()
    {
        $this->addColumn('instances', 'access', 'jsonb');
    }


    public function safeDown()
    {
        $this->dropColumn('instances', 'access');
    }
}
