<?php

namespace app\models;

use app\components\enum\InstanceType;
use app\components\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "es_instances".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property int $port
 * @property string $host
 * @property array $params
 * @property string $access
 * @property string $createdAt
 */
class Instance extends ActiveRecord
{
    /**
     * @var array
     */
    public $jsonbAttributes = ['access', 'params'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instances';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'appId', 'type', 'name', 'host', 'type'], 'required'],
            ['type', 'in', 'range' => InstanceType::values()],
            [['userId', 'port', 'appId', 'port'], 'integer'],
            [['createdAt'], 'safe'],
            [['host'], 'string'],
            ['name', 'unique', 'targetAttribute' => ['name', 'userId']],
            [['access', 'params'], 'safe'],
           // ['configPath', '\app\components\validators\DirAvailableBySSHValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'userId'    => 'User ID',
            'name'      => 'Name',
            'port'      => 'Port',
            'createdAt' => 'Created At',
        ];
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $instance = parent::toArray($fields, $expand, $recursive);

        foreach ($this->jsonbAttributes as $attribute) {
            if (isset($instance[$attribute])) {
                $instance[$attribute] = Json::decode($instance[$attribute]);
            }
        }

        return $instance;
    }
}
