import {Injectable}        from '@angular/core';
import {ConfigService}     from "./config.service";
import {HttpErrorService}  from "./http-error.service";
import {AuthService}       from "../../user/services/auth.service";

@Injectable()
export class StateService {
    key     : string;
    context : any;

    constructor(
        private configService: ConfigService,
        private authService: AuthService,
        private httpErrorService: HttpErrorService) {
    }

    init(key, context) {
        this.key = key;
        this.context  = context;
        this.loadState();
    }

    setState(state) {
        for (let param in state) {
            if (state.hasOwnProperty(param)) {
                this.context[param] = state[param];
            }
        }

        this.saveState();
    }

    saveState() {
        let state = this.context.getState();
        let key   = this.getStorageKey();
        
        this.configService.set(key, state);

        let data = {
            key    : key,
            value  : state,
            userId : this.authService.getUserId()
        };

        this.configService.upsert(data).subscribe(
            ()    => console.log('state syncronyzed'),
            error => this.httpErrorService.handle(error, this)
        );
    }

    private loadState() {
        let state = null;
        let item  = this.configService.find(this.getStorageKey());

        if (item == null) {
            state = this.context.getState();
        } else {
            state = item;
        }

        for (let prop in state) {
            if (state.hasOwnProperty(prop)) {
                this.context[prop] = state[prop];
            }
        }

        let defaultState = this.context.getState();

        for (let prop in defaultState) {
            if (defaultState.hasOwnProperty(prop) && state[prop] == null) {
                this.context[prop] = defaultState[prop];
            }
        }

        console.log('loaded state', state);
    }

    private getStorageKey() {
        return 'component-state-' + this.key;
    }
}