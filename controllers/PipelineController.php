<?php

namespace app\controllers;

use app\components\rest\Controller;
use app\models\Pipeline;
use app\components\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class PipelineController
 * @package app\controllers
 */
class PipelineController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $query = Pipeline::find();
        $query->with('instance');
        $query->where(['userId' => $this->getUserId()]);

        return new ActiveDataProvider(['query' => $query]);
    }

    /**
     * @param $id
     * @return \app\components\models\ActiveRecord
     */
    public function actionView($id)
    {
        return $this->getModel($id);
    }

    /**
     * @return Pipeline
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $pipeline = new Pipeline();
        $pipeline->load($this->getRequestParams());
        $pipeline->userId = $this->getUserId();

        if ($pipeline->save()) {
            $this->setResponseStatusCode(201);
        } elseif (!$pipeline->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $pipeline;
    }

    /**
     * @param $id
     * @return \app\components\models\ActiveRecord
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        $pipeline = $this->getModel($id);
        $pipeline->load($this->getRequestParams());

        if (!$pipeline->save() && !$pipeline->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $pipeline;
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        if (!$this->getModel($id)->delete()) {
            throw new ServerErrorHttpException('Failed to delete the comment for unknown reason.');
        }

        $this->setResponseStatusCode(204);
    }
}