<?php

use yii\db\Migration;

class m170811_141923_create_crawler_sites extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE crawler_sites
            (
                id SERIAL PRIMARY KEY,
                app_id INTEGER NOT NULL,
                user_id INTEGER NOT NULL,
                instance_id INTEGER NOT NULL,
                collection_id INTEGER NOT NULL,
                name TEXT NOT NULL,
                start_url TEXT NOT NULL,
                targets JSONB NOT NULL,
                created_at TIMESTAMP(0) DEFAULT now(),
                updated_at TIMESTAMP(0) DEFAULT NULL
            );
        ");

        $this->execute("ALTER TABLE crawler_sites ADD CONSTRAINT crawler_sites_app_fk FOREIGN KEY (app_id) REFERENCES applications (id)");
        $this->execute("ALTER TABLE crawler_sites ADD CONSTRAINT crawler_sites_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
        $this->execute("ALTER TABLE crawler_sites ADD CONSTRAINT crawler_sites_pages_collection_fk FOREIGN KEY (collection_id) REFERENCES collections (id) ON DELETE SET NULL ON UPDATE CASCADE");
        $this->execute("ALTER TABLE crawler_sites ADD CONSTRAINT crawler_sites_instance_fk FOREIGN KEY (instance_id) REFERENCES instances (id) ON DELETE SET NULL ON UPDATE CASCADE");
    }


    public function safeDown()
    {
        $this->dropTable('crawler_sites');
    }
}
