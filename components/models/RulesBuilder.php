<?php

namespace app\components\models;
use yii\helpers\ArrayHelper;

/**
 * Class RulesBuilder
 * @package app\components\models
 */
class RulesBuilder
{
    protected static $typeAliases = [
        'text' => 'string'
    ];

    /**
     * @param array $fields
     * @return array
     */
    public static function build(array $fields)
    {
        $rules = [];

        foreach ($fields as $field) {
            $type = $field['type'];

            if (isset(self::$typeAliases[$type])) {
                $type = self::$typeAliases[$type];
            }

            if (isset($field['multiple']) && $field['multiple']) {
                if ($type == 'string') {
                    $type = 'arrayOfString';
                }
            }

            $rules[] = [$field['name'], $type];

            if (isset($field['validators'])) {
                foreach ($field['validators'] as $validator) {
                    if ($type == 'arrayOfString' && $validator['name'] == 'in') {
                        $validator['name'] = 'inArrayOfString';
                    }

                    $rule = [$field['name'], $validator['name']];

                    if (isset($validator['params'])) {
                        if ($validator['name'] == 'inArrayOfString') {
                            $rule['params'] = ArrayHelper::map($validator['params'], 'name', 'value');
                        } else {
                            foreach ($validator['params'] as $param) {
                                $rule[$param['name']] = $param['value'];
                            }
                        }
                    }

                    $rules[] = $rule;
                }
            }

            if (isset($field['filters'])) {
                foreach ($field['filters'] as $filter) {
                    $rules[] = [$field['name'], 'filter', 'filter' => $filter];
                }
            }
        }

        return $rules;
    }
}