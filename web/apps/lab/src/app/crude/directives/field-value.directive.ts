import {Directive, ElementRef} from '@angular/core';
import {Input, OnInit}         from '@angular/core';
import {DatePipe, DecimalPipe} from "@angular/common";
import {ConfigService} from "../../core/services/config.service";

@Directive({
    selector: '[fieldValue]',
    providers: [DatePipe, DecimalPipe]
})
export class FieldValueDirective implements OnInit {
    @Input() field : any;
    @Input() item  : any;

    constructor(
        private element: ElementRef,
        private datePipe: DatePipe,
        private decimalPipe: DecimalPipe,
        private configService: ConfigService) {
    }

    ngOnInit() {
        let value = this.item[this.field.name];

        switch (true) {
            case value == null:
                value = '---';
                break;

            case this.field.view == null:
                break;

            case this.field.view == 'date':
                value = this.datePipe.transform(value, 'MM.dd.yyyy');
                break;

            case this.field.view == 'date_time':
                value = this.datePipe.transform(value, 'MM.dd.yyyy HH.mm');
                break;

            case this.field.view == 'number':
                value = this.decimalPipe.transform(value);
                break;

            case this.field.view == 'short_link':
                value = "<a href='" + value + "' target='blank'>link</a>";
                break;

            case this.field.view.startsWith('dictionary-'):
                value = this.dictionaryValue(this.field.view, value);
                break;
        }

        this.element.nativeElement.innerHTML = value;
    }


    dictionaryValue(configKey, value) {
        let configValue = this.configService.findValueByKey(configKey);

        if (configValue && configValue.map) {
            return configValue.map[value];
        }
    }
}