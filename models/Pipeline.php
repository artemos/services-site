<?php

namespace app\models;

use app\components\db\ActiveRecord;

/**
 * This is the model class for table "pipelines".
 *
 * @property int $id
 * @property int $userId
 * @property int $instanceId
 * @property string $name
 * @property string $config
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $logstashConfigFileName
 * @property Instance $instance
 */
class Pipeline extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipelines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'instanceId', 'name', 'config'], 'required'],
            [['userId', 'instanceId'], 'integer'],
            [['config', 'name'], 'string'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['userId', 'name'], 'unique', 'targetAttribute' => ['userId', 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'userId'    => 'User ID',
            'name'      => 'name',
            'config'    => 'config',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstance()
    {
        return $this
            ->hasOne(Instance::className(), ['id' => 'instanceId'])
            ->select(['id', 'name', 'host', 'port']);
    }

    /**
     * @return mixed|string
     */
    public function getLogstashConfigFileName()
    {
        $name = str_replace(' ', '_', trim($this->name));
        $name = mb_strtolower($name);

        return $name;
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $expand[] = 'instance';
        $pipeline = parent::toArray($fields, $expand, $recursive);

        return $pipeline;
    }
}
