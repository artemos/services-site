<?php

use yii\db\Migration;

class m170912_122133_drop_config_type_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('configs', 'type');
    }


    public function safeDown()
    {
    }
}
