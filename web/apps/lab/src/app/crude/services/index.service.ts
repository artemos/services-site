import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ApiService} from '../../core/services/api.service';
import 'rxjs/add/operator/map';

@Injectable()
export class IndexService extends ApiService {
    constructor(protected http: Http) {
        super(http, '/api/indices');
    }

    fetchByName(name, instanceId): Observable <any> {
        return this.http
            .get(this.url('/' + name + '?instanceId=' + instanceId), this.getOptions())
            .map(response => response.json());
    }

    fetchMapping(name, type, instanceId) {
        return this.http
            .get(this.url('/' + name + '/mapping?instanceId=' + instanceId + '&indexType=' + type), this.getOptions())
            .map(response => response.json());
    }

    updateMapping(data) {
        return this.http
            .put(this.url('/mapping'), data, this.getOptions())
            .map(response => response.json());
    }
}