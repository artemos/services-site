<?php

namespace app\components\db;
use yii\db\Expression;

/**
 * Class ActiveQuery
 * @package app\components\db
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @param int $userId
     * @return $this
     */
    public function allowed(int $userId)
    {
        return $this->andWhere(new Expression("\"userId\" = {$userId} OR access @> '[{$userId}]'"));
    }

    /**
     * @return string
     */
    public function getSql()
    {
        $cmd = $this->createCommand();
        return $cmd->rawSql;
    }
}