import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, BrowserXhr } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { NgbAlert, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SortablejsModule } from 'angular-sortablejs';
import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';
import { CommonModule } from '@angular/common';
import { Select2Module } from 'ng2-select2';
import { MomentModule } from 'angular2-moment';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { NavigationComponent } from './core/components/header-navigation/navigation.component';
import { SidebarComponent } from './core/components/sidebar/sidebar.component';
import { RightSidebarComponent } from './core/components/right-sidebar/rightsidebar.component';
import { AppComponent } from './app.component';
import { UserNavigationComponent } from './user/components/navigation/user-navigation.component';
import { TestComponent } from './test/components/test/test.component';
import { DashboardComponent } from './core/components/dashboard/dashboard.component';
import { LoginFormComponent } from './user/components/form/login/login-form.component';
import { InstanceViewComponent } from './instance/components/view/instance-view.component';
import { InstanceCardComponent } from './instance/components/card/instance-card.component';
import { InstanceFormComponent } from './instance/components/form/instance-form.component';
import { InstanceService } from './instance/services/instance.service';
import { HandshakeService } from './core/services/handshake.service';
import { PipelineService } from './pipeline/services/pipeline.service';
import { HttpErrorService } from './core/services/http-error.service';
import { LogstashPluginService } from './pipeline/services/logstash-plugin.service';
import { LogstashConfigService } from './pipeline/services/logstash-config.service';
import { ApplicationService } from './application/services/application.service';
import { AuthService } from './user/services/auth.service';
import { UserService } from './user/services/user.service';
import { ConfigService } from './core/services/config.service';
import { ArrayService } from './core/services/array.service';
import { UrlService } from './core/services/url.service';
import { StateService } from './core/services/state.service';
import { ItemService } from './crude/services/item.service';
import { IndexService } from './crude/services/index.service';
import { CollectionService } from './crude/services/collection.service';
import { AggregationService } from './crude/services/aggregation.service';
import { ChartService } from './crude/services/chart.service';
import { FieldService } from './crude/services/field.service';
import { InstanceListComponent } from './instance/components/list/instance-list.component';
import { InstanceHealthComponent } from './instance/components/health/instance-health.component';
import { InstanceStatusComponent } from './instance/components/status/instance-status.component';
import { PipelineListComponent } from './pipeline/components/list/pipeline-list.component';
import { PipelineFormComponent } from './pipeline/components/form/pipeline-form.component';
import { LogstashConfigStatusComponent } from './pipeline/components/logstash-config-status/logstash-config-status.component';
import { ApplicationListComponent } from './application/components/list/application-list.component';
import { ApplicationFormComponent } from './application/components/form/application-form.component';
import { ApplicationViewComponent } from './application/components/view/application-view.component';
import { ApplicationCardComponent } from './application/components/card/application-card.component';
import { ApplicationSelectComponent } from './application/components/form/select/application-select.component';
import { PreloaderComponent } from './core/components/preloader/preloader.component';
import { StatusLabelComponent } from './core/components/status-label/status-label.component';
import { ErrorMessageComponent } from './core/components/error-message/error-message.component';
import { ConfigFormComponent } from './core/components/config/form/config-form.component';
import { BreadcrumbComponent } from './core/components/breadcrumb/breadcrumb.component';
import { ConfigListComponent } from './core/components/config/list/config-list.component';
import { CollectionListComponent } from './crude/components/collection/list/collection-list.component';
import { CollectionFormComponent } from './crude/components/collection/form/collection-form.component';
import { CollectionViewComponent } from './crude/components/collection/view/collection-view.component';
import { CollectionCardComponent } from './crude/components/collection/card/collection-card.component';
import { CollectionFieldListComponent } from './crude/components/item/field/list/field-list.component';
import { CollectionFieldFormComponent } from './crude/components/item/field/form/field-form.component';
import { ValidatorLabelComponent } from './crude/components/item/field/validator-label/validator-label.component';
import { ItemListComponent } from './crude/components/item/list/item-list.component';
import { ItemFormComponent } from './crude/components/item/form/item-form.component';
import { ItemViewComponent } from './crude/components/item/view/item-view.component';
import { ItemCardComponent } from './crude/components/item/card/item-card.component';
import { CollectionDashboardComponent } from './crude/components/collection/dashboard/collection-dashboard.component';
import { FilterFormComponent } from './crude/components/filter/form/filter-form.component';
import { FilterBarComponent } from './crude/components/filter/bar/filter-bar.component';
import { SortingFormComponent } from './crude/components/sorting/form/sorting-form.component';
import { SortingBarComponent } from './crude/components/sorting/bar/sorting-bar.component';
import { MapToIterablePipe } from './core/pipes/map-to-iterable.pipe';
import { NotHiddenPipe } from './core/pipes/not-hidden.pipe';
import { ChartFormComponent } from './crude/components/chart/form/chart-form.component';
import { FieldSelectComponent } from './crude/components/item/field/form/select/field-select.component';
import { AggregationListComponent } from './crude/components/aggregation/list/aggregation-list.component';
import { AggregationFormComponent } from './crude/components/aggregation/form/aggregation-form.component';
import { AggregationSelectComponent } from './crude/components/aggregation/form/select/aggregation-select.component';
import { AggregationTypeSelectComponent } from './crude/components/form/aggregation-type-select/aggregation-type-select.component';
import { WidgetFrameComponent } from './crude/components/widget/frame/widget-frame.component';
import { WidgetViewComponent } from './crude/components/widget/view/widget-view.component';
import { WidgetFormComponent } from './crude/components/widget/form/widget-form.component';
import { WidgetListComponent } from './crude/components/widget/list/widget-list.component';
import { ProfileComponent } from './user/components/profile/profile.component';
import { UserSelectComponent } from './user/components/form/select/user-select.component';
import { AccessSelectComponent } from './user/components/form/access-select/access-select.component';
import { CollectionSelectComponent } from './crude/components/collection/form/select/collection-select.component';
import { InstanceSelectComponent } from './instance/components/form/select/instance-select.component';
import { IntervalSelectComponent } from './crude/components/form/interval-select/interval-select.component';
import { DateFormatSelectComponent } from './crude/components/form/date-format-select/date-format-select.component';
import { LineChartComponent } from './crude/components/chart/line/line-chart.component';
import { PieChartComponent } from './crude/components/chart/pie/pie-chart.component';
import { BarChartComponent } from './crude/components/chart/bar/bar-chart.component';
import { ItemListHeaderComponent } from './crude/components/item/list/header/item-list-header.component';
import { ItemListSettingsFormComponent } from './crude/components/item/list/settings-form/item-list-settings-form.component';
import { IndexFormComponent } from './crude/components/index/form/index-form.component';
import { IndexMappingViewComponent } from './crude/components/index/mapping/view/index-mapping-view.component';
import { IndexMappingFormComponent } from './crude/components/index/mapping/form/index-mapping-form.component';
import { FieldValueDirective } from './crude/directives/field-value.directive';
import { FieldTypeSelectComponent } from './crude/components/item/field/form/type-select/field-type-select.component';
import { FilterService } from './crude/services/filter.service';
import { SortingService } from './crude/services/sorting.service';
import { MappingService } from './crude/services/mapping.service';
import { ItemListSettingsService } from './crude/services/item-list-settings.service';

import * as moment from 'moment';
moment.locale('ru');

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent
    },
    {
        path: 'test',
        component: TestComponent
    },
    {
        path: 'login',
        component: LoginFormComponent
    },
    {
        path: 'applications',
        component: ApplicationListComponent
    },
    {
        path: 'applications/new',
        component: ApplicationFormComponent
    },
    {
        path: 'applications/:id',
        component: ApplicationViewComponent
    },
    {
        path: 'applications/update/:id',
        component: ApplicationFormComponent
    },
    {
        path: 'instances',
        component: InstanceListComponent
    },
    {
        path: 'instances/new',
        component: InstanceFormComponent
    },
    {
        path: 'instances/:id',
        component: InstanceViewComponent
    },
    {
        path: 'instances/update/:id',
        component: InstanceFormComponent
    },
    {
        path: 'instances/:id/indices/:name/mapping',
        component: IndexMappingViewComponent
    },
    {
        path: 'instances/:id/indices/:name/mapping/update',
        component: IndexMappingFormComponent
    },
    {
        path: 'instances/:id/indices/new',
        component: IndexFormComponent
    },
    {
        path: 'configs',
        component: ConfigListComponent,
    },
    {
        path: 'configs/new',
        component: ConfigFormComponent,
    },
    {
        path: 'configs/update/:id',
        component: ConfigFormComponent
    },
    {
        path: 'collections',
        component: CollectionListComponent
    },
    {
        path: 'collections/new',
        component: CollectionFormComponent
    },
    {
        path: 'collections/:id',
        component: CollectionViewComponent
    },
    {
        path: 'collections/update/:id',
        component: CollectionFormComponent
    },
    {
        path: 'collections/:collectionId/items',
        component: ItemListComponent
    },
    {
        path: 'collections/:collectionId/dashboard',
        component: CollectionDashboardComponent
    },
    {
        path: 'collections/:collectionId/items/new',
        component: ItemFormComponent
    },
    {
        path: 'collections/:collectionId/items/:id',
        component: ItemViewComponent
    },
    {
        path: 'collections/:collectionId/items/update/:id',
        component: ItemFormComponent
    },
    {
        path: 'aggregations',
        component: AggregationListComponent
    },
    {
        path: 'aggregations/new',
        component: AggregationFormComponent
    },
    {
        path: 'aggregations/update/:id',
        component: AggregationFormComponent
    },
    {
        path: 'widgets',
        component: WidgetListComponent
    },
    {
        path: 'widgets/new',
        component: WidgetFormComponent
    },
    {
        path: 'widgets/:id',
        component: WidgetViewComponent
    },
    {
        path: 'widgets/update/:id',
        component: WidgetFormComponent
    },
    {
        path: 'pipelines',
        component: PipelineListComponent
    },
    {
        path: 'pipelines/new',
        component: PipelineFormComponent
    },
    {
        path: 'pipelines/update/:id',
        component: PipelineFormComponent
    }
];

@NgModule({
    declarations: [
        FieldValueDirective,
        AccessSelectComponent,
        AggregationFormComponent,
        AggregationListComponent,
        AggregationSelectComponent,
        AggregationTypeSelectComponent,
        AppComponent,
        ApplicationCardComponent,
        ApplicationFormComponent,
        ApplicationListComponent,
        ApplicationSelectComponent,
        ApplicationViewComponent,
        BarChartComponent,
        BreadcrumbComponent,
        ChartFormComponent,
        CollectionCardComponent,
        CollectionDashboardComponent,
        CollectionFieldFormComponent,
        CollectionFieldListComponent,
        CollectionFormComponent,
        CollectionListComponent,
        CollectionSelectComponent,
        CollectionViewComponent,
        ConfigFormComponent,
        ConfigListComponent,
        DashboardComponent,
        DateFormatSelectComponent,
        ErrorMessageComponent,
        FieldSelectComponent,
        FieldTypeSelectComponent,
        FilterFormComponent,
        InstanceCardComponent,
        InstanceFormComponent,
        InstanceHealthComponent,
        InstanceListComponent,
        InstanceSelectComponent,
        InstanceStatusComponent,
        InstanceViewComponent,
        IndexMappingViewComponent,
        IndexMappingFormComponent,
        IntervalSelectComponent,
        ItemCardComponent,
        ItemFormComponent,
        ItemListComponent,
        ItemListHeaderComponent,
        ItemListSettingsFormComponent,
        ItemViewComponent,
        LineChartComponent,
        LoginFormComponent,
        LogstashConfigStatusComponent,
        MapToIterablePipe,
        NavigationComponent,
        NotHiddenPipe,
        PieChartComponent,
        PipelineFormComponent,
        PipelineListComponent,
        PreloaderComponent,
        ProfileComponent,
        RightSidebarComponent,
        SidebarComponent,
        SortingFormComponent,
        SortingBarComponent,
        StatusLabelComponent,
        TestComponent,
        UserNavigationComponent,
        UserSelectComponent,
        ValidatorLabelComponent,
        WidgetFormComponent,
        WidgetFrameComponent,
        WidgetListComponent,
        WidgetViewComponent,
        FilterBarComponent,
        IndexFormComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        CommonModule,
        Select2Module,
        MomentModule,
        SortablejsModule,
        InfiniteScrollModule,
        NgProgressModule,
        NgbModule.forRoot(),
        RouterModule.forRoot(routes)
    ],
    providers: [
        AggregationService,
        ApplicationService,
        ArrayService,
        AuthService,
        ChartService,
        CollectionService,
        ConfigService,
        FieldService,
        HandshakeService,
        HttpErrorService,
        IndexService,
        InstanceService,
        ItemService,
        LogstashConfigService,
        LogstashPluginService,
        NgbAlert,
        PipelineService,
        StateService,
        UrlService,
        UserService,
        FilterService,
        SortingService,
        MappingService,
        ItemListSettingsService,
        {
            provide: BrowserXhr,
            useClass: NgProgressBrowserXhr
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
