<?php

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;
use app\components\rest\Controller;
use app\forms\LoginForm;
use app\models\User;

/**
 * Class AuthController
 * @package app\controllers
 */
class AuthController extends Controller
{
    public $enableCsrfValidation = false;


    protected function accessRules()
    {
        return [
            [
                'actions' => ['login'],
                'allow'   => true,
                'roles'   => ['?'],
            ],
            [
                'actions' => ['session'],
                'allow'   => true,
                'roles'   => ['@'],
            ]
        ];
    }

    /**se
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws ServerErrorHttpException
     */
    public function actionSocial()
    {
        $token = $this->requireParam('token');
        $data  = file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . $_SERVER['HTTP_HOST']);
        $data  = Json::decode($data);

        if (isset($data['error'])) {
            throw new HttpException($data['error']);
        }

        if (!isset($data['identity'])) {
            throw new BadRequestHttpException('identity required');
        }

        $transaction = Yii::$app->db->beginTransaction();

        $user = User::find()
            ->where(['socialId' => $data['identity']])
            ->one();

        if (!$user) {
            $user = new User();
            $user->attributes = [
                'socialId'   => $data['identity'],
                'socialData' => Json::encode($data),
                'authKey'    => Yii::$app->getSecurity()->generatePasswordHash($data['identity'])
            ];

            if (!$user->save()) {
                $transaction->rollBack();
                throw new ServerErrorHttpException('cant save user');
            }
        }

        $transaction->commit();

        Yii::$app->user->login($user, 3600 * 24 * 90);

        $this->redirect('/dashboard');
    }

    /**
     * @return LoginForm|array
     */
    public function actionLogin()
    {
        $form = new LoginForm([
            'login'    => $this->param('login'),
            'password' => $this->param('password')
        ]);

        if ($form->validate()) {
            return ['authKey' => $form->user->authKey];
        }

        return $form;
    }

    /**
     * @return array
     */
    public function actionSession()
    {
        $attrs = Yii::$app->user->identity->attributes;

        return [
            'id'        => $attrs['id'],
            'login'     => $attrs['login'],
            'role'      => $attrs['role'],
            'createdAt' => $attrs['createdAt']
        ];
    }
}