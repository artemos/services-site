<?php

namespace app\components\services\api;

use yii;
use GuzzleHttp\Client;
use yii\web\ServerErrorHttpException;

/**
 * Class DataServiceApi
 * @package app\components\services\api
 */
class DataServiceApi
{
    /**
     * Service host
     */
    const BASE_URL = 'http://192.168.56.113/api';

    /**
     * Auth key
     */
    const AUTH_KEY = 'data-service-auth-key';

    /**
     * @param $method
     * @param $url
     * @param array $params
     * @return yii\console\Response|yii\web\Response
     * @throws ServerErrorHttpException
     */
    public static function request($method, $url, $params = [])
    {
        $method = strtolower($method);

        $options = [
            'http_errors' => false,
            'headers'     => ['Authorization' => 'Bearer ' . self::AUTH_KEY]
        ];

        if ($params) {
            if (in_array($method, ['get', 'head'])) {
                $url.= '?' . http_build_query($params);
            } else {
                $options['form_params'] = $params;
            }
        }

        $client = new Client();

        $serviceResponse = $client->request(
            $method,
            self::BASE_URL . $url,
            $options
        );

        $statusCode = $serviceResponse->getStatusCode();
        if (!in_array($statusCode, [200, 201, 204, 422])) {
            //throw new ServerErrorHttpException();
        }

        $response = Yii::$app->getResponse();
        $response->setStatusCode($serviceResponse->getStatusCode());
        $response->content = $serviceResponse->getBody();

        return $response;
    }
}