<?php

namespace app\components\db;
use yii\base\UnknownClassException;
use yii\helpers\BaseInflector;
use yii\helpers\Inflector;
use yii\web\NotFoundHttpException;

/**
 * Class ActiveRecord
 * @package app\components\models
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [['class' => 'app\components\models\behaviors\JsonbAttributesBehavior']]);
    }

    /**
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * @param $condition
     * @return \yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getOne($condition)
    {
        $model = parent::findOne($condition);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @param int $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getById(int $id)
    {
        $model = parent::find()->where(['id' => (int) $id])->limit(1)->one();
        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}