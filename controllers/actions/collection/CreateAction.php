<?php

namespace app\controllers\actions\collection;

use app\components\es\Mapper;
use app\models\Collection;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use app\components\rest\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class CreateAction extends Action
{
    public function run()
    {
        $collection = new Collection();
        $collection->load($this->getRequestParams());
        $collection->userId = $this->getUserId();

        if ($collection->validate()) {
            try {
                if ($this->param('createIndex')) {
                    $params = [
                        'index' => $collection->indexName,
                    ];

                    if ($this->param('createMapping')) {
                        $params['body'] = [
                            'mappings' => [
                                $collection->indexType => [
                                    'properties' => Mapper::buildProperties($collection->fieldsArray)
                                ]
                            ]
                        ];
                    }

                    $this->getEsClient($collection->instanceId)->indices()->create($params);
                }

                $collection->save(false);

                $this->setResponseStatusCode(201);
            } catch (ElasticsearchException $e) {
                $collection->addError('instanceId', $e->getMessage());
            }
        }

        return $collection;
    }
}