import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router}   from "@angular/router";
import {InstanceService}          from "../../services/instance.service";
import {HandshakeService}         from "../../../core/services/handshake.service";
import {HttpErrorService}         from "../../../core/services/http-error.service";

@Component({
    templateUrl : './instance-form.component.html'
})
export class InstanceFormComponent implements OnInit {
    @Input() form: any;

    errors : any;
    types  : any;
    crumbs : any;

    constructor(private route: ActivatedRoute,
                private httpErrorService: HttpErrorService,
                private handshakeSevice: HandshakeService,
                private instanceService: InstanceService,
                private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.fetchHandshake();
            this.initCrumbs(params.id);
            this.initForm(params.id);
        });
    }

    fetchInstance(id) {
        this.instanceService.fetchById(id).subscribe(
            response => {
                this.form = response;
                if (this.form.params == null) {
                    this.form.params = {};
                }
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    fetchHandshake() {
        this.handshakeSevice.fetchAll().subscribe(
            response => {
                let map   = response.enums.instanceType.map;
                let types = [];

                for (let i in map) {
                    if (map.hasOwnProperty(i)) {
                        types.push({key: i, val: map[i]});
                    }
                }

                this.types = types;
            },
            error => {
                this.httpErrorService.handle(error, this);
            }
        );
    }

    submit() {
        this.instanceService.save(this.form).subscribe(
            response => this.router.navigate(['instances/' + response.id]),
            error    => this.httpErrorService.handle(error, this)
        );
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs = {
                title: 'New instance',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'Instances', href: '/instances'},
                    {title: 'New instance'}
                ]
            }
        } else {
            this.crumbs = {
                title: 'Update instance',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All instances', href: '/instances'},
                    {title: 'Update instance'}
                ]
            }
        }
    }

    private initForm(id) {
        if (id == null) {
            this.form = {params: {}};
        } else {
            this.fetchInstance(id);
        }
    }
}