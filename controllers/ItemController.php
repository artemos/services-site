<?php

namespace app\controllers;

use Yii;
use app\components\rest\Controller;

/**
 * Class ItemController
 * @package app\controllers
 */
class ItemController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete'],
                'allow'   => true,
                'roles'   => ['@'],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\controllers\actions\item\IndexAction',
            ],
            'create' => [
                'class' => 'app\controllers\actions\item\CreateAction',
            ],
            'update' => [
                'class' => 'app\controllers\actions\item\UpdateAction',
            ],
            'view' => [
                'class' => 'app\controllers\actions\item\ViewAction',
            ],
            'delete' => [
                'class' => 'app\controllers\actions\item\DeleteAction',
            ],
            'options' => [
                'class' => 'app\components\rest\OptionsAction',
            ]
        ];
    }

    public function behaviors()
    {
        $cacheSeconds = 60 * 30;

        return array_merge(parent::behaviors(), [
            [
                'class' => 'yii\filters\HttpCache',
                'only'  => ['index'],
                'cacheControlHeader' => 'public, max-age=' . $cacheSeconds,
                'etagSeed' => function () {
                    $cacheKey = 'e-tag-seed-' . Yii::$app->request->url;
                    $etagSeed = Yii::$app->cache->get($cacheKey);

                    if ($etagSeed !== false) {
                        return $etagSeed;
                    }
                }
            ]
        ]);
    }
}