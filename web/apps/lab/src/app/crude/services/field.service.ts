import {Injectable} from '@angular/core';
import {Subject}    from "rxjs/Subject";

@Injectable()
export class FieldService {
    public collectionFields = new Subject<any>();

    nameToLabel(name) {
        let label = name.replace(/_/g, ' ');
        return label.charAt(0).toUpperCase() + label.slice(1);
    }

    getFieldLabel(fieldName, fields) {
        let field = fields.find(f => f.name == fieldName);

        if (field && field.label) {
            return field.label;
        } else {
            return this.nameToLabel(fieldName)
        }
    }
}