<?php

use yii\db\Migration;

class m170904_150820_alter_users_role_not_null extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE users ALTER COLUMN role SET NOT NULL");
    }


    public function safeDown()
    {
        echo "m170904_150820_alter_users_role_not_null cannot be reverted.\n";
    }
}
