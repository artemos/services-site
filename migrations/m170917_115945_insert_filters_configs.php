<?php

use yii\db\Migration;
use app\components\models\Filters;

class m170917_115945_insert_filters_configs extends Migration
{
    public function safeUp()
    {
        $this->insert('configs', [
            'key'   => 'field-filters',
            'value' => \yii\helpers\Json::encode(Filters::getAll())]
        );
    }

    public function safeDown()
    {
        $this->delete('configs', ['key' => 'field-filters']);
    }
}
