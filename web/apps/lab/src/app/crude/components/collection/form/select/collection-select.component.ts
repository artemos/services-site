import {Component, OnInit, Input} from '@angular/core';
import {CollectionService} from "../../../../services/collection.service";
import {HttpErrorService} from "../../../../../core/services/http-error.service";

@Component({
    selector    : 'collection-select',
    templateUrl : './collection-select.component.html'
})
export class CollectionSelectComponent implements OnInit {
    @Input() model: any;
    collections: any;

    constructor(private collectionService: CollectionService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.collectionService.fetchAll().subscribe(
            response => this.collections = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}