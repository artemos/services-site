import {Component, Input} from '@angular/core';

@Component({
    selector    : 'status-label',
    templateUrl : './status-label.component.html'
})
export class StatusLabelComponent {
    @Input() status: String;
}
