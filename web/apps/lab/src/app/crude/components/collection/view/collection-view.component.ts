import {Component, OnInit}  from '@angular/core';
import {ActivatedRoute}     from "@angular/router";
import {IndexService} from "../../../services/index.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {CollectionService} from "../../../services/collection.service";

@Component({
    templateUrl: './collection-view.component.html'
})
export class CollectionViewComponent implements OnInit {
    crumbs: any = {
        title: 'View collection',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All collections', href: '/collections'},
            {title: 'View collection'}
        ]
    };

    collection: any;
    index: any;

    constructor(private route: ActivatedRoute,
                private indexService: IndexService,
                private httpErrorService: HttpErrorService,
                private collectionService: CollectionService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.fetchCollection(params.id);
        });
    }

    fetchCollection(id) {
        this.collectionService.fetchById(id).subscribe(
            response => {
                this.collection = response;
                this.fetchIndex(this.collection.indexName, this.collection.instanceId);
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    fetchIndex(name, instanceId) {
        this.indexService.fetchByName(name, instanceId).subscribe(
            response => this.collection.index = response,
            error    => this.httpErrorService.handle(error, this)
        )
    }

    createIndex(name, instanceId) {
        let data = {name: name, instanceId: instanceId};

        this.indexService.create(data).subscribe(
            response => {
                if (response.error != null) {
                    this.collection.index.error = response.error;
                } else {
                    this.fetchIndex(name, instanceId);
                }
            },
            error => this.httpErrorService.handle(error, this)
        );
    }
}