import {Injectable} from '@angular/core';
import {Http}       from '@angular/http';
import {ApiService} from "./api.service";

@Injectable()
export class ConfigService extends ApiService {
    private static configs: any = {};

    constructor(protected http: Http) {
        super(http, '/api/configs');
    }

    upsert(data) {
        return this.http
            .post(this.url('/upsert'), data, this.getOptions())
            .map(response => response.json());
    }

    find(key: string) {
        let config = localStorage.getItem(key);

        if (config == null) {
            return null;
        }

        try {
            return JSON.parse(config);
        } catch (e) {
            console.log('Load config error', e);
            return null;
        }
    }

    set(key: string, value: string) {
        ConfigService.configs[key] = value;
        localStorage.setItem(key, JSON.stringify(value));
    }

    findAllByPrefix(prefix) {
        let configs = [];

        for (let key in ConfigService.configs) {
            if (ConfigService.configs.hasOwnProperty(key) && key.startsWith(prefix)) {
                configs.push({
                    key   : key,
                    value : ConfigService.configs[key]
                });
            }
        }

        return configs;
    }

    findValueByKey(key) {
        return ConfigService.configs[key];
    }

    getDictionaries() {
        let configs = this.findAllByPrefix('dictionary-');

        for (let i = 0; i < configs.length; i++) {
            configs[i]['name'] = configs[i].value.name;
            configs[i]['map']  = configs[i].value.map;

            delete configs[i].value;
        }

        return configs;
    }

    generateKey(prefix: string) {
        let date = new Date();

        let parts = [
            date.getFullYear(),
            date.getMonth(),
            date.getDay(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds()
        ];

        return prefix + '-' + parts.join('');
    }
}