<?php

use yii\db\Migration;

class m170906_105554_create_aggregations extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE aggregations
            (
                id            SERIAL PRIMARY KEY,
                user_id       INTEGER NOT NULL,
                collection_id INTEGER NOT NULL,
                title         TEXT NOT NULL,
                config        JSONB NOT NULL,
                created_at TIMESTAMP(0) DEFAULT now(),
                updated_at TIMESTAMP(0) DEFAULT NULL :: TIMESTAMP WITHOUT TIME ZONE
            )
        ");

        $this->execute("ALTER TABLE aggregations ADD CONSTRAINT aggregations_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
        $this->execute("ALTER TABLE aggregations ADD CONSTRAINT aggregations_collection_fk FOREIGN KEY (collection_id) REFERENCES collections (id) ON DELETE SET NULL ON UPDATE CASCADE");
        $this->execute("CREATE UNIQUE INDEX aggregations_collection_title_u ON aggregations(collection_id, title)");
    }

    
    public function safeDown()
    {
        $this->execute("DROP TABLE aggregations");
    }
}
