import {Component}              from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorService}       from '../../../services/http-error.service';
import {ConfigService}          from '../../../services/config.service';
import {FormComponent}          from "../../form-component/form.component";

@Component({
    templateUrl : './config-form.component.html'
})
export class ConfigFormComponent extends FormComponent {
    constructor(
        protected service: ConfigService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected httpErrorService: HttpErrorService) {

        super(service, route, router, httpErrorService, {
            name       : 'config',
            listRoute  : '/configs'
        });
    }

    afterFetch(form: any) {
        form.value = JSON.stringify(form.value, undefined, 4);
        return form;
    }
}