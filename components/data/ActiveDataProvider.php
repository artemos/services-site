<?php

namespace app\components\data;

use Yii;
use yii\db\QueryInterface;
use yii\base\InvalidConfigException;

/**
 * Class ActiveDataProvider
 * @package app\components\data
 */
class ActiveDataProvider extends \yii\data\ActiveDataProvider
{
    /**
     * @var array
     */
    public $filter;

    /**
     * @var array
     */
    protected $arrayParams = ['id'];

    /**
     * @inheritdoc
     */
    protected function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }

        $query = clone $this->query;

        if ($this->filter) {
            $this->applyFilter($query);
        }

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            if ($pagination->totalCount === 0) {
                return [];
            }

            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }

        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }

        return $query->all($this->db);
    }

    /**
     * @param QueryInterface $query
     */
    protected function applyFilter(QueryInterface $query)
    {
        foreach ($this->filter as $param) {
            $value = \Yii::$app->request->get($param);

            if ($value !== null && $value !== '') {
                if (in_array($param, $this->arrayParams) && mb_strpos($value, ',') !== false) {
                    $value = explode(',', $value);
                }

                $query->andWhere([$param => $value]);
            }
        }
    }
}