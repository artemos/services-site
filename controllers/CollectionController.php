<?php

namespace app\controllers;

use app\components\rest\Controller;
use app\models\Collection;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use app\components\data\ActiveDataProvider;
use app\components\es\Mapper as EsMapper;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class CollectionController
 * @package app\controllers
 */
class CollectionController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete', 'index-field'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }


    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\controllers\actions\collection\IndexAction',
            ],
            'create' => [
                'class' => 'app\controllers\actions\collection\CreateAction',
            ],
            'update' => [
                'class' => 'app\controllers\actions\collection\UpdateAction',
            ],
            'view' => [
                'class' => 'app\controllers\actions\collection\ViewAction',
            ],
            'delete' => [
                'class' => 'app\controllers\actions\collection\DeleteAction',
            ],
            'index-field' => [
                'class' => 'app\controllers\actions\collection\IndexFieldAction'
            ],
            'options' => [
                'class' => 'app\components\rest\OptionsAction',
            ]
        ];
    }
}