import {Component, Input} from '@angular/core';
import {ItemService} from "../../../services/item.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    selector    : 'item-card',
    templateUrl : './item-card.component.html'
})
export class ItemCardComponent {
    @Input() item       : any;
    @Input() collection : any;
    @Input() fields     : any;
    @Input() detail     : boolean;
    @Input() settings   : any;

    constructor(private itemService: ItemService,
                private httpErrorService: HttpErrorService) {
    }

    deleteItem(item) {
        if (!confirm('Delete item with id "' + item.id + '" ?')) {
            return;
        }

        this.itemService.delete(item.id, {collectionId: this.collection.id}).subscribe(
            response => item.deleted = true,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    getEsUrl() {
        return 'http://' + this.collection.instance.host + ':' + this.collection.instance.port + '/' +
                this.collection.indexName + '/' + this.collection.indexName + '/' + encodeURIComponent(this.item.id);
    }

    encodeURIComponent(str) {
        return encodeURIComponent(str);
    }
}