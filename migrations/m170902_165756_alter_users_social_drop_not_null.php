<?php

use yii\db\Migration;

class m170902_165756_alter_users_social_drop_not_null extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE users ALTER COLUMN social_id DROP NOT NULL");
        $this->execute("ALTER TABLE users ALTER COLUMN social_data DROP NOT NULL");
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE users ALTER COLUMN social_id SET NOT NULL");
        $this->execute("ALTER TABLE users ALTER COLUMN social_data SET NOT NULL");
    }
}
