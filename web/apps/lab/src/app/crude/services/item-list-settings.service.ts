import {Injectable} from '@angular/core';
import {Subject}    from "rxjs/Subject";

@Injectable()
export class ItemListSettingsService {
    public form = new Subject<any>();
    public data = new Subject<any>();
}