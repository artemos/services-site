<?php

use yii\db\Migration;

class m170914_123228_alter_column_names_camelcase extends Migration
{
    public function safeUp()
    {
        $tables = [
            'applications' => [
                'app_id'     => 'appId',
                'api_key'    => 'apiKey',
                'user_id'    => 'userId',
                'created_at' => 'createdAt'
            ],
            'collections' => [
                'app_id'      => 'appId',
                'user_id'     => 'userId',
                'instance_id' => 'instanceId',
                'index_name'  => 'indexName',
                'index_type'  => 'indexType',
                'created_at'  => 'createdAt',
                'updated_at'  => 'updatedAt'
            ],
            'configs' => [
                'user_id' => 'userId',
            ],
            'crawler_sites' => [
                'app_id'        => 'appId',
                'user_id'       => 'userId',
                'collection_id' => 'collectionId',
                'start_url'     => 'startUrl'
            ],
            'crawler_templates' => [
                'app_id'      => 'appId',
                'user_id'     => 'userId',
                'created_at'  => 'createdAt',
                'updated_at'  => 'updatedAt'
            ],
            'instances' => [
                'app_id'       => 'appId',
                'user_id'      => 'userId',
                'ssh_port'     => 'sshPort',
                'ssh_username' => 'sshUsername',
                'ssh_password' => 'sshPassword',
                'created_at'   => 'createdAt',
                'config_path'  => 'configPath'
            ],
            'pipelines' => [
                'user_id'     => 'userId',
                'instance_id' => 'instanceId',
                'created_at'  => 'createdAt',
                'updated_at'  => 'updatedAt'
            ],
            'users' => [
                'social_id'   => 'socialId',
                'social_data' => 'socialData',
                'auth_key'    => 'authKey',
                'created_at'  => 'createdAt',
                'updated_at'  => 'updatedAt'
            ]
        ];

        foreach ($tables as $table => $columns) {
            foreach ($columns as $oldName => $newName) {
                $this->execute("ALTER TABLE {$table} RENAME COLUMN  \"{$oldName}\" to \"{$newName}\"");
            }
        }
    }


    public function safeDown()
    {
        echo "m170914_123228_alter_column_names_camelcase cannot be reverted.\n";
    }
}
