<?php

use yii\db\Migration;

class m170730_174500_create_pipelines extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE pipelines
            (
                id SERIAL PRIMARY KEY,
                instance_id INT NOT NULL,
                user_id INT NOT NULL,
                name TEXT NOT NULL,
                config TEXT NOT NULL,
                created_at TIMESTAMP(0) DEFAULT NOW(),
                updated_at TIMESTAMP(0) DEFAULT NULL ,
                CONSTRAINT pipelines_user_fk FOREIGN KEY (user_id) REFERENCES users (id)
            );
        ");

        $this->execute("CREATE UNIQUE INDEX pipelines_user_id_name_i ON pipelines(user_id, name)");
        $this->execute("ALTER TABLE pipelines ADD CONSTRAINT pipelines_instance_fk FOREIGN KEY (instance_id) REFERENCES instances (id)");
    }


    public function safeDown()
    {
        $this->dropTable('pipelines');
    }
}
