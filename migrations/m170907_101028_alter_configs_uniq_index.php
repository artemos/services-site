<?php

use yii\db\Migration;

class m170907_101028_alter_configs_uniq_index extends Migration
{
    public function safeUp()
    {
        $this->execute("DROP INDEX users_configs_user_key_i");
        $this->execute("CREATE UNIQUE INDEX configs_user_type_key_i ON configs (user_id, type, key)");
    }

    
    public function safeDown()
    {
        echo "m170907_101028_alter_configs_uniq_index cannot be reverted.\n";
    }
}
