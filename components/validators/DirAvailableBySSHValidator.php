<?php

namespace app\components\validators;

use app\components\services\SshService;
use yii\base\ErrorException;
use yii\validators\Validator;

/**
 * Class DirAvailableBySSHValidator
 * @package app\components\validators
 */
class DirAvailableBySSHValidator extends Validator
{
    /**
     * @param \yii\base\Model $instance
     * @param string $attribute
     */
    public function validateAttribute($instance, $attribute)
    {
        $client = new SshService($instance->host, $instance->ssh_username, $instance->ssh_password, $instance->ssh_port);

        try {
            $client->authenticate();
            if (!$client->isDirectoryExists($instance->config_path)) {
                $this->addError($instance, $attribute, "Directory is not exists");
            }
        } catch (ErrorException $e) {
            $message = $e->getMessage();
            $message = str_replace('ssh2_connect():', '', $message);
            $message = trim($message);

            $this->addError($instance, $attribute, $message);
        }
    }
}