<?php

use yii\db\Migration;

class m170905_090426_drop_servers extends Migration
{
    public function safeUp()
    {
        $this->dropTable('servers');
    }

    public function safeDown()
    {
        echo "m170905_090426_drop_servers cannot be reverted.\n";
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170905_090426_drop_servers cannot be reverted.\n";

        return false;
    }
    */
}
