<?php

namespace app\components\rest;

use Yii;
use yii\base\InvalidCallException;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Elasticsearch\ClientBuilder;
use app\components\db\ActiveRecord;
use app\components\enum\InstanceType;
use app\models\Instance;

/**
 * Class RestController
 * @package app\components\web
 */
class Action extends \yii\rest\Action
{
    public $modelClass = false;

    /**
     * @param $code
     */
    protected function setResponseStatusCode($code)
    {
        $this->controller->setResponseStatusCode($code);
    }

    /**
     * @return array
     */
    protected function getRequestParams()
    {
        return $this->controller->getRequestParams();
    }

    /**
     * @return array
     */
    protected function getQueryParams()
    {
        return $this->controller->getQueryParams();
    }

    /**
     * @param $name
     * @return mixed
     * @throws BadRequestHttpException
     */
    protected function requireParam($name)
    {
        return $this->controller->requireParam($name);
    }

    /**
     * @param $id
     * @param $options
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function getModel($id, $options = [])
    {
        return $this->controller->getModel($id, $options);
    }

    /**
     * @param null $instanceId
     * @return \Elasticsearch\Client
     * @throws BadRequestHttpException
     */
    protected function getEsClient($instanceId = null)
    {
        return $this->controller->getEsClient($instanceId);
    }

    /**
     * @return mixed
     */
    protected function getUserId()
    {
        return $this->controller->getUserId();
    }

    /**
     * @param null $name
     * @param null $defaultValue
     * @return array|mixed
     */
    protected function getParam($name = null, $defaultValue = null)
    {
        return $this->controller->getParam($name, $defaultValue);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    protected function param($name)
    {
        return $this->controller->param($name);
    }

    /**
     * @param Instance $instance
     */
    protected function initEsConnection(Instance $instance)
    {
        $this->controller->initEsConnection($instance);
    }
}