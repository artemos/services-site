import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'notHidden'
})
export class NotHiddenPipe implements PipeTransform {
    transform(items: Array<any>) {
        return items.filter(item => !item.hidden);
    }
}
