import {Component, OnInit, Input} from '@angular/core';
import {InstanceService}          from "../../services/instance.service";
import {HttpErrorService}         from "../../../core/services/http-error.service";

@Component({
    selector: 'instance-health',
    templateUrl: './instance-health.component.html'
})
export class InstanceHealthComponent implements OnInit {
    @Input() instance: any;
    @Input() short: boolean;

    data: any;

    constructor(private instanceService: InstanceService, private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.fetchInstanceHealth();
    }

    fetchInstanceHealth() {
        this.instanceService.fetchHealth(this.instance.id).subscribe(
            response => this.data = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}