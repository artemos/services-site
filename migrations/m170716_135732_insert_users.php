<?php

use yii\db\Migration;

class m170716_135732_insert_users extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO users
            (id, social_id, social_data, auth_key, created_at, updated_at) VALUES 
            (1, 'http://vk.com/id3158115', '{\"uid\": \"3158115\", \"network\": \"vkontakte\", \"profile\": \"http://vk.com/id3158115\", \"identity\": \"http://vk.com/id3158115\", \"last_name\": \"Остапец\", \"first_name\": \"Артем\"}', 'user_auth_key', '2017-07-17 18:46:14', null);
        ");
    }


    public function safeDown()
    {
        $this->delete('users');
    }
}
