<?php

namespace app\models;

use Yii;
use app\components\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property string $appId
 * @property string $apiKey
 * @property int $userId
 * @property string $name
 * @property string $access
 * @property string $description
 * @property string $createdAt
 */
class Application extends ActiveRecord
{
    /**
     * @var array
     */
    public $jsonbAttributes = ['access'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['userId'], 'integer'],
            [['name', 'description'], 'string'],
            [['access'], 'safe'],
            [['userId', 'name'], 'unique', 'targetAttribute' => ['userId', 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'appId'       => 'App ID',
            'apiKey'      => 'Api key',
            'userId'      => 'User ID',
            'name'        => 'Name',
            'createdAt'   => 'Created At',
            'description' => 'Description'
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->userId = Yii::$app->user->identity->id;
            $this->apiKey = base64_encode(join(',', [$this->userId, $this->name, rand(1, 5000), time()]));
        }

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollections()
    {
        return $this->hasMany(Collection::class, ['appId' => 'id'])->select(['id', 'appId', 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstances()
    {
        return $this->hasMany(Instance::class, ['appId' => 'id'])->select(['id', 'appId', 'name', 'type']);
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $expand = ['collections', 'crawlerSites', 'instances'];
        $model  = parent::toArray($fields, $expand, $recursive);

        $model['access'] = Json::decode($model['access']);

        return $model;
    }
}
