<?php

namespace app\models;

use app\components\models\RulesBuilder;
use yii\base\Model;

/**
 * Class Item
 * @package app\models
 */
class Item extends Model
{
    /**
     * @var array
     */
    public $fields;

    /**
     * @var array
     */
    public $data = [];

    /**
     * @return array
     */
    public function rules()
    {
        return RulesBuilder::build($this->fields);
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return array_column($this->fields, 'name');
    }

    /**
     * @param $attribute
     */
    public function arrayOfString($attribute)
    {
        if (!is_array($this->{$attribute})) {
            $this->addError($attribute, 'should be array of strings');
            return;
        }

        foreach ($this->{$attribute} as $value)
        {
            if (!is_string($value)) {
                $this->addError($attribute, 'should be array of strings2');
                return;
            }
        }
    }

    /**
     * @param $attribute
     */
    public function inArrayOfString($attribute, $params)
    {
        foreach ($this->$attribute as $value) {
            if (!in_array($value, $params['range'], true)) {
                $this->addError($attribute, 'contains wrong value');
                return;
            }
        }
    }
}