import {Injectable} from '@angular/core';
import {Subject}    from "rxjs/Subject";

@Injectable()
export class SortingService {
    public data = new Subject<any>();
    public form = new Subject<any>();
}