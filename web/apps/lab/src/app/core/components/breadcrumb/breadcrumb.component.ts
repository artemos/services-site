import {Component, Input, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'breadcrumb',
    templateUrl: './breadcrumb.component.html'
})
export class BreadcrumbComponent implements OnInit {
    @Input() data: any;

    constructor(private titleService: Title) {}

    ngOnInit(): void {
        this.titleService.setTitle(this.data.title);
    }
}
