<?php

namespace app\components\es;
use yii\base\InvalidCallException;

/**
 * Class AggSerializer
 * @package app\components\services\es
 */
class AggSerializer
{
    /**
     * @param array $data
     * @return array
     */
    public static function serialize(array $data)
    {
        switch ($data['type']) {
            case 'term':
                return self::serializeTerm($data);

            case 'histogram':
                return self::serializeHistogram($data);

            case 'date_histogram':
                return self::serializeDateHistogram($data);

            case 'date_histogram_term':
                return self::serializeDateHistogramTerm($data);

            case 'extended_stats':
                return self::serializeExtendedStats($data);

            case 'max':
            case 'min':
                return self::serializeSingle($data['type'], $data);

            default:
                throw new InvalidCallException('unknown aggregation type');
        }
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function serializeTerm(array $data)
    {
        return [
            "group_by_{$data['field']}" => [
                'terms' => [
                    'field' => "{$data['field']}.keyword"
                ]
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function serializeHistogram(array $data)
    {
        return [
            $data['field'] => [
                'histogram' => [
                    'field'         => $data['field'],
                    'interval'      => $data['interval'],
                    'min_doc_count' => $data['minDocCount'] ?? 1
                ]
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function serializeDateHistogram(array $data)
    {
        return [
            "group_by_{$data['field']}_{$data['interval']}" => [
                'date_histogram' => [
                    'field'    => $data['field'],
                    'interval' => $data['interval'],
                    'format'   => $data['format']
                ]
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function serializeDateHistogramTerm(array $data)
    {
        return [
            "group_by_{$data['date_field']}_{$data['interval']}_{$data['term_field']}" => [
                'terms' => [
                    'field' => "{$data['term_field']}.keyword"
                ],
                'aggs' => [
                    "group_by_{$data['date_field']}_{$data['interval']}" => [
                        'date_histogram' => [
                            'field'    => "{$data['date_field']}",
                            'interval' => $data['interval'],
                            'format'   => $data['format']
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    protected static function serializeExtendedStats(array $data)
    {
        return [
            "{$data['field']}_stats" => [
                'extended_stats' => [
                    'field' => $data['field']
                ]
            ]
        ];
    }

    /**
     * @param $name
     * @param array $data
     * @return array
     */
    protected static function serializeSingle($name, array $data)
    {
        return [
            "{$name}_{$data['field']}" => [
                $name => [
                    'field' => $data['field']
                ]
            ]
        ];
    }
}


