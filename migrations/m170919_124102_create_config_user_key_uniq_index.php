<?php

use yii\db\Migration;

class m170919_124102_create_config_user_key_uniq_index extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE UNIQUE INDEX configs_user_id_key_uniq_index ON configs("userId", key)');
        $this->execute('CREATE UNIQUE INDEX configs_key_uniq_index ON configs(key) WHERE "userId" is NULL');
    }

    public function safeDown()
    {
        $this->execute("DROP INDEX configs_user_id_key_uniq_index");
        $this->execute("DROP INDEX configs_key_uniq_index");
    }
}
