<?php

namespace app\components;

/**
 * Class Enum
 */
class Enum
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $class = new \ReflectionClass(static::class);
        return array_values($class->getConstants());
    }
}