import {Component, OnInit}  from '@angular/core';
import {ApplicationService} from "../../services/application.service";
import {HttpErrorService}   from "../../../core/services/http-error.service";

@Component({
    templateUrl : './application-list.component.html'
})
export class ApplicationListComponent implements OnInit {
    applications: any;

    crumbs: any = {
        title: 'All applications',
        buttons: [
            {
                text : 'New application',
                route: '/applications/new',
                class: 'btn btn-primary'
            }
        ],
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All applications'}
        ]
    };

    constructor(private applicationService: ApplicationService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.applicationService.fetchAll().subscribe(
            response => this.applications = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}