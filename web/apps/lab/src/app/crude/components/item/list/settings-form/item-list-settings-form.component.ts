import {Component, OnInit} from '@angular/core';
import {ViewChild}         from '@angular/core';
import {ArrayService}      from "../../../../../core/services/array.service";
import {NgbModal}          from '@ng-bootstrap/ng-bootstrap';
import {ItemListSettingsService} from "../../../../services/item-list-settings.service";
import {ConfigService} from "../../../../../core/services/config.service";

@Component({
  selector    : 'item-list-settings-form',
  templateUrl : './item-list-settings-form.component.html',
  styleUrls   : ['./item-list-settings-form.component.css']
})
export class ItemListSettingsFormComponent implements OnInit {
    @ViewChild('modal') modal: any;

    private sortableOptions : any;
    private positionArrows  : boolean;

    private form: any;
    private dics: Array<any>;

    constructor(
        private arrayService: ArrayService,
        private configService: ConfigService,
        private itemListSettingsService: ItemListSettingsService,
        private modalService: NgbModal) {

        this.positionArrows = false;     
        
        this.sortableOptions = { 
            animation : 150,
            draggable : ".field-row"
        };
    }

    ngOnInit() {
        this.dics = this.configService.getDictionaries();

        this.itemListSettingsService.form.subscribe(
            data => this.form = data
        );
    }

    submit() {
        this.itemListSettingsService.data.next(this.form);
    }

    changeFieldPosition(field, direction) {
        switch (direction) {
            case 'up':
                this.form.fields = this.arrayService.moveUp(this.form.fields, field);
                break;

            case 'down':
                this.form.fields = this.arrayService.moveDown(this.form.fields, field);
                break;
        }

        return false;
    }

    openModal() {
        this.modalService.open(this.modal).result.then(
            result => {
                console.log(`Closed with: ${result}`);
            }
        );
    }    
}
