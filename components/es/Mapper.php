<?php

namespace app\components\es;

/**
 * Class Mapper
 * @package app\components\es
 */
class Mapper
{
    /**
     * @param array $fields
     * @return array
     */
    public static function buildProperties(array $fields)
    {
        $properties = [];

        foreach ($fields as $field) {
            $properties[$field['name']] = [
                'type' => $field['type']
            ];
        }

        return $properties;
    }
}