<?php

namespace app\controllers;

use Yii;
use app\components\rest\Controller;
use app\models\Application;
use app\components\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class ApplicationController
 * @package app\controllers
 */
class ApplicationController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $query = Application::find();
        $query->allowed($this->getUserId());
        $query->with(['collections', 'instances']);

        return new ActiveDataProvider(['query' => $query]);
    }

    /**
     * @param $id
     * @return \app\components\db\ActiveRecord
     */
    public function actionView($id)
    {
        return $this->getModel($id, ['with' => ['collections', 'instances']]);
    }

    /**
     * @return Application
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $application = new Application();
        $application->load($this->getRequestParams());

        if ($application->save()) {
            $this->setResponseStatusCode(201);
        } elseif (!$application->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $application;
    }

    /**
     * @param $id
     * @return \app\components\db\ActiveRecord
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        $application = $this->getModel($id);
        $application->load($this->getRequestParams());

        if (!$application->save() && !$application->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $application;
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        if (!$this->getModel($id)->delete()) {
            throw new ServerErrorHttpException('Failed to delete the comment for unknown reason.');
        }

        $this->setResponseStatusCode(204);
    }
}