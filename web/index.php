<?php
error_reporting(E_ALL);
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
define('ROOT_DIR', realpath(dirname(__FILE__) . '/..'));

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../components/debug.php');

$config = require(__DIR__ . '/../config/web.php');

Yii::setAlias('@runtime/cache', '/var/www/services-site/runtime/cache');

(new yii\web\Application($config))->run();
