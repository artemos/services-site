<?php

use yii\db\Migration;

/**
 * Class m171016_084020_updateQuizDashboardWidgets
 */
class m171016_084020_updateQuizDashboardWidgets extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $config   = Yii::$app->db->createCommand("SELECT * FROM configs WHERE key='collection-3-dashboard'")->queryOne();
        $value    = \yii\helpers\Json::decode($config['value']);
        $chartIds = [];

        foreach ($value['rows'] as $r => $row) {
            foreach ($row['cols'] as $c => $col) {
                $chartIds[] = $col['widgetId'];
            }
        }

        $configs = \app\models\Config::find()->where(['id' => $chartIds])->all();
        foreach ($configs as $config) {
            $value = \yii\helpers\Json::decode($config->value);

            foreach (['field', 'term_field'] as $param) {
                if (isset($value['aggregation'][$param]) && mb_strpos($value['aggregation'][$param], '.keyword') === false) {
                    $value['aggregation'][$param].= ".keyword";
                }
            }

            \app\models\Config::updateAll(['value' => \yii\helpers\Json::encode($value)], ['key' => $config['key']]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171016_084020_updateQuizDashboardWidgets cannot be reverted.\n";
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171016_084020_updateQuizDashboardWidgets cannot be reverted.\n";

        return false;
    }
    */
}
