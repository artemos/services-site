import {Component, OnInit}  from '@angular/core';
import {ActivatedRoute}     from "@angular/router";
import {CollectionService} from "../../../services/collection.service";
import {IndexService} from "../../../services/index.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {ConfigService} from "../../../../core/services/config.service";

@Component({
    templateUrl : './collection-form.component.html'
})
export class CollectionFormComponent implements OnInit {
    validators  : any;
    collection  : any;
    crumbs : any;
    errors      : any;
    types       : Array<String>;
    filters     : Array<String>;
    done        : boolean;

    constructor(private route: ActivatedRoute,
                private collectionService: CollectionService,
                private indexService: IndexService,
                private httpErrorService: HttpErrorService,
                private configService: ConfigService) {
    }

    ngOnInit() {
        this.types      = this.configService.find('field-types');
        this.filters    = this.configService.find('field-filters').map(v => { return {id: v, text: v} });
        this.validators = this.configService.find('field-validators').map(v => { return {id: v, text: v} });

        this.route.params.subscribe(params => {
            this.initCrumbs(params.id);

            if (params.id != null) {
                this.fetchCollection(params.id);
            } else {
                this.initCollection();
            }
        });
    }

    fetchCollection(id) {
        this.collectionService.fetchById(id).subscribe(
            response => this.collection = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    initCollection() {
        this.collection = {
            fields: [],
            settings: {
                detailButton: true,
                editButton: true,
                deleteButton: true
            }
        }
    }

    createFieldsFromMapping() {
        this.indexService.fetchMapping(this.collection.indexName, this.collection.indexType, this.collection.instanceId).subscribe(
            response => {
                for (let name in response) {
                    if (!response.hasOwnProperty(name)) {
                        continue;
                    }

                    let exists = this.collection.fields.find(f => f.name == name);

                    if (!exists) {
                        this.collection.fields.push({
                            name: name,
                            type: response[name].type
                        });
                    }
                }
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    submit() {
        this.done = false;

        this.collectionService.save(this.collection).subscribe(
            response => this.done = true,
            error    => this.httpErrorService.handle(error, this)
        )
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs = {
                title: 'New collection',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All collections', href: '/collections'},
                    {title: 'New collection'}
                ]
            }
        } else {
            this.crumbs = {
                title: 'Update collection',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All collections', href: '/collections'},
                    {title: 'Update collection'}
                ]
            }
        }
    }
}