<?php

namespace app\controllers\actions\collection;

use app\components\rest\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\item
 */
class ViewAction extends Action
{
    /**
     * @param int $id
     * @return mixed
     */
    public function run(int $id)
    {
        return $this->getModel($id, ['with' => ['instance']]);
    }
}