<?php

use yii\db\Migration;

class m170905_090531_add_access_to_applications extends Migration
{
    public function safeUp()
    {
        $this->addColumn('applications', 'access', 'jsonb');
    }


    public function safeDown()
    {
        $this->dropColumn('applications', 'access');
    }
}
