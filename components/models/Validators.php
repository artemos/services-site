<?php

namespace app\components\models;

use app\components\Enum;

/**
 * Class Validators
 * @package app\components\models
 */
class Validators extends Enum
{
    const BOOLEAN   = 'boolean';
    const DOUBLE    = 'double';
    const EMAIL     = 'email';
    const IP        = 'ip';
    const INTEGER   = 'integer';
    const NUMBER    = 'number';
    const REQUIRED  = 'required';
    const STRING    = 'string';
    const URL       = 'url';
    const DATE      = 'date';
    const IN        = 'in';

    /**
     * @return array
     */
    public static function getAllWithParams()
    {
        return [
            self::BOOLEAN  => [],
            self::DOUBLE   => [],
            self::EMAIL    => [],
            self::IP       => [],
            self::INTEGER  => [
                'min' => [
                    'label' => 'Min value',
                    'type'  => 'number'
                ],
                'max' => [
                    'label' => 'Max value',
                    'type'  => 'number'
                ]
            ],
            self::NUMBER   => [],
            self::REQUIRED => [],
            self::URL      => [],
            self::DATE => [
                'format' => [
                    'label' => 'Date format',
                    'type'  => 'string'
                ]
            ],
            self::IN => [
                'range' => [
                    'label' => 'In range',
                    'type'  => 'array'
                ]
            ],
            self::STRING => [
                'min' => [
                    'label' => 'Min length',
                    'type'  => 'number'
                ],
                'max' => [
                    'label' => 'Max length',
                    'type'  => 'number'
                ]
            ]
        ];
    }
}
