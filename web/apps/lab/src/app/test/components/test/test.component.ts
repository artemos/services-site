import {Component, OnInit} from '@angular/core';

// let Chartist: any;
// let Gauge: any;
// let echarts: any;
// //var SweetAlert: any;

@Component({
    selector: 'test',
    templateUrl: './test.component.html',
})
export class TestComponent implements OnInit {
    aggregation: any;
    items: any;

    // constructor(private sw: SwalComponent) {
    // }

    releaseDrop($event) {
        console.log('release drop', $event);
    }

    startDrag(item) {
        console.log('startDrag', item);
    }

    addDropItem($event) {
        console.log('release drop', $event);
    }

    dropEventMouse($event) {
        console.log('dropEventMouse', $event);
    }

    dragEnter($event) {
        console.log('dragEnter', $event);
    } 

    dragLeave($event) {
        console.log('dragLeave', $event);
    } 

    ngOnInit() {
        let items = [];

        for (let i = 0 ;i < 10; i++) {
            items.push({
                name: 'item ' + i
            });     
        }

        this.items = items;

        // var gaugeChart = echarts.init(document.getElementById('gauge-chart'));
        // let option = {
        //     tooltip : {
        //         formatter: "{a} <br/>{b} : {c}%"
        //     },
        //     series : [
        //         {
        //             name:'Speed',
        //             type:'gauge',
        //             detail : {formatter:'{value}%'},
        //             data:[{value: 10, name: 'Speed'}],
        //             axisLine: {            // 坐标轴线
        //                 lineStyle: {       // 属性lineStyle控制线条样式
        //                     color: [[0.2, '#55ce63'],[0.8, '#009efb'],[1, '#f62d51']],
        //                 }
        //             },
        //
        //         }
        //     ]
        // };
        //
        // gaugeChart.setOption(option, true);
    }
}



