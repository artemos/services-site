import {Component, OnInit} from '@angular/core';
import {ActivatedRoute}    from '@angular/router';
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {ConfigService} from "../../../../core/services/config.service";
@Component({
    selector    : 'widget-view',
    templateUrl : './widget-view.component.html'
})
export class WidgetViewComponent implements OnInit {
    config: any;

    crumbs: any = {
        title: 'View widget',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All widgets', href: '/widgets'},
            {title: 'View widget'}
        ]
    };

    constructor(
        private route: ActivatedRoute,
        private configService: ConfigService,
        private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.configService.fetchById(params.id).subscribe(
                response => this.config = response,
                error    => this.httpErrorService.handle(error, this)
            );
        });
    }

    deleteWidget(widget) {
        if (!confirm("Delete widget '" + widget.value.title + "'?")) {
            return;
        }

        alert('delete it');
        // this.configService.delete(widget.id).subscribe(
        //     ()    => this.arrayService.deleteElement(this.widgets, widget),
        //     error => this.httpErrorService.handle(error, this)
        // );
    }
}