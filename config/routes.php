<?php

return [
    'GET /api/test' => 'site/test',

    'POST /api/auth/login'      => 'auth/login',
    'OPTIONS /api/auth/login'   => 'auth/options',
    'GET /api/auth/session'     => 'auth/session',
    'OPTIONS /api/auth/session' => 'auth/options',
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'user',
        'prefix'     => '/api'
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'application',
        'prefix'     => '/api'
    ],
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => 'collection',
        'prefix'        => '/api',
        'extraPatterns' => [
            'GET <id:\d+>/fields'     => 'index-field',
            'OPTIONS <id:\d+>/fields' => 'options'
        ]
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'item',
        'prefix'     => '/api',
        'tokens'     => [
            '{id}' => '<id:.*>',
        ],
    ],
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => 'instance',
        'prefix'        => '/api',
        'extraPatterns' => [
            'GET <id:\d+>/health' => 'health',
            'GET <id:\d+>/ping'   => 'ping',
            'GET <id:\d+>/ssh'    => 'ssh',
        ]
    ],
    'OPTIONS /api/instances/<id:\d+>/health' => 'instance/options',
    'OPTIONS /api/instances/<id:\d+>/ping'   => 'instance/options',
    'OPTIONS /api/instances/<id:\d+>/ssh'    => 'instance/options',
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => 'index',
        'prefix'        => '/api',
        'tokens'        => [
            '{id}' => '<id:[a-z0-9_]+>',
        ],
        'extraPatterns' => [
            'GET <id:[a-z0-9_]+>/mapping'     => 'view-mapping',
            'OPTIONS <id:[a-z0-9_]+>/mapping' => 'options',

            'PUT mapping'      => 'update-mapping',
            'OPTIONS /mapping' => 'options'
        ]
    ],
    'OPTIONS /api/handshake'  => 'handshake/options',
    'GET     /api/handshake'  => 'handshake/index',
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'pipeline',
        'prefix'     => '/api'
    ],
    'GET /api/logstash/plugins'      => 'logstash/plugins',
    'OPTIONS /api/logstash/plugins'  => 'logstash/options',
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'logstash-config',
        'prefix'     => '/api'
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'crawler-site',
        'prefix'     => '/api'
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'crawler-template',
        'prefix'     => '/api'
    ],
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => 'config',
        'prefix'     => '/api',
        'extraPatterns' => [
            'POST upsert'              => 'upsert',
            'OPTIONS upsert'           => 'options',
            'GET create-charts'        => 'create-charts',
            'OPTIONS create-charts'    => 'options',
            'GET create-dashboard'     => 'create-dashboard',
            'OPTIONS create-dashboard' => 'options',
        ]
    ],
];