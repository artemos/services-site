<?php

use yii\db\Migration;

class m170717_115309_create_applications extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE applications
            (
                id        SERIAL PRIMARY KEY,
                app_id    UUID NOT NULL DEFAULT uuid_generate_v4(), 
                api_key   TEXT NOT NULL,
                user_id   INTEGER NOT NULL,
                name      TEXT NOT NULL,
                description TEXT NULL,
                created_at TIMESTAMP(0) DEFAULT now()
            )
        ");

        $this->execute("CREATE UNIQUE INDEX applications_app_id_uniq_i on applications(app_id)");
        $this->execute("CREATE UNIQUE INDEX applications_api_key_uniq_i on applications(api_key)");
        $this->execute("CREATE UNIQUE INDEX applications_user_id_name_uniq_i on applications(user_id, name)");
        $this->execute("ALTER TABLE applications ADD CONSTRAINT applications_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
    }


    public function safeDown()
    {
        $this->execute('DROP TABLE applications');
    }
}
