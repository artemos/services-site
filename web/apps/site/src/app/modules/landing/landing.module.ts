import {NgModule}             from '@angular/core';
import {CommonModule}         from '@angular/common';
import {FormsModule}          from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {LandingComponent}     from "./components/list/landing.component";

const routes: Routes = [
    {
        path: '',
        component: LandingComponent
    }
];

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        LandingComponent
    ],
    providers: [
    ],
})
export default class LandingModule {
}