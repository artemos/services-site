<?php

use yii\db\Migration;

class m170719_132209_create_collections extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE collections
            (
                id SERIAL PRIMARY KEY,
                app_id INTEGER NOT NULL,
                user_id INTEGER NOT NULL,
                instance_id INTEGER NOT NULL,
                name TEXT NOT NULL,
                index_name TEXT NOT NULL,
                fields JSONB NOT NULL,
                created_at TIMESTAMP(0) DEFAULT now(),
                updated_at TIMESTAMP(0) DEFAULT NULL
            );
        ");

        $this->execute("CREATE UNIQUE INDEX instances_name_user_id_i ON collections(user_id, name)");
        $this->execute("CREATE UNIQUE INDEX instances_index_user_i ON collections(user_id, index_name)");

        $this->execute("ALTER TABLE collections ADD CONSTRAINT collections_app_fk FOREIGN KEY (app_id) REFERENCES applications (id)");
        $this->execute("ALTER TABLE collections ADD CONSTRAINT collections_user_fk FOREIGN KEY (user_id) REFERENCES users (id)");
        $this->execute("ALTER TABLE collections ADD CONSTRAINT collections_instance_fk FOREIGN KEY (instance_id) REFERENCES instances (id)");
    }


    public function safeDown()
    {
        $this->dropTable('collections');
    }
}
