<?php

namespace app\components\db;

/**
 * Class Postgres
 * @package app\components\db
 */
class Postgres
{
    /**
     * @param array $array
     * @return array|string
     */
    public static function toPgArray(array $array)
    {
        $array = '{' . implode(',', $array) . '}';
        return $array;
    }

    /**
     * @param $array
     * @return array|string
     */
    public static function fromPgArray($array)
    {
        if (is_array($array)) {
            return $array;
        }

        $array = ltrim($array, '{');
        $array = rtrim($array, '}');

        $values = explode(',', $array);
        foreach ($values as $i => $value) {
            $values[$i] = trim($value);
            $values[$i] = str_replace('"', '', $values[$i]);
        }

        return array_filter($values);
    }
}