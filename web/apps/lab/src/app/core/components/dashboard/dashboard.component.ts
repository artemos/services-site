import {Component, OnInit}  from '@angular/core';

@Component({
    selector    : 'dashboard',
    templateUrl : './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    data: any;

    crumbs: any = {
        title: 'Dashboard',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All applications'}
        ]
    };

    constructor() {
    }

    ngOnInit() {

    }
}