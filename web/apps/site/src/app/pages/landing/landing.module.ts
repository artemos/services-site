import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './landing.component';


const routes: Routes = [{
	path: '',
	data: {
      title: 'LandingComponent',
      urls: [{title: 'Dashboard',url: '/'},{title: 'LandingComponent page'}]
    },
	component: LandingComponent
}];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule, 
    	RouterModule.forChild(routes)
    ],
	declarations: [LandingComponent]
})
export default class LandingModule { }