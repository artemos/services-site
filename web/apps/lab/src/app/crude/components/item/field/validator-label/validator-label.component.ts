import {Component, Input} from '@angular/core';

@Component({
    selector    : 'validator-label',
    templateUrl : './validator-label.component.html'
})
export class ValidatorLabelComponent {
    @Input() validator: String;

    paramsText(params: Array<any>) {
        return params.map(p => {
            let value = p.value;

            if (p.name == 'range') {
                value = value.join(',');

                if (value.length > 15) {
                    value = value.substr(0, 15) + '...';
                }
            }

            return p.name + '=' + value;
        }).join(', ');
    }
}
