import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router}   from "@angular/router";
import {ApplicationService}       from "../../services/application.service";
import {HttpErrorService}         from "../../../core/services/http-error.service";

@Component({
    templateUrl : './application-form.component.html'
})
export class ApplicationFormComponent implements OnInit {
    crumbs : any;
    form   : any;
    errors : any;

    constructor(private route: ActivatedRoute,
                private httpErrorService: HttpErrorService,
                private applicationService: ApplicationService,
                private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.initCrumbs(params.id);

            if (params.id) {
                this.fetchApplication(params.id);
            } else {
                this.form = {};
            }
        });
    }

    fetchApplication(id) {
        this.applicationService.fetchById(id).subscribe(
            response => this.form = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    submit() {
        this.applicationService.save(this.form).subscribe(
            response => this.router.navigate(['applications/' + response.id]),
            error    => this.httpErrorService.handle(error, this)
        )
    }

    private initCrumbs(id) {
        if (id == null) {
            this.crumbs = {
                title: 'New form',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All applications', href: '/applications'},
                    {title: 'New form'}
                ]
            };
        } else {
            this.crumbs = {
                title: 'Update form',
                links: [
                    {title: 'Dashboard', href: '/'},
                    {title: 'All applications', href: '/applications'},
                    {title: 'Update form'}
                ]
            };
        }
    }
}