import {Component, Input} from '@angular/core';
import {NgbModal}         from "@ng-bootstrap/ng-bootstrap";
import {ArrayService} from "../../../../../core/services/array.service";

@Component({
    selector    : 'field-list',
    templateUrl : './field-list.component.html'
})
export class CollectionFieldListComponent {
    @Input() fields: any[];

    activeIndex: number;

    constructor(
        private arrayService: ArrayService,
        private modalService: NgbModal) {
    }

    addField(content) {
        this.activeIndex = null;
        this.modalService.open(content);
    }

    deleteField(field) {
        this.arrayService.deleteElement(this.fields, field);
    }

    editField(content, index) {
        this.activeIndex = index;
        this.openFieldFormModal(content);
    }

    private openFieldFormModal(content) {
        this.modalService.open(content);
    }
}