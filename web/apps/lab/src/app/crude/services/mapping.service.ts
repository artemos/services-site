import {Injectable} from '@angular/core';
import {Subject}    from "rxjs/Subject";

@Injectable()
export class MappingService {
    mappingToArray(mapping) {
        let result = [];

        for (let field in mapping) {
            if (mapping.hasOwnProperty(field)) {
                let data = mapping[field];

                data.name   = field;
                data.fields = JSON.stringify(data.fields);

                result.push(data);
            }
        }

        return result;
    }

    mappingFromArray(mapping) {
        let result = {};

        for (let i = 0; i < mapping.length; i++) {
            let data  = mapping[i];

            result[data.name] = {
                type: data.type
            };

            if (data.fields) {
                result[data.name].fields = JSON.parse(data.fields);
            }

            if (data.fielddata) {
                result[data.name].fielddata = true;
            }

            if (data.index) {
                result[data.name].index = data.index;
            }
        }

        return result;
    }
}