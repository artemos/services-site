<?php

namespace app\components\services;

use GuzzleHttp\Client as HttpClient;
use yii\helpers\Json;

/**
 * Class DataminerService
 * @package app\components\services
 */
class DataminerService
{
    /**
     * Dataminer api host
     */
    const API_HOST = 'http://services-site';

    /**
     * Guzzle http request options
     */
    const REQUEST_OPTIONS = [
        'http_errors' => false,
        'headers' => [
            'Authorization' => 'Bearer $2y$13$YIGy5v/p2gnhLIRAYc7Ceuk4B2CbVXosuCBdgfpXMKSFzGnc5dRMC'
        ]
    ];

    /**
     * @param $id
     * @return mixed
     */
    public static function getCollection($id)
    {
        $response = (new HttpClient())
            ->get(self::API_HOST . '/api/collections/' . $id, self::REQUEST_OPTIONS)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function createCollection($data)
    {
        $options = self::REQUEST_OPTIONS;
        $options['json'] = $data;

        $response = (new HttpClient())
            ->post(self::API_HOST . '/api/collections', $options)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }

    /**
     * @param $id
     * @param $collectionId
     * @return mixed
     */
    public static function getItem($id, $collectionId)
    {
        $options = self::REQUEST_OPTIONS;
        $options['query'] = ['collectionId' => $collectionId];

        $response = ( new HttpClient())
            ->get(self::API_HOST . '/api/items/' . urlencode($id), $options)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function createItem($data)
    {
        $collectionId = $data['collectionId'];
        unset($data['collectionId']);

        $options = self::REQUEST_OPTIONS;
        $options['json'] = $data;

        $response = (new HttpClient())
            ->post(self::API_HOST . '/api/items?collectionId=' . $collectionId, $options)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public static function updateItem($id, $data)
    {
        $options = self::REQUEST_OPTIONS;
        $options['json'] = $data;

        $response = (new HttpClient())
            ->put(self::API_HOST . '/api/items/' . urlencode($id), $options)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public static function getItems($params = [])
    {
        $options = self::REQUEST_OPTIONS;

        if ($params) {
           $options['query'] = $params;
        }

        $response = ( new HttpClient())
            ->get(self::API_HOST . '/api/items', $options)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }

    /**
     * @param $indexName
     * @param $instanceId
     * @return mixed
     */
    public static function deleteIndex($indexName, $instanceId)
    {
        $response = ( new HttpClient())
            ->delete(self::API_HOST . '/api/indices/' . $indexName . '?instanceId=' . $instanceId, self::REQUEST_OPTIONS)
            ->getBody()
            ->getContents();

        return Json::decode($response);
    }
}