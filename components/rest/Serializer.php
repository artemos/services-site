<?php

namespace app\components\rest;

/**
 * Class Serializer
 */
class Serializer extends \yii\rest\Serializer
{
    public function init()
    {
        parent::init();
        //sleep(1);
    }

    /**
     * @param \yii\base\Model $model
     * @return array
     */
    protected function serializeModelErrors($model)
    {
        return [
            'errors' => parent::serializeModelErrors($model)
        ];
    }
}