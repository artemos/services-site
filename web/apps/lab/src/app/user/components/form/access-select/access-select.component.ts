import {Component, OnInit, Input} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    selector    : 'access-select',
    templateUrl : './access-select.component.html'
})
export class AccessSelectComponent implements OnInit {
    @Input() model: any;
    @Input() field: string;

    users: any;

    constructor(private userService: UserService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.userService.fetchAll().subscribe(
            response => {
                this.users = response.map(user => {
                    return {
                        id   : user.id,
                        text : user.login
                    }
                });
            },
            error => this.httpErrorService.handle(error, this)
        );
    }

    onValueChanged(event) {
        this.model = event.value.map((id) => parseInt(id));
        console.log('access value', this.model);
    }
}