import {Component}              from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorService}       from "../../../../core/services/http-error.service";
import {ConfigService}          from "../../../../core/services/config.service";
import {ChartService}           from "../../../services/chart.service";
import {AggregationService}     from "../../../services/aggregation.service";
import {FormComponent}          from "../../../../core/components/form-component/form.component";

@Component({
    selector    : 'widget-form',
    templateUrl : './widget-form.component.html'
})
export class WidgetFormComponent extends FormComponent {
    chartTypes : Array <any>;
    aggTypes   : Array <any>;

    constructor(
        protected service: ConfigService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected httpErrorService: HttpErrorService,
        private   chartService: ChartService,
        private   aggService: AggregationService) {

        super(service, route, router, httpErrorService, {
            name       : 'widget',
            listRoute  : '/widgets'
        });

        this.chartTypes = chartService.getTypes();
        this.aggTypes   = aggService.getTypes();
    }

    initForm() {
        this.form = {
            type  : 'widget',
            value : {
                aggregation: {
                    params: {}
                }
            }
        };
    }

    beforeSubmit(form: any) {
        if (form.value.aggregation.type == 'term') {
            delete form.value.aggregation.params.interval;
            delete form.value.aggregation.params.format;
        }
        return form;
    }
}