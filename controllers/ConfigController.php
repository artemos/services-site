<?php

namespace app\controllers;

use app\components\data\ActiveDataProvider;
use app\components\rest\Controller;
use app\models\Collection;
use app\models\Config;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class ConfigController
 * @package app\controllers
 */
class ConfigController extends Controller
{
    /**
     * @return array
     */
    protected function accessRules()
    {
        return [
            [
                'actions' => ['index', 'view', 'create', 'update', 'upsert', 'delete', 'create-charts', 'create-dashboard'],
                'allow' => true,
                'roles' => ['@'],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $query = Config::find();
        $query->where(['userId' => $this->getUserId()])->orWhere(['userId' => null]);

        if ($type = $this->getParam('type')) {
            $query->andWhere(['like', 'key', $type . '-']);
        }

        return new ActiveDataProvider([
            'query'      => $query,
            'filter'     => ['id'],
            'pagination' => ['pageSize' => 1000]
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->getModel($id);
    }

    /**
     * @return Config
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $config = new Config();
        $config->load($this->getRequestParams());

        $collection = Collection::getById($config['value']['aggregation']['collectionId']);

        $config->key = $this->generateChartKey($config['value']['selector'], $collection, $config['value']['aggregation']);

        if ($config->save()) {
            $this->setResponseStatusCode(201);
        } elseif (!$config->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $config;
    }

    /**
     * @param $id
     * @return \app\components\db\ActiveRecord
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        $config = $this->getModel($id);
        $config->load($this->getRequestParams());

        if (!$config->save() && !$config->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $config;
    }

    /**
     * @return Config
     * @throws ServerErrorHttpException
     */
    public function actionUpsert()
    {
        $key    = $this->requireParam('key');
        $value  = $this->requireParam('value');
        $userId = $this->getUserId();

        $config = Config::find()->where(['userId' => $userId, 'key' => $key])->one();

        if ($config) {
            $config->value = $value;
        } else {
            $config = new Config();
            $config->userId = $userId;
            $config->key    = $key;
            $config->value  = $value;
        }

        if ($config->save()) {
            $this->setResponseStatusCode(201);
        } elseif (!$config->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $config;
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        if (!$this->getModel($id)->delete()) {
            throw new ServerErrorHttpException('Failed to delete the comment for unknown reason.');
        }

        $this->setResponseStatusCode(204);
    }


    public function actionCreateCharts()
    {
        $collection   = Collection::getById($this->requireParam('collectionId'));
        $fields       = $collection->fieldsArray;
        $datePatterns = ['/_at/'];
        $dateFields   = [];
        $termFields   = [];
        $groupExcept  = ['id'];
        $aggregations = [];
        $configs      = [];
        $fieldLabels  = ArrayHelper::map($fields, 'name', 'label');
        $fieldLabels  = array_filter($fieldLabels);

        foreach ($fields as $field) {
            if (!in_array($field['name'], $groupExcept)) {
               $termFields[] = $field['name'];
            }

            foreach ($datePatterns as $pattern) {
                if (preg_match($pattern, $field['name'])) {
                    $dateFields[] = $field['name'];
                }
            }
        }

        foreach ($termFields as $field) {
            $aggregations[] = [
                'type'         => 'term',
                'title'        => isset($fieldLabels[$field]) ? $fieldLabels[$field] : $field,
                'params'       => ['field' => $field],
                'collectionId' => $collection['id']
            ];
        }

        $termCharts = ['pie-chart', 'bar-chart'];

        foreach ($aggregations as $aggregation) {
            foreach ($termCharts as $termChart) {
                $configs[] = [
                    'key'    => $this->generateChartKey($termChart, $collection, $aggregation),
                    'value'  => [
                        'title'       => $collection['name'] . ": " . $aggregation['title'],
                        'selector'    => $termChart,
                        'aggregation' => $aggregation
                    ]
                ];
            }
        }

        $aggregations = [];

        foreach ($dateFields as $field) {
            $aggregations[] = [
                'type'         => 'date_histogram',
                'title'        =>  isset($fieldLabels[$field]) ? $fieldLabels[$field] : $field,
                'params'       => ['field' => $field, 'format' => 'dd.mm', 'interval' => 'day'],
                'collectionId' => $collection['id']
            ];

            foreach ($termFields as $termField) {
                $labels = [
                    isset($fieldLabels[$field])     ? $fieldLabels[$field] : $field,
                    isset($fieldLabels[$termField]) ? $fieldLabels[$termField] : $termField,
                ];

                $aggregations[] = [
                    'type'         => 'date_histogram_term',
                    'title'        => join(', ', $labels),
                    'params'       => ['date_field' => $field, 'format' => 'dd.mm', 'interval' => 'day', 'term_field' => $termField],
                    'collectionId' => $collection['id']
                ];
            }
        }

        $charts = ['line-chart'];

        foreach ($aggregations as $aggregation) {
            foreach ($charts as $chart) {
                $configs[] = [
                    'key'    => $this->generateChartKey($chart, $collection, $aggregation),
                    'value'  => [
                        'title'       => $collection['name'] . ": " . $aggregation['title'],
                        'selector'    => $chart,
                        'aggregation' => $aggregation
                    ]
                ];
            }
        }

        Config::deleteAll("key like 'collection-" . $collection['id']. "-chart-%'");

        foreach ($configs as $data) {
            $config = new Config();
            $config->setAttributes($data);

            var_dump($config->save());
        }
    }


    public function actionCreateDashboard() 
    {
        $collectionId = (int) $this->requireParam('collectionId');
        $widgetIds    = Config::find()->select('id')->where("key like 'collection-" . $collectionId. "-chart-%'")->column();
        $dashboard    = ['rows' => []];

        foreach ($widgetIds as $id) {
            $dashboard['rows'][] = [
                'cols' => [[
                    'class'    => 'col-12',
                    'widgetId' => $id
                ]]
            ];
        }

        $key = "collection-{$collectionId}-dashboard";

        Config::deleteAll(['key' => $key]);

        $config = new Config();
        $config->setAttributes([
            'key'   => $key,
            'value' => $dashboard
        ]);

        var_dump($config->save());
    }

    /**
     * @param $type
     * @param array $collection
     * @param array $aggregation
     * @return mixed|string
     */
    protected function generateChartKey($type, $collection, $aggregation)
    {
        $params = $aggregation['params'];

        ArrayHelper::remove($params, 'interval');
        ArrayHelper::remove($params, 'format');

        foreach ($params as $param => $value) {
            $params[$param] = "{$param}-{$value}";
        }

        $key = 'collection-' . $collection['id'] .  '-chart-' . $type . '-' . $aggregation['type'] . '-' . join('-', array_values($params));
        $key = str_replace('_', '-', $key);

        return $key;
    }
}