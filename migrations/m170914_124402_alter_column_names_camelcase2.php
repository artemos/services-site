<?php

use yii\db\Migration;

class m170914_124402_alter_column_names_camelcase2 extends Migration
{
    public function safeUp()
    {
        $tables = [
            'crawler_sites' => [
                'created_at'  => 'createdAt',
                'updated_at'  => 'updatedAt'
            ]
        ];

        foreach ($tables as $table => $columns) {
            foreach ($columns as $oldName => $newName) {
                $this->execute("ALTER TABLE {$table} RENAME COLUMN  \"{$oldName}\" to \"{$newName}\"");
            }
        }
    }

    public function safeDown()
    {
        echo "m170914_124402_alter_column_names_camelcase2 cannot be reverted.\n";
    }
}
