import {Component, OnInit} from '@angular/core';
import {ArrayService} from "../../../../core/services/array.service";
import {ConfigService} from "../../../../core/services/config.service";
import {HttpErrorService} from "../../../../core/services/http-error.service";

@Component({
    templateUrl : './aggregation-list.component.html'
})
export class AggregationListComponent implements OnInit {
    crumbs : any = {
        title: 'All aggregations',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'All aggregations'}
        ],
        buttons: [
            {
                text : 'New aggregation',
                route: '/aggregations/new',
                class: 'btn btn-primary'
            }
        ]
    };

    aggregations : any;

    constructor(private arrayService: ArrayService,
                private configService: ConfigService,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        let params = {type: 'aggregation'};

        this.configService.fetchAll(params).subscribe(
            response => this.aggregations = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }

    deleteAggregation(aggregation) {
        if (!confirm("Delete aggregation '" + aggregation.value.title + "'?")) {
            return;
        }

        this.configService.delete(aggregation.id).subscribe(
            ()    => this.arrayService.deleteElement(this.aggregations, aggregation),
            error => this.httpErrorService.handle(error, this)
        );
    }
}