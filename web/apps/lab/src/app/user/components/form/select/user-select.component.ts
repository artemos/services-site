import {Component, OnInit, Input} from '@angular/core';
import {HttpErrorService} from "../../../../core/services/http-error.service";
import {UserService} from "../../../services/user.service";

@Component({
    selector    : 'user-select',
    templateUrl : './user-select.component.html'
})
export class UserSelectComponent implements OnInit {
    @Input() model: any;
    users: any;

    constructor(private userService: UserService ,
                private httpErrorService: HttpErrorService) {
    }

    ngOnInit() {
        this.userService.fetchAll().subscribe(
            response => this.users = response,
            error    => this.httpErrorService.handle(error, this)
        );
    }
}