<?php

namespace app\components\models;

use app\components\Enum;

/**
 * Class Filters
 * @package app\components\models
 */
class Filters extends Enum
{
    const TRIM       = 'trim';
    const STRIP_TAGS = 'strip_tags';
}