import {Component, Input}   from '@angular/core';
import {Router}             from "@angular/router";
import {ApplicationService} from "../../services/application.service";
import {HttpErrorService}   from "../../../core/services/http-error.service";

@Component({
    selector    : 'application-card',
    templateUrl : './application-card.component.html'
})
export class ApplicationCardComponent {
    @Input() application: any;
    @Input() detail: boolean;

    constructor(private router: Router,
                private httpErrorService: HttpErrorService,
                private applicationService: ApplicationService) {
    }

    deleteApplication() {
        if (!confirm('Delete form "' + this.application.name + '" ?')) {
            return;
        }

        this.applicationService.delete(this.application.id).subscribe(
            response => this.router.navigateByUrl('/applications'),
            error    => this.httpErrorService.handle(error, this)
        );
    }

}