<?php

return [
    'attributes' => [
        'source_url' => [
            'type' => [
                'es'  => 'text',
                'yii' => 'url'
            ],
            'required' => true
        ],
        'source_host' => [
            'type' => [
                'es' => 'text'
            ],
            'required' => true
        ],
        'car_id' => [
            'type' => [
                'es' => 'integer'
            ],
            'required' => true
        ],
        'car_name' => [
            'type' => [
                'es' => 'text'
            ],
            'required' => true
        ],
        'city_id' => [
            'type' => [
                'es' => 'integer'
            ],
            'required' => true
        ],
        'city_name' => [
            'type' => [
                'es' => 'text'
            ],
            'required' => true
        ],
        'year' => [
            'type' => [
                'es' => 'short'
            ],
            'regexp' => '/[0-9]{4}/',
            'min'    => 1910,
            'required' => true
        ],
        'price' => [
            'type' => [
                'es' => 'integer'
            ],
            'keep_history' => true,
            'required' => true,
            'max' => 100000000
        ],
        'status' => [
            'type' => [
                'es'  => 'text',
                'php' => 'string'
            ],
            'required' => true
        ],
        'transmission' => [
            'type' => [
                'es' => 'text',
            ],
            'enum' => ['mechanical', 'robotic', 'auto', 'cvt'],
            'required' => true
        ],
        'drive' => [
            'type' => [
                'es' => 'text',
            ],
            'enum' => ['front', 'rear', 'full'],
            'required' => true
        ],
        'image' => [
            'type' => [
                'es' => 'text',
            ],
        ],
        'parser' => [
            'type' => [
                'es' => 'text',
            ],
            'required' => true
        ],
        'engine' => [
            'type' => [
                'es' => 'integer',
            ],
            'required' => true
        ],
        'mileage' => [
            'type' => [
                'es' => 'integer',
            ],
            'required' => true
        ],
        'phones' => [
            'type' => [
                'es'  => 'array',
                'php' => 'array'
            ],
            'element' => [
                'regexp' => '/^[0-9]{11}$/'
            ],
            'keep_history' => true,
            'required' => true
        ],
        'state' => [
            'type' => [
                'es' => 'text',
            ],
            'enum' => ['good', 'broken'],
            'required' => true
        ],
        'person' => [
            'type' => [
                'es' => 'text',
            ],
            'enum' => ['legal', 'individual'],
            'required' => true
        ],
        'created_at' => [
            'type' => [
                'es' => 'date'
            ],
            'default' => 'current_timestamp'
        ],
        'updated_at' => [
            'type' => [
                'es' => 'date'
            ],
        ],
        'viisted_at' => [
            'type' => [
                'es' => 'date'
            ],
        ],
        'comments_count' => [
            'type' => [
                'es' => 'integer',
            ],
        ],
        'phone_offers_count' => [
            'type' => [
                'es' => 'integer',
            ],
        ],
        'price_diff' => [
            'type' => [
                'es' => 'integer',
            ],
        ],
        'horse_power' => [
            'type' => [
                'es' => 'short',
            ],
            'required' => true
        ],
        'price_color' => [
            'type' => [
                'es' => 'text'
            ]
        ],
        'description' => [
            'type' => [
                'es' => 'text'
            ],
            'required' => true
        ],
        'body_type' => [
            'type' => [
                'es' => 'text'
            ],
            'enum' => ['hatchback', 'limousine', 'offroad', 'minivan', 'pickup', 'van', 'cabrio', 'versatile', 'coupe', 'saloon', 'crossover', 'minibus']
        ],
        'fuel' => [
            'type' => [
                'es' => 'text'
            ],
            'enum' => ['diesel', 'gas', 'petrol', 'hybrid'],
            'required' => true
        ],
        'tags' => [
            'type' => [
                'es' => 'array'
            ]
        ],
        'ip' => [
            'type' => [
                'es' => 'ip'
            ],
            'required' => true
        ],
        'hashes' => [
            'type' => [
                'es' => 'array'
            ]
        ],
        'duplicates' => [
            'type' => [
                'es' => 'nested'
            ]
        ],
        'popular' => [
            'type' => [
                'es' => 'boolean'
            ]
        ]
    ]
];