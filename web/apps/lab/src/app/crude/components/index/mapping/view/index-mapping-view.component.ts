import {Component, OnInit} from '@angular/core';
import {ActivatedRoute}    from "@angular/router";
import {IndexService}      from "../../../../services/index.service";
import {MappingService}    from "../../../../services/mapping.service";
import {HttpErrorService}  from "../../../../../core/services/http-error.service";

@Component({
    selector    : 'index-mapping-view',
    templateUrl : './index-mapping-view.component.html'
})
export class IndexMappingViewComponent implements OnInit {
    instanceId : number;
    indexName  : string;
    indexType  : string;

    mapping : any;

    crumbs : any = {
        title: 'Collection index mapping',
        links: [
            {title: 'Dashboard', href: '/'},
            {title: 'index mapping'}
        ]
    };

    constructor(private route: ActivatedRoute,
                private httpErrorService: HttpErrorService,
                private mappingService: MappingService,
                private indexService: IndexService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.instanceId = params.id;
            this.indexName  = params.name;
            this.indexType  = params.name;

            this.indexService.fetchMapping(this.indexName, this.indexType, this.instanceId).subscribe(
                response => this.mapping = this.mappingService.mappingToArray(response),
                error    => this.httpErrorService.handle(error, this)
            );
        });
    }
}