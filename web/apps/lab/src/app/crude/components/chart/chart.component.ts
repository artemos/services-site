import {Input, OnInit}    from '@angular/core';
import {HttpErrorService} from "../../../core/services/http-error.service";
import {ItemService}      from "../../services/item.service";

export class ChartComponent implements OnInit {
    @Input() title        : string;
    @Input() elementId    : string;
    @Input() condition    : any;
    @Input() aggregation  : any;
    @Input() collectionId : any;

    protected colors: Array<string> = [
        '#26c6da',
        '#1e88e5',
        '#f4c63d',
        '#745af2',
        '#f62d51',
        '#d17905',
        '#59922b',
        '#453d3f',
        '#dadada',
    ];

    constructor(
         protected httpErrorService: HttpErrorService,
         protected itemService: ItemService) {
    }

    protected updateState() {
        return this.fetchItems();
    }

    protected initChart() {
        throw new Error('method should be overwrited');
    }

    protected buildChartData(data) {
        throw new Error('method should be overwrited');
    }

    ngOnInit() {
        this.fetchItems();
    }

    protected fetchItems() {
        let params = {
            collectionId : this.collectionId,
            aggregation  : this.aggregation,
            limit        : 0
        };

        if (this.condition) {
            params['condition'] = this.condition;
        }

        this.itemService.fetchAll(params).subscribe(
            response => {
                this.buildChartData(response);
                this.initChart();
            }
        );
    }
}